//
//  AsyncImageView.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageViewDelegate.h"

@interface AsyncImageView : UIView {
	NSURLConnection* connection;
    NSMutableData* data;
	id<AsyncImageViewDelegate> delegate;
}
@property (nonatomic,strong) id delegate;
- (void)loadImageFromURL:(NSURL*)url;
- (void)loadImageFromPath:(NSString*)path;
- (UIImage*) image;
@end
