//
//  AsyncImageView.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AsyncImageView.h"

@implementation AsyncImageView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}
- (void)loadImageFromURL:(NSURL*)url {
    NSURLRequest* request = [NSURLRequest requestWithURL:url
											 cachePolicy:NSURLRequestUseProtocolCachePolicy
										 timeoutInterval:3600];
    connection = [[NSURLConnection alloc]
				  initWithRequest:request delegate:self];
    //TODO error handling, what if connection is nil?
}
- (void)connection:(NSURLConnection *)theConnection
	didReceiveData:(NSData *)incrementalData {
    if (data==nil) {
		data =
		[[NSMutableData alloc] initWithCapacity:51200];
    }
    [data appendData:incrementalData];
}
- (void)loadImageFromPath:(NSString*)path
{
    NSURL *url = [NSURL URLWithString:path];
    [self loadImageFromURL:url];
}
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
	
    connection=nil;
	
    if ([[self subviews] count]!=0) {
        [[[self subviews] objectAtIndex:0] removeFromSuperview];
    }
	
    UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:data]];
	
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
	
    [self addSubview:imageView];
    imageView.frame = self.bounds;
    [imageView setNeedsLayout];
    [self setNeedsLayout];
    data=nil;
	if([delegate respondsToSelector:@selector(handleRecieveImage:sender:)])
	{
		[delegate handleRecieveImage:imageView.image sender:self];
	}
}

- (UIImage*) image {
    UIImageView* iv = [[self subviews] objectAtIndex:0];
    return [iv image];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)dealloc {
	[connection cancel];
}
@end
