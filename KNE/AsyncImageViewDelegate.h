//
//  AsyncImageViewDelegate.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AsyncImageView;
@protocol AsyncImageViewDelegate <NSObject>
-(void)handleRecieveImage:(UIImage*)image sender:(AsyncImageView*)sender;

@end
