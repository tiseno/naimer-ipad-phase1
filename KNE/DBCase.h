//
//  DBCase.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/24/12.
//
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "KNECase.h"
#import "DatabaseAction.h"
#import "Jumper.h"
#import "DBTableGraft.h"
#import "KNETblGraftPositionTag.h"
#import "DBSpringReturn.h"
#import "DBSpringReturnContain.h"
#import "KNESpringRPosition.h"
#import "KNESpringReturnContain.h"

@interface DBCase : DBbase{
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;

-(NSArray*)selectItem;
-(NSArray*)selectCaseItem:(KNECase*)knecase;
-(NSArray*)selectItemTopCase;
-(DataBaseInsertionResult)insertcontact:(KNECase*)tcontact;
-(DataBaseUpdateResult)updateCase:(KNECase*)tcase;
-(DataBaseDeletionResult)deletCase:(KNECase*)tCaseItem;
-(void)deletselectedCase:(KNECase*)tCaseItem;

@end
//TotalSwitchingAngle, SwitchingAngle