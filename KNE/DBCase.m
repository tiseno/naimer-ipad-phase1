//
//  DBCase.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/24/12.
//
//

#import "DBCase.h"

@implementation DBCase
@synthesize runBatch;


-(DataBaseInsertionResult)insertcontact:(KNECase*)tcontact
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        
        
        NSString *iCaseName=[tcontact.CaseName stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giCaseName=[iCaseName stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFLook=[tcontact.FLook stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFLook=[iFLook stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFMounting=[tcontact.FMounting stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFMounting=[iFMounting stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFEscutcheon_plate=[tcontact.FEscutcheon_plate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFEscutcheon_plate=[iFEscutcheon_plate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFHandle=[tcontact.FHandle stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFHandle=[iFHandle stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFLatch_mech=[tcontact.FLatch_mech stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFLatch_mech=[iFLatch_mech stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFStop=[tcontact.FStop stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFStop=[iFStop stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFStop_Degree=[tcontact.FStop_Degree stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFStop_Degree=[iFStop_Degree stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFNofStage=[tcontact.FNofStage stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFNofStage=[iFNofStage stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFMasterfData=[tcontact.FMasterfData stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFMasterfData=[iFMasterfData stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFReference=[tcontact.FReference stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFReference=[iFReference stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFDate=[tcontact.FDate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFDate=[iFDate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFModify_Date=[tcontact.FModify_Date stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFModify_Date=[iFModify_Date stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFCustNo=[tcontact.FCustNo stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFCustNo=[iFCustNo stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFCompany=[tcontact.FCompany stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFCompany=[iFCompany stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFVersion=[tcontact.FVersion stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFVersion=[iFVersion stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra1=[tcontact.optionalextra1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra1=[ioptionalextra1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra2=[tcontact.optionalextra2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra2=[ioptionalextra2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra3=[tcontact.optionalextra3 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra3=[ioptionalextra3 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra4=[tcontact.optionalextra4 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra4=[ioptionalextra4 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra5=[tcontact.optionalextra5 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra5=[ioptionalextra5 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra6=[tcontact.optionalextra6 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra6=[ioptionalextra6 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra7=[tcontact.optionalextra7 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra7=[ioptionalextra7 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra8=[tcontact.optionalextra8 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra8=[ioptionalextra8 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra9=[tcontact.optionalextra9 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra9=[ioptionalextra9 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iextra_comment=[tcontact.extra_comment stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giextra_comment=[iextra_comment stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iCustomerArticleNumber=[tcontact.CustomerArticleNumber stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giCustomerArticleNumber=[iCustomerArticleNumber stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iSwitchType=[tcontact.SwitchType stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giSwitchType=[iSwitchType stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iProgram=[tcontact.Program stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giProgram=[iProgram stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iPlateName=[tcontact.PlateName stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giPlateName=[iPlateName stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        insertSQL = [NSString stringWithFormat:@"insert into tblCase(CodeNumber, FLook, FMounting, FEscutcheon_Plate, FHandle, FLatch_mech, FStop, FStop_Degree, FNofStages, FMaster_Data, FReference, FDate, FModify_Date, FCust_no, FCompany, FVersion, pcs1, pcs2, pcs3, pcs4, pcs5, pcs6, pcs7, pcs8, pcs9, optionalextra1, optionalextra2, optionalextra3, optionalextra4, optionalextra5, optionalextra6, optionalextra7, optionalextra8, optionalextra9, extra_comment, SwitchPoint, CustomerArticleNumber, SwitchType, Program, PlateName, TotalSwitchingAngle, SwitchingAngle, NumberOfPosition ) values('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@', %d, %d, %d, %d, %d, %d, %d, %d, %d, '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', %d, '%@', '%@', '%@', '%@', %d, %d, %d);",giCaseName, giFLook, giFMounting, giFEscutcheon_plate, giFHandle, giFLatch_mech, giFStop, giFStop_Degree, giFNofStage, giFMasterfData, giFReference, giFDate, giFModify_Date, giFCustNo, giFCompany, giFVersion, [tcontact.pcs1 intValue], [tcontact.pcs2 intValue], [tcontact.pcs3 intValue], [tcontact.pcs4 intValue], [tcontact.pcs5 intValue], [tcontact.pcs6 intValue], [tcontact.pcs7 intValue], [tcontact.pcs8 intValue], [tcontact.pcs9 intValue], gioptionalextra1, gioptionalextra2, gioptionalextra3, gioptionalextra4, gioptionalextra5, gioptionalextra6, gioptionalextra7, gioptionalextra8, gioptionalextra9, giextra_comment, [tcontact.SwitchPoint intValue], giCustomerArticleNumber, giSwitchType, giProgram, giPlateName, [tcontact.TotalSwitchingAngle intValue], [tcontact.SwitchingAngle intValue], [tcontact.numberOfPosition intValue]];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        NSLog(@"%@",insertSQL);
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSArray*)selectItem
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select caseID, CodeNumber, FLook, FMounting, FEscutcheon_Plate, FHandle, FLatch_mech, FStop, FStop_Degree, FNofStages, FMaster_Data, FReference, FDate, FModify_Date, FCust_no, FCompany, FVersion, pcs1, pcs2, pcs3, pcs4, pcs5, pcs6, pcs7, pcs8, pcs9, optionalextra1, optionalextra2, optionalextra3, optionalextra4, optionalextra5, optionalextra6, optionalextra7, optionalextra8, optionalextra9, extra_comment, SwitchPoint, CustomerArticleNumber, SwitchType, Program, PlateName, TotalSwitchingAngle, SwitchingAngle, NumberOfPosition from tblCase order by caseID desc"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNECase *tcase= [[KNECase alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tcase.CaseID=gCaseID;

            char* controlCStr=(char*)sqlite3_column_text(statement, 1);
            if(controlCStr)
            {
                NSString *gCaseName=[NSString stringWithUTF8String:(char*)controlCStr];
                NSString *wCaseName=[gCaseName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *gwCaseName=[wCaseName stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.CaseName = gwCaseName;
            }
            
            char* FLook=(char*)sqlite3_column_text(statement, 2);
            if(FLook)
            {
                NSString *gFLook=[NSString stringWithUTF8String:(char*)FLook];
                NSString *wgFLook=[gFLook stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *fwgFLook=[wgFLook stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FLook = fwgFLook;
            }
            
            char* FMounting=(char*)sqlite3_column_text(statement, 3);
            if(FMounting)
            {
                NSString *gFMounting=[NSString stringWithUTF8String:(char*)FMounting];
                NSString *iFMounting=[gFMounting stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *giFMounting=[iFMounting stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FMounting = giFMounting;
            }
            
            char* FEscutcheon_Plate=(char*)sqlite3_column_text(statement, 4);
            if(FEscutcheon_Plate)
            {
                NSString *gFEscutcheon_Plate=[NSString stringWithUTF8String:(char*)FEscutcheon_Plate];
                NSString *rgFEscutcheon_Plate=[gFEscutcheon_Plate stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFEscutcheon_Plate=[rgFEscutcheon_Plate stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FEscutcheon_plate = grgFEscutcheon_Plate;
            }
            
            char* FHandle=(char*)sqlite3_column_text(statement, 5);
            if(FHandle)
            {
                NSString *gFHandle=[NSString stringWithUTF8String:(char*)FHandle];
                NSString *rgFHandle=[gFHandle stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFHandle=[rgFHandle stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FHandle = grgFHandle;
            }
            
            char* FLatch_mech=(char*)sqlite3_column_text(statement, 6);
            if(FLatch_mech)
            {
                NSString *gFLatch_mech=[NSString stringWithUTF8String:(char*)FLatch_mech];
                NSString *rgFLatch_mech=[gFLatch_mech stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFLatch_mech=[rgFLatch_mech stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FLatch_mech = grgFLatch_mech;
            }
            
            char* FStop=(char*)sqlite3_column_text(statement, 7);
            if(FStop)
            {
                NSString *gFStop=[NSString stringWithUTF8String:(char*)FStop];
                NSString *rgFStop=[gFStop stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFStop=[rgFStop stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FStop = grgFStop;
            }
            
            char* FStop_Degree=(char*)sqlite3_column_text(statement, 8);
            if(FStop_Degree)
            {
                NSString *gFStop_Degree=[NSString stringWithUTF8String:(char*)FStop_Degree];
                NSString *rgFStop_Degree=[gFStop_Degree stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFStop_Degree=[rgFStop_Degree stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FStop_Degree = grgFStop_Degree;
            }
            
            char* FNofStages=(char*)sqlite3_column_text(statement, 9);
            if(FNofStages)
            {
                NSString *gFNofStages=[NSString stringWithUTF8String:(char*)FNofStages];
                NSString *rgFNofStages=[gFNofStages stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFNofStages=[rgFNofStages stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FNofStage = grgFNofStages;
            }
            
            char* FMaster_Data=(char*)sqlite3_column_text(statement, 10);
            if(FMaster_Data)
            {
                NSString *gFMaster_Data=[NSString stringWithUTF8String:(char*)FMaster_Data];
                NSString *rgFMaster_Data=[gFMaster_Data stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFMaster_Data=[rgFMaster_Data stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FMasterfData = grgFMaster_Data;
            }
            
            char* FReference=(char*)sqlite3_column_text(statement, 11);
            if(FReference)
            {
                NSString *gFReference=[NSString stringWithUTF8String:(char*)FReference];
                NSString *rgFReference=[gFReference stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFReference=[rgFReference stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FReference = grgFReference;
            }
            
            char* FDate=(char*)sqlite3_column_text(statement, 12);
            if(FDate)
            {
                NSString *gFDate=[NSString stringWithUTF8String:(char*)FDate];
                NSString *rgFDate=[gFDate stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFDate=[rgFDate stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FDate = grgFDate;
            }
            
            char* FModify_Date=(char*)sqlite3_column_text(statement, 13);
            if(FModify_Date)
            {
                NSString *gFModify_Date=[NSString stringWithUTF8String:(char*)FModify_Date];
                NSString *rgFModify_Date=[gFModify_Date stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFModify_Date=[rgFModify_Date stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FModify_Date = grgFModify_Date;
            }
            
            char* FCus_no=(char*)sqlite3_column_text(statement, 14);
            if(FCus_no)
            {
                NSString *gFCus_no=[NSString stringWithUTF8String:(char*)FCus_no];
                NSString *rgFCus_no=[gFCus_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFCus_no=[rgFCus_no stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FCustNo = grgFCus_no;
            }
            
            char* FCompany=(char*)sqlite3_column_text(statement, 15);
            if(FCompany)
            {
                NSString *gFCompany=[NSString stringWithUTF8String:(char*)FCompany];
                NSString *rgFCompany=[gFCompany stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFCompany=[rgFCompany stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FCompany = grgFCompany;
            }
            
            char* FVersion=(char*)sqlite3_column_text(statement, 16);
            if(FVersion)
            {
                NSString *gFVersion=[NSString stringWithUTF8String:(char*)FVersion];
                NSString *rgFVersion=[gFVersion stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFVersion=[rgFVersion stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FVersion = grgFVersion;
            }
            
            int gpcs1=sqlite3_column_int(statement, 17);
            NSString *strpcs1=[[NSString alloc]initWithFormat:@"%d", gpcs1 ];
            tcase.pcs1=strpcs1;
            
            int gpcs2=sqlite3_column_int(statement, 18);
            NSString *strpcs2=[[NSString alloc]initWithFormat:@"%d", gpcs2 ];
            tcase.pcs2=strpcs2;
            
            int gpcs3=sqlite3_column_int(statement, 19);
            NSString *strpcs3=[[NSString alloc]initWithFormat:@"%d", gpcs3 ];
            tcase.pcs3=strpcs3;
            
            int gpcs4=sqlite3_column_int(statement, 20);
            NSString *strpcs4=[[NSString alloc]initWithFormat:@"%d", gpcs4 ];
            tcase.pcs4=strpcs4;
            
            int gpcs5=sqlite3_column_int(statement, 21);
            NSString *strpcs5=[[NSString alloc]initWithFormat:@"%d", gpcs5 ];
            tcase.pcs5=strpcs5;
            
            int gpcs6=sqlite3_column_int(statement, 22);
            NSString *strpcs6=[[NSString alloc]initWithFormat:@"%d", gpcs6 ];
            tcase.pcs6=strpcs6;
            
            int gpcs7=sqlite3_column_int(statement, 23);
            NSString *strpcs7=[[NSString alloc]initWithFormat:@"%d", gpcs7 ];
            tcase.pcs7=strpcs7;
            
            int gpcs8=sqlite3_column_int(statement, 24);
            NSString *strpcs8=[[NSString alloc]initWithFormat:@"%d", gpcs8 ];
            tcase.pcs8=strpcs8;
            
            int gpcs9=sqlite3_column_int(statement, 25);
            NSString *strpcs9=[[NSString alloc]initWithFormat:@"%d", gpcs9 ];
            tcase.pcs9=strpcs9;
            
            char* goptionalextra1=(char*)sqlite3_column_text(statement, 26);
            if(goptionalextra1)
            {
                NSString *stroptionalextra1=[NSString stringWithUTF8String:(char*)goptionalextra1];
                NSString *rstroptionalextra1=[stroptionalextra1 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra1=[rstroptionalextra1 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra1 = grstroptionalextra1;
            }
            
            char* goptionalextra2=(char*)sqlite3_column_text(statement, 27);
            if(goptionalextra2)
            {
                NSString *stroptionalextra2=[NSString stringWithUTF8String:(char*)goptionalextra2];
                NSString *rstroptionalextra2=[stroptionalextra2 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra2=[rstroptionalextra2 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra2 = grstroptionalextra2;
            }
            
            char* goptionalextra3=(char*)sqlite3_column_text(statement, 28);
            if(goptionalextra3)
            {
                NSString *stroptionalextra3=[NSString stringWithUTF8String:(char*)goptionalextra3];
                NSString *rstroptionalextra3=[stroptionalextra3 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra3=[rstroptionalextra3 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra3 = grstroptionalextra3;
            }
            
            char* goptionalextra4=(char*)sqlite3_column_text(statement, 29);
            if(goptionalextra4)
            {
                NSString *stroptionalextra4=[NSString stringWithUTF8String:(char*)goptionalextra4];
                NSString *rstroptionalextra4=[stroptionalextra4 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra4=[rstroptionalextra4 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra4 = grstroptionalextra4;
            }
            
            char* goptionalextra5=(char*)sqlite3_column_text(statement, 30);
            if(goptionalextra5)
            {
                NSString *stroptionalextra5=[NSString stringWithUTF8String:(char*)goptionalextra5];
                NSString *rstroptionalextra5=[stroptionalextra5 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra5=[rstroptionalextra5 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra5 = grstroptionalextra5;
            }
            
            char* goptionalextra6=(char*)sqlite3_column_text(statement, 31);
            if(goptionalextra6)
            {
                NSString *stroptionalextra6=[NSString stringWithUTF8String:(char*)goptionalextra6];
                NSString *rstroptionalextra6=[stroptionalextra6 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra6=[rstroptionalextra6 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra6 = grstroptionalextra6;
            }
            
            char* goptionalextra7=(char*)sqlite3_column_text(statement, 32);
            if(goptionalextra7)
            {
                NSString *stroptionalextra7=[NSString stringWithUTF8String:(char*)goptionalextra7];
                NSString *rstroptionalextra7=[stroptionalextra7 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra7=[rstroptionalextra7 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra7 = grstroptionalextra7;
            }
            
            char* goptionalextra8=(char*)sqlite3_column_text(statement, 33);
            if(goptionalextra8)
            {
                NSString *stroptionalextra8=[NSString stringWithUTF8String:(char*)goptionalextra8];
                NSString *rstroptionalextra8=[stroptionalextra8 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra8=[rstroptionalextra8 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra8 = grstroptionalextra8;
            }
            
            char* goptionalextra9=(char*)sqlite3_column_text(statement, 34);
            if(goptionalextra9)
            {
                NSString *stroptionalextra9=[NSString stringWithUTF8String:(char*)goptionalextra9];
                NSString *rstroptionalextra9=[stroptionalextra9 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra9=[rstroptionalextra9 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra9 = grstroptionalextra9;
            }
            
            char* gextra_comment=(char*)sqlite3_column_text(statement, 35);
            if(gextra_comment)
            {
                NSString *stgextra_comment=[NSString stringWithUTF8String:(char*)gextra_comment];
                NSString *rstgextra_comment=[stgextra_comment stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstgextra_comment=[rstgextra_comment stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.extra_comment = grstgextra_comment;
            }
            
            int gSwitchPoint=sqlite3_column_int(statement, 36);
            NSString *strSwitchPoint=[[NSString alloc]initWithFormat:@"%d", gSwitchPoint ];
            tcase.SwitchPoint=strSwitchPoint;
            
            //CustomerArticleNumber, SwitchType, Program, PlateName
            char* gCustomerArticleNumber=(char*)sqlite3_column_text(statement, 37);
            if(gCustomerArticleNumber)
            {
                NSString *strCustomerArticleNumber=[NSString stringWithUTF8String:(char*)gCustomerArticleNumber];
                NSString *rstrCustomerArticleNumber=[strCustomerArticleNumber stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrCustomerArticleNumber=[rstrCustomerArticleNumber stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.CustomerArticleNumber = grstrCustomerArticleNumber;
            }
            
            char* gSwitchType=(char*)sqlite3_column_text(statement, 38);
            if(gSwitchType)
            {
                NSString *strSwitchType=[NSString stringWithUTF8String:(char*)gSwitchType];
                NSString *rstrSwitchType=[strSwitchType stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrSwitchType=[rstrSwitchType stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.SwitchType = grstrSwitchType;
            }
            
            char* gProgram=(char*)sqlite3_column_text(statement, 39);
            if(gProgram)
            {
                NSString *strgProgram=[NSString stringWithUTF8String:(char*)gProgram];
                NSString *rstrgProgram=[strgProgram stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrgProgram=[rstrgProgram stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.Program = grstrgProgram;
            }
            
            char* gPlateName=(char*)sqlite3_column_text(statement, 40);
            if(gPlateName)
            {
                NSString *strgPlateName=[NSString stringWithUTF8String:(char*)gPlateName];
                NSString *rstrgPlateName=[strgPlateName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrgPlateName=[rstrgPlateName stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.PlateName = grstrgPlateName;
            }
            
            int gTotalSwitchingAngle=sqlite3_column_int(statement, 41);
            NSString *strTotalSwitchingAngle=[[NSString alloc]initWithFormat:@"%d", gTotalSwitchingAngle ];
            tcase.TotalSwitchingAngle=strTotalSwitchingAngle;
            
            int gSwitchingAngle=sqlite3_column_int(statement, 42);
            NSString *strSwitchingAngle=[[NSString alloc]initWithFormat:@"%d", gSwitchingAngle ];
            tcase.SwitchingAngle=strSwitchingAngle;
            
            int gNumberOfPosition=sqlite3_column_int(statement, 43);
            NSString *strgNumberOfPosition=[[NSString alloc]initWithFormat:@"%d", gNumberOfPosition ];
            tcase.numberOfPosition=strgNumberOfPosition;
            
            
            [itemarr addObject:tcase];
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)selectCaseItem:(KNECase*)knecase
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select caseID, CodeNumber, FLook, FMounting, FEscutcheon_Plate, FHandle, FLatch_mech, FStop, FStop_Degree, FNofStages, FMaster_Data, FReference, FDate, FModify_Date, FCust_no, FCompany, FVersion, pcs1, pcs2, pcs3, pcs4, pcs5, pcs6, pcs7, pcs8, pcs9, optionalextra1, optionalextra2, optionalextra3, optionalextra4, optionalextra5, optionalextra6, optionalextra7, optionalextra8, optionalextra9, extra_comment, SwitchPoint, CustomerArticleNumber, SwitchType, Program, PlateName, TotalSwitchingAngle, SwitchingAngle, NumberOfPosition from tblCase where caseID='%d'", [knecase.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNECase *tcase= [[KNECase alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tcase.CaseID=gCaseID;
            
            char* controlCStr=(char*)sqlite3_column_text(statement, 1);
            if(controlCStr)
            {
                NSString *gCaseName=[NSString stringWithUTF8String:(char*)controlCStr];
                NSString *wCaseName=[gCaseName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *gwCaseName=[wCaseName stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.CaseName = gwCaseName;
            }
            
            char* FLook=(char*)sqlite3_column_text(statement, 2);
            if(FLook)
            {
                NSString *gFLook=[NSString stringWithUTF8String:(char*)FLook];
                NSString *wgFLook=[gFLook stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *fwgFLook=[wgFLook stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FLook = fwgFLook;
            }
            
            char* FMounting=(char*)sqlite3_column_text(statement, 3);
            if(FMounting)
            {
                NSString *gFMounting=[NSString stringWithUTF8String:(char*)FMounting];
                NSString *iFMounting=[gFMounting stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *giFMounting=[iFMounting stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FMounting = giFMounting;
            }
            
            char* FEscutcheon_Plate=(char*)sqlite3_column_text(statement, 4);
            if(FEscutcheon_Plate)
            {
                NSString *gFEscutcheon_Plate=[NSString stringWithUTF8String:(char*)FEscutcheon_Plate];
                NSString *rgFEscutcheon_Plate=[gFEscutcheon_Plate stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFEscutcheon_Plate=[rgFEscutcheon_Plate stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FEscutcheon_plate = grgFEscutcheon_Plate;
            }
            
            char* FHandle=(char*)sqlite3_column_text(statement, 5);
            if(FHandle)
            {
                NSString *gFHandle=[NSString stringWithUTF8String:(char*)FHandle];
                NSString *rgFHandle=[gFHandle stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFHandle=[rgFHandle stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FHandle = grgFHandle;
            }
            
            char* FLatch_mech=(char*)sqlite3_column_text(statement, 6);
            if(FLatch_mech)
            {
                NSString *gFLatch_mech=[NSString stringWithUTF8String:(char*)FLatch_mech];
                NSString *rgFLatch_mech=[gFLatch_mech stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFLatch_mech=[rgFLatch_mech stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FLatch_mech = grgFLatch_mech;
            }
            
            char* FStop=(char*)sqlite3_column_text(statement, 7);
            if(FStop)
            {
                NSString *gFStop=[NSString stringWithUTF8String:(char*)FStop];
                NSString *rgFStop=[gFStop stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFStop=[rgFStop stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FStop = grgFStop;
            }
            
            char* FStop_Degree=(char*)sqlite3_column_text(statement, 8);
            if(FStop_Degree)
            {
                NSString *gFStop_Degree=[NSString stringWithUTF8String:(char*)FStop_Degree];
                NSString *rgFStop_Degree=[gFStop_Degree stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFStop_Degree=[rgFStop_Degree stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FStop_Degree = grgFStop_Degree;
            }
            
            char* FNofStages=(char*)sqlite3_column_text(statement, 9);
            if(FNofStages)
            {
                NSString *gFNofStages=[NSString stringWithUTF8String:(char*)FNofStages];
                NSString *rgFNofStages=[gFNofStages stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFNofStages=[rgFNofStages stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FNofStage = grgFNofStages;
            }
            
            char* FMaster_Data=(char*)sqlite3_column_text(statement, 10);
            if(FMaster_Data)
            {
                NSString *gFMaster_Data=[NSString stringWithUTF8String:(char*)FMaster_Data];
                NSString *rgFMaster_Data=[gFMaster_Data stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFMaster_Data=[rgFMaster_Data stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FMasterfData = grgFMaster_Data;
            }
            
            char* FReference=(char*)sqlite3_column_text(statement, 11);
            if(FReference)
            {
                NSString *gFReference=[NSString stringWithUTF8String:(char*)FReference];
                NSString *rgFReference=[gFReference stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFReference=[rgFReference stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FReference = grgFReference;
            }
            
            char* FDate=(char*)sqlite3_column_text(statement, 12);
            if(FDate)
            {
                NSString *gFDate=[NSString stringWithUTF8String:(char*)FDate];
                NSString *rgFDate=[gFDate stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFDate=[rgFDate stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FDate = grgFDate;
            }
            
            char* FModify_Date=(char*)sqlite3_column_text(statement, 13);
            if(FModify_Date)
            {
                NSString *gFModify_Date=[NSString stringWithUTF8String:(char*)FModify_Date];
                NSString *rgFModify_Date=[gFModify_Date stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFModify_Date=[rgFModify_Date stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FModify_Date = grgFModify_Date;
            }
            
            char* FCus_no=(char*)sqlite3_column_text(statement, 14);
            if(FCus_no)
            {
                NSString *gFCus_no=[NSString stringWithUTF8String:(char*)FCus_no];
                NSString *rgFCus_no=[gFCus_no stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFCus_no=[rgFCus_no stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FCustNo = grgFCus_no;
            }
            
            char* FCompany=(char*)sqlite3_column_text(statement, 15);
            if(FCompany)
            {
                NSString *gFCompany=[NSString stringWithUTF8String:(char*)FCompany];
                NSString *rgFCompany=[gFCompany stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFCompany=[rgFCompany stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FCompany = grgFCompany;
            }
            
            char* FVersion=(char*)sqlite3_column_text(statement, 16);
            if(FVersion)
            {
                NSString *gFVersion=[NSString stringWithUTF8String:(char*)FVersion];
                NSString *rgFVersion=[gFVersion stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgFVersion=[rgFVersion stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.FVersion = grgFVersion;
            }
            
            int gpcs1=sqlite3_column_int(statement, 17);
            NSString *strpcs1=[[NSString alloc]initWithFormat:@"%d", gpcs1 ];
            tcase.pcs1=strpcs1;
            
            int gpcs2=sqlite3_column_int(statement, 18);
            NSString *strpcs2=[[NSString alloc]initWithFormat:@"%d", gpcs2 ];
            tcase.pcs2=strpcs2;
            
            int gpcs3=sqlite3_column_int(statement, 19);
            NSString *strpcs3=[[NSString alloc]initWithFormat:@"%d", gpcs3 ];
            tcase.pcs3=strpcs3;
            
            int gpcs4=sqlite3_column_int(statement, 20);
            NSString *strpcs4=[[NSString alloc]initWithFormat:@"%d", gpcs4 ];
            tcase.pcs4=strpcs4;
            
            int gpcs5=sqlite3_column_int(statement, 21);
            NSString *strpcs5=[[NSString alloc]initWithFormat:@"%d", gpcs5 ];
            tcase.pcs5=strpcs5;
            
            int gpcs6=sqlite3_column_int(statement, 22);
            NSString *strpcs6=[[NSString alloc]initWithFormat:@"%d", gpcs6 ];
            tcase.pcs6=strpcs6;
            
            int gpcs7=sqlite3_column_int(statement, 23);
            NSString *strpcs7=[[NSString alloc]initWithFormat:@"%d", gpcs7 ];
            tcase.pcs7=strpcs7;
            
            int gpcs8=sqlite3_column_int(statement, 24);
            NSString *strpcs8=[[NSString alloc]initWithFormat:@"%d", gpcs8 ];
            tcase.pcs8=strpcs8;
            
            int gpcs9=sqlite3_column_int(statement, 25);
            NSString *strpcs9=[[NSString alloc]initWithFormat:@"%d", gpcs9 ];
            tcase.pcs9=strpcs9;
            
            char* goptionalextra1=(char*)sqlite3_column_text(statement, 26);
            if(goptionalextra1)
            {
                NSString *stroptionalextra1=[NSString stringWithUTF8String:(char*)goptionalextra1];
                NSString *rstroptionalextra1=[stroptionalextra1 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra1=[rstroptionalextra1 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra1 = grstroptionalextra1;
            }
            
            char* goptionalextra2=(char*)sqlite3_column_text(statement, 27);
            if(goptionalextra2)
            {
                NSString *stroptionalextra2=[NSString stringWithUTF8String:(char*)goptionalextra2];
                NSString *rstroptionalextra2=[stroptionalextra2 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra2=[rstroptionalextra2 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra2 = grstroptionalextra2;
            }
            
            char* goptionalextra3=(char*)sqlite3_column_text(statement, 28);
            if(goptionalextra3)
            {
                NSString *stroptionalextra3=[NSString stringWithUTF8String:(char*)goptionalextra3];
                NSString *rstroptionalextra3=[stroptionalextra3 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra3=[rstroptionalextra3 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra3 = grstroptionalextra3;
            }
            
            char* goptionalextra4=(char*)sqlite3_column_text(statement, 29);
            if(goptionalextra4)
            {
                NSString *stroptionalextra4=[NSString stringWithUTF8String:(char*)goptionalextra4];
                NSString *rstroptionalextra4=[stroptionalextra4 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra4=[rstroptionalextra4 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra4 = grstroptionalextra4;
            }
            
            char* goptionalextra5=(char*)sqlite3_column_text(statement, 30);
            if(goptionalextra5)
            {
                NSString *stroptionalextra5=[NSString stringWithUTF8String:(char*)goptionalextra5];
                NSString *rstroptionalextra5=[stroptionalextra5 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra5=[rstroptionalextra5 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra5 = grstroptionalextra5;
            }
            
            char* goptionalextra6=(char*)sqlite3_column_text(statement, 31);
            if(goptionalextra6)
            {
                NSString *stroptionalextra6=[NSString stringWithUTF8String:(char*)goptionalextra6];
                NSString *rstroptionalextra6=[stroptionalextra6 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra6=[rstroptionalextra6 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra6 = grstroptionalextra6;
            }
            
            char* goptionalextra7=(char*)sqlite3_column_text(statement, 32);
            if(goptionalextra7)
            {
                NSString *stroptionalextra7=[NSString stringWithUTF8String:(char*)goptionalextra7];
                NSString *rstroptionalextra7=[stroptionalextra7 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra7=[rstroptionalextra7 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra7 = grstroptionalextra7;
            }
            
            char* goptionalextra8=(char*)sqlite3_column_text(statement, 33);
            if(goptionalextra8)
            {
                NSString *stroptionalextra8=[NSString stringWithUTF8String:(char*)goptionalextra8];
                NSString *rstroptionalextra8=[stroptionalextra8 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra8=[rstroptionalextra8 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra8 = grstroptionalextra8;
            }
            
            char* goptionalextra9=(char*)sqlite3_column_text(statement, 34);
            if(goptionalextra9)
            {
                NSString *stroptionalextra9=[NSString stringWithUTF8String:(char*)goptionalextra9];
                NSString *rstroptionalextra9=[stroptionalextra9 stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstroptionalextra9=[rstroptionalextra9 stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.optionalextra9 = grstroptionalextra9;
            }
            
            char* gextra_comment=(char*)sqlite3_column_text(statement, 35);
            if(gextra_comment)
            {
                NSString *stgextra_comment=[NSString stringWithUTF8String:(char*)gextra_comment];
                NSString *rstgextra_comment=[stgextra_comment stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstgextra_comment=[rstgextra_comment stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.extra_comment = grstgextra_comment;
            }
            
            int gSwitchPoint=sqlite3_column_int(statement, 36);
            NSString *strSwitchPoint=[[NSString alloc]initWithFormat:@"%d", gSwitchPoint ];
            tcase.SwitchPoint=strSwitchPoint;
            
            //CustomerArticleNumber, SwitchType, Program, PlateName
            char* gCustomerArticleNumber=(char*)sqlite3_column_text(statement, 37);
            if(gCustomerArticleNumber)
            {
                NSString *strCustomerArticleNumber=[NSString stringWithUTF8String:(char*)gCustomerArticleNumber];
                NSString *rstrCustomerArticleNumber=[strCustomerArticleNumber stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrCustomerArticleNumber=[rstrCustomerArticleNumber stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.CustomerArticleNumber = grstrCustomerArticleNumber;
            }
            
            char* gSwitchType=(char*)sqlite3_column_text(statement, 38);
            if(gSwitchType)
            {
                NSString *strSwitchType=[NSString stringWithUTF8String:(char*)gSwitchType];
                NSString *rstrSwitchType=[strSwitchType stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrSwitchType=[rstrSwitchType stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.SwitchType = grstrSwitchType;
            }
            
            char* gProgram=(char*)sqlite3_column_text(statement, 39);
            if(gProgram)
            {
                NSString *strgProgram=[NSString stringWithUTF8String:(char*)gProgram];
                NSString *rstrgProgram=[strgProgram stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrgProgram=[rstrgProgram stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.Program = grstrgProgram;
            }
            
            char* gPlateName=(char*)sqlite3_column_text(statement, 40);
            if(gPlateName)
            {
                NSString *strgPlateName=[NSString stringWithUTF8String:(char*)gPlateName];
                NSString *rstrgPlateName=[strgPlateName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grstrgPlateName=[rstrgPlateName stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.PlateName = grstrgPlateName;
            }
            
            int gTotalSwitchingAngle=sqlite3_column_int(statement, 41);
            NSString *strTotalSwitchingAngle=[[NSString alloc]initWithFormat:@"%d", gTotalSwitchingAngle ];
            tcase.TotalSwitchingAngle=strTotalSwitchingAngle;
            
            int gSwitchingAngle=sqlite3_column_int(statement, 42);
            NSString *strSwitchingAngle=[[NSString alloc]initWithFormat:@"%d", gSwitchingAngle ];
            tcase.SwitchingAngle=strSwitchingAngle;
            
            int gNumberOfPosition=sqlite3_column_int(statement, 43);
            NSString *strgNumberOfPosition=[[NSString alloc]initWithFormat:@"%d", gNumberOfPosition ];
            tcase.numberOfPosition=strgNumberOfPosition;
            
            [itemarr addObject:tcase];
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(NSArray*)selectItemTopCase
{ 
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select caseID, CodeNumber from tblCase order by caseID desc limit 1"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        //NSLog(@"hello tblcaseID~~~~");
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNECase *tcase= [[KNECase alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tcase.CaseID=gCaseID;
            
            //NSLog(@"itemID>>>>>%d",itemID);
            //NSLog(@"tcase.CaseID>>>>>%@",tcase.CaseID);
            
            /**/char* controlCStr=(char*)sqlite3_column_text(statement, 1);
            //NSLog(@"controlCStr--->%s",controlCStr);
            if(controlCStr)
            {
                //NSString *memberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                NSString *gCaseName=[NSString stringWithUTF8String:(char*)controlCStr];
                NSString *rgCaseName=[gCaseName stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                NSString *grgCaseName=[rgCaseName stringByReplacingOccurrencesOfString:@"tisenocodeand" withString:@"&"];
                tcase.CaseName = grgCaseName;
                //NSLog(@"select tcontact.CaseName--->%@",tcase.CaseName);
            }
            
            
            [itemarr addObject:tcase];
            /*prevItem=tlogin;
             prevItemID=curItemID;;*/
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}
 
-(DataBaseUpdateResult)updateCase:(KNECase*)tcase
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString *iCaseName=[tcase.CaseName stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giCaseName=[iCaseName stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFLook=[tcase.FLook stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFLook=[iFLook stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFMounting=[tcase.FMounting stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFMounting=[iFMounting stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFEscutcheon_plate=[tcase.FEscutcheon_plate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFEscutcheon_plate=[iFEscutcheon_plate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFHandle=[tcase.FHandle stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFHandle=[iFHandle stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFLatch_mech=[tcase.FLatch_mech stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFLatch_mech=[iFLatch_mech stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFStop=[tcase.FStop stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFStop=[iFStop stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFStop_Degree=[tcase.FStop_Degree stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFStop_Degree=[iFStop_Degree stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFNofStage=[tcase.FNofStage stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFNofStage=[iFNofStage stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFMasterfData=[tcase.FMasterfData stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFMasterfData=[iFMasterfData stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFReference=[tcase.FReference stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFReference=[iFReference stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFDate=[tcase.FDate stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFDate=[iFDate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFModify_Date=[tcase.FModify_Date stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFModify_Date=[iFModify_Date stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFCustNo=[tcase.FCustNo stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFCustNo=[iFCustNo stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFCompany=[tcase.FCompany stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFCompany=[iFCompany stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iFVersion=[tcase.FVersion stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giFVersion=[iFVersion stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra1=[tcase.optionalextra1 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra1=[ioptionalextra1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra2=[tcase.optionalextra2 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra2=[ioptionalextra2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra3=[tcase.optionalextra3 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra3=[ioptionalextra3 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra4=[tcase.optionalextra4 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra4=[ioptionalextra4 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra5=[tcase.optionalextra5 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra5=[ioptionalextra5 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra6=[tcase.optionalextra6 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra6=[ioptionalextra6 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra7=[tcase.optionalextra7 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra7=[ioptionalextra7 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra8=[tcase.optionalextra8 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra8=[ioptionalextra8 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *ioptionalextra9=[tcase.optionalextra9 stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *gioptionalextra9=[ioptionalextra9 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iextra_comment=[tcase.extra_comment stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giextra_comment=[iextra_comment stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iCustomerArticleNumber=[tcase.CustomerArticleNumber stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giCustomerArticleNumber=[iCustomerArticleNumber stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iSwitchType=[tcase.SwitchType stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giSwitchType=[iSwitchType stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iProgram=[tcase.Program stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giProgram=[iProgram stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        NSString *iPlateName=[tcase.PlateName stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
        NSString *giPlateName=[iPlateName stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];

        NSString* updateSQL=[NSString stringWithFormat:@"update tblCase set CodeNumber='%@', FLook='%@', FMounting='%@', FEscutcheon_Plate='%@', FHandle='%@', FLatch_mech='%@', FStop='%@', FStop_Degree='%@', FNofStages='%@', FMaster_Data='%@', FReference='%@', FDate='%@', FModify_Date='%@', FCust_no='%@', FCompany='%@', FVersion='%@', pcs1='%d', pcs2='%d', pcs3='%d', pcs4='%d', pcs5='%d', pcs6='%d', pcs7='%d', pcs8='%d', pcs9='%d', optionalextra1='%@', optionalextra2='%@', optionalextra3='%@', optionalextra4='%@', optionalextra5='%@', optionalextra6='%@', optionalextra7='%@', optionalextra8='%@', optionalextra9='%@', extra_comment='%@', SwitchPoint='%d', CustomerArticleNumber='%@', SwitchType='%@', Program='%@', PlateName='%@', TotalSwitchingAngle='%d', SwitchingAngle='%d', NumberOfPosition='%d' where caseID='%d' ;",giCaseName, giFLook, giFMounting, giFEscutcheon_plate, giFHandle, giFLatch_mech, giFStop, giFStop_Degree, giFNofStage, giFMasterfData, giFReference, giFDate, giFModify_Date, giFCustNo, giFCompany, giFVersion, [tcase.pcs1 intValue], [tcase.pcs2 intValue], [tcase.pcs3 intValue], [tcase.pcs4 intValue], [tcase.pcs5 intValue], [tcase.pcs6 intValue], [tcase.pcs7 intValue], [tcase.pcs8 intValue], [tcase.pcs9 intValue], gioptionalextra1, gioptionalextra2, gioptionalextra3, gioptionalextra4, gioptionalextra5, gioptionalextra6, gioptionalextra7, gioptionalextra8, gioptionalextra9, giextra_comment, [tcase.SwitchPoint intValue], giCustomerArticleNumber, giSwitchType, giProgram, giPlateName, [tcase.TotalSwitchingAngle intValue], [tcase.SwitchingAngle intValue],[tcase.numberOfPosition intValue], [tcase.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
    
}

-(DataBaseDeletionResult)deletCase:(KNECase*)tCaseItem
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblCase where caseID=%d;",[tCaseItem.CaseID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    
    return DataBaseDeletionFailed;
    
}

-(void)deletselectedCase:(KNECase*)tCaseItem
{
    
    
    /*========delete jumper=============*/
    DatabaseAction *jdb=[[DatabaseAction alloc] init];
    NSArray *jumperarrays  = [jdb retrieveJumper:tCaseItem];
    
    for (Jumper *Jumperitem in jumperarrays)
    {

        [jdb deleteJumper:Jumperitem];
        
    }
    
    /*========delete contact=============*/
    DBTableGraft *dbtblcontact=[[DBTableGraft alloc]init ];
    NSArray *gcontactArr =[dbtblcontact selectItem:tCaseItem];
    for (KNETblGraftPositionTag *teim in gcontactArr )
    {
        [dbtblcontact deletCaseItem:teim];
        
    }
    
    /*=================================delete spring return case===============================================*/
    
    DBSpringReturn *dbtblSpringReturn=[[DBSpringReturn alloc]init ];
    NSArray *gSRtArr =[dbtblSpringReturn selectItem:tCaseItem];
    for (KNESpringRPosition *teim in gSRtArr )
    {
        [dbtblSpringReturn deletSpringReturnItem:teim];
        
    }
    
    /*=================================delete spring return value case===============================================*/
    
    DBSpringReturnContain *dbDBSpringReturnContain=[[DBSpringReturnContain alloc]init ];
    NSArray *dSRContainArr =[dbDBSpringReturnContain selectSRContainItem:tCaseItem];
    for (KNESpringReturnContain *teim in dSRContainArr )
    {
        [dbDBSpringReturnContain deletSRContainItem:teim];
        
    }
    
    
    /*========delete case=============*/
    [self deletCase:tCaseItem];
}

@end
