//
//  DBSpringReturn.h
//  KNE
//
//  Created by Tiseno Mac 2 on 11/23/12.
//
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "KNECase.h"
#import "KNESpringRPosition.h"

@interface DBSpringReturn : DBbase{
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertItemArray:(NSArray*)SRArr;
-(DataBaseInsertionResult)insertSpringReturn:(KNESpringRPosition*)tSpringReturn;
-(NSArray*)selectItem:(KNECase*)SpringReturnCaseArr;
-(DataBaseDeletionResult)deletSpringReturnItem:(KNESpringRPosition*)tSRItem;
@end
