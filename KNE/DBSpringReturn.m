//
//  DBSpringReturn.m
//  KNE
//
//  Created by Tiseno Mac 2 on 11/23/12.
//
//

#import "DBSpringReturn.h"

@implementation DBSpringReturn
@synthesize runBatch;

-(DataBaseInsertionResult)insertItemArray:(NSArray*)SRArr
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    [self openConnection];
    for(KNESpringRPosition* item in SRArr)
    {
        [self insertSpringReturn:item];
        
    }
    [self closeConnection];
    return insertionResult;
}

-(DataBaseInsertionResult)insertSpringReturn:(KNESpringRPosition*)tSpringReturn
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        
        //NSLog(@"[tcontact.imgGbtnTag intValue]>>>>>%d",[tcontact.imgGbtnTag intValue]);
        insertSQL = [NSString stringWithFormat:@"insert into tblSpringReturn(caseID, SRStartTag, SREndTag) values(%d, %d, %d);",[tSpringReturn.CaseID intValue], [tSpringReturn.SRStartPoint intValue], [tSpringReturn.SREndPoint intValue]];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSArray*)selectItem:(KNECase*)SpringReturnCaseArr
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select SpringReturnID, caseID, SRStartTag, SREndTag from tblSpringReturn WHERE caseID= '%d'", [SpringReturnCaseArr.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNESpringRPosition *tSR= [[KNESpringRPosition alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *SRID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tSR.SpringReturnID=SRID;
            
            int caseitemID=sqlite3_column_int(statement, 1);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", caseitemID ];
            tSR.CaseID=gCaseID;
            
            int StartTag=sqlite3_column_int(statement, 2);
            NSString *gStartTag=[[NSString alloc]initWithFormat:@"%d", StartTag ];
            tSR.SRStartPoint=gStartTag;
            
            int EndTag=sqlite3_column_int(statement, 3);
            NSString *gEndTag=[[NSString alloc]initWithFormat:@"%d", EndTag ];
            tSR.SREndPoint = gEndTag;

            [itemarr addObject:tSR];
            /*prevItem=tlogin;
             prevItemID=curItemID;;*/
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(DataBaseDeletionResult)deletSpringReturnItem:(KNESpringRPosition*)tSRItem
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblSpringReturn where SpringReturnID=%d;",[tSRItem.SpringReturnID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }

    return DataBaseDeletionFailed;
        
}

@end
