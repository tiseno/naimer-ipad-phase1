//
//  DBSpringReturnContain.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/3/12.
//
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "KNECase.h"
#import "KNESpringReturnContain.h"
@interface DBSpringReturnContain : DBbase{
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertItemSRContainArray:(NSArray*)SRContainArr;
-(DataBaseInsertionResult)insertSRContain:(KNESpringReturnContain*)tSRContain;
-(NSArray*)selectSRContainItem:(KNECase*)SRContainCaseArr;
-(DataBaseDeletionResult)deletSRContainItem:(KNESpringReturnContain*)tSRContainItem;
@end
