//
//  DBSpringReturnContain.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/3/12.
//
//

#import "DBSpringReturnContain.h"

@implementation DBSpringReturnContain
@synthesize runBatch;

-(DataBaseInsertionResult)insertItemSRContainArray:(NSArray*)SRContainArr;
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    [self openConnection];
    for(KNESpringReturnContain* item in SRContainArr)
    {
        [self insertSRContain:item];
        
    }
    [self closeConnection];
    return insertionResult;
}

-(DataBaseInsertionResult)insertSRContain:(KNESpringReturnContain*)tSRContain
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;

        insertSQL = [NSString stringWithFormat:@"insert into tblSpringReturnContain(CaseID, ContainTag, ContainValue) values(%d, %d, '%@');",[tSRContain.CaseID intValue], [tSRContain.ContainTag intValue], tSRContain.ContainValue ];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSArray*)selectSRContainItem:(KNECase*)SRContainCaseArr
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select ContainID, CaseID, ContainTag, ContainValue from tblSpringReturnContain WHERE CaseID= '%d'", [SRContainCaseArr.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNESpringReturnContain *tSR= [[KNESpringReturnContain alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *SRID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tSR.ContainID=SRID;
            
            int caseitemID=sqlite3_column_int(statement, 1);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", caseitemID ];
            tSR.CaseID=gCaseID;
            
            int StartTag=sqlite3_column_int(statement, 2);
            NSString *gStartTag=[[NSString alloc]initWithFormat:@"%d", StartTag ];
            tSR.ContainTag=gStartTag;

            
            
            char* controlCStr=(char*)sqlite3_column_text(statement, 3);
            if(controlCStr)
            {
                NSString *gcontainValue=[NSString stringWithUTF8String:(char*)controlCStr];
                tSR.ContainValue = gcontainValue;
            }
            
            
            [itemarr addObject:tSR];
            /*prevItem=tlogin;
             prevItemID=curItemID;;*/
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}


-(DataBaseDeletionResult)deletSRContainItem:(KNESpringReturnContain*)tSRContainItem;
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblSpringReturnContain where ContainID=%d;",[tSRContainItem.ContainID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    
    return DataBaseDeletionFailed;
    
}

@end
