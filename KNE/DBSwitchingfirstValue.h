//
//  DBSwitchingfirstValue.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/6/12.
//
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "KNESwitchingFirstValue.h"
@interface DBSwitchingfirstValue : DBbase{
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertItemSwitchingFirstValueArray:(NSArray*)SRvalueArr;
-(DataBaseInsertionResult)insertSwitchingFirstValue:(KNESwitchingFirstValue*)tSRvalue;
-(NSArray*)selectSwitchingFirstValueItem:(KNECase*)SRvalueArr;
-(DataBaseUpdateResult)updateSwitchingFistValue:(KNESwitchingFirstValue*)tvalue;

@end
