//
//  DBSwitchingfirstValue.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/6/12.
//
//

#import "DBSwitchingfirstValue.h"

@implementation DBSwitchingfirstValue
@synthesize runBatch;

-(DataBaseInsertionResult)insertItemSwitchingFirstValueArray:(NSArray*)SRvalueArr
{
    
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    [self openConnection];
    for(KNESwitchingFirstValue* item in SRvalueArr)
    {
        [self insertSwitchingFirstValue:item];
        
    }
    [self closeConnection];
    return insertionResult;
}

-(DataBaseInsertionResult)insertSwitchingFirstValue:(KNESwitchingFirstValue*)tSRvalue
{
    
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        
        //NSLog(@"insert >>tSRvalue.SwitchingFirstValue>>>>>%@",tSRvalue.SwitchingFirstValue);
        insertSQL = [NSString stringWithFormat:@"insert into tblSwitchingFirstValue(CaseID, SwitchingFirstValue) values(%d, %d);",[tSRvalue.CaseID intValue], [tSRvalue.SwitchingFirstValue intValue] ];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
    
}

-(NSArray*)selectSwitchingFirstValueItem:(KNECase*)SRvalueArr
{
    //mergesort(<#void *#>, <#size_t#>, <#size_t#>, <#int (*)(const void *, const void *)#>)
    
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select CaseID, SwitchingFirstValue from tblSwitchingFirstValue WHERE CaseID= '%d'", [SRvalueArr.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNESwitchingFirstValue *tSR= [[KNESwitchingFirstValue alloc] init];
            
            int itemID=sqlite3_column_int(statement, 0);
            NSString *SRID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tSR.CaseID=SRID;
            
            int caseitemID=sqlite3_column_int(statement, 1);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", caseitemID ];
            tSR.SwitchingFirstValue=gCaseID;

            [itemarr addObject:tSR];
            /*prevItem=tlogin;
             prevItemID=curItemID;;*/
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr; 
    
}

-(DataBaseUpdateResult)updateSwitchingFistValue:(KNESwitchingFirstValue*)tvalue
{
    if([self openConnection]==DataBaseConnectionOpened)
    { 
        
        NSString* updateSQL=[NSString stringWithFormat:@"update tblSwitchingFirstValue set SwitchingFirstValue='%d' where CaseID='%d' ;", [tvalue.SwitchingFirstValue intValue], [tvalue.CaseID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
    
}



@end
