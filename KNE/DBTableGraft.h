//
//  DBTableGraft.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/19/12.
//
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "KNETblGraftPositionTag.h"
#import "KNECase.h"
@interface DBTableGraft : DBbase{
    BOOL runBatch;
} 
@property (nonatomic)BOOL runBatch;

-(DataBaseUpdateResult)updateLogin:(KNETblGraftPositionTag*)tlogin;

-(DataBaseInsertionResult)insertcontact:(KNETblGraftPositionTag*)tcontact;
-(DataBaseInsertionResult)insertItemArray:(NSArray*)contactArr;
-(NSArray*)selectItem:(KNECase*)contactCaseArr;
//-(DataBaseDeletionResult)deletCaseItem:(KNECase*)tcaseItem;
-(DataBaseDeletionResult)deletCaseItem:(KNETblGraftPositionTag*)tcaseItem;

@end
