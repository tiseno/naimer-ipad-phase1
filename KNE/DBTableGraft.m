//
//  DBTableGraft.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/19/12.
//
//

#import "DBTableGraft.h"

@implementation DBTableGraft

@synthesize runBatch;

-(DataBaseInsertionResult)insertcontact:(KNETblGraftPositionTag*)tcontact
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        
        //NSLog(@"[tcontact.imgGbtnTag intValue]>>>>>%d",[tcontact.imgGbtnTag intValue]);
        insertSQL = [NSString stringWithFormat:@"insert into tblContact(caseID, contactimgTag, contactValue, contactupdown) values(%d, %d, %d, %d);",[tcontact.CaseID intValue], [tcontact.imgGbtnTag intValue],[tcontact.imgGbtnValueTag intValue], [tcontact.Vposition intValue]];

        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(DataBaseInsertionResult)insertItemArray:(NSArray*)contactArr
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    [self openConnection];
    for(KNETblGraftPositionTag* item in contactArr)
    {
        [self insertcontact:item];
        
    }
    [self closeConnection];
    return insertionResult;
}

-(DataBaseUpdateResult)updateLogin:(KNETblGraftPositionTag*)tcontact
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString* updateSQL=[NSString stringWithFormat:@"update tblContact set contactimgTag=%d , contactValue=%d where caseID=%d ;", [tcontact.imgGbtnTag intValue],[tcontact.imgGbtnValueTag intValue], [tcontact.CaseID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
    
}


-(NSArray*)selectItem:(KNECase*)contactCaseArr
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select contactID, caseID, contactimgTag, contactValue, contactupdown from tblContact WHERE caseID= '%d'", [contactCaseArr.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        //NSLog(@"hello select~~~~");
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            KNETblGraftPositionTag *tcontact= [[KNETblGraftPositionTag alloc] init];

            int itemID=sqlite3_column_int(statement, 0);
            NSString *gcontactCaseID=[[NSString alloc]initWithFormat:@"%d", itemID ];
            tcontact.contactID=gcontactCaseID;
            
            int caseitemID=sqlite3_column_int(statement, 1);
            NSString *gCaseID=[[NSString alloc]initWithFormat:@"%d", caseitemID ];
            tcontact.CaseID=gCaseID;
            
            
            /*char* controlCStr=(char*)sqlite3_column_text(statement, 1);
            //NSLog(@"controlCStr--->%@",controlCStr);
            if(controlCStr)
            {
                //NSString *memberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                NSString *memberid=[NSString stringWithUTF8String:(char*)controlCStr];
                tcontact.imgGbtnTag = memberid;
            }*/
            
            
            
            int imgTag=sqlite3_column_int(statement, 2);
            NSString *gimgTag=[[NSString alloc]initWithFormat:@"%d", imgTag ];
            tcontact.imgGbtnTag=gimgTag;
            
            int imgvalue=sqlite3_column_int(statement, 3);
            NSString *gimgvalue=[[NSString alloc]initWithFormat:@"%d", imgvalue ];
            tcontact.imgGbtnValueTag = gimgvalue;
            
            int imgvalueposition=sqlite3_column_int(statement, 4);
            NSString *gimgvalueposition=[[NSString alloc]initWithFormat:@"%d", imgvalueposition ];
            tcontact.Vposition = gimgvalueposition;
            

            //NSLog(@"tcontact.imgGbtnValueTag>>>>>>>>%@",tcontact.imgGbtnValueTag);
            
            [itemarr addObject:tcontact];
            /*prevItem=tlogin;
             prevItemID=curItemID;;*/
 
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(DataBaseDeletionResult)deletCaseItem:(KNETblGraftPositionTag*)tcaseItem
{
    //NSLog(@"tcaseItem.contactID>>>>%@",tcaseItem.contactID);
    
    //delete from MRU where rowid = ( select rowid from MRU order by DT_ADD limit 1 )
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
    
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblContact where contactID=%d;",[tcaseItem.contactID intValue]];
        //NSString *insertSQL = [NSString stringWithFormat:@"delete from tblContact where caseID=( select * from tblContact where caseID = '%d')",[tcaseItem.CaseID intValue]];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    //NSString *insertSQL = [NSString stringWithFormat:@"delete from tblContact where caseID='%d'",[tcaseItem.CaseID intValue]];
    
    return DataBaseDeletionFailed;
    

}
@end