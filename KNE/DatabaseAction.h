//
//  DatabaseAction.h
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "Jumper.h"
#import "EscutheonPlate.h"

@interface DatabaseAction : DBbase
{
    BOOL runBatch;
}

@property (nonatomic)BOOL runBatch;

-(DataBaseInsertionResult)insertJumper:(Jumper*)jumper;
-(NSArray*)retrieveJumper:(KNECase *)gcase;
-(DataBaseDeletionResult)deleteJumper:(KNECase *)gcase;
-(NSArray*)retrievePlate:(int) PlateID;
-(NSMutableArray*)retrieveAllPlate;
-(DataBaseUpdateResult)updatePlateInformation:(EscutheonPlate*)plate;
-(DataBaseInsertionResult)insertPlate:(EscutheonPlate *)plate;
-(DataBaseDeletionResult)deletePlate:(int)PlateID;
-(NSArray*)checkIfPlateWithEngravingNumberExist:(NSString*) engraveNumber;

@end
