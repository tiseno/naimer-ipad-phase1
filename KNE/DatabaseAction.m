//
//  DatabaseAction.m
//  OrientalDailyiOS
//
//  Created by Tiseno on 8/9/12.
//  Copyright (c) 2012 Tiseno. All rights reserved.
//

#import "DatabaseAction.h"

@implementation DatabaseAction

@synthesize runBatch;

-(DataBaseInsertionResult)insertJumper:(Jumper*)jumper;
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    //NSLog(@"jumper.initial,%d jumper.final %d jumper.CaseID>>>%d", jumper.initial, jumper.final, [jumper.CaseID intValue]);
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into tblJumper(Initial, Final, Isinvert, caseID) values('%d', '%d','%d' , '%d');", jumper.initial, jumper.final, jumper.isinvert, [jumper.CaseID intValue]];
        
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    
    return DataBaseInsertionFailed;
}

-(NSArray*)retrieveJumper:(KNECase *)gcase
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select Initial, Final, caseID, Isinvert from tblJumper where caseID= '%d'",[gcase.CaseID intValue] ];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            Jumper *jumper= [[Jumper alloc] init];
            
            int initial=sqlite3_column_int(statement, 0);
            jumper.initial = initial;
            
            int final=sqlite3_column_int(statement, 1);
            jumper.final = final;
            
            int caseid=sqlite3_column_int(statement, 2);
            NSString *gcaseid=[[NSString alloc] initWithFormat:@"%d", caseid];
            jumper.CaseID = gcaseid;
            
            int isinvert=sqlite3_column_int(statement, 3);
            jumper.isinvert = isinvert;
            
            //NSLog(@"jumper.initial,%d jumper.final %d >>>>[jumper.CaseID intValue] >>>>%d", jumper.initial, jumper.final, [jumper.CaseID intValue]);
            [itemarr addObject:jumper];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(DataBaseDeletionResult)deleteJumper:(KNECase *)gcase
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblJumper where caseID='%d'",[gcase.CaseID
                               intValue] ];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

/*======================================= Plate========================================================================*/

-(NSArray*)retrievePlate:(int) PlateID
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection] == DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select * from tblEscutheonPlate where PlateID=%d", PlateID];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            EscutheonPlate *ep = [[EscutheonPlate alloc] init];
            
            int initial = sqlite3_column_int(statement, 0);
            ep.PlateID = initial;
            
            char* NoOfPosition = (char*)sqlite3_column_text(statement, 1);
            
            if(NoOfPosition)
            {
                NSString *strNoOfPosition = [NSString stringWithUTF8String:(char*)NoOfPosition];
                
                ep.NoOfPosition = strNoOfPosition;
            }
            
            char* headerF = (char*)sqlite3_column_text(statement, 2);
            
            if(headerF)
            {
                NSString *strheaderF = [NSString stringWithUTF8String:(char*)headerF];
                
                ep.headerF = strheaderF;
            }
            
            char* name = (char*)sqlite3_column_text(statement, 3);
            
            if(name)
            {
                NSString *strName = [NSString stringWithUTF8String:(char*)name];
                
                ep.name = strName;
            }
            
            char* desc1 = (char*)sqlite3_column_text(statement, 4);
            
            if(desc1)
            {
                NSString *strDesc1 = [NSString stringWithUTF8String:(char*)desc1];
                
                ep.desc1 = strDesc1;
            }
            
            char* desc2 = (char*)sqlite3_column_text(statement, 5);
            
            if(desc2)
            {
                NSString *strDesc2 = [NSString stringWithUTF8String:(char*)desc2];
                
                ep.desc2 = strDesc2;
            }
            
            char* desc3 = (char*)sqlite3_column_text(statement, 6);
            
            if(desc3)
            {
                NSString *strDesc3 = [NSString stringWithUTF8String:(char*)desc3];
                
                ep.desc3 = strDesc3;
            }
            
            char* desc4 = (char*)sqlite3_column_text(statement, 7);
            
            if(desc4)
            {
                NSString *strDesc4 = [NSString stringWithUTF8String:(char*)desc4];
                
                ep.desc4 = strDesc4;
            }
            
            char* desc5 = (char*)sqlite3_column_text(statement, 8);
            
            if(desc5)
            {
                NSString *strDesc5 = [NSString stringWithUTF8String:(char*)desc5];
                
                ep.desc5 = strDesc5;
            }
            
            char* desc6 = (char*)sqlite3_column_text(statement, 9);
            
            if(desc6)
            {
                NSString *strDesc6 = [NSString stringWithUTF8String:(char*)desc6];
                
                ep.desc6 = strDesc6;
            }
            
            char* desc7 = (char*)sqlite3_column_text(statement, 10);
            
            if(desc7)
            {
                NSString *strDesc7 = [NSString stringWithUTF8String:(char*)desc7];
                
                ep.desc7 = strDesc7;
            }
            
            char* desc8 = (char*)sqlite3_column_text(statement, 11);
            
            if(desc8)
            {
                NSString *strDesc8 = [NSString stringWithUTF8String:(char*)desc8];
                
                ep.desc8 = strDesc8;
            }
            
            char* desc9 = (char*)sqlite3_column_text(statement, 12);
            
            if(desc9)
            {
                NSString *strDesc9 = [NSString stringWithUTF8String:(char*)desc9];
                
                ep.desc9 = strDesc9;
            }
            
            char* desc10 = (char*)sqlite3_column_text(statement, 13);
            
            if(desc10)
            {
                NSString *strDesc10 = [NSString stringWithUTF8String:(char*)desc10];
                
                ep.desc10 = strDesc10;
            }
            
            char* desc11 = (char*)sqlite3_column_text(statement, 14);
            
            if(desc11)
            {
                NSString *strDesc11 = [NSString stringWithUTF8String:(char*)desc11];
                
                ep.desc11 = strDesc11;
            }
            
            char* desc12 = (char*)sqlite3_column_text(statement, 15);
            
            if(desc12)
            {
                NSString *strDesc12 = [NSString stringWithUTF8String:(char*)desc12];
                
                ep.desc12 = strDesc12;
            }
            
            char* desc13 = (char*)sqlite3_column_text(statement, 16);
            
            if(desc13)
            {
                NSString *strDesc13 = [NSString stringWithUTF8String:(char*)desc13];
                
                ep.desc13 = strDesc13;
            }
            
            char* desc14 = (char*)sqlite3_column_text(statement, 17);
            
            if(desc14)
            {
                NSString *strDesc14 = [NSString stringWithUTF8String:(char*)desc14];
                
                ep.desc14 = strDesc14;
            }
            
            char* title1 = (char*)sqlite3_column_text(statement, 18);
            
            if(title1)
            {
                NSString *strTitle1 = [NSString stringWithUTF8String:(char*)title1];
                
                ep.title1 = strTitle1;
            }
            
            char* title2 = (char*)sqlite3_column_text(statement, 19);
            
            if(title2)
            {
                NSString *strTitle2 = [NSString stringWithUTF8String:(char*)title2];
                
                ep.title2 = strTitle2;
            }
            
            char* title3 = (char*)sqlite3_column_text(statement, 20);
            
            if(title3)
            {
                NSString *strTitle3 = [NSString stringWithUTF8String:(char*)title3];
                
                ep.title3 = strTitle3;
            }
            
            char* reference = (char*)sqlite3_column_text(statement, 21);
            
            if(title3)
            {
                NSString *strReference = [NSString stringWithUTF8String:(char*)reference];
                
                ep.reference = strReference;
            }
            
            char* created = (char*)sqlite3_column_text(statement, 22);
            
            if(created)
            {
                NSString *strCreated = [NSString stringWithUTF8String:(char*)created];
                
                ep.created = strCreated;
            }
            
            char* company = (char*)sqlite3_column_text(statement, 23);
            
            if(company)
            {
                NSString *strCompany = [NSString stringWithUTF8String:(char*)company];
                
                ep.company = strCompany;
            }
            
            char* modified = (char*)sqlite3_column_text(statement, 24);
            
            if(modified)
            {
                NSString *strModified = [NSString stringWithUTF8String:(char*)modified];
                
                ep.modified = strModified;
            }
            
            char* customerNo = (char*)sqlite3_column_text(statement, 25);
            
            if(customerNo)
            {
                NSString *strCustomerNo = [NSString stringWithUTF8String:(char*)customerNo];
                
                ep.customerNo = strCustomerNo;
            }
            
            char* header = (char*)sqlite3_column_text(statement, 26);
            
            if(header)
            {
                NSString *strHeader = [NSString stringWithUTF8String:(char*)header];
                
                ep.header = strHeader;
            }
            
            char* desc15 = (char*)sqlite3_column_text(statement, 27);
            
            if(desc15)
            {
                NSString *strDesc15 = [NSString stringWithUTF8String:(char*)desc15];
                
                ep.desc15 = strDesc15;
            }
            
            char* desc16 = (char*)sqlite3_column_text(statement, 28);
            
            if(desc16)
            {
                NSString *strDesc16 = [NSString stringWithUTF8String:(char*)desc16];
                
                ep.desc16 = strDesc16;
            }
            
            [itemarr addObject:ep];
            
        }
        sqlite3_finalize(statement);
        
        [self closeConnection];
    }
    return itemarr;
}

-(NSMutableArray*)retrieveAllPlate
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection] == DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select PlateID, col_name, col_customerNo, col_company, col_created, col_No_of_Position, col_headerF                                                            from tblEscutheonPlate order by PlateID desc"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            EscutheonPlate *ep = [[EscutheonPlate alloc] init];
            
            int initial = sqlite3_column_int(statement, 0);
            ep.PlateID = initial;
            
            char* name = (char*)sqlite3_column_text(statement, 1);
            
            if(name)
            {
                NSString *strName = [NSString stringWithUTF8String:(char*)name];
                
                ep.name = strName;
            }
            
            char* customerNo = (char*)sqlite3_column_text(statement, 2);
            
            if(customerNo)
            {
                NSString *strCustomerNo = [NSString stringWithUTF8String:(char*)customerNo];
                
                ep.customerNo = strCustomerNo;
            }
            
            char* company = (char*)sqlite3_column_text(statement, 3);
            
            if(company)
            {
                NSString *strCompany = [NSString stringWithUTF8String:(char*)company];
                
                ep.company = strCompany;
            }
            
            char* created = (char*)sqlite3_column_text(statement, 4);
            
            if(created)
            {
                NSString *strCreated = [NSString stringWithUTF8String:(char*)created];
                
                ep.created = strCreated;
            }
            
            char* numOfPosition = (char*)sqlite3_column_text(statement, 5);
            
            if(numOfPosition)
            {
                NSString *strnumOfPosition = [NSString stringWithUTF8String:(char*)numOfPosition];
                
                ep.NoOfPosition = strnumOfPosition;
            }
            
            char* headerf = (char*)sqlite3_column_text(statement, 6);
            
            if(headerf)
            {
                NSString *strheaderf = [NSString stringWithUTF8String:(char*)headerf];
                
                ep.headerF = strheaderf;
            }
            
            [itemarr addObject:ep];
            
            
            
        }
        
        sqlite3_finalize(statement);
        
        [self closeConnection];
    }
    
    return itemarr;
}

-(DataBaseUpdateResult)updatePlateInformation:(EscutheonPlate*)plate
{
    if([self openConnection] == DataBaseConnectionOpened)
    {
        NSString* updateSQL = [NSString stringWithFormat:@"update tblEscutheonPlate set col_desc1='%@', col_desc2='%@', col_desc3='%@', col_desc4='%@', col_desc5='%@', col_desc6='%@', col_desc7='%@', col_desc8='%@', col_desc9='%@', col_desc10='%@', col_desc11='%@', col_desc12='%@', col_desc13='%@', col_desc14='%@', col_title1='%@', col_title2='%@', col_title3='%@', col_reference='%@', col_created='%@', col_modified='%@', col_customerNo='%@', col_company='%@', col_header='%@', col_name='%@', col_headerF='%@', col_No_Of_Position='%@', col_desc15='%@', col_desc16='%@' where PlateID = %d", plate.desc1, plate.desc2, plate.desc3, plate.desc4, plate.desc5, plate.desc6, plate.desc7, plate.desc8, plate.desc9, plate.desc10, plate.desc11, plate.desc12, plate.desc13, plate.desc14, plate.title1, plate.title2, plate.title3, plate.reference, plate.created, plate.modified, plate.customerNo, plate.company, plate.header, plate.name, plate.headerF, plate.NoOfPosition, plate.desc15, plate.desc16, plate.PlateID];
        
        sqlite3_stmt *statement;
        
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self closeConnection];
            
            return DataBaseUpdateSuccessful;
        }
        
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        
        sqlite3_finalize(statement);
        
        [self closeConnection];
        
        return DataBaseUpdateFailed;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
}

-(DataBaseInsertionResult)insertPlate:(EscutheonPlate *)plate
{
    BOOL connectionIsOpenned=YES;
    
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into tblEscutheonPlate(col_desc1, col_desc2, col_desc3, col_desc4, col_desc5, col_desc6, col_desc7, col_desc8, col_desc9, col_desc10, col_desc11, col_desc12, col_desc13, col_desc14, col_title1, col_title2, col_title3, col_reference, col_created, col_company, col_modified, col_customerNo, col_name, col_header, col_headerF, col_No_of_Position, col_desc15, col_desc16) values('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@');", plate.desc1, plate.desc2, plate.desc3, plate.desc4, plate.desc5, plate.desc6, plate.desc7, plate.desc8, plate.desc9, plate.desc10, plate.desc11, plate.desc12, plate.desc13, plate.desc14, plate.title1, plate.title2, plate.title3, plate.reference, plate.created, plate.company, plate.modified, plate.customerNo, plate.name, plate.header, plate.headerF, plate.NoOfPosition, plate.desc15, plate.desc16];
        
        NSLog(@"insert headerF>>>>%@",plate.headerF);
        sqlite3_stmt *statement;
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            if(!runBatch)
            {
                [self closeConnection];
            }
            
            return DataBaseInsertionSuccessful;
        }
        
        sqlite3_finalize(statement);
        
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    
    return DataBaseInsertionFailed;
}

-(DataBaseDeletionResult)deletePlate:(int)PlateID
{
    if([self openConnection] == DataBaseConnectionOpened)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"delete from tblEscutheonPlate where PlateID=%d", PlateID];
        
        sqlite3_stmt *statement;
        
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            return DataBaseDeletionSuccessful;
        }
        
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        
        sqlite3_finalize(statement);
        
        [self closeConnection];
        
    }
    
    return DataBaseDeletionFailed;
}

-(NSArray*)checkIfPlateWithEngravingNumberExist:(NSString*) engraveNumber
{
    NSMutableArray *itemarr = [[NSMutableArray alloc] init];
    
    if([self openConnection] == DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select PlateID, col_name                                                             from tblEscutheonPlate where col_headerF='%@'", engraveNumber];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            EscutheonPlate *ep = [[EscutheonPlate alloc] init];
            
            int initial = sqlite3_column_int(statement, 0);
            ep.PlateID = initial;
            
            char* name = (char*)sqlite3_column_text(statement, 1);
            
            if(name)
            {
                NSString *strName = [NSString stringWithUTF8String:(char*)name];
                
                ep.name = strName;
            }
            
            [itemarr addObject:ep];
            
        }
        
        sqlite3_finalize(statement);
        
        [self closeConnection];
    }
    
    return itemarr;

}

@end
