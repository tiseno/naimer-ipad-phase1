//
//  EscutheonPlatePortrait.m
//  KNE
//
//  Created by Jermin Bazazian on 12/28/12.
//
//

#import "EscutheonPlatePortrait.h"
#import "DatabaseAction.h"
#import <QuartzCore/QuartzCore.h>

@interface EscutheonPlatePortrait ()

@end

@implementation EscutheonPlatePortrait
@synthesize popoverController;
@synthesize option, PlateID;
@synthesize txtViewDesc1, txtViewDesc2, txtViewDesc3, txtViewDesc4, txtViewDesc5, txtViewDesc6, txtViewDesc7, txtViewDesc8, txtViewDesc9, txtViewDesc10, txtViewDesc11, txtViewDesc12, txtViewDesc13, txtViewDesc14, txtViewDesc15, txtViewDesc16;
@synthesize txtReference, txtCompany, txtCreated, txtCustomerNo, txtModified;
@synthesize txtViewTitle1, txtViewTitle2, txtViewTitle3;
@synthesize txtHeader1, txtHeader2, txtHeader3, txtHeader4, txtHeader5, txtHeader6;
@synthesize plateInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    EscutheonPlatePortrait *gKNEEditSwitchCaseViewController = [[EscutheonPlatePortrait alloc] initWithNibName:@"EscutheonPlatePortrait" bundle:nil];
    
    
    if([option isEqualToString:@"edit"])
    {
        NSLog(@"editediteditediteditediteditediteditediteditediteditediteditediteditedit");
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
            rightButton.frame = CGRectMake(0, 0, 62, 33);
            [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
        
        gKNEEditSwitchCaseViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    }
    else if([option isEqualToString:@"email"])
    {
        UIToolbar *tool = [UIToolbar new];
        tool.frame = CGRectMake(0, 0, 160, 43);
        UIImage *image = [[UIImage imageNamed:@"black_bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [[UIToolbar appearance] setBackgroundImage:image forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
        [[UINavigationBar appearance] setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:2];
        

        
        UIButton *optionButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [optionButton setImage:[UIImage imageNamed:@"btn_option.png"] forState:UIControlStateNormal];
        optionButton.frame = CGRectMake(0, 0, 62, 33);
        [optionButton addTarget:self action:@selector(popoverselected) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *btnoption = [[UIBarButtonItem alloc] initWithCustomView: optionButton];
        [items addObject:btnoption];
        
        UIButton *closeButton =[UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton setImage:[UIImage imageNamed:@"btn_close.png"] forState:UIControlStateNormal];
        closeButton.frame = CGRectMake(0, 0, 62, 33);
        [closeButton addTarget:self action:@selector(backToListOfPlate) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *btnclose = [[UIBarButtonItem alloc] initWithCustomView: closeButton];
        [items addObject:btnclose];
        
        /*================================================*/
        
        [tool setItems:items];
        tool.barStyle =UIBarStyleDefault;
        tool.backgroundColor = [UIColor clearColor];
        
        self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:tool];
        
        gKNEEditSwitchCaseViewController.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView: tool];
    }
    else
    {
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
            rightButton.frame = CGRectMake(0, 0, 62, 33);
            [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
        
        gKNEEditSwitchCaseViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    }
    
    
    
    
    
    [self deactivatedAllNecessaryAttribute];
    
    if([option isEqualToString:@"edit"])
    {
        [self initializePlate];
    }
    else if([option isEqualToString:@"email"])
    {
        [self initializePlate];
        
        [self disableInput];
    }
    else if([option isEqualToString:@"passPlate"])
    {
        [self initializePassInfo];
        
        NSDate* currentDate = [NSDate date];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm"];
        
        NSString *dateString = [dateFormat stringFromDate:currentDate];
        
        txtCreated.text = dateString;
    }
    else
    {
        //new plate
        NSDate* currentDate = [NSDate date];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm"];
        
        NSString *dateString = [dateFormat stringFromDate:currentDate];
        
        txtCreated.text = dateString;
    }
}

-(void)popoverselected
{
    /*=====================pop over===========================*/
    UIViewController* popoverContent = [[UIViewController alloc] init];
    UIView *popoverView = [[UIView alloc] init];
    popoverView.backgroundColor = [UIColor blackColor];
    NSMutableArray *ButtonArray=[[NSMutableArray alloc ]init];
    UIBarButtonItem *space=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [ButtonArray addObject:space];

    
    
    /*===========save================*/

    
    btnsavefirstPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsavefirstPage setImage:[UIImage imageNamed:@"btn_print_large.png"] forState:UIControlStateNormal];
    btnsavefirstPage.frame = CGRectMake(15, 10, 200, 45);
    [btnsavefirstPage addTarget:self action:@selector(printdochalf:) forControlEvents:UIControlEventTouchUpInside];
    btnPrint1 = [[UIBarButtonItem alloc] initWithCustomView: btnsavefirstPage] ;
    [popoverView addSubview:btnsavefirstPage];
    
    
    UIButton *btnsavesecondPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsavesecondPage setImage:[UIImage imageNamed:@"btn_email_large.png"] forState:UIControlStateNormal];
    btnsavesecondPage.frame = CGRectMake(15, 65, 200, 45);
    [btnsavesecondPage addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    [popoverView addSubview:btnsavesecondPage];
    
    
    btnsaveFullPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsaveFullPage setImage:[UIImage imageNamed:@"btn_save_large.png"] forState:UIControlStateNormal];
    btnsaveFullPage.frame = CGRectMake(15, 120, 200, 45);
    [btnsaveFullPage addTarget:self action:@selector(saveimagefirstPage) forControlEvents:UIControlEventTouchUpInside];
    
    [popoverView addSubview:btnsaveFullPage];
    
    /*===========================*/
    
    popoverContent.view = popoverView;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    [popoverController setPopoverContentSize:CGSizeMake(230, 180) animated:NO];
    [popoverController presentPopoverFromRect:CGRectMake(635, 0, 10, 1) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionUp animated:YES];
    
}

-(void)saveimagefirstPage
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    UIGraphicsBeginImageContext(self.view.frame.size);
	[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Image has been saved in your photo gallery" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
}

/*================print image=============================*/

-(IBAction)printdochalf:(id)sender
{
    
    UIGraphicsBeginImageContext(self.view.frame.size);
	
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	
    UIGraphicsEndImageContext();
    
    if (viewImage != nil)
    {
        imageData = UIImagePNGRepresentation(viewImage);
        [self printimage:imageData];
        
    }
    
}

-(void)printimage:(NSData*)image
{
    pic = [UIPrintInteractionController sharedPrintController];
    
    
    if(pic && [UIPrintInteractionController canPrintData: image] ) {
        
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        //printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = imageData;
        
        
    }
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        //self.content = nil;
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    [pic presentFromBarButtonItem:btnPrint1 animated:YES completionHandler:completionHandler];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) initializePassInfo
{
    txtViewDesc1.text = plateInfo.desc1;
    txtViewDesc2.text = plateInfo.desc2;
    txtViewDesc3.text = plateInfo.desc3;
    txtViewDesc4.text = plateInfo.desc4;
    txtViewDesc5.text = plateInfo.desc5;
    txtViewDesc6.text = plateInfo.desc6;
    txtViewDesc7.text = plateInfo.desc7;
    txtViewDesc8.text = plateInfo.desc8;
    txtViewDesc9.text = plateInfo.desc9;
    txtViewDesc10.text = plateInfo.desc10;
    txtViewDesc11.text = plateInfo.desc11;
    txtViewDesc12.text = plateInfo.desc12;
    txtViewDesc13.text = plateInfo.desc13;
    txtViewDesc14.text = plateInfo.desc14;
    txtViewDesc15.text = plateInfo.desc15;
    txtViewDesc16.text = plateInfo.desc16;
    
    
    txtViewTitle1.text = plateInfo.title1;
    txtViewTitle2.text = plateInfo.title2;
    txtViewTitle3.text = plateInfo.title3;
    txtReference.text = plateInfo.reference;
    txtCompany.text = plateInfo.company;
    txtCustomerNo.text = plateInfo.customerNo;
}


-(void) deactivatedAllNecessaryAttribute
{
    //initialize textview attribute
    
    txtCreated.enabled = NO;
    txtModified.enabled = NO;
    
    txtHeader1.keyboardType = UIKeyboardTypeNumberPad;
    txtHeader2.keyboardType = UIKeyboardTypeNumberPad;
    
    [txtHeader3 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtHeader3 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtHeader5 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtHeader5 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtHeader6 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtHeader6 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc1 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc1 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc2 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc2 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc3 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc3 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc4 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc4 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc5 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc5 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc6 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc6 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc7 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc7 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc8 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc8 setSpellCheckingType:UITextSpellCheckingTypeNo];

    [txtViewDesc9 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc9 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc10 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc10 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc11 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc11 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc12 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc12 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc13 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc13 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc14 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc14 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewTitle1 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewTitle1 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewTitle2 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewTitle2 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewTitle3 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewTitle3 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtReference setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtReference setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtCustomerNo setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtCustomerNo setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtCompany setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtCompany setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc15 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc15 setSpellCheckingType:UITextSpellCheckingTypeNo];
    
    [txtViewDesc16 setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtViewDesc16 setSpellCheckingType:UITextSpellCheckingTypeNo];
}

-(void) initializePlate
{
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    NSArray *plateDetails = [da retrievePlate: PlateID];
    
    EscutheonPlate *ep = [plateDetails objectAtIndex:0];
    
    NSString *tempStr = [ep.desc1 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc1.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc2 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc2.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc3 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc3.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc4 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc4.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc5 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc5.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc6 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc6.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc7 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc7.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc8 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc8.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc9 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc9.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc10 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc10.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc11 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc11.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc12 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc12.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc13 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc13.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc14 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc14.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc15 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc15.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.desc16 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewDesc16.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    
    
    tempStr = [ep.title1 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewTitle1.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.title2 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewTitle2.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.title3 stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtViewTitle3.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    
    
    tempStr = [ep.reference stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtReference.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.customerNo stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtCustomerNo.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    tempStr = [ep.company stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    txtCompany.text = [tempStr stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    txtCreated.text = ep.created;
    txtModified.text = ep.modified;
    
    NSString *testString = ep.header;
    
    NSArray *array = [testString componentsSeparatedByString:@","];
    
    if(array.count > 0)
    {
        NSString *tempHeader = [[array objectAtIndex:2] stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
        
        txtHeader1.text = [array objectAtIndex:0];
        txtHeader2.text = [array objectAtIndex:1];
        
        txtHeader3.text = [tempHeader stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
        
        tempHeader = [[array objectAtIndex:3] stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
        txtHeader4.text = [tempHeader stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
        
        tempHeader = [[array objectAtIndex:4] stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
        txtHeader5.text = [tempHeader stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
        
        tempHeader = [ep.headerF stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
        txtHeader6.text = [tempHeader stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    }
    
}

-(void) disableInput
{
    [txtViewDesc1 setEditable:NO];
    [txtViewDesc2 setEditable:NO];
    [txtViewDesc3 setEditable:NO];
    [txtViewDesc4 setEditable:NO];
    [txtViewDesc5 setEditable:NO];
    [txtViewDesc6 setEditable:NO];
    [txtViewDesc7 setEditable:NO];
    [txtViewDesc8 setEditable:NO];
    [txtViewDesc9 setEditable:NO];
    [txtViewDesc10 setEditable:NO];
    [txtViewDesc11 setEditable:NO];
    [txtViewDesc12 setEditable:NO];
    [txtViewDesc13 setEditable:NO];
    [txtViewDesc14 setEditable:NO];
    [txtViewDesc15 setEditable:NO];
    [txtViewDesc16 setEditable:NO];
    
    [txtReference setEnabled:NO];
    [txtCreated setEnabled:NO];
    [txtCustomerNo setEnabled:NO];
    [txtCompany setEnabled:NO];
    [txtModified setEnabled:NO];
    
    [txtViewTitle1 setEditable:NO];
    [txtViewTitle2 setEditable:NO];
    [txtViewTitle3 setEditable:NO];
    
    [txtHeader1 setEnabled:NO];
    [txtHeader2 setEnabled:NO];
    [txtHeader3 setEnabled:NO];
    [txtHeader4 setEnabled:NO];
    [txtHeader5 setEnabled:NO];
    [txtHeader6 setEnabled:NO];
}

-(void) BackTapped:(id)gesture
{
    NSLog(@"back button tapped");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) DoneTapped:(id)gesture
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self performSelector:@selector(doneTappedDelay:) withObject:0 afterDelay:1];
}

-(void) doneTappedDelay:(int)x
{
    if([option isEqualToString:@"edit"])
    {
        [self insertIntoDB];
        
        [self backToListOfPlate];
    }
    else if([option isEqualToString:@"email"])
    {
        [self sendPrintScreen];
    }
    else
    {
        //validation here... need to ensure only when user enter all the information will the data can be saved
        
        //if(txtViewDesc1.text.length == 0 || txtViewDesc2.text.length == 0 || txtViewDesc3.text.length == 0 || txtViewDesc4.text.length == 0 ||txtViewDesc5.text.length == 0 || txtViewDesc6.text.length == 0 || txtViewDesc7.text.length == 0 || txtViewDesc8.text.length == 0 || txtViewDesc9.text.length == 0 || txtViewDesc10.text.length == 0 || txtViewDesc11.text.length == 0 || txtViewDesc12.text.length == 0 || txtViewDesc13.text.length == 0 || txtViewDesc14.text.length == 0 || txtViewTitle1.text.length == 0 || txtViewTitle2.text.length == 0 || txtViewTitle3.text.length == 0 || txtReference.text.length == 0 || txtCustomerNo.text.length == 0 || txtCreated.text.length == 0 || txtCompany.text.length == 0 || txtModified.text.length == 0)
        
//        if(txtHeader1.text.length == 0 || txtHeader2.text.length == 0 || txtHeader3.text.length == 0 || txtHeader5.text.length == 0 || txtHeader6.text.length == 0)
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter header info" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            
//            [alert show]; 
//            
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        }
//        else
//        {
            [self insertIntoDB];
            
//            NSLog(@"okay.jpg");
//        }
    }
}

-(void) backToListOfPlate
{
    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void) backToMainPage
{
    if([option isEqualToString:@"passPlate"])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    }
}

-(void)sendPrintScreen
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
	
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	
    UIGraphicsEndImageContext();
    
    if (viewImage != nil)
    {
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
        NSData *imageData = UIImagePNGRepresentation(viewImage);
        
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:path atomically:YES];
        
        if ([MFMailComposeViewController canSendMail]) 
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            NSString *mailsubject=[[NSString alloc]initWithFormat:@""];
            [mailer setSubject:mailsubject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer addAttachmentData:imageData mimeType:@"image/png"   fileName:@"EscutcheonImage.png"];
            
            NSString *emailBody = [[NSString alloc]initWithFormat:@""];
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [self presentModalViewController:mailer animated:YES];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                    message:@"Your device doesn't support the composer sheet"
                        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void) insertIntoDB
{    
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    //need to validate whether the label after F* is unique
    
    if(![option isEqualToString:@"edit"])
    {
        NSArray *arr = [da checkIfPlateWithEngravingNumberExist:txtHeader6.text];
        
        if(arr.count > 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"The Engraving number already existed, please use another number" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
            return;
        }
    }
    
    NSDate* currentDate = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSString *dateString = [dateFormat stringFromDate:currentDate];
    
    EscutheonPlate *ep = [[EscutheonPlate alloc] init];
    
    NSString *tempStr = [txtViewDesc1.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc1 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc2.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc2 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc3.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc3 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc4.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc4 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc5.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc5 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc6.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc6 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc7.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc7 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc8.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc8 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc9.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc9 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc10.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc10 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc11.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc11 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc12.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc12 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc13.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc13 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc14.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc14 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc15.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc15 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewDesc16.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.desc16 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    
    
    tempStr = [txtViewTitle1.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.title1 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewTitle2.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.title2 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtViewTitle3.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.title3 = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    
    
    tempStr = [txtReference.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.reference = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtCustomerNo.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.customerNo = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    tempStr = [txtCompany.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.company = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    

    int numOfStage=0;

    for ( int loopstage=0; loopstage<1; loopstage++) {
        if (txtViewTitle2.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewTitle3.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc1.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc2.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc3.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc4.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc5.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc6.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc7.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc8.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc9.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc10.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc11.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc12.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc13.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc14.text.length!=0) {
            numOfStage++;
        }
        
        if (txtViewDesc15.text.length!=0) {
            numOfStage++;
        }
    
        if (txtViewDesc16.text.length!=0) {
            numOfStage++;
        }
    
    }
    NSLog(@"numOfStage>>>>>%d",numOfStage);
    
    NSString *strnumOfStage=[[NSString alloc]initWithFormat:@"%d",numOfStage];
    ep.NoOfPosition=strnumOfStage;
    
    ep.created = txtCreated.text;
    ep.modified = dateString;
    
    
    NSString *tempHeader3 = [txtHeader3.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    NSString *tempHeader4 = [txtHeader4.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    NSString *tempHeader5 = [txtHeader5.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    
    ep.header = [NSString stringWithFormat:@"%@,%@,%@,%@,%@", txtHeader1.text, txtHeader2.text, [tempHeader3 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"], [tempHeader4 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"], [tempHeader5 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"]];
    
    ep.name = [NSString stringWithFormat:@"S%@  F99%@/%@/%@  -%@", txtHeader1.text, txtHeader2.text, [tempHeader3 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"], [tempHeader4 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"], [tempHeader5 stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"]];
    
    
    tempStr = [txtHeader6.text stringByReplacingOccurrencesOfString:@"'" withString:@"tiseno_Squote"];
    ep.headerF = [tempStr stringByReplacingOccurrencesOfString:@"&" withString:@"tiseno_AndSign"];
    
    
    if([option isEqualToString:@"edit"])
    {
        ep.PlateID = PlateID;
        
        [da updatePlateInformation:ep];
    }
    else
    {        
        [da insertPlate:ep];
    }
    
    
    [self backToMainPage];
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtHeader1 || textField == txtHeader2 || textField == txtHeader3 || textField == txtHeader4 || textField == txtHeader5 || textField == txtHeader6)
    {
        return;
    }
    
    heightOfEditedView = textField.frame.size.height;
    heightOffset = textField.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}

-(void) textViewDidBeginEditing:(UITextView *) textView
{
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}

// set the maximum number of character can be typed
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtHeader1)
    {
        /*  limit to only numeric characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        /* switch to next header field */
        if(newLength > 2)
        {
            [txtHeader1 resignFirstResponder];
            [txtHeader2 becomeFirstResponder];
        }
        
        return (newLength > 2) ? NO : YES;
    }
    
    if(textField == txtHeader2)
    {
        /*  limit to only 1 or 0 characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"01"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        /* switch to next header field */
        if(newLength > 1)
        {
            [txtHeader2 resignFirstResponder];
            [txtHeader3 becomeFirstResponder];
        }
        
        return (newLength > 1) ? NO : YES;
    }
    
    if(textField == txtHeader3)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        /* switch to next header field */
        if(newLength > 3)
        {
            [txtHeader3 resignFirstResponder];
            [txtHeader4 becomeFirstResponder];
        }
        
        return (newLength > 3) ? NO : YES;
    }
    
//    if(textField == txtHeader4)
//    {
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        
//        /* switch to next header field */
//        if(newLength > 1)
//        {
//            [txtHeader4 resignFirstResponder];
//            [txtHeader5 becomeFirstResponder];
//        }
//        
//        return (newLength > 1) ? NO : YES;
//    }
    
    if(textField == txtHeader5)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        /* switch to next header field */
        if(newLength > 5)
        {
            [txtHeader5 resignFirstResponder];
            [txtHeader6 becomeFirstResponder];
        }
        
        return (newLength > 5) ? NO : YES;
    }
    
    return YES;
}

@end
