//
//  Jumper2DTopView.h
//  KNE
//
//  Created by tiseno on 11/28/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface Jumper2DTopView : UIView
{
    UIImageView *circle;
    
    UIView *combinedview;
    
    UILabel *numberlabel;
    
    int i1, firsttag, secondtag;
    
    float firstX, firstY, secondX, secondY, thirdX, thirdY, forthX, forthY, fifthX, fifthY, sixthX, sixthY, gapx, gapy, GradientX, GradientY, HorizontalX, VerticalY;
    
    NSTimer *timer;
}

@property (nonatomic, strong)NSMutableArray *initialtag, *finaltag;

@end
