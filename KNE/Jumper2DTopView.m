//
//  Jumper2DTopView.m
//  KNE
//
//  Created by tiseno on 11/28/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import "Jumper2DTopView.h"

@implementation Jumper2DTopView

@synthesize initialtag, finaltag;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderWidth = 1;
        
        UILabel *titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 21)];
        titlelabel.textAlignment = UITextAlignmentCenter;
        titlelabel.text = @"Jumpers";
        titlelabel.backgroundColor = [UIColor clearColor];
        titlelabel.font = [UIFont systemFontOfSize:14];
        titlelabel.numberOfLines = 1;
        titlelabel.layer.borderWidth = 1;
        [self addSubview:titlelabel];
        
        int n = 48;
        int i=1, rows = 12, columns = 4;
        i1=0;
        
        gapx = (self.frame.size.width - columns * 20) / (columns + 1);
        gapy = (self.frame.size.height - 21 - rows * 10) / (rows + 1);
        
        while(i<n)
        {
            int yy = gapy + i1 * (10 + gapy) + 21;
            int j=0;
            for(j=0; j<columns;j++){
                if (i>=n+1) break;
                
                if(j+1 % 4 == 1)
                {
                    combinedview = [[UIView alloc]initWithFrame:CGRectMake(gapx + 0 * (gapx + 20), yy, 20, 10)];
                }
                else if(j+1 % 4 == 2)
                {
                    combinedview = [[UIView alloc]initWithFrame:CGRectMake(gapx + 3 * (gapx + 20), yy, 20, 10)];
                }
                else if(j+1 % 4 == 3)
                {
                    combinedview = [[UIView alloc]initWithFrame:CGRectMake(gapx + 1 * (gapx + 20), yy, 20, 10)];
                }
                else
                {
                    combinedview = [[UIView alloc]initWithFrame:CGRectMake(gapx + 2 * (gapx + 20), yy, 20, 10)];
                }
                combinedview.backgroundColor = [UIColor clearColor];
                combinedview.tag = i;
                
                if((j+1) % 4 == 1 || (j+1) % 4 == 0)
                {
                    circle = [[UIImageView alloc]initWithFrame:CGRectMake(10,0,10,10)];
                    numberlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
                }
                else
                {
                    circle = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,10,10)];
                    numberlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 10, 10)];
                }
                
                circle.image = [UIImage imageNamed:@"circle_lite.png"];
                [combinedview addSubview:circle];
                
                numberlabel.textAlignment = UITextAlignmentCenter;
                numberlabel.text = [NSString stringWithFormat:@"%d", i];
                numberlabel.backgroundColor = [UIColor clearColor];
                numberlabel.font = [UIFont systemFontOfSize:8];
                [combinedview addSubview:numberlabel];
                
                [self addSubview:combinedview];
                i++;
            }
            i1 = i1+1;
        }
    }
    
    GradientX = 3;
    GradientY = gapy / 2;
    
    HorizontalX = gapx / 2;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(refresh) userInfo:nil repeats:YES];
    return self;
}


- (void)drawRect:(CGRect)rect
{
    if(self.initialtag.count != 0 && self.finaltag.count != 0)
    {
        CGContextRef c = UIGraphicsGetCurrentContext();
        
        CGFloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
        CGContextSetStrokeColor(c, black);
        CGContextBeginPath(c);
        
        for(int i = 0; i < self.initialtag.count;i++)
        {
            firsttag = [[self.initialtag objectAtIndex:i] integerValue];
            secondtag = [[self.finaltag objectAtIndex:i] integerValue];
            
            if(secondtag < firsttag)
            {
                int temp = firsttag;
                firsttag = secondtag;
                secondtag = temp;
            }
            
            int secondtagrownumber = secondtag / 4;
            if((secondtag % 4) > 0)
                secondtagrownumber++;
            UIView *final = [self viewWithTag:secondtag];
            
            int firsttagrownumber = firsttag / 4;
            if((firsttag % 4) > 0)
                firsttagrownumber++;
            UIView *initial = [self viewWithTag:firsttag];
            
            if(secondtagrownumber - firsttagrownumber == 1)
            {
                if(firsttag % 4 == secondtag % 4)
                {
                    if(firsttag % 4 == 1 || firsttag % 4 == 0)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width - 5;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = final.frame.origin.x + final.frame.size.width - 5;
                        secondY = final.frame.origin.y;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x + 5;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = final.frame.origin.x + 5;
                        secondY = final.frame.origin.y;
                    }
                }
                else
                {
                    if(firsttag % 4 == 1)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 2 || secondtag % 4 == 3)
                        {
                            forthX = final.frame.origin.x;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX - GradientX;
                            thirdY = forthY - GradientY;
                        }
                        else
                        {
                            forthX = final.frame.origin.x + final.frame.size.width / 2;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX - GradientX;
                            thirdY = forthY - GradientY;
                        }
                    }
                    else if(firsttag % 4 == 3) 
                    {
                        if(secondtag % 4 == 2 || secondtag % 4 == 0)
                        {
                            firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX + GradientX;
                            secondY = firstY + GradientY;
                            
                            if(secondtag % 4 == 2)
                            {
                                forthX = final.frame.origin.x;
                                forthY = final.frame.origin.y;
                                
                                thirdX = forthX - GradientX;
                                thirdY = forthY - GradientY;
                            }
                            else
                            {
                                forthX = final.frame.origin.x + initial.frame.size.width / 2;
                                forthY = final.frame.origin.y;
                                
                                thirdX = forthX - GradientX;
                                thirdY = forthY - GradientY;
                            }
                        }
                        else
                        {
                            firstX = initial.frame.origin.x;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX - GradientX;
                            secondY = firstY + GradientY;
                            
                            forthX = final.frame.origin.x + final.frame.size.width;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX + GradientX;
                            thirdY = forthY - GradientY;
                        }
                    }
                    else if(firsttag % 4 == 0)
                    {
                        if(secondtag % 4 == 1 || secondtag % 4 == 3)
                        {
                            firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX - GradientX;
                            secondY = firstY + GradientY;
                            
                            if(secondtag % 4 == 3)
                            {
                                forthX = final.frame.origin.x + initial.frame.size.width / 2;
                                forthY = final.frame.origin.y;
                                
                                thirdX = forthX + GradientX;
                                thirdY = forthY - GradientY;
                            }
                            else
                            {
                                forthX = final.frame.origin.x + final.frame.size.width;
                                forthY = final.frame.origin.y;
                                
                                thirdX = forthX + GradientX;
                                thirdY = forthY - GradientY;
                            }
                        }
                        else
                        {
                            firstX = initial.frame.origin.x + initial.frame.size.width;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX + GradientX;
                            secondY = firstY + GradientY;
                            
                            forthX = final.frame.origin.x;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX - GradientX;
                            thirdY = forthY - GradientY;
                        }
                    }
                    else
                    {
                        firstX = initial.frame.origin.x;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 1 || secondtag % 4 == 0)
                        {
                            forthX = final.frame.origin.x + final.frame.size.width;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX + GradientX;
                            thirdY = forthY - GradientY;
                        }
                        else
                        {
                            forthX = final.frame.origin.x + final.frame.size.width / 2;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX + GradientX;
                            thirdY = forthY - GradientY;
                        }
                    }
                }
            }
            else if(secondtagrownumber == firsttagrownumber)
            {
                if(firsttag % 4 == 1)
                {
                    if(secondtag % 4 == 3)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width;
                        firstY = initial.frame.origin.y + initial.frame.size.height - 5;
                        
                        secondX = final.frame.origin.x;
                        secondY = final.frame.origin.y + initial.frame.size.height - 5;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        forthX = final.frame.origin.x + final.frame.size.width / 2;
                        forthY = final.frame.origin.y + final.frame.size.height;
                        
                        thirdX = forthX - GradientX;
                        thirdY = forthY + GradientY;
                    }
                }
                else
                {
                    if(secondtag % 4 == 0)
                    {
                        firstX = initial.frame.origin.x;
                        firstY = initial.frame.origin.y + initial.frame.size.height - 5;
                        
                        secondX = final.frame.origin.x + initial.frame.size.width;
                        secondY = final.frame.origin.y + initial.frame.size.height - 5;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        forthX = final.frame.origin.x + initial.frame.size.width / 2;
                        forthY = final.frame.origin.y + final.frame.size.height;
                        
                        thirdX = forthX + GradientX;
                        thirdY = forthY + GradientY;
                    }
                }
            }
            else
            {
                if(firsttag % 4 == 1)
                {
                    if(secondtag % 4 == 1)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        sixthX = final.frame.origin.x + final.frame.size.width / 2;
                        sixthY = final.frame.origin.y;
                        
                        fifthX = sixthX - GradientX;
                        fifthY = sixthY - GradientY;
                        
                        thirdX = final.frame.origin.x - HorizontalX;
                        thirdY = secondY;
                        
                        forthX = thirdX;
                        forthY = fifthY;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 3)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX + GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else if(secondtag % 4 == 0)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else
                        {
                            sixthX = final.frame.origin.x;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - final.frame.size.width - HorizontalX * 3;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                    }
                }
                else if(firsttag % 4 == 3)
                {
                    if(secondtag % 4 == 1)
                    {
                        firstX = initial.frame.origin.x;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        sixthX = final.frame.origin.x + final.frame.size.width / 2;
                        sixthY = final.frame.origin.y;
                        
                        fifthX = sixthX - GradientX;
                        fifthY = sixthY - GradientY;
                        
                        thirdX = final.frame.origin.x - HorizontalX;
                        thirdY = secondY;
                        
                        forthX = thirdX;
                        forthY = fifthY;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 3)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX + GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else if(secondtag % 4 == 0)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else
                        {
                            sixthX = final.frame.origin.x;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - final.frame.size.width - HorizontalX * 3;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                    }
                }
                else if(firsttag % 4 == 0)
                {
                    if(secondtag % 4 == 1)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        sixthX = final.frame.origin.x + final.frame.size.width / 2;
                        sixthY = final.frame.origin.y;
                        
                        fifthX = sixthX - GradientX;
                        fifthY = sixthY - GradientY;
                        
                        thirdX = final.frame.origin.x - HorizontalX;
                        thirdY = secondY;
                        
                        forthX = thirdX;
                        forthY = fifthY;
                    }
                    else if(secondtag % 4 == 2)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        sixthX = final.frame.origin.x + final.frame.size.width / 2;
                        sixthY = final.frame.origin.y;
                        
                        fifthX = sixthX + GradientX;
                        fifthY = sixthY - GradientY;
                        
                        thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                        thirdY = secondY;
                        
                        forthX = thirdX;
                        forthY = fifthY;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 3)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX + GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                    }
                }
                else
                {
                    if(secondtag % 4 == 2)
                    {
                        firstX = initial.frame.origin.x + initial.frame.size.width / 2;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX + GradientX;
                        secondY = firstY + GradientY;
                        
                        sixthX = final.frame.origin.x + final.frame.size.width / 2;
                        sixthY = final.frame.origin.y;
                        
                        fifthX = sixthX - GradientX;
                        fifthY = sixthY - GradientY;
                        
                        thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                        thirdY = secondY;
                        
                        forthX = thirdX;
                        forthY = fifthY;
                    }
                    else
                    {
                        firstX = initial.frame.origin.x;
                        firstY = initial.frame.origin.y + initial.frame.size.height;
                        
                        secondX = firstX - GradientX;
                        secondY = firstY + GradientY;
                        
                        if(secondtag % 4 == 3)
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX + GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x + final.frame.size.width + HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                        else
                        {
                            sixthX = final.frame.origin.x + final.frame.size.width / 2;
                            sixthY = final.frame.origin.y;
                            
                            fifthX = sixthX - GradientX;
                            fifthY = sixthY - GradientY;
                            
                            thirdX = final.frame.origin.x - HorizontalX;
                            thirdY = secondY;
                            
                            forthX = thirdX;
                            forthY = fifthY;
                        }
                    }
                }
            }
            
            CGContextMoveToPoint(c, firstX, firstY);
            CGContextAddLineToPoint(c, secondX, secondY);
            if(thirdX != 0)
            {
                if(forthX != 0)
                {
                    if(fifthX != 0)
                    {
                        if(sixthX != 0)
                        {
                            CGContextAddLineToPoint(c, thirdX, thirdY);
                            CGContextAddLineToPoint(c, forthX, forthY);
                            CGContextAddLineToPoint(c, fifthX, fifthY);
                            CGContextAddLineToPoint(c, sixthX, sixthY);
                        }
                        else
                        {
                            CGContextAddLineToPoint(c, thirdX, thirdY);
                            CGContextAddLineToPoint(c, forthX, forthY);
                            CGContextAddLineToPoint(c, fifthX, fifthY);
                        }
                    }
                    else
                    {
                        CGContextAddLineToPoint(c, thirdX, thirdY);
                        CGContextAddLineToPoint(c, forthX, forthY);
                    }
                }
                else
                {
                    CGContextAddLineToPoint(c, thirdX, thirdY);
                }
            }
            
            firstX = 0;
            firstY = 0;
            secondX = 0;
            secondY = 0;
            thirdX = 0;
            thirdY = 0;
            forthX = 0;
            forthY = 0;
            fifthX = 0;
            fifthY = 0;
            sixthX = 0;
            sixthY = 0;
        }
        
        CGContextStrokePath(c);
    }
}

-(void)refresh
{
    [self setNeedsDisplay];
}

@end
