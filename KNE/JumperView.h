//
//  Line.h
//  KNE
//
//  Created by tiseno on 11/22/12.
//  Copyright (c) 2012 com.tiseno. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DatabaseAction.h"
#import "Jumper.h"
#import "Jumper2DTopView.h"
#import "KNEAppDelegate.h"

@interface JumperView : UIView<UITextFieldDelegate>
{
    BOOL issecondclicked, isinvert, isrepeat, iscorrect, iswrong;
    
    int firsttag, secondtag, gradientX, gradientY, turningpoint;
    
    UIView *jumperdisplayview;
    
    UIImageView *circleimageview, *sideimageview;
    
    UIButton *jumperbutton;
    
    UITextField *jumpertextfield;
    
    float firstX, firstY, secondX, secondY, thirdX, thirdY, forthX, forthY, fifthX, fifthY, sixthX, sixthY;
    
    NSMutableArray *normalXY, *invertXY, *initialtag, *finaltag, *invertline, *maxline, *highlevel, *mediumlevel, *topline, *bottomline, *sideline;
    
    NSArray *jumperarray;
    
    DatabaseAction *db;
    
    UIAlertView *alert;
    
    Jumper2DTopView *jumper2dtopview;
}

@property (nonatomic) int caseID, x, y;
@property (nonatomic, strong) NSMutableArray *initialtag;
@property (nonatomic, strong) NSMutableArray *invertline;
-(void)storejumper;
-(void)retrievejumper;
-(void)deleteJumperTapped;
@end
