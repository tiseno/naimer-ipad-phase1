//
//  KNEAppDelegate.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNESpringRPosition.h"
#import "KNETblGraftPositionTag.h"
#import "KNESpringReturnContain.h"
#import "KNECase.h"
@class KNEViewController;

@interface KNEAppDelegate : UIResponder <UIApplicationDelegate>{
    
}

@property (strong, nonatomic) KNECase *gKNECase;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KNEViewController *viewController;
@property (nonatomic, strong) NSString *firstPnum;
@property (nonatomic, strong) NSString *startingpoint; 
@property (nonatomic, strong) NSMutableArray *springReturnPosition;
@property (nonatomic, strong) NSMutableArray *MutblSpringReturnContain;
@property (nonatomic, strong) NSMutableArray *tblGraftPositiontag;

@property (nonatomic, strong) KNESpringRPosition *gKNESpringRPosition;

-(void)addTblGraftPositiontag:(KNETblGraftPositionTag *)objectsTblGraftPositionTag;
-(void)deletetblContactRowIndex:(NSUInteger)index;
-(void)deleteSpringReturnRowIndex:(NSUInteger)index;
-(void)gaddSpringReturnpoint:(KNESpringRPosition *)gKNESpringRobjects;
-(void)clearSpringReturn;
-(void)addSpringReturnConrain:(KNESpringReturnContain *)SRContain;
-(void)clearSpringReturnContain;
-(void)clearKNECase;
-(void)clearfirstPnum;
@end
