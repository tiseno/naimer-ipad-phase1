//
//  KNEAppDelegate.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNEAppDelegate.h"

#import "KNEViewController.h"

@implementation KNEAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize firstPnum;
@synthesize startingpoint;
@synthesize springReturnPosition, gKNESpringRPosition;
@synthesize tblGraftPositiontag;
@synthesize MutblSpringReturnContain;
@synthesize gKNECase;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[KNEViewController alloc] initWithNibName:@"KNEViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
/*===================================contact table========================================

-(void)addTblGraftPositiontag:(KNETblGraftPositionTag *)objectsTblGraftPositionTag
{
    
    KNETblGraftPositionTag *ggKNETblGraftPositionTag=[[KNETblGraftPositionTag alloc]init];
    if(tblGraftPositiontag==nil)
    {
        tblGraftPositiontag= [[NSMutableArray alloc]init];
        
    }
    
    ggKNETblGraftPositionTag.imgGbtnTag=objectsTblGraftPositionTag.imgGbtnTag;
    ggKNETblGraftPositionTag.imgGbtnValueTag=objectsTblGraftPositionTag.imgGbtnValueTag;
    ggKNETblGraftPositionTag.CaseID=objectsTblGraftPositionTag.CaseID;
    
    [tblGraftPositiontag addObject:ggKNETblGraftPositionTag];

    
    //[tblGraftPositiontag removeObserver:<#(NSObject *)#> forKeyPath:<#(NSString *)#>];
    
}======*/

-(void)deletetblContactRowIndex:(NSUInteger)index
{
    [tblGraftPositiontag removeObjectAtIndex:index];
}

/*===================================Spring Return=========================================

-(void)deleteSpringReturnRowIndex:(NSUInteger)index
{
    [springReturnPosition removeObjectAtIndex:index];
    
}

-(void)gaddSpringReturnpoint:(KNESpringRPosition *)gKNESpringRobjects
{
    KNESpringRPosition *ggKNESpringRPosition=[[KNESpringRPosition alloc]init];
    
    if(springReturnPosition==nil)
    {
        springReturnPosition= [[NSMutableArray alloc]init];
         
    }
    
    ggKNESpringRPosition.SRStartPoint=gKNESpringRobjects.SRStartPoint;
    ggKNESpringRPosition.SREndPoint=gKNESpringRobjects.SREndPoint;

    
    [springReturnPosition addObject:ggKNESpringRPosition];
    //NSLog(@"springReturnPosition--->%@",springReturnPosition);
}

-(void)clearSpringReturn
{
 
    if (springReturnPosition !=nil) {
        springReturnPosition=nil;
    }
    
    [springReturnPosition release];
}
=====*/

-(void)addSpringReturnConrain:(KNESpringReturnContain *)SRContain
{
    KNESpringReturnContain *gKNESpringReturnContain =[[KNESpringReturnContain alloc] init];
    
    if(MutblSpringReturnContain==nil)
    {
        MutblSpringReturnContain= [[NSMutableArray alloc]init];
        
    }
    
    gKNESpringReturnContain.ContainTag=SRContain.ContainTag;
    gKNESpringReturnContain.ContainValue=SRContain.ContainValue;
    
    
    [MutblSpringReturnContain addObject:gKNESpringReturnContain];
}

-(void)clearSpringReturnContain
{
    
    if (MutblSpringReturnContain !=nil) {
        MutblSpringReturnContain=nil;
    }
    
}

-(void)clearKNECase
{
    
    if (gKNECase !=nil) {
        gKNECase=nil;
    }
    
}

-(void)clearfirstPnum
{
    
    if (firstPnum !=nil) {
        firstPnum=nil;
    }
    
}


@end






