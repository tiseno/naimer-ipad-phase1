//
//  KNECase.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import <Foundation/Foundation.h>

@interface KNECase : NSObject{
    
}

@property (nonatomic,strong) NSString *CaseID;
@property (nonatomic, strong) NSString *CaseName;

@property (nonatomic, strong) NSString *FLook;
@property (nonatomic, strong) NSString *FMounting;
@property (nonatomic, strong) NSString *FEscutcheon_plate;
@property (nonatomic, strong) NSString *FHandle;
@property (nonatomic, strong) NSString *FLatch_mech;
@property (nonatomic, strong) NSString *FStop;
@property (nonatomic, strong) NSString *FStop_Degree;
@property (nonatomic, strong) NSString *FNofStage;
@property (nonatomic, strong) NSString *FMasterfData;
@property (nonatomic, strong) NSString *FReference;
@property (nonatomic, strong) NSString *FDate;
@property (nonatomic, strong) NSString *FModify_Date;
@property (nonatomic, strong) NSString *FCustNo;
@property (nonatomic, strong) NSString *FCompany;
@property (nonatomic, strong) NSString *FVersion;

@property (nonatomic, strong) NSString *pcs1;
@property (nonatomic, strong) NSString *pcs2;
@property (nonatomic, strong) NSString *pcs3;
@property (nonatomic, strong) NSString *pcs4;
@property (nonatomic, strong) NSString *pcs5;
@property (nonatomic, strong) NSString *pcs6;
@property (nonatomic, strong) NSString *pcs7;
@property (nonatomic, strong) NSString *pcs8;
@property (nonatomic, strong) NSString *pcs9;

@property (nonatomic, strong) NSString *optionalextra1;
@property (nonatomic, strong) NSString *optionalextra2;
@property (nonatomic, strong) NSString *optionalextra3;
@property (nonatomic, strong) NSString *optionalextra4;
@property (nonatomic, strong) NSString *optionalextra5;
@property (nonatomic, strong) NSString *optionalextra6;
@property (nonatomic, strong) NSString *optionalextra7;
@property (nonatomic, strong) NSString *optionalextra8;
@property (nonatomic, strong) NSString *optionalextra9;

@property (nonatomic, strong) NSString *extra_comment;

@property (nonatomic, strong) NSString *SwitchPoint;

@property (nonatomic, strong) NSString *CustomerArticleNumber, *SwitchType, *Program, *PlateName;

@property (nonatomic, strong) NSString *TotalSwitchingAngle, *SwitchingAngle, *numberOfPosition;

@end
