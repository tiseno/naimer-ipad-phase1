//
//  KNECase.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import "KNECase.h"

@implementation KNECase
@synthesize CaseID;
@synthesize CaseName;
@synthesize FLook, FMounting, FCompany, FCustNo, FDate, FEscutcheon_plate, FHandle, FLatch_mech, FMasterfData, FModify_Date, FNofStage, FReference, FStop ,   FStop_Degree, FVersion;

@synthesize pcs1, pcs2, pcs3, pcs4, pcs5, pcs6, pcs7, pcs8, pcs9;
@synthesize optionalextra1, optionalextra2, optionalextra3, optionalextra4, optionalextra5, optionalextra6, optionalextra7, optionalextra8, optionalextra9;
@synthesize extra_comment, SwitchPoint, CustomerArticleNumber, SwitchType, Program, PlateName;
@synthesize TotalSwitchingAngle, SwitchingAngle, numberOfPosition;


@end
