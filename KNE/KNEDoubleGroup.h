//
//  KNEDoubleGroup.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KNEDoubleGroup : NSObject{
    
}

@property (nonatomic, strong) NSString *col2;
@property (nonatomic, strong) NSString *col4;
@property (nonatomic, strong) NSString *col6;
@property (nonatomic, strong) NSString *col8;
@property (nonatomic, strong) NSString *col10;

@property (nonatomic, strong) NSString *col12;
@property (nonatomic, strong) NSString *col14;
@property (nonatomic, strong) NSString *col16;
@property (nonatomic, strong) NSString *col18;
@property (nonatomic, strong) NSString *col20;

@property (nonatomic, strong) NSString *col22;
@property (nonatomic, strong) NSString *col24;

@property (nonatomic, strong) NSString *row2;
@property (nonatomic, strong) NSString *row4;
@property (nonatomic, strong) NSString *row6;
@property (nonatomic, strong) NSString *row8;
@property (nonatomic, strong) NSString *row10;

@property (nonatomic, strong) NSString *row12;
@property (nonatomic, strong) NSString *row14;
@property (nonatomic, strong) NSString *row16;
@property (nonatomic, strong) NSString *row18;
@property (nonatomic, strong) NSString *row20;

@property (nonatomic, strong) NSString *row22;
@property (nonatomic, strong) NSString *row24;

@end
