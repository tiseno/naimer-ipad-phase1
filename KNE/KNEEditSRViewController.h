//
//  KNEEditSRViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/5/12.
//
//

#import <UIKit/UIKit.h>
#import "KNESpringReturnCell.h"
#import "KNEAppDelegate.h"

//@class KNEEditSwitchCaseViewController;

@interface KNEEditSRViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>{
    KNESpringReturnCell *cell;
    int txtindexpath;
}

@property (nonatomic, strong) NSArray *numarr;
//@property (nonatomic, strong) KNEEditSwitchCaseViewController *gKNEEditSwitchCaseViewController;
-(void)getfirstpNum;

@end
