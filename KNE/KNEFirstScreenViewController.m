//
//  KNEFirstScreenViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNEFirstScreenViewController.h"

@interface KNEFirstScreenViewController ()

@end

@implementation KNEFirstScreenViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) 
    {

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    KNEFirstScreenViewController *gBpNoPersonViewController=[[KNEFirstScreenViewController alloc] initWithNibName:@"KNEFirstScreenViewController" bundle:nil];

    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];    
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(singletapcaptured:) forControlEvents:UIControlEventTouchUpInside];   
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton]; 
    
    gBpNoPersonViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    

    
}

- (void)viewDidUnload
{

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
    return NO;
}


@end






