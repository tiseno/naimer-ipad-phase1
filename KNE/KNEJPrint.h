//
//  KNEJPrint.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/12/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface KNEJPrint : UIView
{
    UIImageView *circle;
    
    UIView *combinedview;
    
    UILabel *numberlabel;
    
    int i1, firsttag, secondtag;
    
    float firstX, firstY, secondX, secondY, thirdX, thirdY, forthX, forthY, fifthX, fifthY, sixthX, sixthY, gapx, gapy, GradientX, GradientY, HorizontalX, VerticalY;
    
    NSTimer *timer;
}

@property (nonatomic, strong)NSMutableArray *initialtag, *finaltag;

@end
