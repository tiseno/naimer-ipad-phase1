//
//  KNEJShow.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/8/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DatabaseAction.h"
#import "Jumper.h"
#import "KNEJPrint.h"
#import "KNEAppDelegate.h"

@interface KNEJShow : UIView{
    BOOL issecondclicked, isinvert, isrepeat, iscorrect, iswrong;
    
    int firsttag, secondtag, gradientX, gradientY, turningpoint;
    
    UIView *jumperdisplayview;
    
    UIImageView *circleimageview, *sideimageview;
    
    UIButton *jumperbutton;
    
    UILabel *jumpertextfield;
    
    float firstX, firstY, secondX, secondY, thirdX, thirdY, forthX, forthY, fifthX, fifthY, sixthX, sixthY;
    
    NSMutableArray *normalXY, *invertXY, *initialtag, *finaltag, *invertline, *maxline, *highlevel, *mediumlevel, *topline, *bottomline, *sideline;
    
    NSArray *jumperarray;
    
    DatabaseAction *db;
    
    UIAlertView *alert;
    
    KNEJPrint *jumper2dtopview;
}

@property (nonatomic) int caseID, x, y;
@property (nonatomic, strong) NSMutableArray *initialtag;
@property (nonatomic, strong) NSMutableArray *invertline;
//-(void)storejumper;
-(void)retrievejumper;

@end
