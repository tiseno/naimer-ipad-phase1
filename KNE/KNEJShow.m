//
//  KNEJShow.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/8/12.
//
//


#define Circle_D 55
#define ButtonWidth 35
#define ButtonHeight 21
#define SidelinkerWidth 10
#define SidelinkerHeight 60

#define LowGradientX 2
#define LowGradientY 5

#define MediumGradientX 4
#define MediumGradientY 8 

#define MediumGradient_secondX 6
#define MediumGradient_secondY 12

#define HighGradientX 8
#define HighGradientY 15

#define StraightGradientY 18

//#define ExtremeGradientX 2
//#define ExtremeGradientY 20

#define GapX 0

#import "KNEJShow.h"

@implementation KNEJShow
@synthesize initialtag,invertline;
@synthesize caseID, x, y;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        normalXY = [[NSMutableArray alloc]init];
        invertXY = [[NSMutableArray alloc]init];
        initialtag = [[NSMutableArray alloc]init];
        finaltag = [[NSMutableArray alloc]init];
        invertline = [[NSMutableArray alloc]init];
        maxline = [[NSMutableArray alloc]init];
        topline = [[NSMutableArray alloc]init];
        bottomline = [[NSMutableArray alloc]init];
        highlevel = [[NSMutableArray alloc]init];
        mediumlevel = [[NSMutableArray alloc]init];
        sideline = [[NSMutableArray alloc] init];
        
        self.backgroundColor = [UIColor whiteColor];
        
        for(int i = 1; i <= 24; i++)
        {
            for(int j = 1 ; j<=2;j++)
            {
                if(j % 2 != 0)
                {
                    jumperbutton = [[UIButton alloc]initWithFrame:CGRectMake((i-1) * ButtonWidth, self.frame.origin.y + 21, ButtonWidth, ButtonHeight)];
                    jumperbutton.tag = i * 2 - 1;
                    [jumperbutton setTitle:[NSString stringWithFormat:@"%d", i * 2 - 1] forState:UIControlStateNormal];
                    
                    jumpertextfield = [[UILabel alloc]initWithFrame:CGRectMake((i-1) * ButtonWidth, self.frame.origin.y, ButtonWidth, ButtonHeight)];
                    jumpertextfield.tag = i * 2 - 1 + 300;
                }
                else
                {
                    jumperbutton = [[UIButton alloc]initWithFrame:CGRectMake((i-1) * ButtonWidth, self.frame.size.height - ButtonHeight - 21, ButtonWidth, ButtonHeight)];
                    jumperbutton.tag = i * j;
                    [jumperbutton setTitle:[NSString stringWithFormat:@"%d", i * j] forState:UIControlStateNormal];
                    
                    jumpertextfield = [[UILabel alloc]initWithFrame:CGRectMake((i-1) * ButtonWidth, self.frame.size.height - ButtonHeight, ButtonWidth, ButtonHeight)];
                    jumpertextfield.tag = i * j + 300;
                }
                
                jumperbutton.layer.borderWidth = 1;
                [jumperbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [jumperbutton addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:jumperbutton];
                
                jumpertextfield.layer.borderWidth = 0.5;
                jumpertextfield.font = [UIFont systemFontOfSize:12];
                jumpertextfield.textAlignment = UITextAlignmentCenter;
                UIColor *color = [UIColor colorWithRed:230.0/255.0 green:221.0/255.0 blue:159.0/255.0 alpha:1.0];
                jumpertextfield.backgroundColor = color;
                [self addSubview:jumpertextfield];
            }
        }
        
        for(int i = 0; i < 12; i++)
        {
            for(int j = 1; j <= 2;j++)
            {
                if(j % 2 == 0)
                {
                    sideimageview = [[UIImageView alloc]initWithFrame:CGRectMake(3 + i * (ButtonWidth * 2), (self.frame.size.height - (SidelinkerHeight + ButtonHeight * 2)) / 2 + ButtonHeight , SidelinkerWidth, SidelinkerHeight)];
                    sideimageview.tag = i * j + 200;
                    sideimageview.image = [UIImage imageNamed:@"left.png"];
                }
                else
                {
                    sideimageview = [[UIImageView alloc]initWithFrame:CGRectMake((ButtonWidth - SidelinkerWidth) + ButtonWidth + i * (ButtonWidth * 2) - 3, (self.frame.size.height - (SidelinkerHeight + ButtonHeight * 2)) / 2 + ButtonHeight , SidelinkerWidth, SidelinkerHeight)];
                    sideimageview.tag = i + 201;
                    sideimageview.image = [UIImage imageNamed:@"right.png"];
                }
                sideimageview.hidden = YES;
                [self addSubview:sideimageview];
            }
            
            if(i !=0)
            {
                UIImageView *temp = (UIImageView*)[self viewWithTag:i+100];
                circleimageview = [[UIImageView alloc]initWithFrame:CGRectMake(temp.frame.origin.x + temp.frame.size.width + 15, (self.frame.size.height - (Circle_D + ButtonHeight * 2)) / 2 + ButtonHeight , Circle_D, Circle_D)];
                circleimageview.tag = i+101;
                circleimageview.image = [UIImage imageNamed:@"circle_2.png"];
                [self addSubview:circleimageview];
            }
            else
            {
                circleimageview = [[UIImageView alloc]initWithFrame:CGRectMake(8, (self.frame.size.height - (Circle_D + ButtonHeight * 2)) / 2 + ButtonHeight , Circle_D, Circle_D)];
                circleimageview.tag = i+101;
                circleimageview.image = [UIImage imageNamed:@"circle_2.png"];
                [self addSubview:circleimageview];
            }
            circleimageview.hidden = YES;
        }
        
        for(int i = 0; i< 48;i++)
        {
            [maxline addObject:[NSNumber numberWithInt:0]];
            [highlevel addObject:[NSNumber numberWithBool:NO]];
            [mediumlevel addObject:[NSNumber numberWithBool:NO]];
            [topline addObject:[NSNumber numberWithBool:NO]];
            [bottomline addObject:[NSNumber numberWithBool:NO]];
            
            if(i < 12)
                [sideline addObject:[NSNumber numberWithBool:NO]];
        }
        
        jumper2dtopview = [[KNEJPrint alloc]initWithFrame:CGRectMake(210, 610, 210, 256)];
        jumper2dtopview.initialtag = initialtag;
        jumper2dtopview.finaltag = finaltag;
        
        [self addSubview:jumper2dtopview];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [super drawRect:(CGRect)rect];
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGFloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    CGContextSetStrokeColor(c, black);
    CGContextBeginPath(c);
    if(normalXY.count > 0 && normalXY.count % 4 == 0)
    {
        for(int i =0; i < normalXY.count; i+=4)
        {
            CGContextMoveToPoint(c, [[normalXY objectAtIndex:i] CGPointValue].x, [[normalXY objectAtIndex:i] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+1] CGPointValue].x, [[normalXY objectAtIndex:i+1] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+2] CGPointValue].x, [[normalXY objectAtIndex:i+2] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+3] CGPointValue].x, [[normalXY objectAtIndex:i+3] CGPointValue].y);
        }
    }
    
    if(invertXY.count > 0 && invertXY.count % 6 == 0)
    {
        for(int i =0; i < invertXY.count; i+=6)
        {
            CGContextMoveToPoint(c, [[invertXY objectAtIndex:i] CGPointValue].x, [[invertXY objectAtIndex:i] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+1] CGPointValue].x, [[invertXY objectAtIndex:i+1] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+2] CGPointValue].x, [[invertXY objectAtIndex:i+2] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+3] CGPointValue].x, [[invertXY objectAtIndex:i+3] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+4] CGPointValue].x, [[invertXY objectAtIndex:i+4] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+5] CGPointValue].x, [[invertXY objectAtIndex:i+5] CGPointValue].y);
        }
    }
    CGContextStrokePath(c);
}

-(IBAction)clicked:(id)sender
{
    NSLog(@"%d", [sender tag]);
    for(int i = 1; i <=48; i++)
    {
        if([sender tag] == i)
        {
            if(issecondclicked)
            {
                UIButton *firstbutton = (UIButton*)[self viewWithTag:firsttag];
                firstbutton.backgroundColor = [UIColor whiteColor];
                [firstbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                secondtag = [sender tag];
                
                if(secondtag == firsttag)
                {
                    issecondclicked = NO;
                }
                else
                {
                    [self updating];
                }
            }
            else
            {
                firsttag = [sender tag];
                UIButton *firstbutton = (UIButton*)[self viewWithTag:firsttag];
                firstbutton.backgroundColor = [UIColor blackColor];
                [firstbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                issecondclicked = YES;
            }
            
            break;
        }
        else
        {
            continue;
        }
    }
}

-(void)updating
{
    //NSLog(@"updating");
    if(secondtag < firsttag)
    {
        int temp = firsttag;
        firsttag = secondtag;
        secondtag = temp;
    }
    
    isrepeat = NO;
    int countnormal = 0, countinvert = 0;
    for(int z = 0; z< initialtag.count; z++)
    {
        
        if(firsttag == [[initialtag objectAtIndex:z] integerValue])
        {
            if(secondtag == [[finaltag objectAtIndex:z] integerValue])
            {
                if([[invertline objectAtIndex:z] boolValue] == 0)
                {
                    [normalXY removeObjectAtIndex:countnormal * 4 + 3];
                    [normalXY removeObjectAtIndex:countnormal * 4 + 2];
                    [normalXY removeObjectAtIndex:countnormal * 4 + 1];
                    [normalXY removeObjectAtIndex:countnormal * 4];
                    
                    if(secondtag - firsttag == 4)
                    {
                        if((secondtag - firsttag) % 2 == 0)
                        {
                            for(int i = firsttag; i <= secondtag; i+=2)
                            {
                                if(i != firsttag && i != secondtag)
                                {
                                    [mediumlevel replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:NO]];
                                }
                            }
                        }
                    }
                    else if (secondtag - firsttag > 5)
                    {
                        if((secondtag - firsttag) % 2 == 0)
                        {
                            for(int i = firsttag; i <= secondtag; i+=2)
                            {
                                if(i != firsttag && i != secondtag)
                                {
                                    [highlevel replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:NO]];
                                }
                            }
                        }
                    }
                    else if(secondtag - firsttag == 2)
                    {
                        [highlevel replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:NO]];
                        [highlevel replaceObjectAtIndex:secondtag-1 withObject:[NSNumber numberWithBool:NO]];
                    }
                }
                else
                {
                    [invertXY removeObjectAtIndex:countinvert * 6 + 5];
                    [invertXY removeObjectAtIndex:countinvert * 6 + 4];
                    [invertXY removeObjectAtIndex:countinvert * 6 + 3];
                    [invertXY removeObjectAtIndex:countinvert * 6 + 2];
                    [invertXY removeObjectAtIndex:countinvert * 6 + 1];
                    [invertXY removeObjectAtIndex:countinvert * 6];
                    
                    int check1 = firsttag / 4;
                    if(firsttag % 4 > 0)
                        check1++;
                    
                    int check2 = secondtag / 4;
                    if(secondtag % 4 > 0)
                        check2++;
                    
                    [sideline replaceObjectAtIndex:check1 - 1 withObject:[NSNumber numberWithBool:NO]];
                    
                    if(check1 == check2)
                    {
                        if(firsttag % 2 != 0)
                        {
                            [topline replaceObjectAtIndex:secondtag - 4 withObject:[NSNumber numberWithBool:NO]];
                            [topline replaceObjectAtIndex:secondtag - 2 withObject:[NSNumber numberWithBool:NO]];
                            [bottomline replaceObjectAtIndex:secondtag - 1 withObject:[NSNumber numberWithBool:NO]];
                        }
                        else
                        {
                            [bottomline replaceObjectAtIndex:secondtag - 2 withObject:[NSNumber numberWithBool:NO]];
                            [bottomline replaceObjectAtIndex:secondtag withObject:[NSNumber numberWithBool:NO]];
                            [topline replaceObjectAtIndex:secondtag - 1 withObject:[NSNumber numberWithBool:NO]];
                        }
                    }
                    else
                    {
                        if(firsttag % 4 == 1)
                        {
                            [topline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:NO]];
                            [topline replaceObjectAtIndex:firsttag+1 withObject:[NSNumber numberWithBool:NO]];
                            
                            for(int i = firsttag + 2; i <secondtag; i+=2)
                            {
                                [bottomline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                            }
                        }
                        else if(firsttag % 4 == 2)
                        {
                            [bottomline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:NO]];
                            [bottomline replaceObjectAtIndex:firsttag+1 withObject:[NSNumber numberWithBool:NO]];
                            
                            for(int i = firsttag + 2; i <secondtag; i+=2)
                            {
                                [topline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                            }
                        }
                        else if(firsttag % 4 == 3)
                        {
                            [topline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:NO]];
                            for(int i = firsttag; i <secondtag; i+=2)
                            {
                                [bottomline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                            }
                        }
                        else
                        {
                            [bottomline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:NO]];
                            for(int i = firsttag; i <secondtag; i+=2)
                            {
                                [topline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
                            }
                        }
                    }
                }
                
                [initialtag removeObjectAtIndex:z];
                [finaltag removeObjectAtIndex:z];
                [invertline removeObjectAtIndex:z];
                
                [maxline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:firsttag-1] integerValue]-1]];
                [maxline replaceObjectAtIndex:secondtag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:secondtag-1] integerValue]-1]];
                
                isrepeat = YES;
                [self setNeedsDisplay];
                
                issecondclicked = NO;
                countnormal = 0;
                countinvert = 0;
                break;
            }
            else
            {
                if([[invertline objectAtIndex:z] boolValue] == 0)
                {
                    countnormal++;
                }
                else
                {
                    countinvert++;
                }
            }
        }
        else
        {
            if([[invertline objectAtIndex:z] boolValue] == 0)
            {
                countnormal++;
            }
            else
            {
                countinvert++;
            }
        }
    }
    //}
    
    
    
    if([[maxline objectAtIndex:firsttag -1] integerValue] < 7 && [[maxline objectAtIndex:secondtag-1] integerValue] < 7)
    {
        if(!isrepeat)
        {
            int secondcircletag = secondtag / 4;
            if((secondtag % 4) > 0)
                secondcircletag++;
            UIImageView *final = (UIImageView*)[self viewWithTag:secondcircletag + 100];
            
            int firstcircletag = firsttag / 4;
            if((firsttag % 4) > 0)
                firstcircletag++;
            UIImageView *initial = (UIImageView*)[self viewWithTag:firstcircletag + 100];
            
            if(firstcircletag == secondcircletag )
            {
                if((firsttag % 4 == 1 && secondtag % 4 == 2) || (firsttag % 4 == 3 && secondtag % 4 == 0))
                {
                    iscorrect = NO;
                    issecondclicked = NO;
                }
                else
                {
                    iscorrect = YES;
                }
            }
            else
            {
                iscorrect = YES;
            }
            
            if(iscorrect)
            {
                if((secondtag % 2 == 0 && firsttag % 2 != 0) || (secondtag % 2 != 0 && firsttag % 2 == 0))
                {
                    for(int i = firsttag; i <= secondtag; i++)
                    {
                        //NSLog(@"before top %d = %d", i, [[topline objectAtIndex:i - 1] boolValue]);
                        //NSLog(@"before bottom %d = %d", i, [[bottomline objectAtIndex:i - 1] boolValue]);
                    }
                    
                    isinvert = YES;
                    if((secondtag % 2 != 0 && firsttag % 2 == 0))
                    {
                        if([[topline objectAtIndex:firsttag-1] boolValue] != 0 || [[bottomline objectAtIndex:secondtag-1] boolValue] != 0)
                        {
                            [self alerttrigger];
                            iswrong = YES;
                        }
                        else
                        {
                            if([[sideline objectAtIndex:firstcircletag - 1] boolValue] == 0)
                            {
                                if(firsttag % 4 == 2)
                                {
                                    firstX = initial.frame.origin.x + GapX;
                                    firstY = initial.frame.origin.y + initial.frame.size.height;
                                    
                                    secondX = firstX;
                                    secondY = firstY + StraightGradientY;
                                    
                                    UIButton *button = (UIButton*)[self viewWithTag:firstcircletag * 4];
                                    int horizontal = button.frame.origin.x + button.frame.size.width - (initial.frame.origin.x + GapX);
                                    thirdX = secondX + horizontal;
                                    thirdY = secondY;
                                    
                                    if(secondcircletag == firstcircletag)
                                    {
                                        for(int i = firstcircletag * 4 - 4; i < firstcircletag * 4;i++)
                                        {
                                            if(i != firstcircletag * 4 - 4)
                                            {
                                                if([[topline objectAtIndex:i] boolValue] != 0 || [[bottomline objectAtIndex:i] boolValue] != 0)
                                                {
                                                    [self alerttrigger];
                                                    iswrong = YES;
                                                    break;
                                                }
                                                else
                                                {
                                                    if(i == firstcircletag * 4 - 1)
                                                    {
                                                        [bottomline replaceObjectAtIndex:i - 2 withObject:[NSNumber numberWithBool:YES]];
                                                        [bottomline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
                                                        [topline replaceObjectAtIndex:i - 1 withObject:[NSNumber numberWithBool:YES]];
                                                        [sideline replaceObjectAtIndex:firstcircletag - 1 withObject:[NSNumber numberWithBool:YES]];
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        turningpoint = button.tag;
                                        if([[bottomline objectAtIndex:firsttag-1] boolValue] == 0 && [[bottomline objectAtIndex:turningpoint-1] boolValue] == 0)
                                        {
                                            for(int i = turningpoint; i <secondtag; i+=2)
                                            {
                                                if([[topline objectAtIndex:i] boolValue] == 0)
                                                {
                                                    if(i == secondtag - 1)
                                                    {
                                                        [bottomline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:YES]];
                                                        [bottomline replaceObjectAtIndex:turningpoint-1 withObject:[NSNumber numberWithBool:YES]];
                                                        for(int j = turningpoint; j <secondtag; j+=2)
                                                        {
                                                            [topline replaceObjectAtIndex:j withObject:[NSNumber numberWithBool:YES]];
                                                        }
                                                    }
                                                    else
                                                        continue;
                                                }
                                                else
                                                {
                                                    [self alerttrigger];
                                                    iswrong = YES;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            [self alerttrigger];
                                            iswrong = YES;
                                        }
                                    }
                                }
                                else
                                {
                                    firstX = initial.frame.origin.x + initial.frame.size.width - GapX;
                                    firstY = initial.frame.origin.y + initial.frame.size.height;
                                    
                                    secondX = firstX;
                                    secondY = firstY + StraightGradientY;
                                    
                                    UIButton *button = (UIButton*)[self viewWithTag:firstcircletag * 4];
                                    int horizontal = button.frame.origin.x + button.frame.size.width - (initial.frame.origin.x + initial.frame.size.width - GapX);
                                    thirdX = secondX + horizontal;
                                    thirdY = secondY;
                                    
                                    turningpoint = button.tag;
                                    if([[bottomline objectAtIndex:firsttag-1] boolValue] == 0 )
                                    {
                                        for(int i = turningpoint; i <secondtag; i+=2)
                                        {
                                            if([[topline objectAtIndex:i] boolValue] == 0 )
                                            {
                                                if(i == secondtag - 1)
                                                {
                                                    [bottomline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:YES]];
                                                    for(int j = turningpoint; j <secondtag; j+=2)
                                                    {
                                                        [topline replaceObjectAtIndex:j withObject:[NSNumber numberWithBool:YES]];
                                                    }
                                                }
                                                else
                                                    continue;
                                            }
                                            else
                                            {
                                                [self alerttrigger];
                                                iswrong = YES;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        [self alerttrigger];
                                        iswrong = YES;
                                    }
                                }
                                
                                if(secondtag % 4 == 1)
                                {
                                    sixthX = final.frame.origin.x + GapX;
                                    sixthY = final.frame.origin.y;
                                    
                                    fifthX = sixthX;
                                    fifthY = sixthY - StraightGradientY;
                                }
                                else
                                {
                                    sixthX = final.frame.origin.x + final.frame.size.width - GapX;
                                    sixthY = final.frame.origin.y;
                                    
                                    fifthX = sixthX;
                                    fifthY = sixthY - StraightGradientY;
                                }
                                
                                forthX = thirdX;
                                forthY = thirdY - initial.frame.size.height - StraightGradientY * 2;
                            }
                            else
                            {
                                [self alerttrigger];
                                iswrong = YES;
                            }
                        }
                    }
                    else
                    {
                        if([[topline objectAtIndex:firsttag-1] boolValue] != 0 || [[bottomline objectAtIndex:secondtag - 1] boolValue] != 0)
                        {
                            [self alerttrigger];
                            iswrong = YES;
                        }
                        else
                        {
                            if([[sideline objectAtIndex:firstcircletag - 1] boolValue] == 0)
                            {
                                if(firsttag % 4 == 1)
                                {
                                    firstX = initial.frame.origin.x + GapX;
                                    firstY = initial.frame.origin.y;
                                    
                                    secondX = firstX;
                                    secondY = firstY - StraightGradientY;
                                    
                                    UIButton *button = (UIButton*)[self viewWithTag:firstcircletag * 4 - 1];
                                    int horizontal = button.frame.origin.x + button.frame.size.width - (initial.frame.origin.x + GapX);
                                    thirdX = secondX + horizontal;
                                    thirdY = secondY;
                                    
                                    if(secondcircletag == firstcircletag)
                                    {
                                        for(int i = firstcircletag* 4 - 4; i < firstcircletag*4;i++)
                                        {
                                            if(i != firstcircletag * 4 - 3)
                                            {
                                                if([[topline objectAtIndex:i] boolValue] != 0 || [[bottomline objectAtIndex:i] boolValue] != 0)
                                                {
                                                    [self alerttrigger];
                                                    iswrong = YES;
                                                    break;
                                                }
                                                else
                                                {
                                                    if(i == firstcircletag * 4 - 1)
                                                    {
                                                        [topline replaceObjectAtIndex:i - 3 withObject:[NSNumber numberWithBool:YES]];
                                                        [topline replaceObjectAtIndex:i - 1 withObject:[NSNumber numberWithBool:YES]];
                                                        [bottomline replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:YES]];
                                                        [sideline replaceObjectAtIndex:firstcircletag - 1 withObject:[NSNumber numberWithBool:YES]];
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        turningpoint = button.tag;
                                        if([[topline objectAtIndex:firsttag-1] boolValue] == 0 && [[topline objectAtIndex:turningpoint-1] boolValue] == 0)
                                        {
                                            for(int i = turningpoint + 2; i <secondtag; i+=2)
                                            {
                                                if([[bottomline objectAtIndex:i] boolValue] == 0)
                                                {
                                                    if(i == secondtag-1)
                                                    {
                                                        [topline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:YES]];
                                                        [topline replaceObjectAtIndex:turningpoint-1 withObject:[NSNumber numberWithBool:YES]];
                                                        for(int j = turningpoint + 2; j <secondtag; j+=2)
                                                        {
                                                            [bottomline replaceObjectAtIndex:j withObject:[NSNumber numberWithBool:YES]];
                                                        }
                                                    }
                                                    else
                                                        continue;
                                                }
                                                else
                                                {
                                                    [self alerttrigger];
                                                    iswrong = YES;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            [self alerttrigger];
                                            iswrong = YES;
                                        }
                                    }
                                }
                                else
                                {
                                    firstX = initial.frame.origin.x + initial.frame.size.width - GapX;
                                    firstY = initial.frame.origin.y;
                                    
                                    secondX = firstX;
                                    secondY = firstY - StraightGradientY;
                                    
                                    UIButton *button = (UIButton*)[self viewWithTag:firstcircletag * 4 - 1];
                                    int horizontal = button.frame.origin.x + button.frame.size.width - (initial.frame.origin.x + initial.frame.size.width - GapX);
                                    thirdX = secondX + horizontal;
                                    thirdY = secondY;
                                    
                                    turningpoint = button.tag;
                                    if([[topline objectAtIndex:firsttag - 1] boolValue] == 0)
                                    {
                                        for(int i = turningpoint + 2; i <secondtag; i+=2)
                                        {
                                            if([[bottomline objectAtIndex:i] boolValue] == 0)
                                            {
                                                if(i == secondtag-1)
                                                {
                                                    [topline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:YES]];
                                                    for(int j = turningpoint + 2; j <secondtag; j+=2)
                                                    {
                                                        [bottomline replaceObjectAtIndex:j withObject:[NSNumber numberWithBool:YES]];
                                                    }
                                                }
                                                else
                                                    continue;
                                            }
                                            else
                                            {
                                                [self alerttrigger];
                                                iswrong = YES;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        [self alerttrigger];
                                        iswrong = YES;
                                    }
                                }
                                
                                if(secondtag % 4 == 2)
                                {
                                    sixthX = final.frame.origin.x + GapX;
                                    sixthY = final.frame.origin.y + final.frame.size.height;
                                    
                                    fifthX = sixthX;
                                    fifthY = sixthY + StraightGradientY;
                                }
                                else
                                {
                                    sixthX = final.frame.origin.x + final.frame.size.width - GapX;
                                    sixthY = final.frame.origin.y + final.frame.size.height;
                                    
                                    fifthX = sixthX;
                                    fifthY = sixthY + StraightGradientY;
                                }
                                
                                forthX = thirdX;
                                forthY = thirdY + initial.frame.size.height + StraightGradientY * 2;
                            }
                            else
                            {
                                [self alerttrigger];
                                iswrong = YES;
                            }
                        }
                    }
                    
                    if(!iswrong)
                    {
                        
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(firstX, firstY)]];
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(secondX, secondY)]];
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(thirdX, thirdY)]];
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(forthX, forthY)]];
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(fifthX, fifthY)]];
                        [invertXY addObject:[NSValue valueWithCGPoint:CGPointMake(sixthX, sixthY)]];
                        
                        [self setNeedsDisplay];
                        
                        [initialtag addObject:[NSNumber numberWithInt:firsttag]];
                        [finaltag addObject:[NSNumber numberWithInt:secondtag]];
                        [invertline addObject:[NSNumber numberWithBool:isinvert]];
                        
                        [maxline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:firsttag-1] integerValue]+1]];
                        [maxline replaceObjectAtIndex:secondtag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:secondtag-1]integerValue]+1]];
                    }
                    
                    for(int i = firsttag; i <= secondtag; i++)
                    {
                        //NSLog(@"after top %d = %d", i, [[topline objectAtIndex:i - 1] boolValue]);
                        //NSLog(@"after bottom %d = %d", i, [[bottomline objectAtIndex:i - 1] boolValue]);
                    }
                }
                else
                {
                    isinvert = NO;
                    if(firstcircletag == secondcircletag)
                    {
                        gradientX = LowGradientX;
                        gradientY = LowGradientY;
                    }
                    else
                    {
                        if(secondcircletag - firstcircletag == 1 && secondtag - firsttag == 2)
                        {
                            if([[highlevel objectAtIndex:secondtag-1] boolValue] == 0 && [[highlevel objectAtIndex:firsttag-1] boolValue] == 0)
                            {
                                gradientX = HighGradientX;
                                gradientY = HighGradientY;
                                [highlevel replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithBool:YES]];
                                [highlevel replaceObjectAtIndex:secondtag-1 withObject:[NSNumber numberWithBool:YES]];
                            }
                            else
                            {
                                [self alerttrigger];
                                iswrong = YES;
                            }
                        }
                        else
                        {
                            if(secondtag - firsttag == 4)
                            {
                                for(int i = firsttag; i <= secondtag; i+=2)
                                {
//                                    if([[mediumlevel objectAtIndex:i-1] boolValue] == 0)
//                                    {
                                        if(i == secondtag)
                                        {
                                            if ((firsttag == 1 || secondtag == 5) || (firsttag == 5 || secondtag == 9) || (firsttag == 9 || secondtag == 13)|| (firsttag == 13 || secondtag == 17)|| (firsttag == 17 || secondtag == 21)|| (firsttag == 21 || secondtag == 25)|| (firsttag == 25 || secondtag == 29)|| (firsttag == 29 || secondtag == 33)|| (firsttag == 33 || secondtag == 37)|| (firsttag == 37 || secondtag == 41)|| (firsttag == 41 || secondtag == 45) || (firsttag == 2 || secondtag == 6) || (firsttag == 6 || secondtag == 10) || (firsttag == 10 || secondtag == 14) || (firsttag == 14 || secondtag == 18) || (firsttag == 18 || secondtag == 22) || (firsttag == 22 || secondtag == 26) || (firsttag == 26 || secondtag == 30) || (firsttag == 30 || secondtag == 34) || (firsttag == 34 || secondtag == 38) || (firsttag == 38 || secondtag == 42) || (firsttag == 42 || secondtag == 46)) {
                                                gradientX = MediumGradientX;
                                                gradientY = MediumGradientY;
                                            }else
                                            {
                                                gradientX = MediumGradient_secondX;
                                                gradientY = MediumGradient_secondY;
                                            }
                                            for(int i = firsttag; i <= secondtag; i+=2)
                                            {
                                                if(i != firsttag && i != secondtag)
                                                {
                                                    [mediumlevel replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                                                }
                                            }
                                        }
                                        else
                                        {
                                            continue;
                                        }
//                                    }
//                                    else
//                                    {
//                                        [self alerttrigger];
//                                        iswrong = YES;
//                                        break;
//                                    }
                                }
                            }
                            else if (secondtag - firsttag > 5)
                            {
                                if((secondtag - firsttag) % 2 == 0)
                                {
                                    for(int i = firsttag; i <= secondtag; i+=2)
                                    {
                                        if([[highlevel objectAtIndex:i-1] boolValue] == 0)
                                        {
                                            if(i == secondtag)
                                            {
                                                gradientX = HighGradientX;
                                                gradientY = HighGradientY;
                                                for(int i = firsttag; i <= secondtag; i+=2)
                                                {
                                                    if(i != firsttag && i != secondtag)
                                                    {
                                                        [highlevel replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            [self alerttrigger];
                                            iswrong = YES;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if(!iswrong)
                    {
                        if(firsttag % 4 == 1)
                        {
                            //first
                            firstX = initial.frame.origin.x + GapX;
                            firstY = initial.frame.origin.y;
                            
                            secondX = firstX + gradientX;
                            secondY = firstY - gradientY;
                        }
                        else if(firsttag % 4 == 2)
                        {
                            //second
                            firstX = initial.frame.origin.x + GapX;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX + gradientX;
                            secondY = firstY + gradientY;
                            
                        }
                        else if(firsttag % 4 == 3)
                        {
                            //third
                            firstX = initial.frame.origin.x + initial.frame.size.width - GapX;
                            firstY = initial.frame.origin.y;
                            
                            secondX = firstX + gradientX;
                            secondY = firstY - gradientY;
                        }
                        else
                        {
                            //forth
                            firstX = initial.frame.origin.x + initial.frame.size.width - GapX;
                            firstY = initial.frame.origin.y + initial.frame.size.height;
                            
                            secondX = firstX + gradientX;
                            secondY = firstY + gradientY;
                        }
                        
                        
                        if(secondtag % 4 == 1)
                        {
                            //first
                            forthX = final.frame.origin.x + GapX;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX - gradientX;
                            thirdY = forthY - gradientY;
                        }
                        else if(secondtag % 4 == 2)
                        {
                            //second
                            forthX = final.frame.origin.x + GapX;
                            forthY = final.frame.origin.y + final.frame.size.height;
                            
                            thirdX = forthX - gradientX;
                            thirdY = forthY + gradientY;
                            
                        }
                        else if(secondtag % 4 == 3)
                        {
                            //third
                            
                            forthX = final.frame.origin.x + final.frame.size.width - GapX;
                            forthY = final.frame.origin.y;
                            
                            thirdX = forthX - gradientX;
                            thirdY = forthY - gradientY;
                        }
                        else
                        {
                            //forth
                            forthX = final.frame.origin.x + final.frame.size.width - GapX;
                            forthY = final.frame.origin.y + final.frame.size.height;
                            
                            thirdX = forthX - gradientX;
                            thirdY = forthY + gradientY;
                        }
                        
                        [normalXY addObject:[NSValue valueWithCGPoint:CGPointMake(firstX, firstY)]];
                        [normalXY addObject:[NSValue valueWithCGPoint:CGPointMake(secondX, secondY)]];
                        [normalXY addObject:[NSValue valueWithCGPoint:CGPointMake(thirdX, thirdY)]];
                        [normalXY addObject:[NSValue valueWithCGPoint:CGPointMake(forthX, forthY)]];
                        
                        [self setNeedsDisplay];
                        
                        [initialtag addObject:[NSNumber numberWithInt:firsttag]];
                        [finaltag addObject:[NSNumber numberWithInt:secondtag]];
                        [invertline addObject:[NSNumber numberWithBool:isinvert]];
                        
                        jumper2dtopview.initialtag = initialtag;
                        
                        [maxline replaceObjectAtIndex:firsttag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:firsttag-1] integerValue]+1]];
                        [maxline replaceObjectAtIndex:secondtag-1 withObject:[NSNumber numberWithInt:[[maxline objectAtIndex:secondtag-1]integerValue]+1]];
                    }
                }
                issecondclicked = NO;
                iswrong = NO;
            }
        }
    }
    else
    {
        [self alerttrigger];
        issecondclicked = NO;
    }
}

-(void)alerttrigger
{
    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry, I can't do this jumper" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)storejumper
{
    KNEAppDelegate *appDelegateSave = [UIApplication sharedApplication].delegate;
    
    //NSLog(@"appDelegateSave.gKNECase>>>>>>>>>>>>>>>>>>>>>>>>>>%@", appDelegateSave.gKNECase.CaseID);
    
    for(int i = 0; i<initialtag.count; i++)
    {
        Jumper *jumper = [[Jumper alloc]init];
        jumper.initial = [[initialtag objectAtIndex:i] integerValue];
        jumper.final = [[finaltag objectAtIndex:i] integerValue];
        
        /*if(([[initialtag objectAtIndex:i] integerValue] % 2 == 0 && [[finaltag objectAtIndex:i] integerValue] % 2 != 0) || ([[initialtag objectAtIndex:i] integerValue] % 2 != 0 && [[finaltag objectAtIndex:i] integerValue] % 2 == 0))
         {
         jumper.isinvert = 1;
         }
         else
         {
         jumper.isinvert = 0;
         }*/
        jumper.CaseID=appDelegateSave.gKNECase.CaseID;
        DatabaseAction *jdb=[[DatabaseAction alloc] init];
        
        [jdb insertJumper:jumper];
        //NSLog(@"inside for~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
    
    //NSLog(@"hello done~~~~~jumper store page tapped~~~~~~~~~~~~~~~~~~~~~~~~~");
}

-(void)retrievejumper
{
    KNEAppDelegate *appDelegateSave = [UIApplication sharedApplication].delegate;
    
    DatabaseAction *jdb=[[DatabaseAction alloc] init];
    //db = [[DatabaseAction alloc] init];
    NSArray *jumperarrays=[[NSArray alloc] init];
    jumperarrays = [jdb retrieveJumper:appDelegateSave.gKNECase];
    
    for (Jumper *Jumperitem in jumperarrays)
    {
        
        firsttag = Jumperitem.initial;
        secondtag = Jumperitem.final;
        
        //[initialtag addObject:[NSNumber numberWithInt:Jumperitem.initial]];
        //[finaltag addObject:[NSNumber numberWithInt:Jumperitem.final]];
        //[invertline addObject:[NSNumber numberWithInt:Jumperitem.isinvert]];
        [self updating];
        
    }
    
    //NSLog(@"hello done~~~~~jumper retrievejumper page tapped~~~~~~~~~~~~~~~~~~~~~~~~~");
}

@end
