//
//  KNEMenuPageViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/7/12.
//
//

#import <UIKit/UIKit.h>
#import "KNESwitchMainPageViewController.h"
#import "EscutheonPlateWindow.h"
#import "SwitchMainPageViewController.h"

@interface KNEMenuPageViewController : UIViewController{
    
}
//@property(strong, nonatomic) SwitchMainPageViewController *gKNESwitchMainPageViewController;
-(IBAction)BtnSwitchTapped:(id)sender;
-(IBAction)BtnPlateTapped:(id)sender;
-(IBAction)BtnSyncTapped:(id)sender;

@end
