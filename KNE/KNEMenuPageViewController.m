//
//  KNEMenuPageViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/7/12.
//
//

#import "KNEMenuPageViewController.h"

@implementation KNEMenuPageViewController
//@synthesize gKNESwitchMainPageViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(IBAction)BtnSwitchTapped:(id)sender
{
    /*for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    //[self.view removeFromSuperview];
   
    if (self.navigationController.view !=gKNESwitchMainPageViewController.view) {
        
        
    }else
    {
        NSLog(@"navigationController2");
        [self.navigationController pushViewController:gKNESwitchMainPageViewController animated:YES];
    }
     */
     

    NSLog(@"hello menu~");

    SwitchMainPageViewController *gKNESwitchMainPageViewController=[[SwitchMainPageViewController alloc] initWithNibName:@"SwitchMainPageViewController"  bundle:nil];
    
   
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(110, 3, 200, 24)];
    label.text=@"Switch";
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 3;
    label.lineBreakMode=UILineBreakModeWordWrap;
    label.font = [UIFont fontWithName:@"Arial-BoldMT" size:21];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    //gKNESwitchMainPageViewController.navigationItem.titleView=label;
    /*
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gKNESwitchMainPageViewController];
    //tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    //gKNEMenuPageViewController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    gKNESwitchMainPageViewController.navigationItem.titleView = label;
    
    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    //[self.view addSubview:tnavController.view];
    [self.navigationController pushViewController:gKNESwitchMainPageViewController animated:YES];
    */
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gKNESwitchMainPageViewController];
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    //gKNEMenuPageViewController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    gKNESwitchMainPageViewController.navigationItem.titleView = label;
    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    /*
    [self.view addSubview:tnavController.view];*/
    [self presentModalViewController:tnavController animated:YES];

}

-(IBAction)BtnPlateTapped:(id)sender
{
    EscutheonPlateWindow *gEscutheonPlateWindow=[[EscutheonPlateWindow alloc] initWithNibName:@"EscutheonPlateWindow"  bundle:nil];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(110, 3, 200, 24)];
    label.text=@"Escutheon Plate";
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 3;
    label.lineBreakMode=UILineBreakModeWordWrap;
    label.font = [UIFont fontWithName:@"Arial-BoldMT" size:21];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    gEscutheonPlateWindow.navigationItem.titleView=label;
    
    //gEscutheonPlateWindow.title=@"Escutheon Plate";
    //[self.navigationController pushViewController:gEscutheonPlateWindow animated:YES];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gEscutheonPlateWindow];
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    //gKNEMenuPageViewController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    /*
     [self.view addSubview:tnavController.view];*/
    [self presentModalViewController:tnavController animated:YES];
}

-(IBAction)BtnSyncTapped:(id)sender
{
    
}

@end
