//
//  KNEShowSRCell.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/5/12.
//
//

#import <UIKit/UIKit.h>
#import "springReturndelegate.h"
#import "KNEAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface KNEShowSRCell : UITableViewCell{
    
}
@property (nonatomic, strong) id<springReturndelegate> delegate;
@property (nonatomic,strong) UITextField *txtuserremark;
@property (nonatomic, strong) UILabel *lblSpringNum;
@property (nonatomic, strong) NSArray *numarr;
@property (nonatomic, strong) UIButton *firstPositionNum;

@end
