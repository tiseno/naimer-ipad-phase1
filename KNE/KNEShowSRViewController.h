//
//  KNEShowSRViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/5/12.
//
//

#import <UIKit/UIKit.h>
#import "KNEShowSRCell.h"
#import "KNEAppDelegate.h"

@class KNESwitchShowViewController;

@interface KNEShowSRViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>{
    KNEShowSRCell *cell;
    int txtindexpath;
}

//@property(nonatomic,retain) NSString *firstpositionnum;
@property (nonatomic, strong) NSArray *numarr;
@property (nonatomic, strong) KNESwitchShowViewController *gKNESwitchShowViewController;
-(void)getfirstpNum;
@end
