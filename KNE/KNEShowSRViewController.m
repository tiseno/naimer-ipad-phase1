//
//  KNEShowSRViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/5/12.
//
//

#import "KNEShowSRViewController.h"

@interface KNEShowSRViewController ()

@end

@implementation KNEShowSRViewController
@synthesize gKNESwitchShowViewController;
@synthesize numarr;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        [self getfirstpNum];
    }
    return self;
}


-(void)getfirstpNum
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.firstPnum=@"330";
    
    int fP=[appDelegate.firstPnum intValue];
    int firstposition = fP-15;
    //int rowvalue=0;
    
    NSMutableArray *graftNumbermArr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<24;i++)
    {

        if (firstposition >=360) {
            firstposition=15;
        }else {
            firstposition+=15;
        }
        
        
        //NSLog(@"firstposition--->>>>%d",firstposition);
        
        
        
        NSString *invalue=[[NSString alloc]initWithFormat:@"%d",firstposition];
        
        if ([invalue isEqualToString:@"360"]) {
            invalue=@"0";
        }
        [graftNumbermArr addObject:invalue];
        
    }
    
    self.numarr=graftNumbermArr;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self getfirstpNum];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return numarr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    cell = [[KNEShowSRCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.lblSpringNum.text=[self.numarr objectAtIndex:indexPath.row];
    txtindexpath=indexPath.row;
    
    cell.txtuserremark.delegate = self;
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


#pragma mark - Table view delegate



@end
