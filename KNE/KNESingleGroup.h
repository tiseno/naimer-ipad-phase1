//
//  KNESingleGroup.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KNESingleGroup : NSObject{
    
}

@property (nonatomic, strong) NSString *col1;
@property (nonatomic, strong) NSString *row1;

@property (nonatomic, strong) NSString *col3;
@property (nonatomic, strong) NSString *col5;
@property (nonatomic, strong) NSString *col7;
@property (nonatomic, strong) NSString *col9;

@property (nonatomic, strong) NSString *col11;
@property (nonatomic, strong) NSString *col13;
@property (nonatomic, strong) NSString *col15;
@property (nonatomic, strong) NSString *col17;
@property (nonatomic, strong) NSString *col19;

@property (nonatomic, strong) NSString *col21;
@property (nonatomic, strong) NSString *col23;

@property (nonatomic, strong) NSString *row3;
@property (nonatomic, strong) NSString *row5;
@property (nonatomic, strong) NSString *row7;
@property (nonatomic, strong) NSString *row9;

@property (nonatomic, strong) NSString *row11;
@property (nonatomic, strong) NSString *row13;
@property (nonatomic, strong) NSString *row15;
@property (nonatomic, strong) NSString *row17;
@property (nonatomic, strong) NSString *row19;

@property (nonatomic, strong) NSString *row21;
@property (nonatomic, strong) NSString *row23;



@end
