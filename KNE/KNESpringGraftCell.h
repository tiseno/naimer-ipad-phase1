//
//  KNESpringGraftCell.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface KNESpringGraftCell : UITableViewCell<UITextFieldDelegate>{
    
}
/*===============column 1======================*/
@property (nonatomic, strong) UITextField *txtgraftnum1;
@property (nonatomic, strong) UILabel *lblgraftNum1;
@property (nonatomic, strong) UIImageView *imgpicture1;
/*===============column 1======================*/
@property (nonatomic, strong) UITextField *txtgraftnum2;
@property (nonatomic, strong) UILabel *lblgraftNum2;
@property (nonatomic, strong) UIImageView *imgpicture2;
/*===============column 3======================*/
@property (nonatomic, strong) UITextField *txtgraftnum3;
@property (nonatomic, strong) UILabel *lblgraftNum3;
@property (nonatomic, strong) UIImageView *imgpicture3;
/*===============column 4======================*/
@property (nonatomic, strong) UITextField *txtgraftnum4;
@property (nonatomic, strong) UILabel *lblgraftNum4;
@property (nonatomic, strong) UIImageView *imgpicture4;
/*===============column 5======================*/
@property (nonatomic, strong) UITextField *txtgraftnum5;
@property (nonatomic, strong) UILabel *lblgraftNum5;
@property (nonatomic, strong) UIImageView *imgpicture5;
/*===============column 6======================*/
@property (nonatomic, strong) UITextField *txtgraftnum6;
@property (nonatomic, strong) UILabel *lblgraftNum6;
@property (nonatomic, strong) UIImageView *imgpicture6;
/*===============column 7======================*/
@property (nonatomic, strong) UITextField *txtgraftnum7;
@property (nonatomic, strong) UILabel *lblgraftNum7;
@property (nonatomic, strong) UIImageView *imgpicture7;
/*===============column 8======================*/
@property (nonatomic, strong) UITextField *txtgraftnum8;
@property (nonatomic, strong) UILabel *lblgraftNum8;
@property (nonatomic, strong) UIImageView *imgpicture8;
/*===============column 9======================*/
@property (nonatomic, strong) UITextField *txtgraftnum9;
@property (nonatomic, strong) UILabel *lblgraftNum9;
@property (nonatomic, strong) UIImageView *imgpicture9;

/*===============column 10======================*/
@property (nonatomic, strong) UITextField *txtgraftnum10;
@property (nonatomic, strong) UILabel *lblgraftNum10;
@property (nonatomic, strong) UIImageView *imgpicture10;

/*===============column 11======================*/
@property (nonatomic, strong) UITextField *txtgraftnum11;
@property (nonatomic, strong) UILabel *lblgraftNum11;
@property (nonatomic, strong) UIImageView *imgpicture11;
/*===============column 12======================*/
@property (nonatomic, strong) UITextField *txtgraftnum12;
@property (nonatomic, strong) UILabel *lblgraftNum12;
@property (nonatomic, strong) UIImageView *imgpicture12;
/*===============column 13======================*/
@property (nonatomic, strong) UITextField *txtgraftnum13;
@property (nonatomic, strong) UILabel *lblgraftNum13;
@property (nonatomic, strong) UIImageView *imgpicture13;
/*===============column 14======================*/
@property (nonatomic, strong) UITextField *txtgraftnum14;
@property (nonatomic, strong) UILabel *lblgraftNum14;
@property (nonatomic, strong) UIImageView *imgpicture14;
/*===============column 15======================*/
@property (nonatomic, strong) UITextField *txtgraftnum15;
@property (nonatomic, strong) UILabel *lblgraftNum15;
@property (nonatomic, strong) UIImageView *imgpicture15;

/*===============column 16======================*/
@property (nonatomic, strong) UITextField *txtgraftnum16;
@property (nonatomic, strong) UILabel *lblgraftNum16;
@property (nonatomic, strong) UIImageView *imgpicture16;
/*===============column 17======================*/
@property (nonatomic, strong) UITextField *txtgraftnum17;
@property (nonatomic, strong) UILabel *lblgraftNum17;
@property (nonatomic, strong) UIImageView *imgpicture17;
/*===============column 18======================*/
@property (nonatomic, strong) UITextField *txtgraftnum18;
@property (nonatomic, strong) UILabel *lblgraftNum18;
@property (nonatomic, strong) UIImageView *imgpicture18;
/*===============column 19======================*/
@property (nonatomic, strong) UITextField *txtgraftnum19;
@property (nonatomic, strong) UILabel *lblgraftNum19;
@property (nonatomic, strong) UIImageView *imgpicture19;
/*===============column 20======================*/
@property (nonatomic, strong) UITextField *txtgraftnum20;
@property (nonatomic, strong) UILabel *lblgraftNum20;
@property (nonatomic, strong) UIImageView *imgpicture20;

/*===============column 21======================*/
@property (nonatomic, strong) UITextField *txtgraftnum21;
@property (nonatomic, strong) UILabel *lblgraftNum21;
@property (nonatomic, strong) UIImageView *imgpicture21;
/*===============column 22======================*/
@property (nonatomic, strong) UITextField *txtgraftnum22;
@property (nonatomic, strong) UILabel *lblgraftNum22;
@property (nonatomic, strong) UIImageView *imgpicture22;
/*===============column 23======================*/
@property (nonatomic, strong) UITextField *txtgraftnum23;
@property (nonatomic, strong) UILabel *lblgraftNum23;
@property (nonatomic, strong) UIImageView *imgpicture23;
/*===============column 24======================*/
@property (nonatomic, strong) UITextField *txtgraftnum24;
@property (nonatomic, strong) UILabel *lblgraftNum24;
@property (nonatomic, strong) UIImageView *imgpicture24;





@end
