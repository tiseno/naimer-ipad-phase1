//
//  KNESpringGraftCell.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNESpringGraftCell.h"

@implementation KNESpringGraftCell
@synthesize txtgraftnum1, lblgraftNum1,imgpicture1;
@synthesize txtgraftnum2, lblgraftNum2,imgpicture2;
@synthesize lblgraftNum3,lblgraftNum4, lblgraftNum5;
@synthesize txtgraftnum3,txtgraftnum4, txtgraftnum5;
@synthesize imgpicture3,imgpicture4, imgpicture5;
@synthesize lblgraftNum6, lblgraftNum7, lblgraftNum8, lblgraftNum9;
@synthesize txtgraftnum6, txtgraftnum7, txtgraftnum8, txtgraftnum9;
@synthesize imgpicture6, imgpicture7, imgpicture8, imgpicture9;

@synthesize lblgraftNum10,txtgraftnum10,imgpicture10;

@synthesize lblgraftNum11, lblgraftNum12, lblgraftNum13, lblgraftNum14, lblgraftNum15;
@synthesize txtgraftnum11, txtgraftnum12, txtgraftnum13, txtgraftnum14, txtgraftnum15;
@synthesize imgpicture11, imgpicture12, imgpicture13, imgpicture14, imgpicture15;

@synthesize lblgraftNum16, lblgraftNum17, lblgraftNum18, lblgraftNum19, lblgraftNum20;
@synthesize txtgraftnum16, txtgraftnum17, txtgraftnum18, txtgraftnum19, txtgraftnum20;
@synthesize imgpicture16, imgpicture17, imgpicture18, imgpicture19, imgpicture20;

@synthesize lblgraftNum21, lblgraftNum22, lblgraftNum23, lblgraftNum24;
@synthesize txtgraftnum21, txtgraftnum22, txtgraftnum23, txtgraftnum24;
@synthesize imgpicture21, imgpicture22, imgpicture23, imgpicture24;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        /*===============column 1======================*/
        UITextField *gtxtgraftnum1 = [[UITextField alloc] initWithFrame:CGRectMake(0, 5, 50, 30)];
        gtxtgraftnum1.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum1.textColor = [UIColor blackColor];
        gtxtgraftnum1.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum1.layer.borderWidth = 1;
        
        /*UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singletapcaptured:)];
        [gtxtgraftnum1 addGestureRecognizer:singletap];
        [gtxtgraftnum1 setUserInteractionEnabled:YES];
        [singletap release];*/
        
        gtxtgraftnum1.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum1.backgroundColor = [UIColor clearColor];
        self.txtgraftnum1 = gtxtgraftnum1;
        [self addSubview:txtgraftnum1];
        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 45, 21)];
        glblgraftNum1.textAlignment = UITextAlignmentRight;
        glblgraftNum1.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum1.layer.borderWidth = 1;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        
        /**/
        
        self.lblgraftNum1 = glblgraftNum1;
        [self addSubview:lblgraftNum1];
        
        
        UIImageView *gimgpicture1= [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 20, 21)];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        //gimgpicture1.backgroundColor=[UIColor blackColor];
        self.imgpicture1=gimgpicture1;
        [self addSubview:imgpicture1];
        
        /*===============column 2======================*/
        UITextField *gtxtgraftnum2 = [[UITextField alloc] initWithFrame:CGRectMake(50, 5, 50, 30)];
        gtxtgraftnum2.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum2.textColor = [UIColor blackColor];
        gtxtgraftnum2.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum2.layer.borderWidth = 1;
        gtxtgraftnum2.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum2.backgroundColor = [UIColor clearColor];
        self.txtgraftnum2 = gtxtgraftnum2;
        [self addSubview:txtgraftnum2];
        
        UILabel *glblgraftNum2 = [[UILabel alloc] initWithFrame:CGRectMake(50, 5, 45, 21)];
        glblgraftNum2.textAlignment = UITextAlignmentRight;
        glblgraftNum2.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum2.layer.borderWidth = 1;
        glblgraftNum2.textColor = [UIColor blackColor];
        glblgraftNum2.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum2.backgroundColor = [UIColor clearColor];
        self.lblgraftNum2 = glblgraftNum2;
        [self addSubview:lblgraftNum2];
        
        UIImageView *gimgpicture2= [[UIImageView alloc]initWithFrame:CGRectMake(60, 5, 20, 21)];        
        gimgpicture2.backgroundColor=[UIColor blackColor];
        self.imgpicture2=gimgpicture2;
        [self addSubview:imgpicture2];
        
        /*===============column 3======================*/
        UITextField *gtxtgraftnum3 = [[UITextField alloc] initWithFrame:CGRectMake(100, 5, 50, 30)];
        gtxtgraftnum3.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum3.textColor = [UIColor blackColor];
        gtxtgraftnum3.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum3.layer.borderWidth = 1;
        gtxtgraftnum3.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum3.backgroundColor = [UIColor clearColor];
        self.txtgraftnum3 = gtxtgraftnum3;
        [self addSubview:txtgraftnum3];
        
        UILabel *glblgraftNum3 = [[UILabel alloc] initWithFrame:CGRectMake(100, 5, 45, 21)];
        glblgraftNum3.textAlignment = UITextAlignmentRight;
        glblgraftNum3.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum3.layer.borderWidth = 1;
        glblgraftNum3.textColor = [UIColor blackColor];
        glblgraftNum3.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum3.backgroundColor = [UIColor clearColor];
        self.lblgraftNum3 = glblgraftNum3;
        [self addSubview:lblgraftNum3];
        
        UIImageView *gimgpicture3= [[UIImageView alloc]initWithFrame:CGRectMake(110, 5, 20, 21)];        
        gimgpicture3.backgroundColor=[UIColor blackColor];
        self.imgpicture3=gimgpicture3;
        [self addSubview:imgpicture3];
        
        /*===============column 4======================*/
        UITextField *gtxtgraftnum4 = [[UITextField alloc] initWithFrame:CGRectMake(150, 5, 50, 30)];
        gtxtgraftnum4.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum4.textColor = [UIColor blackColor];
        gtxtgraftnum4.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum4.layer.borderWidth = 1;
        gtxtgraftnum4.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum4.backgroundColor = [UIColor clearColor];
        self.txtgraftnum4 = gtxtgraftnum4;
        [self addSubview:txtgraftnum4];
        
        UILabel *glblgraftNum4 = [[UILabel alloc] initWithFrame:CGRectMake(150, 5, 45, 21)];
        glblgraftNum4.textAlignment = UITextAlignmentRight;
        glblgraftNum4.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum4.layer.borderWidth = 1;
        glblgraftNum4.textColor = [UIColor blackColor];
        glblgraftNum4.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum4.backgroundColor = [UIColor clearColor];
        self.lblgraftNum4 = glblgraftNum4;
        [self addSubview:lblgraftNum4];
        
        UIImageView *gimgpicture4= [[UIImageView alloc]initWithFrame:CGRectMake(160, 5, 20, 21)];        
        gimgpicture4.backgroundColor=[UIColor blackColor];
        self.imgpicture4=gimgpicture4;
        [self addSubview:imgpicture4];
        
        /*===============column 5======================*/
        UITextField *gtxtgraftnum5 = [[UITextField alloc] initWithFrame:CGRectMake(200, 5, 50, 30)];
        gtxtgraftnum5.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum5.textColor = [UIColor blackColor];
        gtxtgraftnum5.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum5.layer.borderWidth = 1;
        gtxtgraftnum5.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum5.backgroundColor = [UIColor clearColor];
        self.txtgraftnum5 = gtxtgraftnum5;
        [self addSubview:txtgraftnum5];
        
        UILabel *glblgraftNum5 = [[UILabel alloc] initWithFrame:CGRectMake(200, 5, 45, 21)];
        glblgraftNum5.textAlignment = UITextAlignmentRight;
        glblgraftNum5.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum5.layer.borderWidth = 1;
        glblgraftNum5.textColor = [UIColor blackColor];
        glblgraftNum5.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum5.backgroundColor = [UIColor clearColor];
        self.lblgraftNum5 = glblgraftNum5;
        [self addSubview:lblgraftNum5];
        
        UIImageView *gimgpicture5= [[UIImageView alloc]initWithFrame:CGRectMake(210, 5, 20, 21)];        
        gimgpicture5.backgroundColor=[UIColor blackColor];
        self.imgpicture5=gimgpicture5;
        [self addSubview:imgpicture5];
        
        /*===============column 6======================*/
        UITextField *gtxtgraftnum6 = [[UITextField alloc] initWithFrame:CGRectMake(250, 5, 50, 30)];
        gtxtgraftnum6.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum6.textColor = [UIColor blackColor];
        gtxtgraftnum6.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum6.layer.borderWidth = 1;
        gtxtgraftnum6.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum6.backgroundColor = [UIColor clearColor];
        self.txtgraftnum6 = gtxtgraftnum6;
        [self addSubview:txtgraftnum6];
        
        UILabel *glblgraftNum6 = [[UILabel alloc] initWithFrame:CGRectMake(250, 5, 45, 21)];
        glblgraftNum6.textAlignment = UITextAlignmentRight;
        glblgraftNum6.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum6.layer.borderWidth = 1;
        glblgraftNum6.textColor = [UIColor blackColor];
        glblgraftNum6.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum6.backgroundColor = [UIColor clearColor];
        self.lblgraftNum6 = glblgraftNum6;
        [self addSubview:lblgraftNum6];
        
        UIImageView *gimgpicture6= [[UIImageView alloc]initWithFrame:CGRectMake(260, 5, 20, 21)];        
        gimgpicture6.backgroundColor=[UIColor blackColor];
        self.imgpicture6=gimgpicture6;
        [self addSubview:imgpicture6];

        /*===============column 7======================*/
        UITextField *gtxtgraftnum7 = [[UITextField alloc] initWithFrame:CGRectMake(300, 5, 50, 30)];
        gtxtgraftnum7.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum7.textColor = [UIColor blackColor];
        gtxtgraftnum7.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum7.layer.borderWidth = 1;
        gtxtgraftnum7.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum7.backgroundColor = [UIColor clearColor];
        self.txtgraftnum7 = gtxtgraftnum7;
        [self addSubview:txtgraftnum7];
        
        UILabel *glblgraftNum7 = [[UILabel alloc] initWithFrame:CGRectMake(300, 5, 45, 21)];
        glblgraftNum7.textAlignment = UITextAlignmentRight;
        glblgraftNum7.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum7.layer.borderWidth = 1;
        glblgraftNum7.textColor = [UIColor blackColor];
        glblgraftNum7.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum7.backgroundColor = [UIColor clearColor];
        self.lblgraftNum7 = glblgraftNum7;
        [self addSubview:lblgraftNum7];
        
        UIImageView *gimgpicture7= [[UIImageView alloc]initWithFrame:CGRectMake(310, 5, 20, 21)];        
        gimgpicture7.backgroundColor=[UIColor blackColor];
        self.imgpicture7=gimgpicture7;
        [self addSubview:imgpicture7];
        
        /*===============column 8======================*/
        UITextField *gtxtgraftnum8 = [[UITextField alloc] initWithFrame:CGRectMake(350, 5, 50, 30)];
        gtxtgraftnum8.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum8.textColor = [UIColor blackColor];
        gtxtgraftnum8.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum8.layer.borderWidth = 1;
        gtxtgraftnum8.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum8.backgroundColor = [UIColor clearColor];
        self.txtgraftnum8 = gtxtgraftnum8;
        [self addSubview:txtgraftnum8];
        
        UILabel *glblgraftNum8 = [[UILabel alloc] initWithFrame:CGRectMake(350, 5, 45, 21)];
        glblgraftNum8.textAlignment = UITextAlignmentRight;
        glblgraftNum8.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum8.layer.borderWidth = 1;
        glblgraftNum8.textColor = [UIColor blackColor];
        glblgraftNum8.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum8.backgroundColor = [UIColor clearColor];
        self.lblgraftNum8 = glblgraftNum8;
        [self addSubview:lblgraftNum8];
        
        UIImageView *gimgpicture8= [[UIImageView alloc]initWithFrame:CGRectMake(360, 5, 20, 21)];        
        gimgpicture8.backgroundColor=[UIColor blackColor];
        self.imgpicture8=gimgpicture8;
        [self addSubview:imgpicture8];
        
        /*===============column 9======================*/
        UITextField *gtxtgraftnum9 = [[UITextField alloc] initWithFrame:CGRectMake(400, 5, 50, 30)];
        gtxtgraftnum9.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum9.textColor = [UIColor blackColor];
        gtxtgraftnum9.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum9.layer.borderWidth = 1;
        gtxtgraftnum9.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum9.backgroundColor = [UIColor clearColor];
        self.txtgraftnum9 = gtxtgraftnum9;
        [self addSubview:txtgraftnum9];
        
        UILabel *glblgraftNum9 = [[UILabel alloc] initWithFrame:CGRectMake(400, 5, 45, 21)];
        glblgraftNum9.textAlignment = UITextAlignmentRight;
        glblgraftNum9.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum9.layer.borderWidth = 1;
        glblgraftNum9.textColor = [UIColor blackColor];
        glblgraftNum9.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum9.backgroundColor = [UIColor clearColor];
        self.lblgraftNum9 = glblgraftNum9;
        [self addSubview:lblgraftNum9];
        
        UIImageView *gimgpicture9= [[UIImageView alloc]initWithFrame:CGRectMake(410, 5, 20, 21)];        
        gimgpicture9.backgroundColor=[UIColor blackColor];
        self.imgpicture9=gimgpicture9;
        [self addSubview:imgpicture9];
        
        /*===============column 10======================*/
        UITextField *gtxtgraftnum10 = [[UITextField alloc] initWithFrame:CGRectMake(450, 5, 50, 30)];
        gtxtgraftnum10.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum10.textColor = [UIColor blackColor];
        gtxtgraftnum10.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum10.layer.borderWidth = 1;
        gtxtgraftnum10.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum10.backgroundColor = [UIColor clearColor];
        self.txtgraftnum10 = gtxtgraftnum10;
        [self addSubview:txtgraftnum10];
        
        UILabel *glblgraftNum10 = [[UILabel alloc] initWithFrame:CGRectMake(450, 5, 45, 21)];
        glblgraftNum10.textAlignment = UITextAlignmentRight;
        glblgraftNum10.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum10.layer.borderWidth = 1;
        glblgraftNum10.textColor = [UIColor blackColor];
        glblgraftNum10.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum10.backgroundColor = [UIColor clearColor];
        self.lblgraftNum10 = glblgraftNum10;
        [self addSubview:lblgraftNum10];
        
        UIImageView *gimgpicture10= [[UIImageView alloc]initWithFrame:CGRectMake(460, 5, 20, 21)];        
        gimgpicture10.backgroundColor=[UIColor blackColor];
        self.imgpicture10=gimgpicture10;
        [self addSubview:imgpicture10];
        
        /*===============column 11======================*/
        UITextField *gtxtgraftnum11 = [[UITextField alloc] initWithFrame:CGRectMake(500, 5, 50, 30)];
        gtxtgraftnum11.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum11.textColor = [UIColor blackColor];
        gtxtgraftnum11.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum11.layer.borderWidth = 1;
        gtxtgraftnum11.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum11.backgroundColor = [UIColor clearColor];
        self.txtgraftnum11 = gtxtgraftnum11;
        [self addSubview:txtgraftnum11];
        
        UILabel *glblgraftNum11 = [[UILabel alloc] initWithFrame:CGRectMake(500, 5, 45, 21)];
        glblgraftNum11.textAlignment = UITextAlignmentRight;
        glblgraftNum11.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum11.layer.borderWidth = 1;
        glblgraftNum11.textColor = [UIColor blackColor];
        glblgraftNum11.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum11.backgroundColor = [UIColor clearColor];
        self.lblgraftNum11 = glblgraftNum11;
        [self addSubview:lblgraftNum11];
        
        UIImageView *gimgpicture11= [[UIImageView alloc]initWithFrame:CGRectMake(510, 5, 20, 21)];        
        gimgpicture11.backgroundColor=[UIColor blackColor];
        self.imgpicture11=gimgpicture11;
        [self addSubview:imgpicture11];
        
        /*===============column 12======================*/
        UITextField *gtxtgraftnum12 = [[UITextField alloc] initWithFrame:CGRectMake(550, 5, 50, 30)];
        gtxtgraftnum12.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum12.textColor = [UIColor blackColor];
        gtxtgraftnum12.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum12.layer.borderWidth = 1;
        gtxtgraftnum12.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum12.backgroundColor = [UIColor clearColor];
        self.txtgraftnum12 = gtxtgraftnum12;
        [self addSubview:txtgraftnum12];
        
        UILabel *glblgraftNum12 = [[UILabel alloc] initWithFrame:CGRectMake(550, 5, 45, 21)];
        glblgraftNum12.textAlignment = UITextAlignmentRight;
        glblgraftNum12.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum12.layer.borderWidth = 1;
        glblgraftNum12.textColor = [UIColor blackColor];
        glblgraftNum12.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum12.backgroundColor = [UIColor clearColor];
        self.lblgraftNum12 = glblgraftNum12;
        [self addSubview:lblgraftNum12];
        
        UIImageView *gimgpicture12= [[UIImageView alloc]initWithFrame:CGRectMake(560, 5, 20, 21)];        
        gimgpicture12.backgroundColor=[UIColor blackColor];
        self.imgpicture12=gimgpicture12;
        [self addSubview:imgpicture12];
        
        /*===============column 13======================*/
        UITextField *gtxtgraftnum13 = [[UITextField alloc] initWithFrame:CGRectMake(600, 5, 50, 30)];
        gtxtgraftnum13.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum13.textColor = [UIColor blackColor];
        gtxtgraftnum13.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum13.layer.borderWidth = 1;
        gtxtgraftnum13.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum13.backgroundColor = [UIColor clearColor];
        self.txtgraftnum13 = gtxtgraftnum13;
        [self addSubview:txtgraftnum13];
        
        UILabel *glblgraftNum13 = [[UILabel alloc] initWithFrame:CGRectMake(600, 5, 45, 21)];
        glblgraftNum13.textAlignment = UITextAlignmentRight;
        glblgraftNum13.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum13.layer.borderWidth = 1;
        glblgraftNum13.textColor = [UIColor blackColor];
        glblgraftNum13.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum13.backgroundColor = [UIColor clearColor];
        self.lblgraftNum13 = glblgraftNum13;
        [self addSubview:lblgraftNum13];
        
        UIImageView *gimgpicture13= [[UIImageView alloc]initWithFrame:CGRectMake(610, 5, 20, 21)];        
        gimgpicture13.backgroundColor=[UIColor blackColor];
        self.imgpicture13=gimgpicture13;
        [self addSubview:imgpicture13];
        
        /*===============column 14======================*/
        UITextField *gtxtgraftnum14 = [[UITextField alloc] initWithFrame:CGRectMake(650, 5, 50, 30)];
        gtxtgraftnum14.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum14.textColor = [UIColor blackColor];
        gtxtgraftnum14.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum14.layer.borderWidth = 1;
        gtxtgraftnum14.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum14.backgroundColor = [UIColor clearColor];
        self.txtgraftnum14 = gtxtgraftnum14;
        [self addSubview:txtgraftnum14];
        
        UILabel *glblgraftNum14 = [[UILabel alloc] initWithFrame:CGRectMake(650, 5, 45, 21)];
        glblgraftNum14.textAlignment = UITextAlignmentRight;
        glblgraftNum14.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum14.layer.borderWidth = 1;
        glblgraftNum14.textColor = [UIColor blackColor];
        glblgraftNum14.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum14.backgroundColor = [UIColor clearColor];
        self.lblgraftNum14 = glblgraftNum14;
        [self addSubview:lblgraftNum14];
        
        UIImageView *gimgpicture14= [[UIImageView alloc]initWithFrame:CGRectMake(660, 5, 20, 21)];        
        gimgpicture14.backgroundColor=[UIColor blackColor];
        self.imgpicture14=gimgpicture14;
        [self addSubview:imgpicture14];
        
        /*===============column 15======================*/
        UITextField *gtxtgraftnum15 = [[UITextField alloc] initWithFrame:CGRectMake(700, 5, 50, 30)];
        gtxtgraftnum15.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum15.textColor = [UIColor blackColor];
        gtxtgraftnum15.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum15.layer.borderWidth = 1;
        gtxtgraftnum15.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum15.backgroundColor = [UIColor clearColor];
        self.txtgraftnum15 = gtxtgraftnum15;
        [self addSubview:txtgraftnum15];
        
        UILabel *glblgraftNum15 = [[UILabel alloc] initWithFrame:CGRectMake(700, 5, 45, 21)];
        glblgraftNum15.textAlignment = UITextAlignmentRight;
        glblgraftNum15.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum15.layer.borderWidth = 1;
        glblgraftNum15.textColor = [UIColor blackColor];
        glblgraftNum15.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum15.backgroundColor = [UIColor clearColor];
        self.lblgraftNum15 = glblgraftNum15;
        [self addSubview:lblgraftNum15];
        
        UIImageView *gimgpicture15= [[UIImageView alloc]initWithFrame:CGRectMake(710, 5, 20, 21)];        
        gimgpicture15.backgroundColor=[UIColor blackColor];
        self.imgpicture15=gimgpicture15;
        [self addSubview:imgpicture15];
        
        /*===============column 16======================*/
        UITextField *gtxtgraftnum16 = [[UITextField alloc] initWithFrame:CGRectMake(750, 5, 50, 30)];
        gtxtgraftnum16.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum16.textColor = [UIColor blackColor];
        gtxtgraftnum16.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum16.layer.borderWidth = 1;
        gtxtgraftnum16.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum16.backgroundColor = [UIColor clearColor];
        self.txtgraftnum16 = gtxtgraftnum16;
        [self addSubview:txtgraftnum16];
        
        UILabel *glblgraftNum16 = [[UILabel alloc] initWithFrame:CGRectMake(750, 5, 45, 21)];
        glblgraftNum16.textAlignment = UITextAlignmentRight;
        glblgraftNum16.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum16.layer.borderWidth = 1;
        glblgraftNum16.textColor = [UIColor blackColor];
        glblgraftNum16.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum16.backgroundColor = [UIColor clearColor];
        self.lblgraftNum16 = glblgraftNum16;
        [self addSubview:lblgraftNum16];
        
        UIImageView *gimgpicture16= [[UIImageView alloc]initWithFrame:CGRectMake(760, 5, 20, 21)];        
        gimgpicture16.backgroundColor=[UIColor blackColor];
        self.imgpicture16=gimgpicture16;
        [self addSubview:imgpicture16];
        
        /*===============column 17======================*/
        UITextField *gtxtgraftnum17 = [[UITextField alloc] initWithFrame:CGRectMake(800, 5, 50, 30)];
        gtxtgraftnum17.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum17.textColor = [UIColor blackColor];
        gtxtgraftnum17.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum17.layer.borderWidth = 1;
        gtxtgraftnum17.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum17.backgroundColor = [UIColor clearColor];
        self.txtgraftnum17 = gtxtgraftnum17;
        [self addSubview:txtgraftnum17];
        
        UILabel *glblgraftNum17 = [[UILabel alloc] initWithFrame:CGRectMake(800, 5, 45, 21)];
        glblgraftNum17.textAlignment = UITextAlignmentRight;
        glblgraftNum17.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum17.layer.borderWidth = 1;
        glblgraftNum17.textColor = [UIColor blackColor];
        glblgraftNum17.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum17.backgroundColor = [UIColor clearColor];
        self.lblgraftNum17 = glblgraftNum17;
        [self addSubview:lblgraftNum17];
        
        UIImageView *gimgpicture17= [[UIImageView alloc]initWithFrame:CGRectMake(810, 5, 20, 21)];        
        gimgpicture17.backgroundColor=[UIColor blackColor];
        self.imgpicture17=gimgpicture17;
        [self addSubview:imgpicture17];

        /*===============column 18======================*/
        UITextField *gtxtgraftnum18 = [[UITextField alloc] initWithFrame:CGRectMake(850, 5, 50, 30)];
        gtxtgraftnum18.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum18.textColor = [UIColor blackColor];
        gtxtgraftnum18.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum18.layer.borderWidth = 1;
        gtxtgraftnum18.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum18.backgroundColor = [UIColor clearColor];
        self.txtgraftnum18 = gtxtgraftnum18;
        [self addSubview:txtgraftnum18];
        
        UILabel *glblgraftNum18 = [[UILabel alloc] initWithFrame:CGRectMake(850, 5, 45, 21)];
        glblgraftNum18.textAlignment = UITextAlignmentRight;
        glblgraftNum18.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum18.layer.borderWidth = 1;
        glblgraftNum18.textColor = [UIColor blackColor];
        glblgraftNum18.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum18.backgroundColor = [UIColor clearColor];
        self.lblgraftNum18 = glblgraftNum18;
        [self addSubview:lblgraftNum18];
        
        UIImageView *gimgpicture18= [[UIImageView alloc]initWithFrame:CGRectMake(860, 5, 20, 21)];        
        gimgpicture18.backgroundColor=[UIColor blackColor];
        self.imgpicture18=gimgpicture18;
        [self addSubview:imgpicture18];
        
        /*===============column 19======================*/
        UITextField *gtxtgraftnum19 = [[UITextField alloc] initWithFrame:CGRectMake(900, 5, 50, 30)];
        gtxtgraftnum19.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum19.textColor = [UIColor blackColor];
        gtxtgraftnum19.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum19.layer.borderWidth = 1;
        gtxtgraftnum19.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum19.backgroundColor = [UIColor clearColor];
        self.txtgraftnum19 = gtxtgraftnum19;
        [self addSubview:txtgraftnum19];
        
        UILabel *glblgraftNum19 = [[UILabel alloc] initWithFrame:CGRectMake(900, 5, 45, 21)];
        glblgraftNum19.textAlignment = UITextAlignmentRight;
        glblgraftNum19.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum19.layer.borderWidth = 1;
        glblgraftNum19.textColor = [UIColor blackColor];
        glblgraftNum19.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum19.backgroundColor = [UIColor clearColor];
        self.lblgraftNum19 = glblgraftNum19;
        [self addSubview:lblgraftNum19];
        
        UIImageView *gimgpicture19= [[UIImageView alloc]initWithFrame:CGRectMake(910, 5, 20, 21)];        
        gimgpicture19.backgroundColor=[UIColor blackColor];
        self.imgpicture19=gimgpicture19;
        [self addSubview:imgpicture19];
        
        /*===============column 20======================*/
        UITextField *gtxtgraftnum20 = [[UITextField alloc] initWithFrame:CGRectMake(950, 5, 50, 30)];
        gtxtgraftnum20.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum20.textColor = [UIColor blackColor];
        gtxtgraftnum20.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum20.layer.borderWidth = 1;
        gtxtgraftnum20.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum20.backgroundColor = [UIColor clearColor];
        self.txtgraftnum20 = gtxtgraftnum20;
        [self addSubview:txtgraftnum20];
        
        UILabel *glblgraftNum20 = [[UILabel alloc] initWithFrame:CGRectMake(950, 5, 45, 21)];
        glblgraftNum20.textAlignment = UITextAlignmentRight;
        glblgraftNum20.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum20.layer.borderWidth = 1;
        glblgraftNum20.textColor = [UIColor blackColor];
        glblgraftNum20.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum20.backgroundColor = [UIColor clearColor];
        self.lblgraftNum20 = glblgraftNum20;
        [self addSubview:lblgraftNum20];
        
        UIImageView *gimgpicture20= [[UIImageView alloc]initWithFrame:CGRectMake(960, 5, 20, 21)];        
        gimgpicture20.backgroundColor=[UIColor blackColor];
        self.imgpicture20=gimgpicture20;
        [self addSubview:imgpicture20];
        
        /*===============column 21======================*/
        UITextField *gtxtgraftnum21 = [[UITextField alloc] initWithFrame:CGRectMake(1000, 5, 50, 30)];
        gtxtgraftnum21.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum21.textColor = [UIColor blackColor];
        gtxtgraftnum21.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum21.layer.borderWidth = 1;
        gtxtgraftnum21.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum21.backgroundColor = [UIColor clearColor];
        self.txtgraftnum21 = gtxtgraftnum21;
        [self addSubview:txtgraftnum21];
        
        UILabel *glblgraftNum21 = [[UILabel alloc] initWithFrame:CGRectMake(1000, 5, 45, 21)];
        glblgraftNum21.textAlignment = UITextAlignmentRight;
        glblgraftNum21.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum21.layer.borderWidth = 1;
        glblgraftNum21.textColor = [UIColor blackColor];
        glblgraftNum21.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum21.backgroundColor = [UIColor clearColor];
        self.lblgraftNum21 = glblgraftNum21;
        [self addSubview:lblgraftNum21];
        
        UIImageView *gimgpicture21= [[UIImageView alloc]initWithFrame:CGRectMake(1010, 5, 20, 21)];        
        gimgpicture21.backgroundColor=[UIColor blackColor];
        self.imgpicture21=gimgpicture21;
        [self addSubview:imgpicture21];
        
        /*===============column 22======================*/
        UITextField *gtxtgraftnum22 = [[UITextField alloc] initWithFrame:CGRectMake(1050, 5, 50, 30)];
        gtxtgraftnum22.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum22.textColor = [UIColor blackColor];
        gtxtgraftnum22.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum22.layer.borderWidth = 1;
        gtxtgraftnum22.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum22.backgroundColor = [UIColor clearColor];
        self.txtgraftnum22 = gtxtgraftnum22;
        [self addSubview:txtgraftnum22];
        
        UILabel *glblgraftNum22 = [[UILabel alloc] initWithFrame:CGRectMake(1050, 5, 45, 21)];
        glblgraftNum22.textAlignment = UITextAlignmentRight;
        glblgraftNum22.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum22.layer.borderWidth = 1;
        glblgraftNum22.textColor = [UIColor blackColor];
        glblgraftNum22.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum22.backgroundColor = [UIColor clearColor];
        self.lblgraftNum22 = glblgraftNum22;
        [self addSubview:lblgraftNum22];
        
        UIImageView *gimgpicture22= [[UIImageView alloc]initWithFrame:CGRectMake(1060, 5, 20, 21)];        
        gimgpicture22.backgroundColor=[UIColor blackColor];
        self.imgpicture22=gimgpicture22;
        [self addSubview:imgpicture22];
        
        /*===============column 23======================*/
        UITextField *gtxtgraftnum23 = [[UITextField alloc] initWithFrame:CGRectMake(1100, 5, 50, 30)];
        gtxtgraftnum23.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum23.textColor = [UIColor blackColor];
        gtxtgraftnum23.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum23.layer.borderWidth = 1;
        gtxtgraftnum23.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum23.backgroundColor = [UIColor clearColor];
        self.txtgraftnum23 = gtxtgraftnum23;
        [self addSubview:txtgraftnum23];
        
        UILabel *glblgraftNum23 = [[UILabel alloc] initWithFrame:CGRectMake(1100, 5, 45, 21)];
        glblgraftNum23.textAlignment = UITextAlignmentRight;
        glblgraftNum23.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum23.layer.borderWidth = 1;
        glblgraftNum23.textColor = [UIColor blackColor];
        glblgraftNum23.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum23.backgroundColor = [UIColor clearColor];
        self.lblgraftNum23 = glblgraftNum23;
        [self addSubview:lblgraftNum23];
        
        UIImageView *gimgpicture23= [[UIImageView alloc]initWithFrame:CGRectMake(1110, 5, 20, 21)];        
        gimgpicture23.backgroundColor=[UIColor blackColor];
        self.imgpicture23=gimgpicture23;
        [self addSubview:imgpicture23];
        
        /*===============column 24======================*/
        UITextField *gtxtgraftnum24 = [[UITextField alloc] initWithFrame:CGRectMake(1150, 5, 50, 30)];
        gtxtgraftnum24.textAlignment = UITextAlignmentCenter;
        gtxtgraftnum24.textColor = [UIColor blackColor];
        gtxtgraftnum24.layer.borderColor=[UIColor blackColor].CGColor;
        gtxtgraftnum24.layer.borderWidth = 1;
        gtxtgraftnum24.font = [UIFont fontWithName:@"Helvetica" size:15];
        gtxtgraftnum24.backgroundColor = [UIColor clearColor];
        self.txtgraftnum24 = gtxtgraftnum24;
        [self addSubview:txtgraftnum24];
        
        UILabel *glblgraftNum24 = [[UILabel alloc] initWithFrame:CGRectMake(1150, 5, 45, 21)];
        glblgraftNum24.textAlignment = UITextAlignmentRight;
        glblgraftNum24.layer.borderColor=[UIColor blackColor].CGColor;
        glblgraftNum24.layer.borderWidth = 1;
        glblgraftNum24.textColor = [UIColor blackColor];
        glblgraftNum24.font = [UIFont fontWithName:@"Helvetica" size:15];
        glblgraftNum24.backgroundColor = [UIColor clearColor];
        self.lblgraftNum24 = glblgraftNum24;
        [self addSubview:lblgraftNum24];
        
        UIImageView *gimgpicture24= [[UIImageView alloc]initWithFrame:CGRectMake(1160, 5, 20, 21)];        
        gimgpicture24.backgroundColor=[UIColor blackColor];
        self.imgpicture24=gimgpicture24;
        [self addSubview:imgpicture24];


        
    }
    return self;
}



-(void) singletapcaptured:(UIGestureRecognizer *)gesture
{
    NSLog(@"hello~~");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
