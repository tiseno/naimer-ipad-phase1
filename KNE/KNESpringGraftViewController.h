//
//  KNESpringGraftViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNESpringGraftCell.h"
#import "KNESingleGroup.h"
#import "KNEDoubleGroup.h"

@class KNEStartBallViewController;
@interface KNESpringGraftViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>{
    
}

@property (nonatomic, strong) NSArray *numarr;
@property (nonatomic, strong) KNEStartBallViewController *gKNEStartBallViewController;

@end
