//
//  KNESpringGraftViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNESpringGraftViewController.h"

@interface KNESpringGraftViewController ()

@end

@implementation KNESpringGraftViewController
@synthesize numarr, gKNEStartBallViewController;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        
        
        //numarr=[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24", nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.numarr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //KNESpringReturnCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    KNESpringGraftCell *cell = [[KNESpringGraftCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    //BpEvent* gBpEventcell=[self.numarr objectAtIndex:indexPath.row];
    //KNESingleGroup *gKNESingleGroup=[self.numarr ];
    
   
    cell.lblgraftNum1.text=cell.txtgraftnum1.text;
    //cell.lblgraftNum1.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum2.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum3.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum4.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum5.text=[self.numarr objectAtIndex:indexPath.row];
    
    cell.lblgraftNum6.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum7.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum8.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum9.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum10.text=[self.numarr objectAtIndex:indexPath.row];
    
    cell.lblgraftNum11.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum12.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum13.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum14.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum15.text=[self.numarr objectAtIndex:indexPath.row];
    
    cell.lblgraftNum16.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum17.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum18.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum19.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum20.text=[self.numarr objectAtIndex:indexPath.row];
    
    cell.lblgraftNum21.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum22.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum23.text=[self.numarr objectAtIndex:indexPath.row];
    cell.lblgraftNum24.text=[self.numarr objectAtIndex:indexPath.row];
    
        
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    /*if ( ( textField != inputMenge ) && ( textField != inputAlter ) ) {
     NSTimeInterval animationDuration = 0.300000011920929;
     CGRect frame = self.view.frame;
     frame.origin.y += kOFFSET_FOR_KEYBOARD;
     frame.size.height -= kOFFSET_FOR_KEYBOARD;
     [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
     [UIView setAnimationDuration:animationDuration];
     self.view.frame = frame;
     [UIView commitAnimations];              
     }
    
    KNESpringGraftCell *cell = (KNESpringGraftCell*) [[textField superview] superview];
    [self.tblgraft scrollToRowAtIndexPath:[self.tblgraft indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionMiddle animated:YES]; 
    */
    NSLog(@"yoyo~~2end~~");
    // Additional Code
}

@end
