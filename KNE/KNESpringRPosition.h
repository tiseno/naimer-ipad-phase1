//
//  KNESpringRPosition.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/25/12.
//
//

#import <Foundation/Foundation.h>
#import "KNECase.h"

@interface KNESpringRPosition : KNECase{
    
}

@property (nonatomic,strong) NSString *SpringReturnID;
@property (nonatomic, strong) NSString *SRStartPoint;
@property (nonatomic, strong) NSString *SREndPoint;


@end
