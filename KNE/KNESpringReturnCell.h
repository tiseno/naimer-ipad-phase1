//
//  KNESpringReturnCell.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "springReturndelegate.h"
#import "KNEAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface KNESpringReturnCell : UITableViewCell{
    
    KNESpringReturnCell *cell;
    int txtindexpath;
}

//@property(nonatomic,retain) NSString *firstpositionnum;
//@property (nonatomic, strong) NSArray *numarr;
//@property (nonatomic, strong) id<springReturndelegate> delegate;
@property (nonatomic,strong) UITextField *txtuserremark;
@property (nonatomic, strong) UILabel *lblSpringNum;
//@property (nonatomic, strong) NSArray *numarr;
@property (nonatomic, strong) UIButton *firstPositionNum;
@end
