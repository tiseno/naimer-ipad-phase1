//
//  KNESpringReturnCell.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNESpringReturnCell.h"

@implementation KNESpringReturnCell
@synthesize txtuserremark, lblSpringNum;
//@synthesize numarr, firstPositionNum, delegate;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        /*UITextField *glblBodytext = [[UITextField alloc] initWithFrame:CGRectMake(10, 5, 50, 21)];
        glblBodytext.textAlignment = UITextAlignmentLeft;
        glblBodytext.textColor = [UIColor blackColor];
        glblBodytext.text=@"hello~";
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytext.backgroundColor = [UIColor clearColor];
        self.txtuserremark = glblBodytext;
        [glblBodytext release];
        [self addSubview:txtuserremark];
        */
        UILabel *glblSpringNum = [[UILabel alloc] initWithFrame:CGRectMake(-1, 0, 48, 27)];
        glblSpringNum.textAlignment = UITextAlignmentCenter;
        glblSpringNum.textColor = [UIColor blackColor];
        glblSpringNum.layer.borderColor=[UIColor blackColor].CGColor;
        glblSpringNum.layer.borderWidth = 1;
        glblSpringNum.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblSpringNum.backgroundColor = [UIColor yellowColor];
        self.lblSpringNum = glblSpringNum;
        [self addSubview:lblSpringNum];
        
        //UIButton *gfirstPositionNum = [[UIButton alloc] initWithFrame:CGRectMake(130, 5, 50, 21)];
//        UIButton *gfirstPositionNum = [[UIButton alloc] initWithFrame:CGRectMake(0, 2, 45, 21)];
//        [gfirstPositionNum addTarget:self action:@selector(reloadtblSpringReturn) forControlEvents:UIControlEventTouchDown];
//        gfirstPositionNum.backgroundColor=[UIColor clearColor];
//        self.firstPositionNum = gfirstPositionNum;
//        [self addSubview:firstPositionNum];
        
    }
    return self;
} 
/*
-(void)reloadtblSpringReturn
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.firstPnum=self.lblSpringNum.text;
    
    [self.delegate reloadtblSpringReturn];
    //[self.tblSpringReturn reloadData];
}
*/
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
