//
//  KNESpringReturnContain.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/3/12.
//
//

#import <Foundation/Foundation.h>
#import "KNECase.h"

@interface KNESpringReturnContain : KNECase
{
    
}
@property (nonatomic,strong) NSString *ContainID;
@property (nonatomic,strong) NSString *ContainTag;
@property (nonatomic,strong) NSString *ContainValue;

@end
