//
//  KNESpringReturnViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNESpringReturnCell.h"
#import "KNEAppDelegate.h"
//#import "springReturndelegate.h"
//@class KNEEditSRViewController;
//@class KNEStartBallViewController;

@interface KNESpringReturnViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>{
    KNESpringReturnCell *cell;
    int txtindexpath;
}

//@property(nonatomic,retain) NSString *firstpositionnum;
@property (nonatomic, strong) NSArray *numarr;
//@property (nonatomic, strong) KNEStartBallViewController *gKNEStartBallViewController;

//@property (nonatomic, retain) id<springReturndelegate> delegate;
-(void)getfirstpNum; 
@end
