//
//  KNESpringReturnViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNESpringReturnViewController.h"

@interface KNESpringReturnViewController ()

@end

@implementation KNESpringReturnViewController
@synthesize numarr;//, gKNEStartBallViewController;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        [self getfirstpNum];
        
        //numarr=[[NSArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24", nil];
    }
    return self;
}

-(void)getfirstpNum
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.firstPnum=@"330";
    
    int fP=[appDelegate.firstPnum intValue];
    int firstposition = fP-15;
    //int rowvalue=0;
    
    NSMutableArray *graftNumbermArr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<24;i++)
    {
        
        
        
        if (firstposition >=360) {
            firstposition=15;
        }else {
            firstposition+=15;
        }
        
        
        //NSLog(@"firstposition--->>>>%d",firstposition);
        
        
        
        NSString *invalue=[[NSString alloc]initWithFormat:@"%d",firstposition];
        
        if ([invalue isEqualToString:@"360"]) {
            invalue=@"0";
        }
        [graftNumbermArr addObject:invalue];
        
    }
    
    self.numarr=graftNumbermArr;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    //[self getfirstpNum];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return numarr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //KNESpringReturnCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[KNESpringReturnCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
   
    //BpEvent* gBpEventcell=[self.numarr objectAtIndex:indexPath.row];
    //NSString *springnum=[[NSString alloc]initWithFormat:@"%@",[self.numarr objectAtIndex:indexPath.row]];
    
    //cell.delegate=self.gKNEStartBallViewController;
    //cell.delegate=self.gKNESwitchShowViewController;
    
    
    cell.lblSpringNum.text=[self.numarr objectAtIndex:indexPath.row];
    txtindexpath=indexPath.row;
    
    cell.txtuserremark.delegate = self;
    //cell.txtuserremark.tag = indexPath.row;
    //if(indexPath.row==3)
    //{
    //    [cell.txtuserremark becomeFirstResponder];
    //}
    
    //[cell.txtuserremark becomeFirstResponder];
    
    /*static NSString *CellIdentifier = @"Cell";
    //BpEventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[BpEventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    BpEvent* gBpEventcell=[self.eventArr objectAtIndex:indexPath.row];
    
    cell.delegate =self.gBpEventViewController;
    cell.BpEventcell=gBpEventcell;
    cell.lblTitle.text=gBpEventcell.eventsTitle;
    
    cell.imgsmallflag.image=[UIImage imageNamed:@"icon_events_2.png"];;
    */

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    cell= (KNESpringReturnCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    NSLog(@"indexPath>>>%d",indexPath.row);
    
    
        NSString* key=[numarr objectAtIndex:indexPath.row];
        
        NSLog(@"key>>>%@",key);
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.firstPnum=key;
    
    [self getfirstpNum];

//        
}

/*
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"textField>>>end!!");
 
 
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    //UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //[self.tableView setContentOffset:CGPointMake(0, cell.frame.size.height) animated:YES];
    
    cell= (KNESpringReturnCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    cell.delegate=self.gKNEStartBallViewController;
    
    cell.txtuserremark.tag = indexPath.row;
    
    cell.txtuserremark.delegate = self;
    if(indexPath.row==3)
    {
        [cell.txtuserremark becomeFirstResponder];
    }
     NSLog(@"textFieldDidEndEditing indexPath>>>%d",indexPath.row);

}
*/
@end
