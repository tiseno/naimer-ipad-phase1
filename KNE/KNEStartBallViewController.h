//
//  KNEStartBallViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
//#import "KNESpringReturnViewController.h"
#import "KNESpringGraftViewController.h"
#import "KNESpringGraftCell.h"
#import "ButtonWithGraftindex.h"
#import "KNESpringRPosition.h"
#import "KNEAppDelegate.h"
#import "KNETblGraftPositionTag.h"
#import "DBTableGraft.h"
#import "KNESwitchShowViewController.h"
#import "DBCase.h"
#import "DBSpringReturn.h"
#import "KNEindex.h"
//#import "KNEJumperView.h"
#import "KNESpringReturnContain.h"
#import "DBSpringReturnContain.h"
#import "DBSwitchingfirstValue.h"
#import "KNESwitchingFirstValue.h"
#import "jumperDelegate.h"
#import "JumperView.h"
#import "KNESwitchPoint.h"
#import "SwitchMainPageViewController.h"
#import "KNEjumperSwitchAngle.h"
#import "PlateInfo.h"
#import "KNEMenuPageViewController.h"
#import "KNESpringReturnCell.h"
#import "MBProgressHUD.h"

@interface KNEStartBallViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UIScrollViewDelegate>
{
    int rows;
    int maxNumberOfLines;
    //NSMutableArray *springReturnPosition;
    
    KNESpringReturnCell *cell;
    int txtindexpath;
    
    NSMutableArray *graftNumbermArr;
    NSMutableArray *graftNumbermArr2;
    BOOL showblackselectpicker;
    
    /*====================springReturn============================*/
    BOOL springReturnStart;
    BOOL springReturnEnd;
    int Startx;
    int Starty;
    int Endx;
    int Endy;
    int heightLinePoint;
    
    int StartTag;
    int MidTag1;
    int MidTag2;
    int EndTag;
    
    int btnStart;
    int btnEnd;
    
    int clearStartTag;
    int clearEndTag;

    BOOL linkcollection;
    /*=======================Switch Angle==============================*/
    BOOL SwitchAngle;
    BOOL btn0T;
    BOOL btn30T;
    BOOL btn45T;
    BOOL btn60T;
    BOOL btn90T;
    BOOL btn120T;
    BOOL btn135T;
    BOOL btn150T;
    BOOL btn180T;
    BOOL btn210T;
    BOOL btn225T;
    BOOL btn240T;
    BOOL btn270T;
    BOOL btn300T;
    BOOL btn315T;
    BOOL btn330T;
    
    /*=====================================================*/
    
    BOOL singletapON;
    
    int index;
    int blackValue;
    BOOL GraftValueSave;
    int btnTag;
    
    /*=====================Picker selected================================*/
    BOOL SwitchingAngleSelect;
    BOOL tblgraftSelect;
    
    BOOL SwitchTypeSelected;
    BOOL ProgramSelected;
    BOOL LookSelected;
    BOOL MountingSelected;
    BOOL EscuteonSelected;
    BOOL HandleSeleted;
    BOOL LatchSelected;
    BOOL StopSelected;
    
    int heightOfEditedView;
    int heightOffset;
    
    /*=====================number of stage================================*/
    BOOL stage1;
    BOOL stage2;
    BOOL stage3;
    BOOL stage4;
    BOOL stage5;
    BOOL stage6;
    BOOL stage7;
    BOOL stage8;
    BOOL stage9;
    BOOL stage10;
    BOOL stage11;
    BOOL stage12;
    
    int stageno;
    
    

}

@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblpage1;
@property (strong, nonatomic) IBOutlet UILabel *lblpage2;
@property (strong, nonatomic) UIButton *btnSwitchType;
@property (strong, nonatomic) UIButton *btnprogram;
@property (strong, nonatomic) SwitchMainPageViewController *gSwitchMainPageViewController;
-(IBAction)closeTapped:(id)sender;

@property (nonatomic, strong) NSMutableArray *springReturnPosition;
@property (strong, nonatomic) IBOutlet UITableView *tblSprinReturnExtra;
@property (strong, nonatomic) IBOutlet UIImageView *imgtblcontact;
@property (nonatomic, strong) JumperView *jumperview;
@property (nonatomic, strong) KNEjumperSwitchAngle *switchjumperAngleview;

@property (nonatomic, strong) NSString *springStart;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalSwitchingangle;
@property (strong, nonatomic) IBOutlet UILabel *lblSwitchingAngle;
-(IBAction)SwitchingAngleTapped:(id)sender;
@property (nonatomic, strong) UIView* fauxView;
@property (nonatomic, strong) NSString *Noperson;
@property (strong, nonatomic) UIImageView *imageballView;
@property (strong, nonatomic) UIStepper *graftNumber;
@property (strong, nonatomic) IBOutlet UIScrollView *mainscrollview;
@property (strong, nonatomic) IBOutlet UITableView *tblSpringReturn;
//@property (strong, nonatomic) KNESpringReturnViewController *gKNESpringReturnViewController;
@property (strong, nonatomic) KNESpringGraftViewController *gKNESpringGraftViewController;
@property (strong, nonatomic) IBOutlet UITableView *tblgraft;
@property (strong, nonatomic) IBOutlet UIScrollView *GraftScrollview;
@property (strong, nonatomic) UIPickerView *GraftNumPicker;
@property (strong, nonatomic) UIPickerView *GraftNumPicker2;

@property (strong, nonatomic) NSMutableArray *SwitchTypeMutblArr;
@property (strong, nonatomic) NSMutableArray *ProrgamMutblArr;
@property (strong, nonatomic) NSMutableArray *LookMutblArr;
@property (strong, nonatomic) NSMutableArray *MountingMutblArr;
@property (strong, nonatomic) NSMutableArray *EscuteonMutblArr;
@property (strong, nonatomic) NSMutableArray *HandleMutblArr;
@property (strong, nonatomic) NSMutableArray *LatchMutblArr;
@property (strong, nonatomic) NSMutableArray *StopMutblArr;
@property (strong, nonatomic) NSMutableArray *SwitchAngleMutblArr;

@property (strong, nonatomic) IBOutlet UIImageView *imgball1;
@property (strong, nonatomic) IBOutlet UIImageView *imgball2;

@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) IBOutlet UILabel *lbl0;
@property (strong, nonatomic) IBOutlet UILabel *lbl30;
@property (strong, nonatomic) IBOutlet UILabel *lbl45;
@property (strong, nonatomic) IBOutlet UILabel *lbl60;
@property (strong, nonatomic) IBOutlet UILabel *lbl90;
@property (strong, nonatomic) IBOutlet UILabel *lbl120;
@property (strong, nonatomic) IBOutlet UILabel *lbl135;
@property (strong, nonatomic) IBOutlet UILabel *lbl150;
@property (strong, nonatomic) IBOutlet UILabel *lbl180;
@property (strong, nonatomic) IBOutlet UILabel *lbl210;
@property (strong, nonatomic) IBOutlet UILabel *lbl225;
@property (strong, nonatomic) IBOutlet UILabel *lbl240;
@property (strong, nonatomic) IBOutlet UILabel *lbl270;
@property (strong, nonatomic) IBOutlet UILabel *lbl300;
@property (strong, nonatomic) IBOutlet UILabel *lbl315;
@property (strong, nonatomic) IBOutlet UILabel *lbl330;

@property (strong, nonatomic) NSString *firstpNumbr;
@property (strong, nonatomic) KNESwitchPoint *gKNESwitchPoint;

@property (strong, nonatomic) IBOutlet UIButton *btno;
@property (strong, nonatomic) IBOutlet UIButton *btn30;
@property (strong, nonatomic) IBOutlet UIButton *btn45;
@property (strong, nonatomic) IBOutlet UIButton *btn60;
@property (strong, nonatomic) IBOutlet UIButton *btn90;
@property (strong, nonatomic) IBOutlet UIButton *btn120;
@property (strong, nonatomic) IBOutlet UIButton *btn135;
@property (strong, nonatomic) IBOutlet UIButton *btn150;
@property (strong, nonatomic) IBOutlet UIButton *btn180;
@property (strong, nonatomic) IBOutlet UIButton *btn210;
@property (strong, nonatomic) IBOutlet UIButton *btn225;
@property (strong, nonatomic) IBOutlet UIButton *btn240;
@property (strong, nonatomic) IBOutlet UIButton *btn270;
@property (strong, nonatomic) IBOutlet UIButton *btn300;
@property (strong, nonatomic) IBOutlet UIButton *btn315;
@property (strong, nonatomic) IBOutlet UIButton *btn330;

-(IBAction)btn0Tapped:(id)sender;
-(IBAction)btn30Tapped:(id)sender;
-(IBAction)btn45Tapped:(id)sender;
-(IBAction)btn60Tapped:(id)sender;
-(IBAction)btn90Tapped:(id)sender;
-(IBAction)btn120Tapped:(id)sender;
-(IBAction)btn135Tapped:(id)sender;
-(IBAction)btn150Tapped:(id)sender;
-(IBAction)btn180Tapped:(id)sender;
-(IBAction)btn210Tapped:(id)sender;
-(IBAction)btn225Tapped:(id)sender;
-(IBAction)btn240Tapped:(id)sender;
-(IBAction)btn270Tapped:(id)sender;
-(IBAction)btn300Tapped:(id)sender;
-(IBAction)btn315Tapped:(id)sender;
-(IBAction)btn330Tapped:(id)sender;


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
@property (nonatomic, strong) NSString *strlbllook;
@property (nonatomic, strong) NSString *strlblMounting;
@property (nonatomic, strong) NSString *strlblEscutcheonPlate;
@property (nonatomic, strong) NSString *strlblHandle;
@property (nonatomic, strong) NSString *strlblLatchMech;
@property (nonatomic, strong) NSString *strlblStop;
@property (nonatomic, strong) NSString *strlblStopdegree;
@property (nonatomic, strong) NSString *strlblNoofStages;
@property (nonatomic, strong) NSString *strlblMasterdata;
@property (nonatomic, strong) NSString *strlblReference;
@property (nonatomic, strong) NSString *strlblDate;
@property (nonatomic, strong) NSString *strlblModifyDate;
@property (nonatomic, strong) NSString *strlblCustNO;
@property (nonatomic, strong) NSString *strlblCompany;
@property (nonatomic, strong) NSString *strlblVersion;

@property (strong, nonatomic) IBOutlet UILabel *lbllook;
@property (strong, nonatomic) IBOutlet UILabel *lblMounting;
@property (strong, nonatomic) IBOutlet UILabel *lblEscutcheonPlate;
@property (strong, nonatomic) IBOutlet UILabel *lblHandle;
@property (strong, nonatomic) IBOutlet UILabel *lblLatchMech;
@property (strong, nonatomic) IBOutlet UILabel *lblStop;
@property (strong, nonatomic) IBOutlet UILabel *lblStopdegree;
@property (strong, nonatomic) IBOutlet UILabel *lblNoofStages;
@property (strong, nonatomic) IBOutlet UILabel *lblMasterdata;
@property (strong, nonatomic) IBOutlet UILabel *lblReference;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblModifyDate;
@property (strong, nonatomic) IBOutlet UILabel *lblCustNO;
@property (strong, nonatomic) IBOutlet UILabel *lblCompany;
@property (strong, nonatomic) IBOutlet UILabel *lblVersion;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
@property (strong, nonatomic) IBOutlet UILabel *lbltxt0;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt30;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt45;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt60;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt90;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt120;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt135;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt150;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt180;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt210;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt225;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt240;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt270;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt300;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt315;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt330;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
@property (strong, nonatomic) IBOutlet UITextField *txtlook;
@property (strong, nonatomic) IBOutlet UITextField *txtmounting;
@property (strong, nonatomic) IBOutlet UITextField *txtescutcheon;
@property (strong, nonatomic) IBOutlet UITextField *txthandle;
@property (strong, nonatomic) IBOutlet UITextField *txtlatch_mech;
@property (strong, nonatomic) IBOutlet UITextField *txtstop;
@property (strong, nonatomic) IBOutlet UITextField *txtstop_degree;
@property (strong, nonatomic) IBOutlet UITextField *txtno_of_stage;
@property (strong, nonatomic) IBOutlet UITextField *txtmaster_data;
@property (strong, nonatomic) IBOutlet UITextField *txtreference;
@property (strong, nonatomic) IBOutlet UITextField *txtdate;
@property (strong, nonatomic) IBOutlet UITextField *txtmodify_date;
@property (strong, nonatomic) IBOutlet UITextField *txtcust_no;
@property (strong, nonatomic) IBOutlet UITextField *txtcompany;
@property (strong, nonatomic) IBOutlet UITextField *txtversion; 

@property (strong, nonatomic) IBOutlet UILabel *lblpsc;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs1;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs2;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs3;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs4;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs5;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs6;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs7;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs8;
@property (strong, nonatomic) IBOutlet UITextField *txtPcs9;

@property (strong, nonatomic) IBOutlet UILabel *lbloptextra;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra1;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra2;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra3;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra4;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra5;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra6;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra7;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra8;
@property (strong, nonatomic) IBOutlet UITextField *txtOptExtra9;
//@property (strong, nonatomic) IBOutlet UITextField *txtinfocontent;
@property (strong, nonatomic) IBOutlet UITextView *txtinfocontent;

@property (strong, nonatomic) IBOutlet UIImageView *tblinfocontent;

-(void) DoneTapped:(id)gesture;

@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UILabel *lblCA10;
@property (strong, nonatomic) IBOutlet UILabel *SGL719;
@property (strong, nonatomic) IBOutlet UILabel *Labellbl;
@property (strong, nonatomic) IBOutlet UITextField *txtPlateName;
@property (strong, nonatomic) IBOutlet UITextField *txtSwitchType;
@property (strong, nonatomic) IBOutlet UITextField *txtcustomerArticleNum;
@property (strong, nonatomic) IBOutlet UITextField *txtProgram;
@property (strong, nonatomic) IBOutlet UIImageView *imgtblswitch;

@property (strong, nonatomic) IBOutlet UIImageView *imgSpringReturn;
@property (strong, nonatomic) IBOutlet UIImageView *imgjumpertbl;

-(IBAction)passPlateInfoToNewPlate:(id)sender;
-(IBAction)SwitchAngleTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgtblspringReturnValueExtra;

@property (strong, nonatomic) IBOutlet UIButton *btnNewPlate;

@property (nonatomic, strong) NSArray *numarr;
@end








