//
//  KNEStartBallViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNEStartBallViewController.h"
#import "EscutheonPlatePortrait.h"

@interface KNEStartBallViewController ()

@end

@implementation KNEStartBallViewController
//@synthesize delegate;
@synthesize mainscrollview, springReturnPosition,numarr;
@synthesize tblSpringReturn, btnSwitchType, btnprogram;
//@synthesize gKNESpringReturnViewController,
@synthesize  gSwitchMainPageViewController;
@synthesize tblgraft;
@synthesize GraftScrollview;
@synthesize imgball1;
@synthesize imgball2;
@synthesize lbl0;
@synthesize lbl30;
@synthesize lbl45;
@synthesize lbl60;
@synthesize lbl90;
@synthesize lbl120;
@synthesize lbl135;
@synthesize lbl150;
@synthesize lbl180;
@synthesize lbl210; 
@synthesize lbl225;
@synthesize lbl240;
@synthesize lbl270;
@synthesize lbl300;
@synthesize lbl315;
@synthesize lbl330;
@synthesize gKNESpringGraftViewController;
@synthesize imageballView;
@synthesize graftNumber;
@synthesize GraftNumPicker,GraftNumPicker2;
@synthesize Noperson;
@synthesize fauxView;
@synthesize firstpNumbr;
@synthesize label;
@synthesize springStart;
@synthesize lblSwitchingAngle,lblTotalSwitchingangle;
@synthesize jumperview, switchjumperAngleview;
@synthesize lbltxt0, lbltxt120, lbltxt135, lbltxt150, lbltxt180, lbltxt210, lbltxt225, lbltxt240, lbltxt270, lbltxt30, lbltxt300, lbltxt315, lbltxt330, lbltxt45, lbltxt60, lbltxt90;
@synthesize txtcompany, txtcust_no, txtdate, txtescutcheon, txthandle, txtlatch_mech, txtlook, txtmaster_data, txtmodify_date, txtmounting, txtno_of_stage, txtreference, txtstop, txtstop_degree, txtversion;
@synthesize txtPcs9, txtPcs1, txtPcs2, txtPcs3, txtPcs4, txtPcs5, txtPcs6, txtPcs7, txtPcs8;
@synthesize txtOptExtra1, txtOptExtra2, txtOptExtra3, txtOptExtra4, txtOptExtra5, txtOptExtra6, txtOptExtra7, txtOptExtra8, txtOptExtra9;
@synthesize lblpsc, lbloptextra, tblinfocontent, txtinfocontent;
@synthesize gKNESwitchPoint, txtPlateName, txtcustomerArticleNum, txtProgram, txtSwitchType;
@synthesize SwitchTypeMutblArr, ProrgamMutblArr, LookMutblArr, MountingMutblArr, EscuteonMutblArr, HandleMutblArr,LatchMutblArr, StopMutblArr, SwitchAngleMutblArr;
@synthesize strlbllook, strlblCompany, strlblCustNO, strlblDate, strlblEscutcheonPlate, strlblHandle, strlblLatchMech, strlblMasterdata, strlblModifyDate, strlblMounting, strlblNoofStages, strlblReference, strlblStop, strlblStopdegree, strlblVersion;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {

        /*===================== label title =================================*/
        
        strlbllook=@" Look";
        strlblMounting=@" Mounting";
        strlblEscutcheonPlate=@" Escutcheon Plate";
        strlblHandle=@" Handle";
        strlblLatchMech=@" Latch. Mech.";
        strlblStop=@" Stop";
        strlblStopdegree=@" Stop Degree";
        strlblNoofStages=@" No. of Stages";
        strlblMasterdata=@" Master Data";
        strlblReference=@" Reference";
        strlblDate=@" Date";
        strlblModifyDate=@" Modify Date";
        strlblCustNO=@" Cus. No.";
        strlblCompany=@" Company";
        strlblVersion=@" Version";
        
        /*===============Switch Type================================*/
        SwitchTypeMutblArr = [[NSMutableArray alloc]init];
        
        [SwitchTypeMutblArr addObject:@"Switch Type 1"];
        [SwitchTypeMutblArr addObject:@"Switch Type 2"];
        [SwitchTypeMutblArr addObject:@"Switch Type 3"];
        [SwitchTypeMutblArr addObject:@"Switch Type 4"];
        [SwitchTypeMutblArr addObject:@"Switch Type 5"];

        /*===============Program================================*/
        ProrgamMutblArr = [[NSMutableArray alloc]init];
        
        [ProrgamMutblArr addObject:@"program 1"];
        [ProrgamMutblArr addObject:@"program 2"];
        [ProrgamMutblArr addObject:@"program 3"];
        [ProrgamMutblArr addObject:@"program 4"];
        [ProrgamMutblArr addObject:@"program 5"];
        
        /*===============Look================================*/
        LookMutblArr = [[NSMutableArray alloc]init];
        
        [LookMutblArr addObject:@"Look 1"];
        [LookMutblArr addObject:@"Look 2"];
        [LookMutblArr addObject:@"Look 3"];
        [LookMutblArr addObject:@"Look 4"];
        [LookMutblArr addObject:@"Look 5"];
        
        /*===============Mounting================================*/
        MountingMutblArr = [[NSMutableArray alloc]init];
        
        [MountingMutblArr addObject:@"Mounting 1"];
        [MountingMutblArr addObject:@"Mounting 2"];
        [MountingMutblArr addObject:@"Mounting 3"];
        [MountingMutblArr addObject:@"Mounting 4"];
        [MountingMutblArr addObject:@"Mounting 5"];
        
        /*===============Escutcheon Plate================================*/
        EscuteonMutblArr = [[NSMutableArray alloc]init];
        
        [EscuteonMutblArr addObject:@"Escutcheon Plate 1"];
        [EscuteonMutblArr addObject:@"Escutcheon Plate 2"];
        [EscuteonMutblArr addObject:@"Escutcheon Plate 3"];
        [EscuteonMutblArr addObject:@"Escutcheon Plate 4"];
        [EscuteonMutblArr addObject:@"Escutcheon Plate 5"];
        
        /*===============Handle================================*/
        HandleMutblArr = [[NSMutableArray alloc]init];
        
        [HandleMutblArr addObject:@"Handle 1"];
        [HandleMutblArr addObject:@"Handle 2"];
        [HandleMutblArr addObject:@"Handle 3"];
        [HandleMutblArr addObject:@"Handle 4"];
        [HandleMutblArr addObject:@"Handle 5"];
        
        /*=============== Latch. Mech.================================*/
        LatchMutblArr = [[NSMutableArray alloc]init];
        
        [LatchMutblArr addObject:@"Latch 1"];
        [LatchMutblArr addObject:@"Latch 2"];
        [LatchMutblArr addObject:@"Latch 3"];
        [LatchMutblArr addObject:@"Latch 4"];
        [LatchMutblArr addObject:@"Latch 5"];
        
        /*=============== Stop ================================*/
        StopMutblArr = [[NSMutableArray alloc]init];
        
        [StopMutblArr addObject:@"Stop 1"];
        [StopMutblArr addObject:@"Stop 2"];
        [StopMutblArr addObject:@"Stop 3"];
        [StopMutblArr addObject:@"Stop 4"];
        [StopMutblArr addObject:@"Stop 5"];
        
        
        /*=====================================================*/
        [self getfirstpNum];
        springReturnStart=NO;
        springReturnEnd=NO;
        SwitchAngle=NO;
        btn0T=YES;
        btn30T=YES;
        btn45T=YES;
        btn60T=YES;
        btn90T=YES;
        btn120T=YES;
        btn135T=YES;
        btn150T=YES;
        btn180T=YES;
        btn210T=YES;
        btn225T=YES;
        btn240T=YES;
        btn270T=YES;
        btn300T=YES;
        btn315T=YES;
        btn330T=YES;
        singletapON=NO;
        linkcollection=NO;
        GraftValueSave=NO;
        SwitchingAngleSelect= NO;
        tblgraftSelect = NO;
        SwitchTypeSelected=NO;
        ProgramSelected=NO;
        LookSelected=NO;
        MountingSelected=NO;
        EscuteonSelected=NO;
        HandleSeleted=NO;
        LatchSelected=NO;
        StopSelected=NO;
        stage1=NO;
        stage2=NO;
        stage3=NO;
        stage4=NO;
        stage5=NO;
        stage6=NO;
        stage7=NO;
        stage8=NO;
        stage9=NO;
        stage10=NO;
        stage11=NO;
        stage12=NO;

    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lbllook.text=strlbllook;
    self.lblMounting.text=strlblMounting;
    self.lblEscutcheonPlate.text=strlblEscutcheonPlate;
    self.lblHandle.text=strlblHandle;
    self.lblLatchMech.text=strlblLatchMech;
    self.lblStop.text=strlblStop;
    self.lblStopdegree.text=strlblStopdegree;
    self.lblNoofStages.text=strlblNoofStages;
    self.lblMasterdata.text=strlblMasterdata;
    self.lblReference.text=strlblReference;
    self.lblDate.text=strlblDate;
    self.lblModifyDate.text=strlblModifyDate;
    self.lblCustNO.text=strlblCustNO;
    self.lblCompany.text=strlblCompany;
    self.lblVersion.text=strlblVersion;
    
    KNEStartBallViewController *gBpNoPersonViewController=[[KNEStartBallViewController alloc] initWithNibName:@"KNEStartBallViewController" bundle:nil];
    
    /*
     UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
     [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
     leftButton.frame = CGRectMake(0, 0, 50, 33);
     [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];
     
     self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease];
     gBpNoPersonViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
     */
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    gBpNoPersonViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    [self initwithtblSwitchrowcolor];
    [self initwithGraftrowcolor];
    
    stageno=0;
    
    /*===========table==============
    if(gKNESpringReturnViewController == nil)
    {
        KNESpringReturnViewController *gBpEventTableViewController = [[KNESpringReturnViewController alloc] init];
        self.gKNESpringReturnViewController = gBpEventTableViewController;
        //self.gKNESpringReturnViewController.gKNEStartBallViewController=self;
        //[gBpEventTableViewController release];
    } 
    
    
    [tblSpringReturn setDataSource:self.gKNESpringReturnViewController];
    [tblSpringReturn setDelegate:self.gKNESpringReturnViewController];
    [self.tblSprinReturnExtra setDataSource:self.gKNESpringReturnViewController];
    
    self.gKNESpringReturnViewController.view = self.gKNESpringReturnViewController.tableView;
    //self.gBpEventTableViewController.view = self.gBpEventTableViewController.view;
    ==*/
    /*===========================*/
    
    /*===========table graft==============
    if(gKNESpringGraftViewController == nil)
    {
        KNESpringGraftViewController *gKNESpringGraftViewController = [[KNESpringGraftViewController alloc] init];
        self.gKNESpringGraftViewController = gKNESpringGraftViewController;
        self.gKNESpringGraftViewController.gKNEStartBallViewController=self;
        //[gKNESpringGraftViewController release];
    } 
    
    
    [tblgraft setDataSource:self.gKNESpringGraftViewController];
    [tblgraft setDelegate:self.gKNESpringGraftViewController];
    
    
    self.gKNESpringGraftViewController.view = self.gKNESpringGraftViewController.tableView;==*/
    //self.gBpEventTableViewController.view = self.gBpEventTableViewController.view;
    
    /*===========================*/
    //tblSpringReturn.frame=CGRectMake(45, 310, 205, 648);
    tblSpringReturn.frame=CGRectMake(205, 311, 60, 648);
    tblSpringReturn.rowHeight = 27;
    [tblSpringReturn reloadData];
    tblSpringReturn.scrollEnabled = NO; 
    tblSpringReturn.backgroundColor=[UIColor clearColor];
    [self.mainscrollview addSubview:tblSpringReturn];
    
    self.tblSprinReturnExtra.frame=CGRectMake(1283, 311, 60, 648);
    self.tblSprinReturnExtra.rowHeight=27;
    [self.tblSprinReturnExtra reloadData];
    self.tblSprinReturnExtra.scrollEnabled=NO;
    self.tblSprinReturnExtra.editing=NO;
    self.tblSprinReturnExtra.backgroundColor=[UIColor clearColor];
    //self.tblSprinReturnExtra.separatorColor = [UIColor clearColor];
    [self.mainscrollview addSubview:self.tblSprinReturnExtra];
    /*
    tblgraft.frame=CGRectMake(288, 228, 1200, 630);
    tblgraft.rowHeight = 30;
    [tblgraft reloadData];
    */
    tblgraft.hidden=YES;
    
    self.mainscrollview.contentSize=CGSizeMake(2304, 960);
    
    /**/
    graftNumbermArr = [[NSMutableArray alloc]init];
    [graftNumbermArr addObject:@"1"];
    [graftNumbermArr addObject:@"2"];
    [graftNumbermArr addObject:@"3"];
    [graftNumbermArr addObject:@"4"];
    [graftNumbermArr addObject:@"5"];
    [graftNumbermArr addObject:@"6"];
    [graftNumbermArr addObject:@"7"];
    [graftNumbermArr addObject:@"8"];
    [graftNumbermArr addObject:@"9"];
    [graftNumbermArr addObject:@"10"];
    [graftNumbermArr addObject:@"11"];
    [graftNumbermArr addObject:@"12"];
    [graftNumbermArr addObject:@"13"];
    [graftNumbermArr addObject:@"14"];
    
    graftNumbermArr2 = [[NSMutableArray alloc]init];
    [graftNumbermArr2 addObject:@"30"];
    [graftNumbermArr2 addObject:@"45"];
    [graftNumbermArr2 addObject:@"60"];
    [graftNumbermArr2 addObject:@"90"];
    
    SwitchAngleMutblArr = [[NSMutableArray alloc]init];
    [SwitchAngleMutblArr addObject:@"0"];
    [SwitchAngleMutblArr addObject:@"30"];
    [SwitchAngleMutblArr addObject:@"45"];
    [SwitchAngleMutblArr addObject:@"60"];
    [SwitchAngleMutblArr addObject:@"90"];
    [SwitchAngleMutblArr addObject:@"120"];
    [SwitchAngleMutblArr addObject:@"135"];
    [SwitchAngleMutblArr addObject:@"150"];
    [SwitchAngleMutblArr addObject:@"180"];
    [SwitchAngleMutblArr addObject:@"210"];
    [SwitchAngleMutblArr addObject:@"225"];
    [SwitchAngleMutblArr addObject:@"240"];
    [SwitchAngleMutblArr addObject:@"270"];
    [SwitchAngleMutblArr addObject:@"300"];
    [SwitchAngleMutblArr addObject:@"315"];
    [SwitchAngleMutblArr addObject:@"330"];
    
    [self startingpointBall];
    [self getspringreturn];
    [self thirdpart];
    
    [self initwithballleftwing];
    [self initwithballrightwing];
    [self initwithBallleftimage];
    [self initwithBallrightimage];
    [self autosugestbtn];
    
    /*
    UIView *jumperViewswitchAngleView = [[[UIView alloc] initWithFrame:CGRectMake(251, 98, 1032, 214)] autorelease];
    switchjumperAngleview = [[[KNEjumperSwitchAngle alloc]initWithFrame:CGRectMake(0, 0, 1032, 214)] autorelease];
    jumperViewswitchAngleView.backgroundColor = [UIColor clearColor];
    switchjumperAngleview.backgroundColor = [UIColor clearColor];
    [jumperViewswitchAngleView addSubview:switchjumperAngleview];
    [self.mainscrollview addSubview:jumperViewswitchAngleView];
    */
    
    rows=0;
    maxNumberOfLines=5;
    
    UIView *jumperView = [[UIView alloc] initWithFrame:CGRectMake(251, 98, 1032, 214)];
     jumperview = [[JumperView alloc]initWithFrame:CGRectMake(0, 0, 1032, 214)];
     jumperView.backgroundColor = [UIColor clearColor];
     jumperview.backgroundColor = [UIColor clearColor];
     [jumperView addSubview:jumperview];
     [self.mainscrollview addSubview:jumperView];
    
    [self.mainscrollview addSubview:self.imgLogo];
    
    
    [self.imgjumpertbl setFrame:CGRectMake(251, 98, 1032, 214)];
    [self.mainscrollview addSubview:self.imgjumpertbl ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    [self performSelector:@selector(initwithGraft) withObject:nil afterDelay:0.5];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = mainscrollview.frame.size.width;
    int page = floor((mainscrollview.contentOffset.x - pageWidth / 100) / pageWidth) + 1;
    
    int sndPage=self.view.frame.size.width;
    int npositions= 86;
    
    if (page==1) {
        
        /*=============button switch type=====================*/
        
        
        [self.imgLogo setFrame:CGRectMake(sndPage + 10 , 24, 202, 46)];
        [btnSwitchType setFrame:CGRectMake(sndPage + 341-npositions, 51, 24, 24)];
        [btnprogram setFrame:CGRectMake(sndPage + 492-npositions, 51, 24, 24)];
        
        [self.lbl setFrame:CGRectMake(sndPage + 251, 77, 464, 21)];

        self.lbl.layer.borderColor=[UIColor blackColor].CGColor;
        self.lbl.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lbl ];
        
//        [self.txtPlateName setFrame:CGRectMake(sndPage + 0, 77, 252, 21)];
//
//        self.txtPlateName.layer.borderColor=[UIColor blackColor].CGColor;
//        self.txtPlateName.layer.borderWidth = 1;

        
        [self.lblCA10 setFrame:CGRectMake(sndPage + 337-npositions, 49, 152, 28)];
        self.lblCA10.layer.borderColor=[UIColor blackColor].CGColor;
        self.lblCA10.layer.borderWidth = 1;
        
        [self.txtSwitchType setFrame:CGRectMake(sndPage + 368-npositions, 49, 117, 28)];
//        self.txtSwitchType.layer.borderColor=[UIColor blackColor].CGColor;
//        self.txtSwitchType.layer.borderWidth = 1;
        
        
        
        [self.SGL719 setFrame:CGRectMake(sndPage + 488-npositions, 49, 313, 28)];
        self.SGL719.layer.borderColor=[UIColor blackColor].CGColor;
        self.SGL719.layer.borderWidth = 1;
        
        [self.txtProgram setFrame:CGRectMake(sndPage + 520-npositions, 49, 150, 28)];
//        self.txtProgram.layer.borderColor=[UIColor blackColor].CGColor;
//        self.txtProgram.layer.borderWidth = 1;
        
        [self.Labellbl setFrame:CGRectMake(sndPage + 337-npositions, 20, 464, 30)];
        self.Labellbl.layer.borderColor=[UIColor blackColor].CGColor;
        self.Labellbl.layer.borderWidth = 1;
        
        [self.txtcustomerArticleNum setFrame:CGRectMake(sndPage + 340-npositions, 20, 420, 30)];
//        self.txtcustomerArticleNum.layer.borderColor=[UIColor blackColor].CGColor;
//        self.txtcustomerArticleNum.layer.borderWidth = 1;

    }else
    {
        [self.imgLogo setFrame:CGRectMake(41, 24, 202, 46)];
        [btnSwitchType setFrame:CGRectMake( 341, 51, 24, 24)];
        [btnprogram setFrame:CGRectMake( 492, 51, 24, 24)];
        
        [self.lbl setFrame:CGRectMake(251, 77, 1032, 21)];
        self.lbl.layer.borderColor=[UIColor blackColor].CGColor;
        self.lbl.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lbl ];
        
        [self.txtPlateName setFrame:CGRectMake(0, 77, 252, 21)];
        txtPlateName.autocorrectionType=UITextAutocorrectionTypeNo;
        [self.mainscrollview addSubview:self.txtPlateName ];
        
        [self.lblCA10 setFrame:CGRectMake(337, 49, 152, 28)];
        self.lblCA10.layer.borderColor=[UIColor blackColor].CGColor;
        self.lblCA10.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lblCA10 ];
        
        [self.txtSwitchType setFrame:CGRectMake(368, 49, 117, 28)];
        txtSwitchType.autocorrectionType=UITextAutocorrectionTypeNo;
        //self.txtSwitchType.layer.borderColor=[UIColor blackColor].CGColor;
        //self.txtSwitchType.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.txtSwitchType ];
        
        
        
        [self.SGL719 setFrame:CGRectMake(488, 49, 795, 28)];
        self.SGL719.layer.borderColor=[UIColor blackColor].CGColor;
        self.SGL719.layer.borderWidth = 1;
        self.SGL719.backgroundColor=[UIColor clearColor];
        [self.mainscrollview addSubview:self.SGL719 ];
        
        [self.txtProgram setFrame:CGRectMake(520, 49, 150, 28)];
        txtProgram.autocorrectionType=UITextAutocorrectionTypeNo;
        //self.txtProgram.layer.borderColor=[UIColor blackColor].CGColor;
        //self.txtProgram.layer.borderWidth = 1;
        self.txtProgram.backgroundColor=[UIColor clearColor];
        [self.mainscrollview addSubview:self.txtProgram ];
        
        [self.Labellbl setFrame:CGRectMake(337, 20, 946, 30)];
        self.Labellbl.layer.borderColor=[UIColor blackColor].CGColor;
        self.Labellbl.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.Labellbl ];
        
        [self.txtcustomerArticleNum setFrame:CGRectMake(340, 20, 420, 30)];
        //self.txtcustomerArticleNum.layer.borderColor=[UIColor blackColor].CGColor;
        //self.txtcustomerArticleNum.layer.borderWidth = 1;
        txtcustomerArticleNum.autocorrectionType=UITextAutocorrectionTypeNo;
        [self.mainscrollview addSubview:self.txtcustomerArticleNum ];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    NSLog(@"warning !!!");
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}



-(void)startingpointBall
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    //NSString *startingpointstr=[[NSString alloc]initWithFormat:@"%@",appDelegate.startingpoint ];
    if ([lbl0.text isEqualToString:appDelegate.startingpoint]) {
        lbl0.backgroundColor=[UIColor blackColor];
    }else if ([lbl30.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl30.backgroundColor=[UIColor blackColor];
    }else if ([lbl45.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl45.backgroundColor=[UIColor blackColor];
    }else if ([lbl60.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl60.backgroundColor=[UIColor blackColor];
    }else if ([lbl90.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl90.backgroundColor=[UIColor blackColor];
    }else if ([lbl120.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl120.backgroundColor=[UIColor blackColor];
    }else if ([lbl135.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl135.backgroundColor=[UIColor blackColor];
    }else if ([lbl150.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl150.backgroundColor=[UIColor blackColor];
    }else if ([lbl180.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl180.backgroundColor=[UIColor blackColor];
    }else if ([lbl210.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl210.backgroundColor=[UIColor blackColor];
    }else if ([lbl225.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl225.backgroundColor=[UIColor blackColor];
    }else if ([lbl240.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl240.backgroundColor=[UIColor blackColor];
    }else if ([lbl270.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl270.backgroundColor=[UIColor blackColor];
    }else if ([lbl300.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl300.backgroundColor=[UIColor blackColor];
    }else if ([lbl315.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl315.backgroundColor=[UIColor blackColor];
    }else if ([lbl330.text isEqualToString:appDelegate.startingpoint]) 
    {
        lbl330.backgroundColor=[UIColor blackColor];
    }
    
    
}

- (void)viewDidUnload
{
    [self setMainscrollview:nil];
    [self setTblSpringReturn:nil];
    [self setTblgraft:nil];
    [self setGraftScrollview:nil];
    [self setImgball1:nil];
    [self setImgball2:nil];
    [self setLbl0:nil];
    [self setLbl30:nil];
    [self setLbl45:nil];
    [self setLbl60:nil];
    [self setLbl90:nil];
    [self setLbl120:nil];
    [self setLbl135:nil];
    [self setLbl150:nil];
    [self setLbl180:nil];
    [self setLbl210:nil];
    [self setLbl225:nil];
    [self setLbl240:nil];
    [self setLbl270:nil];
    [self setLbl300:nil];
    [self setLbl315:nil];
    [self setLbl330:nil];
    [self setBtno:nil];
    [self setBtn30:nil];
    [self setBtn45:nil];
    [self setBtn60:nil];
    [self setBtn90:nil];
    [self setBtn120:nil];
    [self setBtn135:nil];
    [self setBtn150:nil];
    [self setBtn180:nil];
    [self setBtn210:nil];
    [self setBtn225:nil];
    [self setBtn240:nil];
    [self setBtn270:nil];
    [self setBtn240:nil];
    [self setBtn300:nil];
    [self setBtn315:nil];
    [self setBtn330:nil];
    [self setLblSwitchingAngle:nil];
    [self setLbllook:nil];
    [self setLblMounting:nil];
    [self setLblEscutcheonPlate:nil];
    [self setLblHandle:nil];
    [self setLblLatchMech:nil];
    //[self setLbl:nil];
    [self setLblStop:nil];
    [self setLblStopdegree:nil];
    [self setLblNoofStages:nil];
    [self setLblMasterdata:nil];
    [self setLblNoofStages:nil];
    [self setLblReference:nil];
    [self setLblDate:nil];
    [self setLblModifyDate:nil];
    [self setLblCustNO:nil];
    [self setLblCompany:nil];
    [self setLblVersion:nil];
    [self setLblTotalSwitchingangle:nil];
    [self setImgtblcontact:nil];
    
    [self setLbltxt0:nil];
    [self setLbltxt30:nil];
    [self setLbltxt45:nil];
    [self setLbltxt60:nil];
    [self setLbltxt90:nil];
    [self setLbltxt120:nil];
    [self setLbltxt135:nil];
    [self setLbltxt150:nil];
    [self setLbltxt180:nil];
    [self setLbltxt210:nil];
    [self setLbltxt225:nil];
    [self setLbltxt240:nil];
    [self setLbltxt270:nil];
    [self setLbltxt300:nil];
    [self setLbltxt315:nil];
    [self setLbltxt330:nil];
    [self setTxtlook:nil];
    [self setTxtmounting:nil];
    [self setTxtescutcheon:nil];
    [self setTxthandle:nil];
    [self setTxtlatch_mech:nil];
    [self setTxtstop:nil];
    [self setTxtstop_degree:nil];
    [self setTxtno_of_stage:nil];
    [self setTxtmaster_data:nil];
    [self setTxtreference:nil];
    [self setTxtdate:nil];
    [self setTxtmodify_date:nil];
    [self setTxtcust_no:nil];
    [self setTxtcompany:nil];
    [self setTxtversion:nil];
    [self setTxtPcs1:nil];
    [self setTxtPcs2:nil];
    [self setTxtPcs3:nil];
    [self setTxtPcs4:nil];
    [self setTxtPcs5:nil];
    [self setTxtPcs6:nil];
    [self setTxtPcs7:nil];
    [self setTxtPcs8:nil];
    
    [self setTxtOptExtra1:nil];
    [self setTxtOptExtra2:nil];
    [self setTxtOptExtra3:nil];
    [self setTxtOptExtra4:nil];
    [self setTxtOptExtra5:nil];
    [self setTxtOptExtra6:nil];
    [self setTxtOptExtra7:nil];
    [self setTxtOptExtra8:nil];
    [self setTxtOptExtra9:nil];
    [self setLblpsc:nil];
    [self setLbloptextra:nil];
    [self setTblinfocontent:nil];
    [self setTxtinfocontent:nil];
    [self setLbl:nil];
    [self setLblCA10:nil];
    [self setSGL719:nil];
    [self setLabellbl:nil];
    [self setTxtPlateName:nil];
    [self setTxtSwitchType:nil];
    [self setTxtcustomerArticleNum:nil];
    [self setTxtProgram:nil];
    [self setImgtblswitch:nil];
    [self setImgSpringReturn:nil];
    [self setImgjumpertbl:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [self setTblSprinReturnExtra:nil];

    [self setImgtblspringReturnValueExtra:nil];
    [self setBtnNewPlate:nil];
    [self setLblpage1:nil];
    [self setLblpage2:nil];
    [self setImgLogo:nil];
    [self setTxtinfocontent:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)initwithBallleftimage
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(260 +widthOffset*xOffset, yOffset+160, 66, 66)];
        gimgpictureball1.tag=i+ 10000+1;
        //gimgpictureball1.backgroundColor=[UIColor blueColor];
        gimgpictureball1.image=[UIImage imageNamed:@"circle1.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=86;
        xOffset++;
        
        
    }
}

-(void)initwithballleftwing
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]init];
        [gimgpictureball1 setFrame:CGRectMake(255+widthOffset*xOffset, yOffset+ 159, 13, 70)];
        gimgpictureball1.tag=i+ 20000+1;
        gimgpictureball1.image=[UIImage imageNamed:@"left.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=86;
        xOffset++;
        
        
    }
}

-(void)initwithBallrightimage
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(260 +widthOffset*xOffset, yOffset+160, 66, 66)];
        gimgpictureball1.tag=i+ 35000+1;
        //gimgpictureball1.backgroundColor=[UIColor blueColor];
        gimgpictureball1.image=[UIImage imageNamed:@"circle1.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=86;
        xOffset++;

    }
}

-(void)initwithballrightwing
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(319 +widthOffset*xOffset, yOffset+ 159, 13, 70)];
        gimgpictureball1.tag=i+ 30000+1;
        gimgpictureball1.image=[UIImage imageNamed:@"right.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=86;
        xOffset++;
        
        
    }
}

-(void)initwithBall
{
    int widthOffset = 0;
    int yOffset=24;
    int xOffset=0;
    for(int i=0;i<24;i++)
    {
        if(i!=0 && (i%24)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(253+widthOffset*xOffset, yOffset+145, 38, 94)];
        gimgpictureball1.tag=i+ 10000+1;
        //gimgpictureball1.backgroundColor=[UIColor blueColor];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        
        widthOffset=43;
        xOffset++;
        
        
    }
}
/*======================================NumberOfStage=====================================================*/
-(void)NumberOfStageTapped:(int)textField
{
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553)  ||  (lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2) )
    {
       
        if (stage1==NO)
        {
             NSLog(@"stage 1");
            stageno+=1;
            
            stage1=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3)   ||   (lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        
        if (stage2==NO)
        {
            NSLog(@"stage 2");
            stageno+=1;
            
            stage2=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5)     ||     (lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        if (stage3==NO)
        {
            NSLog(@"stage 3");
            stageno+=1;
            
            stage3=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7)    ||   (lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        if (stage4==NO)
        {
            NSLog(@"stage 4");
            stageno+=1;
            
            stage4=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9)    ||    (lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        if (stage5==NO)
        {
            NSLog(@"stage 5");
            stageno+=1;
            
            stage5=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11)  ||    (lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        if (stage6==NO)
        {
            NSLog(@"stage 6");
            stageno+=1;
            
            stage6=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13)    ||    (lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        if (stage7==NO)
        {
            NSLog(@"stage 7");
            stageno+=1;
            
            stage7=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15)    ||    (lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        if (stage8==NO)
        {
            NSLog(@"stage 8");
            stageno+=1;
            
            stage8=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17)    ||    (lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        if (stage9==NO)
        {
            NSLog(@"stage 9");
            stageno+=1;
            
            stage9=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19)    ||    (lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        if (stage10==NO)
        {
            NSLog(@"stage 10");
            stageno+=1;
            
            stage10=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21)    ||    (lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        if (stage11==NO)
        {
            NSLog(@"stage 11");
            stageno+=1;
            
            stage11=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23)    ||    (lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        if (stage12==NO)
        {
            NSLog(@"stage 12");
            stageno+=1;
            
            stage12=YES;
        }
    }
    
    
    [self ShowNumberOfStageTapped];
    
}

-(void)ShowNumberOfStageTapped
{
    NSString *strnoofStage=[[NSString alloc] initWithFormat:@"%d",stageno];
    txtno_of_stage.text=strnoofStage;
}

-(void)cancelNumberOfStageTapped:(int)textField
{
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553)  ||  (lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2) )
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3025];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3049];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3073];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3097];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3121];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3145];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3169];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3193];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3217];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3241];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3265];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3289];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3313];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3337];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3361];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3385];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3409];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3433];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3457];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3481];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3505];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3529];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3553];
        
        
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 +2];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 +2];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 +2];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 +2];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 +2];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 +2];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 +2];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 +2];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 +2];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 +2];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 +2];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 +2];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 +2];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 +2];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 +2];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 +2];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 +2];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 +2];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 +2];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 +2];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 +2];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 +2];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 +2];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 +2];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage1==YES)
                {
                    NSLog(@"stage 1");
                    stageno-=1;
                    
                    stage1=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3)   ||   (lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 3];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 3];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 3];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 3];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 3];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 3];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 3];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 3];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 3];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 3];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 3];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 3];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 3];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 3];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 3];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 3];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 3];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 3];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 3];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 3];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 3];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 3];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 3];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 3];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 4];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 4];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 4];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 4];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 4];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 4];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 4];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 4];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 4];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 4];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 4];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 4];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 4];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 4];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 4];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 4];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 4];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 4];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 4];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 4];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 4];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 4];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 4];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 4];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage2==YES)
                {
                    NSLog(@"stage 2");
                    stageno-=1;
                    
                    stage2=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5)     ||     (lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 5];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 5];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 5];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 5];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 5];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 5];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 5];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 5];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 5];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 5];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 5];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 5];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 5];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 5];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 5];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 5];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 5];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 5];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 5];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 5];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 5];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 5];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 5];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 5];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 6];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 6];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 6];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 6];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 6];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 6];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 6];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 6];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 6];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 6];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 6];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 6];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 6];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 6];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 6];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 6];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 6];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 6];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 6];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 6];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 6];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 6];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 6];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 6];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage3==YES)
                {
                    NSLog(@"stage 3");
                    stageno-=1;
                    
                    stage3=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7)    ||   (lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 7];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 7];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 7];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 7];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 7];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 7];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 7];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 7];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 7];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 7];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 7];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 7];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 7];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 7];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 7];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 7];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 7];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 7];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 7];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 7];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 7];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 7];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 7];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 7];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 8];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 8];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 8];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 8];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 8];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 8];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 8];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 8];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 8];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 8];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 8];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 8];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 8];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 8];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 8];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 8];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 8];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 8];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 8];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 8];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 8];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 8];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 8];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 8];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage4==YES)
                {
                    NSLog(@"stage 4");
                    stageno-=1;
                    
                    stage4=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9)    ||    (lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 9];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 9];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 9];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 9];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 9];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 9];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 9];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 9];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 9];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 9];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 9];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 9];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 9];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 9];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 9];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 9];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 9];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 9];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 9];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 9];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 9];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 9];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 9];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 9];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 10];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 10];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 10];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 10];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 10];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 10];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 10];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 10];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 10];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 10];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 10];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 10];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 10];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 10];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 10];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 10];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 10];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 10];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 10];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 10];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 10];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 10];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 10];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 10];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage5==YES)
                {
                    NSLog(@"stage 5");
                    stageno-=1;
                    
                    stage5=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11)  ||    (lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 11];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 11];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 11];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 11];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 11];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 11];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 11];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 11];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 11];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 11];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 11];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 11];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 11];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 11];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 11];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 11];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 11];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 11];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 11];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 11];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 11];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 11];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 11];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 11];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 12];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 12];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 12];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 12];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 12];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 12];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 12];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 12];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 12];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 12];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 12];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 12];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 12];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 12];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 12];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 12];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 12];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 12];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 12];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 12];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 12];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 12];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 12];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 12];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage6==YES)
                {
                    NSLog(@"stage 6");
                    stageno-=1;
                    
                    stage6=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13)    ||    (lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 13];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 13];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 13];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 13];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 13];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 13];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 13];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 13];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 13];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 13];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 13];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 13];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 13];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 13];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 13];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 13];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 13];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 13];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 13];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 13];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 13];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 13];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 13];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 13];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001 + 14];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 14];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 14];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 14];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 14];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 14];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 14];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 14];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 14];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 14];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 14];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 14];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 14];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 14];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 14];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 14];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 14];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 14];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 14];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 14];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 14];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 14];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 14];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 14];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage7==YES)
                {
                    NSLog(@"stage 7");
                    stageno-=1;
                    
                    stage7=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15)    ||    (lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 15];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 15];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 15];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 15];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 15];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 15];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 15];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 15];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 15];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 15];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 15];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 15];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 15];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 15];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 15];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 15];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 15];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 15];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 15];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 15];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 15];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 15];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 15];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 15];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 16];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 16];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 16];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 16];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 16];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 16];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 16];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 16];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 16];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 16];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 16];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 16];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 16];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 16];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 16];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 16];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 16];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 16];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 16];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 16];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 16];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 16];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 16];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 16];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage8==YES)
                {
                    NSLog(@"stage 8");
                    stageno-=1;
                    
                    stage8=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17)    ||    (lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 17];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 17];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 17];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 17];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 17];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 17];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 17];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 17];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 17];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 17];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 17];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 17];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 17];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 17];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 17];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 17];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 17];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 17];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 17];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 17];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 17];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 17];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 17];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 17];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 18];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 18];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 18];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 18];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 18];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 18];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 18];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 18];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 18];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 18];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 18];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 18];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 18];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 18];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 18];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 18];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 18];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 18];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 18];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 18];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 18];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 18];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 18];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 18];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage9==YES)
                {
                    NSLog(@"stage 9");
                    stageno-=1;
                    
                    stage9=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19)    ||    (lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 19];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 19];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 19];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 19];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 19];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 19];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 19];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 19];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 19];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 19];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 19];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 19];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 19];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 19];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 19];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 19];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 19];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 19];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 19];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 19];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 19];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 19];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 19];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 19];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 20];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 20];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 20];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 20];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 20];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 20];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 20];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 20];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 20];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 20];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 20];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 20];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 20];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 20];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 20];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 20];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 20];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 20];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 20];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 20];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 20];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 20];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 20];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 20];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage10==YES)
                {
                    NSLog(@"stage 10");
                    stageno-=1;
                    
                    stage10=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21)    ||    (lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 21];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 21];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 21];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 21];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 21];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 21];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 21];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 21];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 21];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 21];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 21];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 21];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 21];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 21];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 21];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 21];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 21];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 21];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 21];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 21];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 21];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 21];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 21];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 21];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 22];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 22];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 22];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 22];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 22];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 22];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 22];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 22];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 22];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 22];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 22];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 22];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 22];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 22];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 22];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 22];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 22];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 22];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 22];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 22];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 22];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 22];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 22];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 22];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage11==YES)
                {
                    NSLog(@"stage 11");
                    stageno-=1;
                    
                    stage11=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23)    ||    (lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 23];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 23];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 23];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 23];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 23];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 23];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 23];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 23];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 23];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 23];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 23];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 23];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 23];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 23];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 23];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 23];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 23];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 23];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 23];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 23];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 23];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 23];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 23];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 23];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 24];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 24];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 24];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 24];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 24];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 24];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 24];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 24];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 24];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 24];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 24];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 24];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 24];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 24];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 24];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 24];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 24];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 24];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 24];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 24];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 24];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 24];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 24];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 24];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage12==YES)
                {
                    NSLog(@"stage 12");
                    stageno-=1;
                    
                    stage12=NO;
                }
            }
        }
        
    }
    
    
    [self ShowNumberOfStageTapped];
    
}

/*======================================contact jumper image=====================================================*/
-(void)contactjumperTapped:(int)textField
{
    [self NumberOfStageTapped:textField];
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553) )
    {

        //NSLog(@"here");
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10001];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20001];

        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
        
    }else if((lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35001];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30001];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10002];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20002];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35002];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30002];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10003];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20003];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35003];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30003];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10004];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20004];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35004];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30004];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10005];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20005];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35005];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30005];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10006];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20006];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35006];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30006];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10007];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20007];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35007];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30007];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10008];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20008];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35008];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30008];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10009];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20009];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35009];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30009];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10010];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20010];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35010];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30010];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10011];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20011];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35011];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30011];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10012];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20012];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35012];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30012];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }
}

-(void)contactcanclejumperTapped:(int)textField
{
    //NSLog(@"cancle here");
    [self cancelNumberOfStageTapped:textField];
    
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553) )
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3025];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3049];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3073];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3097];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3121];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3145];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3169];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3193];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3217];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3241];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3265];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3289];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3313];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3337];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3361];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3385];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3409];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3433];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3457];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3481];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3505];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3529];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3553];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10001];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20001];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 +2];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 +2];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 +2];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 +2];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 +2];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 +2];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 +2];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 +2];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 +2];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 +2];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 +2];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 +2];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 +2];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 +2];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 +2];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 +2];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 +2];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 +2];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 +2];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 +2];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 +2];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 +2];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 +2];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 +2];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35001];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30001];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 3) || (lblgraftNum.tag ==3000 + 24 +3) || (lblgraftNum.tag == 3000 + 48 +3) || (lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 + 3 )||(lblgraftNum.tag ==3000 + 120 + 3) ||(lblgraftNum.tag ==3000 + 144 + 3) ||(lblgraftNum.tag ==3000 + 168 + 3) ||(lblgraftNum.tag ==3000 + 192 + 3) ||(lblgraftNum.tag ==3000 + 216 + 3) ||(lblgraftNum.tag ==3000 + 240 + 3) ||(lblgraftNum.tag ==3000 + 264 + 3) ||(lblgraftNum.tag ==3000 + 288 + 3) ||(lblgraftNum.tag ==3000 + 312 + 3) ||(lblgraftNum.tag ==3000 + 336 + 3) ||(lblgraftNum.tag ==3000 + 360 + 3) ||(lblgraftNum.tag ==3000 + 384 + 3) ||(lblgraftNum.tag ==3000 + 408 + 3) ||(lblgraftNum.tag ==3000 + 432 + 3) ||(lblgraftNum.tag ==3000 + 456 + 3) ||(lblgraftNum.tag ==3000 + 480 + 3) ||(lblgraftNum.tag ==3000 + 504 + 3) ||(lblgraftNum.tag ==3000 + 528 + 3) ||(lblgraftNum.tag ==3000 + 552 + 3))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 3];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 3];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 3];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 3];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 3];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 3];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 3];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 3];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 3];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 3];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 3];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 3];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 3];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 3];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 3];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 3];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 3];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 3];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 3];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 3];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 3];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 3];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 3];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 3];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10002];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20002];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 4)||(lblgraftNum.tag ==3000 + 24 + 4)||(lblgraftNum.tag == 3000 + 48 + 4)||(lblgraftNum.tag ==3000 + 72 + 4)||(lblgraftNum.tag == 3000 + 96 + 4 )||(lblgraftNum.tag ==3000 + 120 + 4) ||(lblgraftNum.tag ==3000 + 144 + 4) ||(lblgraftNum.tag ==3000 + 168 + 4) ||(lblgraftNum.tag ==3000 + 192 + 4) ||(lblgraftNum.tag ==3000 + 216 + 4) ||(lblgraftNum.tag ==3000 + 240 + 4) ||(lblgraftNum.tag ==3000 + 264 + 4) ||(lblgraftNum.tag ==3000 + 288 + 4) ||(lblgraftNum.tag ==3000 + 312 + 4) ||(lblgraftNum.tag ==3000 + 336 + 4) ||(lblgraftNum.tag ==3000 + 360 + 4) ||(lblgraftNum.tag ==3000 + 384 + 4) ||(lblgraftNum.tag ==3000 + 408 + 4) ||(lblgraftNum.tag ==3000 + 432 + 4) ||(lblgraftNum.tag ==3000 + 456 + 4) ||(lblgraftNum.tag ==3000 + 480 + 4) ||(lblgraftNum.tag ==3000 + 504 + 4) ||(lblgraftNum.tag ==3000 + 528 + 4) ||(lblgraftNum.tag ==3000 + 552 + 4))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 4];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 4];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 4];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 4];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 4];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 4];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 4];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 4];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 4];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 4];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 4];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 4];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 4];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 4];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 4];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 4];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 4];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 4];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 4];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 4];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 4];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 4];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 4];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 4];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35002];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30002];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 5)||(lblgraftNum.tag ==3000 + 24 + 5)||(lblgraftNum.tag == 3000 + 48 + 5)||(lblgraftNum.tag ==3000 + 72 + 5)||(lblgraftNum.tag == 3000 + 96 + 5 )||(lblgraftNum.tag ==3000 + 120 + 5) ||(lblgraftNum.tag ==3000 + 144 + 5) ||(lblgraftNum.tag ==3000 + 168 + 5) ||(lblgraftNum.tag ==3000 + 192 + 5) ||(lblgraftNum.tag ==3000 + 216 + 5) ||(lblgraftNum.tag ==3000 + 240 + 5) ||(lblgraftNum.tag ==3000 + 264 + 5) ||(lblgraftNum.tag ==3000 + 288 + 5) ||(lblgraftNum.tag ==3000 + 312 + 5) ||(lblgraftNum.tag ==3000 + 336 + 5) ||(lblgraftNum.tag ==3000 + 360 + 5) ||(lblgraftNum.tag ==3000 + 384 + 5) ||(lblgraftNum.tag ==3000 + 408 + 5) ||(lblgraftNum.tag ==3000 + 432 + 5) ||(lblgraftNum.tag ==3000 + 456 + 5) ||(lblgraftNum.tag ==3000 + 480 + 5) ||(lblgraftNum.tag ==3000 + 504 + 5) ||(lblgraftNum.tag ==3000 + 528 + 5) ||(lblgraftNum.tag ==3000 + 552 + 5))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 5];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 5];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 5];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 5];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 5];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 5];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 5];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 5];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 5];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 5];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 5];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 5];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 5];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 5];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 5];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 5];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 5];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 5];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 5];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 5];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 5];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 5];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 5];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 5];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10003];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20003];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 6)||(lblgraftNum.tag ==3000 + 24 + 6)||(lblgraftNum.tag == 3000 + 48 + 6)||(lblgraftNum.tag ==3000 + 72 + 6)||(lblgraftNum.tag == 3000 + 96 + 6 )||(lblgraftNum.tag ==3000 + 120 + 6) ||(lblgraftNum.tag ==3000 + 144 + 6) ||(lblgraftNum.tag ==3000 + 168 + 6) ||(lblgraftNum.tag ==3000 + 192 + 6) ||(lblgraftNum.tag ==3000 + 216 + 6) ||(lblgraftNum.tag ==3000 + 240 + 6) ||(lblgraftNum.tag ==3000 + 264 + 6) ||(lblgraftNum.tag ==3000 + 288 + 6) ||(lblgraftNum.tag ==3000 + 312 + 6) ||(lblgraftNum.tag ==3000 + 336 + 6) ||(lblgraftNum.tag ==3000 + 360 + 6) ||(lblgraftNum.tag ==3000 + 384 + 6) ||(lblgraftNum.tag ==3000 + 408 + 6) ||(lblgraftNum.tag ==3000 + 432 + 6) ||(lblgraftNum.tag ==3000 + 456 + 6) ||(lblgraftNum.tag ==3000 + 480 + 6) ||(lblgraftNum.tag ==3000 + 504 + 6) ||(lblgraftNum.tag ==3000 + 528 + 6) ||(lblgraftNum.tag ==3000 + 552 + 6))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 6];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 6];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 6];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 6];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 6];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 6];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 6];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 6];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 6];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 6];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 6];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 6];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 6];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 6];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 6];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 6];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 6];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 6];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 6];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 6];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 6];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 6];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 6];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 6];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35003];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30003];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 7)||(lblgraftNum.tag ==3000 + 24 + 7)||(lblgraftNum.tag == 3000 + 48 + 7)||(lblgraftNum.tag ==3000 + 72 + 7)||(lblgraftNum.tag == 3000 + 96 + 7 )||(lblgraftNum.tag ==3000 + 120 + 7) ||(lblgraftNum.tag ==3000 + 144 + 7) ||(lblgraftNum.tag ==3000 + 168 + 7) ||(lblgraftNum.tag ==3000 + 192 + 7) ||(lblgraftNum.tag ==3000 + 216 + 7) ||(lblgraftNum.tag ==3000 + 240 + 7) ||(lblgraftNum.tag ==3000 + 264 + 7) ||(lblgraftNum.tag ==3000 + 288 + 7) ||(lblgraftNum.tag ==3000 + 312 + 7) ||(lblgraftNum.tag ==3000 + 336 + 7) ||(lblgraftNum.tag ==3000 + 360 + 7) ||(lblgraftNum.tag ==3000 + 384 + 7) ||(lblgraftNum.tag ==3000 + 408 + 7) ||(lblgraftNum.tag ==3000 + 432 + 7) ||(lblgraftNum.tag ==3000 + 456 + 7) ||(lblgraftNum.tag ==3000 + 480 + 7) ||(lblgraftNum.tag ==3000 + 504 + 7) ||(lblgraftNum.tag ==3000 + 528 + 7) ||(lblgraftNum.tag ==3000 + 552 + 7))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 7];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 7];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 7];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 7];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 7];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 7];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 7];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 7];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 7];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 7];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 7];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 7];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 7];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 7];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 7];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 7];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 7];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 7];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 7];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 7];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 7];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 7];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 7];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 7];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10004];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20004];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 8)||(lblgraftNum.tag ==3000 + 24 + 8)||(lblgraftNum.tag == 3000 + 48 + 8)||(lblgraftNum.tag ==3000 + 72 + 8)||(lblgraftNum.tag == 3000 + 96 + 8 )||(lblgraftNum.tag ==3000 + 120 + 8) ||(lblgraftNum.tag ==3000 + 144 + 8) ||(lblgraftNum.tag ==3000 + 168 + 8) ||(lblgraftNum.tag ==3000 + 192 + 8) ||(lblgraftNum.tag ==3000 + 216 + 8) ||(lblgraftNum.tag ==3000 + 240 + 8) ||(lblgraftNum.tag ==3000 + 264 + 8) ||(lblgraftNum.tag ==3000 + 288 + 8) ||(lblgraftNum.tag ==3000 + 312 + 8) ||(lblgraftNum.tag ==3000 + 336 + 8) ||(lblgraftNum.tag ==3000 + 360 + 8) ||(lblgraftNum.tag ==3000 + 384 + 8) ||(lblgraftNum.tag ==3000 + 408 + 8) ||(lblgraftNum.tag ==3000 + 432 + 8) ||(lblgraftNum.tag ==3000 + 456 + 8) ||(lblgraftNum.tag ==3000 + 480 + 8) ||(lblgraftNum.tag ==3000 + 504 + 8) ||(lblgraftNum.tag ==3000 + 528 + 8) ||(lblgraftNum.tag ==3000 + 552 + 8))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 8];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 8];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 8];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 8];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 8];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 8];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 8];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 8];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 8];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 8];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 8];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 8];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 8];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 8];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 8];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 8];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 8];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 8];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 8];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 8];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 8];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 8];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 8];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 8];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35004];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30004];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 9)||(lblgraftNum.tag ==3000 + 24 + 9)||(lblgraftNum.tag == 3000 + 48 + 9)||(lblgraftNum.tag ==3000 + 72 + 9)||(lblgraftNum.tag == 3000 + 96 + 9 )||(lblgraftNum.tag ==3000 + 120 + 9) ||(lblgraftNum.tag ==3000 + 144 + 9) ||(lblgraftNum.tag ==3000 + 168 + 9) ||(lblgraftNum.tag ==3000 + 192 + 9) ||(lblgraftNum.tag ==3000 + 216 + 9) ||(lblgraftNum.tag ==3000 + 240 + 9) ||(lblgraftNum.tag ==3000 + 264 + 9) ||(lblgraftNum.tag ==3000 + 288 + 9) ||(lblgraftNum.tag ==3000 + 312 + 9) ||(lblgraftNum.tag ==3000 + 336 + 9) ||(lblgraftNum.tag ==3000 + 360 + 9) ||(lblgraftNum.tag ==3000 + 384 + 9) ||(lblgraftNum.tag ==3000 + 408 + 9) ||(lblgraftNum.tag ==3000 + 432 + 9) ||(lblgraftNum.tag ==3000 + 456 + 9) ||(lblgraftNum.tag ==3000 + 480 + 9) ||(lblgraftNum.tag ==3000 + 504 + 9) ||(lblgraftNum.tag ==3000 + 528 + 9) ||(lblgraftNum.tag ==3000 + 552 + 9))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 9];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 9];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 9];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 9];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 9];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 9];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 9];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 9];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 9];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 9];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 9];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 9];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 9];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 9];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 9];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 9];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 9];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 9];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 9];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 9];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 9];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 9];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 9];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 9];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10005];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20005];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 10];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 10];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 10];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 10];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 10];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 10];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 10];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 10];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 10];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 10];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 10];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 10];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 10];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 10];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 10];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 10];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 10];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 10];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 10];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 10];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 10];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 10];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 10];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 10];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35005];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30005];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 11];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 11];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 11];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 11];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 11];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 11];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 11];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 11];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 11];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 11];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 11];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 11];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 11];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 11];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 11];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 11];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 11];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 11];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 11];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 11];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 11];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 11];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 11];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 11];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10006];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20006];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 12];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 12];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 12];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 12];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 12];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 12];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 12];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 12];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 12];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 12];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 12];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 12];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 12];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 12];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 12];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 12];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 12];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 12];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 12];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 12];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 12];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 12];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 12];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 12];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35006];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30006];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 13];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 13];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 13];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 13];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 13];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 13];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 13];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 13];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 13];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 13];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 13];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 13];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 13];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 13];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 13];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 13];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 13];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 13];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 13];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 13];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 13];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 13];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 13];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 13];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10007];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20007];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001 + 14];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 14];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 14];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 14];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 14];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 14];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 14];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 14];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 14];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 14];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 14];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 14];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 14];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 14];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 14];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 14];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 14];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 14];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 14];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 14];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 14];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 14];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 14];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 14];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35007];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30007];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 15];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 15];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 15];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 15];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 15];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 15];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 15];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 15];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 15];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 15];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 15];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 15];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 15];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 15];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 15];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 15];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 15];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 15];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 15];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 15];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 15];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 15];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 15];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 15];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10008];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20008];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 16];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 16];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 16];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 16];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 16];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 16];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 16];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 16];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 16];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 16];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 16];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 16];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 16];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 16];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 16];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 16];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 16];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 16];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 16];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 16];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 16];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 16];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 16];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 16];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35008];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30008];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 17];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 17];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 17];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 17];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 17];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 17];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 17];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 17];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 17];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 17];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 17];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 17];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 17];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 17];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 17];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 17];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 17];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 17];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 17];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 17];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 17];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 17];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 17];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 17];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10009];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20009];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 18];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 18];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 18];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 18];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 18];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 18];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 18];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 18];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 18];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 18];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 18];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 18];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 18];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 18];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 18];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 18];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 18];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 18];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 18];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 18];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 18];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 18];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 18];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 18];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35009];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30009];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 19];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 19];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 19];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 19];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 19];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 19];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 19];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 19];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 19];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 19];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 19];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 19];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 19];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 19];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 19];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 19];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 19];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 19];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 19];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 19];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 19];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 19];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 19];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 19];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10010];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20010];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 20];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 20];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 20];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 20];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 20];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 20];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 20];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 20];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 20];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 20];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 20];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 20];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 20];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 20];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 20];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 20];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 20];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 20];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 20];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 20];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 20];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 20];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 20];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 20];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35010];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30010];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 21];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 21];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 21];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 21];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 21];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 21];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 21];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 21];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 21];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 21];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 21];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 21];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 21];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 21];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 21];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 21];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 21];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 21];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 21];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 21];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 21];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 21];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 21];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 21];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10011];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20011];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 22];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 22];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 22];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 22];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 22];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 22];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 22];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 22];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 22];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 22];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 22];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 22];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 22];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 22];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 22];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 22];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 22];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 22];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 22];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 22];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 22];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 22];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 22];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 22];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35011];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30011];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 23];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 23];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 23];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 23];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 23];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 23];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 23];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 23];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 23];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 23];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 23];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 23];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 23];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 23];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 23];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 23];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 23];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 23];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 23];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 23];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 23];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 23];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 23];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 23];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10012];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:20012];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 24];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 24];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 24];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 24];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 24];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 24];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 24];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 24];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 24];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 24];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 24];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 24];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 24];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 24];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 24];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 24];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 24];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 24];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 24];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 24];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 24];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 24];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 24];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 24];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35012];
            UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30012];
            imgrighttwingView.hidden=YES;
            imgballView.hidden=YES;
        }
        
    }
    
}

/*======================================table graft=====================================================*/

-(void)initwithGraft
{
    int widthOffset = 0;
    int yOffset=24;
    int xOffset=0;
    
    for(int i = 0; i < 576; i++)
    {
        if(i != 0 && (i%24) == 0)
        {
            yOffset += 27;
            xOffset = 0;
        }
        
        /*===============column 1======================*/
        
        UIButton *gbtnsaveEvent = [[UIButton alloc] initWithFrame:CGRectMake(251+widthOffset*xOffset, yOffset+ 287, 43, 27)];
        //gbtnsaveEvent.layer.borderColor=[UIColor blackColor].CGColor;
        //gbtnsaveEvent.layer.borderWidth = 1;
        gbtnsaveEvent.tag=i+1;
        
        [gbtnsaveEvent addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchUpInside];
        //[gbtnsaveEvent addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
        [gbtnsaveEvent addTarget:self action:@selector(touchDownRepeat:) forControlEvents:UIControlEventTouchDownRepeat];
        
        [self.mainscrollview addSubview:gbtnsaveEvent];

        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(251 +widthOffset*xOffset, yOffset+ 287, 45, 27)];
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 3000+1;
        
        //NSString *contactid=[[NSString alloc] initWithFormat:@"%d",i+1];
        //glblgraftNum1.text=contactid;
        
        [self.mainscrollview addSubview:glblgraftNum1];
        
        
        UILabel *glblvalueposition = [[UILabel alloc] initWithFrame:CGRectMake(251 +widthOffset*xOffset, yOffset+ 287, 45, 27)];
        glblvalueposition.textAlignment = UITextAlignmentCenter;
        glblvalueposition.textColor = [UIColor clearColor];
        glblvalueposition.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblvalueposition.backgroundColor = [UIColor clearColor];
        glblvalueposition.tag=i + 9000+1;
        [self.mainscrollview addSubview:glblvalueposition];
        
        
        /**/
        UIImageView *gimgpictureoverup= [[UIImageView alloc]initWithFrame:CGRectMake(260+widthOffset*xOffset, yOffset+ 308, 26, 8)];
        gimgpictureoverup.tag=i+ 40000+1;
        gimgpictureoverup.hidden=YES;
        gimgpictureoverup.image=[UIImage imageNamed:@"u_large.png"];
        [self.mainscrollview addSubview:gimgpictureoverup];
        
        UIImageView *gimgpicture0= [[UIImageView alloc]initWithFrame:CGRectMake(260+widthOffset*xOffset, yOffset+ 287, 26, 27)];
        gimgpicture0.tag=i+ 2000+1;
        [self.mainscrollview addSubview:gimgpicture0];
        
        UIImageView *gimgpictureoverdwn= [[UIImageView alloc]initWithFrame:CGRectMake(260+widthOffset*xOffset, yOffset+ 287, 26, 8)];
        gimgpictureoverdwn.tag=i+ 50000+1;
        gimgpictureoverdwn.hidden=YES;
        gimgpictureoverdwn.image=[UIImage imageNamed:@"n_large.png"];
        [self.mainscrollview addSubview:gimgpictureoverdwn];
        
        widthOffset=43;
        xOffset++;
    }
    
    
    self.imgtblcontact.backgroundColor=[UIColor clearColor];
    [self.mainscrollview addSubview:self.imgtblcontact];
    
    
}


-(void)initwithGraftrowcolor
{
    int widthOffset = 0;
    int yOffset=24;
    int xOffset=0;
    
    for(int i = 0; i < 576; i++)
    {
        if(i != 0 && (i%24) == 0)
        {
            yOffset += 27;
            xOffset = 0;
        }
        
        
        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(250 +widthOffset*xOffset, yOffset+ 287, 45, 27)];
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 8000+1;
        
        [self.mainscrollview addSubview:glblgraftNum1];
        //[glblgraftNum1 release];
        
        
        widthOffset=43;
        xOffset++;
    }
    
}

-(void)initwithtblSwitchrowcolor
{
    int widthOffset = 0;
    int yOffset=1;
    int xOffset=0;
    
    for(int i = 0; i < 24; i++)
    {
        if(i != 0 && (i%1) == 0)
        {
            yOffset += 27;
            xOffset = 0;
        }

        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(205 +widthOffset*xOffset, yOffset+ 310, 45, 27)];
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 7000+1;
        
        [self.mainscrollview addSubview:glblgraftNum1];

        
        /*
        UILabel *glblgraftNumextra = [[UILabel alloc] initWithFrame:CGRectMake(1284 +widthOffset*xOffset, yOffset+ 310, 45, 27)];
        glblgraftNumextra.textAlignment = UITextAlignmentCenter;
        glblgraftNumextra.textColor = [UIColor blackColor];
        glblgraftNumextra.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNumextra.backgroundColor = [UIColor clearColor];
        glblgraftNumextra.tag=i + 7100+1;
        
        [self.mainscrollview addSubview:glblgraftNumextra];
        [glblgraftNumextra release];
        */
        widthOffset=43;
        xOffset++;
    }
}

/*======================================table graft picker view=====================================================*/

#pragma mark Button UIControl Actions
- (void) touchDown:(id)sender
{
	//NSLog(@"Touch Down");
	// give it 0.2 sec for second touch
	[self performSelector:@selector(singleTapOnButton:) withObject:sender afterDelay:0.1];
}

- (void) touchDownRepeat:(id)sender
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTapOnButton:) object:sender];
	[self doubleTapOnButton:sender];
}

- (void) singleTapOnButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int btntag=[button tag];
//    NSLog(@"btntag>>>%d",btntag);
    NSString *getbtnid=[[NSString alloc] initWithFormat:@"%d",btntag];
    
    for(int i = 0 ; i < self.mainscrollview.subviews.count; i++)
    {
    
        UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000];

        /*----*/
        UILabel *lblgraftNumup=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000- 24];
        UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000+ 24];
        NSString *txtvalueup=[[NSString alloc]initWithFormat:@"%@", lblgraftNumup.text];
        NSString *txtvaluedwn=[[NSString alloc]initWithFormat:@"%@", lblgraftNumdwn.text];
        
        UILabel *lblgraftvalueposition=(UILabel *)[self.mainscrollview viewWithTag:btntag+ 9000];
        /*----*/
 
        if (lblgraftNum.text==Nil)
        {

            if ((lblgraftNumup.text==Nil && lblgraftNumdwn.text==Nil) || ([txtvalueup isEqualToString:@"0"] && [txtvaluedwn isEqualToString:@"0"]) || (lblgraftNumup.text==Nil && [txtvaluedwn isEqualToString:@"0"]) || (lblgraftNumdwn.text==Nil && [txtvalueup isEqualToString:@"0"]))
            {

                    UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 2000];
                    
                    [imageView setBackgroundColor:[UIColor blackColor]];
                    lblgraftNum.text=@"0";
                    lblgraftvalueposition.text=@"0";
                
                    [self contactjumperTapped:btntag];
                
                
                [switchjumperAngleview getSwitchAngle1:getbtnid];
            }

        }else
        {

            UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+2000];
            
            [imageView setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *imageViewoverupp=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 50000];
            UIImageView *imageViewoverdwnn=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 40000];
            imageViewoverupp.hidden=YES;
            imageViewoverdwnn.hidden=YES;
            //[imageViewoverupp setBackgroundColor:[UIColor clearColor]];
            //[imageViewoverdwnn setBackgroundColor:[UIColor clearColor]];
            [switchjumperAngleview cancleSwitchAngle:getbtnid];
            
            lblgraftNum.text=nil;

                if (![txtvalueup isEqualToString:@"0"])
                {
                    
                    UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 40000 - 24];
                    imageViewover.hidden=YES;
                    //[imageViewover setBackgroundColor:[UIColor clearColor]];
                    
                    lblgraftNumup.text=nil;                   
                    lblgraftvalueposition.text=nil;
                    
                }

                if (![txtvaluedwn isEqualToString:@"0"])
                {
                    
                    UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 50000 + 24];
                    imageViewover.hidden=YES;
                    //[imageViewover setBackgroundColor:[UIColor clearColor]];
                    lblgraftNumdwn.text=nil;
                    lblgraftvalueposition.text=nil;
                }
            
            [self contactcanclejumperTapped:btntag];

        }
        
        
        //NSLog(@"btntag>>>%d", btntag);
        //NSLog(@"lblgraftNum.text>>>%@", lblgraftNum.text);
        
        break;
    }
    
    
} 

- (void) doubleTapOnButton:(id)sender
{
    SwitchingAngleSelect= NO;
    tblgraftSelect = YES;
    
    UIButton *button = (UIButton *)sender;

    int btntag=[button tag];
    
    for(int i = 0 ; i < self.mainscrollview.subviews.count; i++)
    {
        
        UILabel *lblgraftNumup=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000- 24];
        UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000+ 24];

        if (lblgraftNumup.text!=nil || lblgraftNumdwn.text!=nil)
        {

            NSString *txtvalueup=[[NSString alloc]initWithFormat:@"%@", lblgraftNumup.text];
            NSString *txtvaluedwn=[[NSString alloc]initWithFormat:@"%@", lblgraftNumdwn.text];
            
            if (![txtvalueup isEqualToString:@"0"] || ![txtvaluedwn isEqualToString:@"0"])
            {
                if (showblackselectpicker==NO)
                {
                    self.fauxView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    self.fauxView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
                    //fauxView.alpha = 1.3;
                    
                    self.GraftNumPicker=[[UIPickerView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(216/2), 320, 216)];
                    self.GraftNumPicker.delegate=self;
                    self.GraftNumPicker.dataSource=self;
                    self.GraftNumPicker.showsSelectionIndicator=YES;
                    
                    UIButton *gbtnselectnum = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(50/2)+160, 320, 50)];
                    //[gbtnsaveEvent setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
                    gbtnselectnum.layer.borderColor=[UIColor whiteColor].CGColor;
                    gbtnselectnum.layer.borderWidth = 1;
                    //gbtnselectnum.buttonType=UIButtonTypeRoundedRect;
                    [gbtnselectnum setTitle:@"Select" forState:UIControlStateNormal];
                    gbtnselectnum.titleLabel.Font = [UIFont fontWithName:@"Helvetica-Bold" size:17 ];
                    gbtnselectnum.titleLabel.TextColor = [UIColor blackColor ];
                    [gbtnselectnum.layer setMasksToBounds:YES];
                    [gbtnselectnum.layer setCornerRadius:10.0f];
                    [gbtnselectnum.layer setBackgroundColor:[UIColor whiteColor].CGColor];
                    
                    gbtnselectnum.tag=[button tag];
                    [gbtnselectnum addTarget:self action:@selector(selectednumTapped:) forControlEvents:UIControlEventTouchDown];
                    
                    [self.fauxView addSubview:gbtnselectnum];
                    [self.fauxView addSubview: self.GraftNumPicker];
                    [self.view addSubview: self.fauxView];
                    
                }
            }
        }
        
        break;
    }
}

-(IBAction)selectednumTapped:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    
    int btntag=[button tag];
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //KNETblGraftPositionTag *gKNETblGraftPositionTag=[[[KNETblGraftPositionTag alloc]init]autorelease ];
    //NSArray *contactArr=[[[NSArray alloc]init ] autorelease];
    NSArray *contactArr=appDelegate.tblGraftPositiontag;
    
    
    for(int i = 0 ; i < self.mainscrollview.subviews.count; i++)
    {
        UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000];
        lblgraftNum.text=self.Noperson;
        
        UILabel *lblgraftNumup=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000- 24];
        UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:btntag+3000+ 24];
        
        UILabel *lblgraftvalueposition=(UILabel *)[self.mainscrollview viewWithTag:btntag+ 9000];

        if (lblgraftNumup.text!=nil || lblgraftNumdwn.text!=nil)
        {
            
            NSString *txtvalueup=[[NSString alloc]initWithFormat:@"%@", lblgraftNumup.text];
            NSString *txtvaluedwn=[[NSString alloc]initWithFormat:@"%@", lblgraftNumdwn.text];
            
            if (![txtvalueup isEqualToString:@"0"] || ![txtvaluedwn isEqualToString:@"0"])
            {
                /**/int deleteindex;
                //NSString *Sndmid;//=[[[NSString alloc]init] autorelease];
                
                for (int idexss = 0; idexss<contactArr.count; idexss++ )
                {
                    KNETblGraftPositionTag* key=[contactArr objectAtIndex:idexss];
                    
                    if ([key.imgGbtnTag intValue]==btntag)
                    {
                        NSLog(@"double tapped--->yes");
                        
                        deleteindex=idexss;
                        //clearStartTag=[key.SRStartPoint intValue];
                        //clearEndTag=[key.SREndPoint intValue];
                        
                        //Sndmid=@"Yes";
                        break;
                    }else
                    {
                        //NSLog(@"Pstartx--->no");
                    }
                    
                }
                
                
                if (![txtvalueup isEqualToString:@"0"])
                {
                    UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+2000];
                    
                    [imageView setBackgroundColor:[UIColor clearColor]];
                    
                    UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 40000];
                    imageViewover.hidden=NO;
                    //[imageViewover setBackgroundColor:[UIColor blackColor]];
                    
                    NSString *Vup=[[NSString alloc] initWithFormat:@"1"];
                    lblgraftvalueposition.text=Vup;
                    
                    //NSLog(@"lblgraftvalueposition.text.....>>>>>>%@",lblgraftvalueposition.text);
                    
                }else 
                {
                    UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+2000];
                    
                    [imageView setBackgroundColor:[UIColor clearColor]];
                    
                    UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 50000];
                    imageViewover.hidden=NO;
                    //[imageViewover setBackgroundColor:[UIColor blackColor]];
                    
                    NSString *Vdwn=[[NSString alloc] initWithFormat:@"2"];
                    lblgraftvalueposition.text=Vdwn;
                    
                    //NSLog(@"lblgraftvalueposition.text.....>>>>>>%@",lblgraftvalueposition.text);

                }
                                
            }else
            {
                lblgraftNum.text=nil;
                
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+2000];
                
                [imageView setBackgroundColor:[UIColor clearColor]];
                
                UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 50000];
                imageViewover.hidden=YES;
                //[imageViewover setBackgroundColor:[UIColor clearColor]];
            }
            
            //[txtvalueup release];
            //[txtvaluedwn release];
            
        }else
        {
            lblgraftNum.text=nil;
            
            UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+2000];
            
            [imageView setBackgroundColor:[UIColor clearColor]];
            
            UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 50000];
            imageViewover.hidden=YES;
            //[imageViewover setBackgroundColor:[UIColor clearColor]];
        }

        
        break;
    }
    
    [self.fauxView removeFromSuperview];
    for (UIView* child in self.fauxView.subviews) {
        [child removeFromSuperview];
    }

    SwitchingAngleSelect= NO;
    tblgraftSelect = NO;
}

- (void)stepperOneChanged:(UIStepper*)stepperOne{
    //This method would be called on the target of your first stepper on UIControlEventsValueChanged

    //double stepperValue = graftNumber.value;
    
    NSString *numbr = [NSString stringWithFormat:@"%.f", graftNumber.value];
    NSLog(@"numbr.>>>>>>%@",numbr);
    
    //self.stepperValueLabel.text = [NSString stringWithFormat:@"%.f", stepperValue];
    
    //Decrease the value by 1
    //graftNumber.value --;
    
    //OR
    //Increase the value by 1
    //graftNumber.value ++;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)GraftNumPicker{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *) GraftNumPicker numberOfRowsInComponent:(NSInteger)component{
    NSInteger Arrreturn;
    if (tblgraftSelect==YES)
    {
        Arrreturn= [graftNumbermArr count];
    }else if(SwitchingAngleSelect == YES)
    {
        Arrreturn= [graftNumbermArr2 count];
    }else if(SwitchTypeSelected == YES)
    {
        Arrreturn= [SwitchTypeMutblArr count];
    }else if(ProgramSelected == YES)
    {
        Arrreturn= [ProrgamMutblArr count];
    }else if(LookSelected == YES)
    {
        Arrreturn= [LookMutblArr count];
    }else if(MountingSelected == YES)
    {
        Arrreturn= [MountingMutblArr count];
    }else if(EscuteonSelected == YES)
    {
        Arrreturn= [EscuteonMutblArr count];
    }else if(HandleSeleted == YES)
    {
        Arrreturn= [HandleMutblArr count];
    }else if(LatchSelected == YES)
    {
        Arrreturn= [LatchMutblArr count];
    }else if(StopSelected == YES)
    {
        Arrreturn= [StopMutblArr count];
    }else if(SwitchAngle == YES)
    {
        Arrreturn= [SwitchAngleMutblArr count];
    }
    
    

    return Arrreturn;
}

-(NSString *)pickerView:(UIPickerView *)GraftNumPicker titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //NSString *Arrreturn=[[NSString alloc]init];
    if (tblgraftSelect==YES) {
        Noperson= [graftNumbermArr objectAtIndex:row];
    }else if(SwitchingAngleSelect == YES)
    {
        Noperson= [graftNumbermArr2 objectAtIndex:row];
    }else if(SwitchTypeSelected == YES)
    {
        Noperson= [SwitchTypeMutblArr objectAtIndex:row];
    }else if(ProgramSelected == YES)
    {
        Noperson= [ProrgamMutblArr objectAtIndex:row];
    }else if(LookSelected == YES)
    {
        Noperson= [LookMutblArr objectAtIndex:row];
    }else if(MountingSelected == YES)
    {
        Noperson= [MountingMutblArr objectAtIndex:row];
    }else if(EscuteonSelected == YES)
    {
        Noperson= [EscuteonMutblArr objectAtIndex:row];
    }else if(HandleSeleted == YES)
    {
        Noperson= [HandleMutblArr objectAtIndex:row];
    }else if(LatchSelected == YES)
    {
        Noperson= [LatchMutblArr objectAtIndex:row];
    }else if(StopSelected == YES)
    {
        Noperson= [StopMutblArr objectAtIndex:row];
    }else if(SwitchAngle == YES)
    {
        Noperson= [SwitchAngleMutblArr objectAtIndex:row];
    }
    
     //NSLog(@"Noperson>>>%@",Noperson);
    
    return Noperson;
}


-(void)pickerView:(UIPickerView *)GraftNumPicker didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (tblgraftSelect==YES) {
        Noperson= [graftNumbermArr objectAtIndex:row];
    }else if(SwitchingAngleSelect == YES)
    {
        Noperson= [graftNumbermArr2 objectAtIndex:row];
    }else if(SwitchTypeSelected == YES)
    {
        Noperson= [SwitchTypeMutblArr objectAtIndex:row];
    }else if(ProgramSelected == YES)
    {
        Noperson= [ProrgamMutblArr objectAtIndex:row];
    }else if(LookSelected == YES)
    {
        Noperson= [LookMutblArr objectAtIndex:row];
    }else if(MountingSelected == YES)
    {
        Noperson= [MountingMutblArr objectAtIndex:row];
    }else if(EscuteonSelected == YES)
    {
        Noperson= [EscuteonMutblArr objectAtIndex:row];
    }else if(HandleSeleted == YES)
    {
        Noperson= [HandleMutblArr objectAtIndex:row];
    }else if(LatchSelected == YES)
    {
        Noperson= [LatchMutblArr objectAtIndex:row];
    }else if(StopSelected == YES)
    {
        Noperson= [StopMutblArr objectAtIndex:row];
    }else if(SwitchAngle == YES)
    {
        Noperson= [SwitchAngleMutblArr objectAtIndex:row];
    }
    
}

/*===========================Switching Angle=====================================*/

-(IBAction)SwitchingAngleTapped:(id)sender
{
    SwitchingAngleSelect= YES;
    tblgraftSelect = NO;
    
    UIButton *button = (UIButton *)sender;
    
    //int btntag=[button tag];
    
    if (showblackselectpicker==NO)
    {
        self.fauxView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.fauxView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        //fauxView.alpha = 1.3;
        
        self.GraftNumPicker=[[UIPickerView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(216/2), 320, 216)];
        self.GraftNumPicker.delegate=self;
        self.GraftNumPicker.dataSource=self;
        self.GraftNumPicker.showsSelectionIndicator=YES;
        
        UIButton *gbtnselectnum = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(50/2)+160, 320, 50)];
        //[gbtnsaveEvent setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        gbtnselectnum.layer.borderColor=[UIColor whiteColor].CGColor;
        gbtnselectnum.layer.borderWidth = 1;
        //gbtnselectnum.buttonType=UIButtonTypeRoundedRect;
        [gbtnselectnum setTitle:@"Select" forState:UIControlStateNormal];
        gbtnselectnum.titleLabel.Font = [UIFont fontWithName:@"Helvetica-Bold" size:17 ];
        gbtnselectnum.titleLabel.TextColor = [UIColor blackColor ];
        [gbtnselectnum.layer setMasksToBounds:YES];
        [gbtnselectnum.layer setCornerRadius:10.0f];
        [gbtnselectnum.layer setBackgroundColor:[UIColor whiteColor].CGColor];
        
        gbtnselectnum.tag=[button tag];
        [gbtnselectnum addTarget:self action:@selector(SwitchingAngleSlectTappled:) forControlEvents:UIControlEventTouchDown];
        
        [self.fauxView addSubview:gbtnselectnum];
        [self.fauxView addSubview: self.GraftNumPicker];
        [self.view addSubview: self.fauxView];
        
    }
}

-(IBAction)SwitchingAngleSlectTappled:(id)sender
{
    self.lblSwitchingAngle.text=Noperson;
    
    //NSLog(@"Noperson2--->%@",Noperson);
    
    [self.fauxView removeFromSuperview];
    for (UIView* child in self.fauxView.subviews) {
        [child removeFromSuperview];
    }
    
    SwitchingAngleSelect= NO;
    tblgraftSelect = NO;
}

-(IBAction)closeTapped:(id)sender
{
    /*
    [self dismissModalViewControllerAnimated:YES];
    
    KNEMenuPageViewController *gKNEMenuPageViewController=[[KNEMenuPageViewController alloc] initWithNibName:@"KNEMenuPageViewController" bundle:nil];
    //UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gKNEMenuPageViewController];
    //tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    //gKNEMenuPageViewController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    //gKNEMenuPageViewController.navigationItem.titleView = label;
    //gKNEMenuPageViewController.title=@"Kraus & Naimer";
    //UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
    //[tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    [self presentModalViewController:gKNEMenuPageViewController animated:YES];
     */
}
-(void) DoneTapped:(id)gesture
{

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self performSelector:@selector(getSaveCasetapped) withObject:nil afterDelay:1];

 }  

-(void)getSaveCasetapped
{
    DBCase *createCase=[[DBCase alloc]init ];
    NSString *sw=txtSwitchType.text;
    NSString *pr=txtProgram.text;
    NSArray *Allitem=[createCase selectItem];

    if (Allitem.count!=0)
    {
        NSString *ckswpr;
        for (KNECase *caseitems in Allitem)
        {
            if ([caseitems.SwitchType isEqualToString: sw] && [caseitems.Program isEqualToString: pr]) {
                
                //NSLog(@"same");
                ckswpr=@"same";
                //txtSwitchType.text=nil;
                //txtProgram.text=nil;
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"The combination already in use." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                
                break;

            }else
            {
                //NSLog(@"nosame");
                ckswpr=@"nosame";
                
            }
        }
        
        if ([ckswpr isEqualToString:@"same"]) {
            
        }else{
            
            
                if ([txtSwitchType.text length] != 0)
                {
                    if ([txtProgram.text length] != 0)
                    {
                        //NSLog(@"hello done~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                        /*=================================save case===============================================*/
                        KNECase *crcase=[[KNECase alloc] init];
                        crcase.CaseName=txtPlateName.text;
                        crcase.FLook=self.txtlook.text;
                        crcase.FMounting=self.txtmounting.text;
                        crcase.FEscutcheon_plate=self.txtescutcheon.text;
                        crcase.FHandle=self.txthandle.text;
                        crcase.FLatch_mech=self.txtlatch_mech.text;
                        crcase.FStop=self.txtstop.text;
                        crcase.FStop_Degree=self.txtstop_degree.text;
                        crcase.FNofStage=self.txtno_of_stage.text;
                        crcase.FMasterfData=self.txtmaster_data.text;
                        crcase.FReference=self.txtreference.text;
                        crcase.FDate=self.txtdate.text;
                        crcase.FModify_Date=self.txtmodify_date.text;
                        crcase.FCustNo=self.txtcust_no.text;
                        crcase.FCompany=self.txtcompany.text;
                        crcase.FVersion=self.txtversion.text;
                        
                        crcase.pcs1=txtPcs1.text;
                        crcase.pcs2=txtPcs2.text;
                        crcase.pcs3=txtPcs3.text;
                        crcase.pcs4=txtPcs4.text;
                        crcase.pcs5=txtPcs5.text;
                        crcase.pcs6=txtPcs6.text;
                        crcase.pcs7=txtPcs7.text;
                        crcase.pcs8=txtPcs8.text;
                        crcase.pcs9=txtPcs9.text;
                        
                        crcase.optionalextra1=txtOptExtra1.text;
                        crcase.optionalextra2=txtOptExtra2.text;
                        crcase.optionalextra3=txtOptExtra3.text;
                        crcase.optionalextra4=txtOptExtra4.text;
                        crcase.optionalextra5=txtOptExtra5.text;
                        crcase.optionalextra6=txtOptExtra6.text;
                        crcase.optionalextra7=txtOptExtra7.text;
                        crcase.optionalextra8=txtOptExtra8.text;
                        crcase.optionalextra9=txtOptExtra9.text;
                        
                        crcase.extra_comment=txtinfocontent.text;
                        crcase.SwitchPoint=self.firstpNumbr;
                        
                        crcase.CustomerArticleNumber=txtcustomerArticleNum.text;
                        crcase.SwitchType=txtSwitchType.text;
                        crcase.Program=txtProgram.text;
                        crcase.PlateName=txtPlateName.text;
                        crcase.TotalSwitchingAngle=lblTotalSwitchingangle.text;
                        crcase.SwitchingAngle=lblSwitchingAngle.text;
                        
                        NSMutableArray *MutblnumberOfPositionArr=[[NSMutableArray alloc] init];
                        
                        for (int i=0; i<25; i++)
                        {
                            UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:300000000+i];
                            
                            if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0) {
                                
                                NSString *strContainTag=[[NSString alloc] initWithFormat:@"%d", glbltextvalue.tag];
                                KNESpringReturnContain *gKNESRContain=[[KNESpringReturnContain alloc] init];
                                //gKNESpringReturnContain.CaseID=strCaseID;
                                gKNESRContain.ContainTag=strContainTag;
                                gKNESRContain.ContainValue=glbltextvalue.text;
                                
                                [MutblnumberOfPositionArr addObject:gKNESRContain];
                                
                            }else
                            {
                                //NSLog(@"textboX~~nil!~~~~");
                            }
                        }
                        //NSLog(@"MutblSRContainArr.count>>>>>>>>>>>>>>>>>>%d",MutblnumberOfPositionArr.count);
                        
                        NSString *StrnumberOfPosition=[[NSString alloc] initWithFormat:@"%d",MutblnumberOfPositionArr.count];
                        crcase.numberOfPosition=StrnumberOfPosition;
                        
                        [createCase insertcontact:crcase];
                        
                        /*=================================save Switch Point===========================================*/
                        //gKNESwitchPoint.SwitchPoint
                        /*=================================load case===============================================*/
                        
                        NSArray *thiscaseIDArr= [createCase selectItemTopCase];
                        NSString *strCaseID;
                        for (KNECase *caseIDar in thiscaseIDArr)
                        {
                            //NSLog(@"caseIDar.CaseID>>>>>>>>>>>%@",caseIDar.CaseID);
                            strCaseID=[[NSString alloc] initWithFormat:@"%@",caseIDar.CaseID];
                            
                            if (strCaseID != nil)
                            {
                                
                                /*=================================save contact===============================================*/
                                NSMutableArray *contactMutblArr=[[NSMutableArray alloc]init ];
                                
                                NSString *btntag;
                                NSString *strvalue;
                                NSString *VP;
                                
                                for(int i = 1 ; i <= 576; i++)
                                {
                                    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:i+3000];
                                    UILabel *lblgraftNumVP=(UILabel *)[self.mainscrollview viewWithTag:i+9000];
                                    
                                    if (lblgraftNum.text!=nil)
                                    {
                                        btntag=[[NSString alloc]initWithFormat:@"%d",i];
                                        strvalue=[[NSString alloc]initWithFormat:@"%@",lblgraftNum.text];
                                        VP=[[NSString alloc]initWithFormat:@"%@",lblgraftNumVP.text];
                                        
                                        
                                        KNETblGraftPositionTag *ggKNETblGraftPositionTag=[[KNETblGraftPositionTag alloc]init];
                                        ggKNETblGraftPositionTag.imgGbtnTag=btntag;
                                        ggKNETblGraftPositionTag.imgGbtnValueTag=strvalue;
                                        ggKNETblGraftPositionTag.CaseID=strCaseID;
                                        ggKNETblGraftPositionTag.Vposition=VP;
                                        
                                        
                                        [contactMutblArr addObject:ggKNETblGraftPositionTag];
                                    }
                                }
                                
                                DBTableGraft *gDBTableGraft=[[DBTableGraft alloc]init ];
                                [gDBTableGraft insertItemArray:contactMutblArr];
                                
                                /*=================================save Spring Return===============================================*/
                                
                                
                                KNEAppDelegate *appDelegateSave = [UIApplication sharedApplication].delegate;
                                appDelegateSave.gKNECase=caseIDar;
                                NSArray *positionsArr=self.springReturnPosition;
                                NSMutableArray *SRMutblArr=[[NSMutableArray alloc]init ];
                                
                                for (KNESpringRPosition *itemSpringreturn in positionsArr)
                                {
                                    //NSLog(@"itemSpringreturn.SRStartPoint>>>>%@",itemSpringreturn.SRStartPoint);
                                    //NSLog(@"itemSpringreturn.SREndPoint>>>>%@",itemSpringreturn.SREndPoint);
                                    
                                    KNESpringRPosition *gKNESpringRPosition=[[KNESpringRPosition alloc] init];
                                    gKNESpringRPosition.CaseID=strCaseID;
                                    gKNESpringRPosition.SRStartPoint=itemSpringreturn.SRStartPoint;
                                    gKNESpringRPosition.SREndPoint=itemSpringreturn.SREndPoint;
                                    
                                    [SRMutblArr addObject:gKNESpringRPosition];
                                }
                                
                                DBSpringReturn *gDBSpringReturn=[[DBSpringReturn alloc] init];
                                [gDBSpringReturn insertItemArray:SRMutblArr];
                                
                                /*=================================save Spring Return Contain===============================================*/
                                
                                NSMutableArray *MutblSRContainArr=[[NSMutableArray alloc] init];

                                for (int i=0; i<25; i++)
                                {
                                    UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:300000000+i];
                                    
                                    if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0) {
                                        
                                        NSString *strContainTag=[[NSString alloc] initWithFormat:@"%d", glbltextvalue.tag];
                                        KNESpringReturnContain *gKNESpringReturnContain=[[KNESpringReturnContain alloc] init];
                                        gKNESpringReturnContain.CaseID=strCaseID;
                                        gKNESpringReturnContain.ContainTag=strContainTag;
                                        gKNESpringReturnContain.ContainValue=glbltextvalue.text;
                                        
                                        [MutblSRContainArr addObject:gKNESpringReturnContain];
                                        
                                    }else
                                    {
                                        //NSLog(@"textboX~~nil!~~~~");
                                    }
                                }
                                NSLog(@"MutblSRContainArr.count>>>>>>>>>>>>>>>>>>%d",MutblSRContainArr.count);
                                DBSpringReturnContain *gDBSpringReturnContain=[[DBSpringReturnContain alloc] init];
                                
                                [gDBSpringReturnContain insertItemSRContainArray:MutblSRContainArr];
                                
                                /*=================================save Switching First Value===============================================*/
                                
                                //NSLog(@"appDelegateSave.firstPnum>>>>>>>>>>>>>>>>>%@",appDelegateSave.firstPnum);
                                
                                /**/
                                DBSwitchingfirstValue *dbDBSwitchingfirstValue=[[DBSwitchingfirstValue alloc ] init];
                                KNESwitchingFirstValue *gKNESwitchingFirstValue=[[KNESwitchingFirstValue alloc] init];
                                
                                gKNESwitchingFirstValue.CaseID=strCaseID;
                                gKNESwitchingFirstValue.SwitchingFirstValue=appDelegateSave.firstPnum;
                                
                                
                                [dbDBSwitchingfirstValue insertSwitchingFirstValue:gKNESwitchingFirstValue];
                                
                                /*=================================save jumper===============================================*/
                                
                                [jumperview storejumper];
                                
                                /*=================================clear==========================================*/
                                //[appDelegateSave clearSpringReturn];
                                [appDelegateSave clearKNECase];
                                [appDelegateSave clearfirstPnum];
                                /*=================================go to next page==========================================
                                 
                                 
                                 KNESwitchShowViewController *gKNESwitchShowViewController=[[KNESwitchShowViewController alloc] initWithNibName:@"KNESwitchShowViewController" bundle:nil];
                                 gKNESwitchShowViewController.KNECaseclass=caseIDar;
                                 [self.navigationController pushViewController:gKNESwitchShowViewController animated:YES];
                                 [gKNESwitchShowViewController release];
                                 ==
                                 
                                 UIViewController* menuViewController=nil;
                                 for(UIViewController* viewController in self.navigationController.viewControllers)
                                 {
                                 if([viewController isKindOfClass:[SwitchMainPageViewController class]])
                                 {
                                 menuViewController=viewController;
                                 break;
                                 }
                                 }
                                 ===*/
                                //[self.navigationController popToViewController:menuViewController animated:YES];
                                //[self.navigationController popViewControllerAnimated:YES];
                                
                                //[self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                                //UILabel *lblServiceTypeSelected=(UILabel*)[self.gBpNewAppointmentViewController lblemail];
                                //[[(KNESwitchMainPageViewController *)self.gSwitchMainPageViewController gtblSwitchMainPageViewController] reloadData];
                                
                                //[[(SwitchMainPageViewController *)self.gSwitchMainPageViewController gtblSwitchMainPageViewController] reloadInputViews];
                                
                                //UITableView *tblCase=(UITableView *)[self.gSwitchMainPageViewController gtblSwitchMainPageViewController];
                                //[tblCase reloadInputViews];
                                //[self.navigationController popToRootViewControllerAnimated:YES];
                                [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                                
                            }else
                            {
                                NSLog(@"nilllll");
                            }
                            
                        }
                        
                        //[createCase release];
                    }else
                    {
                        //NSLog(@"Please insert plate name!!");
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                        message:@"Please Insert Program" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        
                        [alert show];
                    }
                }else
                {
                    //NSLog(@"Please insert plate name!!");
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Please Insert Switch Type" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [alert show];
                }
            /*
            if ([txtPlateName.text length] != 0)
            {    
            }else
            {
                //NSLog(@"Please insert plate name!!");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Please Insert Plate Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
                [alert release];
            }
            */
            
        }
        
    }else{
        NSLog(@"0");
        
        
            if ([txtSwitchType.text length] != 0)
            {
                if ([txtProgram.text length] != 0)
                {
                    //NSLog(@"hello done~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                    /*=================================save case===============================================*/
                    KNECase *crcase=[[KNECase alloc] init];
                    crcase.CaseName=txtPlateName.text;
                    crcase.FLook=self.txtlook.text;
                    crcase.FMounting=self.txtmounting.text;
                    crcase.FEscutcheon_plate=self.txtescutcheon.text;
                    crcase.FHandle=self.txthandle.text;
                    crcase.FLatch_mech=self.txtlatch_mech.text;
                    crcase.FStop=self.txtstop.text;
                    crcase.FStop_Degree=self.txtstop_degree.text;
                    crcase.FNofStage=self.txtno_of_stage.text;
                    crcase.FMasterfData=self.txtmaster_data.text;
                    crcase.FReference=self.txtreference.text;
                    crcase.FDate=self.txtdate.text;
                    crcase.FModify_Date=self.txtmodify_date.text;
                    crcase.FCustNo=self.txtcust_no.text;
                    crcase.FCompany=self.txtcompany.text;
                    crcase.FVersion=self.txtversion.text;
                    
                    crcase.pcs1=txtPcs1.text;
                    crcase.pcs2=txtPcs2.text;
                    crcase.pcs3=txtPcs3.text;
                    crcase.pcs4=txtPcs4.text;
                    crcase.pcs5=txtPcs5.text;
                    crcase.pcs6=txtPcs6.text;
                    crcase.pcs7=txtPcs7.text;
                    crcase.pcs8=txtPcs8.text;
                    crcase.pcs9=txtPcs9.text;
                    
                    crcase.optionalextra1=txtOptExtra1.text;
                    crcase.optionalextra2=txtOptExtra2.text;
                    crcase.optionalextra3=txtOptExtra3.text;
                    crcase.optionalextra4=txtOptExtra4.text;
                    crcase.optionalextra5=txtOptExtra5.text;
                    crcase.optionalextra6=txtOptExtra6.text;
                    crcase.optionalextra7=txtOptExtra7.text;
                    crcase.optionalextra8=txtOptExtra8.text;
                    crcase.optionalextra9=txtOptExtra9.text;
                    
                    crcase.extra_comment=txtinfocontent.text;
                    crcase.SwitchPoint=self.firstpNumbr;
                    
                    crcase.CustomerArticleNumber=txtcustomerArticleNum.text;
                    crcase.SwitchType=txtSwitchType.text;
                    crcase.Program=txtProgram.text;
                    crcase.PlateName=txtPlateName.text;
                    crcase.TotalSwitchingAngle=lblTotalSwitchingangle.text;
                    crcase.SwitchingAngle=lblSwitchingAngle.text;
                    
                    NSMutableArray *MutblnumberOfPositionArr=[[NSMutableArray alloc] init];
                    
                    for (int i=0; i<25; i++)
                    {
                        UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:300000000+i];
                        
                        if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0) {
                            
                            NSString *strContainTag=[[NSString alloc] initWithFormat:@"%d", glbltextvalue.tag];
                            KNESpringReturnContain *gKNESRContain=[[KNESpringReturnContain alloc] init];
                            //gKNESpringReturnContain.CaseID=strCaseID;
                            gKNESRContain.ContainTag=strContainTag;
                            gKNESRContain.ContainValue=glbltextvalue.text;
                            
                            [MutblnumberOfPositionArr addObject:gKNESRContain];
                            
                        }else
                        {
                            //NSLog(@"textboX~~nil!~~~~");
                        }
                    }
                    //NSLog(@"MutblSRContainArr.count>>>>>>>>>>>>>>>>>>%d",MutblnumberOfPositionArr.count);
                    
                    NSString *StrnumberOfPosition=[[NSString alloc] initWithFormat:@"%d",MutblnumberOfPositionArr.count];
                    crcase.numberOfPosition=StrnumberOfPosition;
                    
                    [createCase insertcontact:crcase];
                    
                    /*=================================save Switch Point===========================================*/
                    //gKNESwitchPoint.SwitchPoint
                    /*=================================load case===============================================*/
                    
                    NSArray *thiscaseIDArr= [createCase selectItemTopCase];
                    NSString *strCaseID;
                    for (KNECase *caseIDar in thiscaseIDArr)
                    {
                        //NSLog(@"caseIDar.CaseID>>>>>>>>>>>%@",caseIDar.CaseID);
                        strCaseID=[[NSString alloc] initWithFormat:@"%@",caseIDar.CaseID];
                        
                        if (strCaseID != nil)
                        {
                            
                            /*=================================save contact===============================================*/
                            NSMutableArray *contactMutblArr=[[NSMutableArray alloc]init ];
                            
                            NSString *btntag;
                            NSString *strvalue;
                            NSString *VP;
                            
                            for(int i = 1 ; i <= 576; i++)
                            {
                                UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:i+3000];
                                UILabel *lblgraftNumVP=(UILabel *)[self.mainscrollview viewWithTag:i+9000];
                                
                                if (lblgraftNum.text!=nil)
                                {
                                    btntag=[[NSString alloc]initWithFormat:@"%d",i];
                                    strvalue=[[NSString alloc]initWithFormat:@"%@",lblgraftNum.text];
                                    VP=[[NSString alloc]initWithFormat:@"%@",lblgraftNumVP.text];
                                    
                                    
                                    KNETblGraftPositionTag *ggKNETblGraftPositionTag=[[KNETblGraftPositionTag alloc]init];
                                    ggKNETblGraftPositionTag.imgGbtnTag=btntag;
                                    ggKNETblGraftPositionTag.imgGbtnValueTag=strvalue;
                                    ggKNETblGraftPositionTag.CaseID=strCaseID;
                                    ggKNETblGraftPositionTag.Vposition=VP;
                                    
                                    
                                    [contactMutblArr addObject:ggKNETblGraftPositionTag];
                                }
                            }
                            
                            DBTableGraft *gDBTableGraft=[[DBTableGraft alloc]init ];
                            [gDBTableGraft insertItemArray:contactMutblArr];
                            
                            /*=================================save Spring Return===============================================*/
                            
                            
                            KNEAppDelegate *appDelegateSave = [UIApplication sharedApplication].delegate;
                            appDelegateSave.gKNECase=caseIDar;
                            NSArray *positionsArr=self.springReturnPosition;
                            NSMutableArray *SRMutblArr=[[NSMutableArray alloc]init ];
                            
                            for (KNESpringRPosition *itemSpringreturn in positionsArr)
                            {
                                //NSLog(@"itemSpringreturn.SRStartPoint>>>>%@",itemSpringreturn.SRStartPoint);
                                //NSLog(@"itemSpringreturn.SREndPoint>>>>%@",itemSpringreturn.SREndPoint);
                                
                                KNESpringRPosition *gKNESpringRPosition=[[KNESpringRPosition alloc] init];
                                gKNESpringRPosition.CaseID=strCaseID;
                                gKNESpringRPosition.SRStartPoint=itemSpringreturn.SRStartPoint;
                                gKNESpringRPosition.SREndPoint=itemSpringreturn.SREndPoint;
                                
                                [SRMutblArr addObject:gKNESpringRPosition];
                            }
                            
                            DBSpringReturn *gDBSpringReturn=[[DBSpringReturn alloc] init];
                            [gDBSpringReturn insertItemArray:SRMutblArr];
                            
                            /*=================================save Spring Return Contain===============================================*/
                            
                            NSMutableArray *MutblSRContainArr=[[NSMutableArray alloc] init];
                            
                            
                            for (int i=0; i<25; i++)
                            {
                                UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:300000000+i];
                                
                                if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0) {
                                    
                                    NSString *strContainTag=[[NSString alloc] initWithFormat:@"%d", glbltextvalue.tag];
                                    KNESpringReturnContain *gKNESpringReturnContain=[[KNESpringReturnContain alloc] init];
                                    gKNESpringReturnContain.CaseID=strCaseID;
                                    gKNESpringReturnContain.ContainTag=strContainTag;
                                    gKNESpringReturnContain.ContainValue=glbltextvalue.text;
                                    
                                    [MutblSRContainArr addObject:gKNESpringReturnContain];
                                    
                                }else
                                {
                                    //NSLog(@"textboX~~nil!~~~~");
                                }
                            }
                            
                            
                            DBSpringReturnContain *gDBSpringReturnContain=[[DBSpringReturnContain alloc] init];
                            
                            [gDBSpringReturnContain insertItemSRContainArray:MutblSRContainArr];
                            
                            /*=================================save Switching First Value===============================================*/
                            
                            //NSLog(@"appDelegateSave.firstPnum>>>>>>>>>>>>>>>>>%@",appDelegateSave.firstPnum);
                            
                            /**/
                            DBSwitchingfirstValue *dbDBSwitchingfirstValue=[[DBSwitchingfirstValue alloc ] init];
                            KNESwitchingFirstValue *gKNESwitchingFirstValue=[[KNESwitchingFirstValue alloc] init];
                            
                            gKNESwitchingFirstValue.CaseID=strCaseID;
                            gKNESwitchingFirstValue.SwitchingFirstValue=appDelegateSave.firstPnum;
                            
                            
                            [dbDBSwitchingfirstValue insertSwitchingFirstValue:gKNESwitchingFirstValue];
                            
                            /*=================================save jumper===============================================*/
                            
                            [jumperview storejumper];
                            
                            /*=================================clear==========================================*/
                            //[appDelegateSave clearSpringReturn];
                            [appDelegateSave clearKNECase];
                            [appDelegateSave clearfirstPnum];
                            /*=================================go to next page==========================================
                             
                             
                             KNESwitchShowViewController *gKNESwitchShowViewController=[[KNESwitchShowViewController alloc] initWithNibName:@"KNESwitchShowViewController" bundle:nil];
                             gKNESwitchShowViewController.KNECaseclass=caseIDar;
                             [self.navigationController pushViewController:gKNESwitchShowViewController animated:YES];
                             [gKNESwitchShowViewController release];
                             ==
                             
                             UIViewController* menuViewController=nil;
                             for(UIViewController* viewController in self.navigationController.viewControllers)
                             {
                             if([viewController isKindOfClass:[SwitchMainPageViewController class]])
                             {
                             menuViewController=viewController;
                             break;
                             }
                             }
                             ===*/
                            //[self.navigationController popToViewController:menuViewController animated:YES];
                            //[self.navigationController popViewControllerAnimated:YES];
                            
                            //[self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
                            [self.navigationController popToRootViewControllerAnimated:YES];
                            
                        }else
                        {
                            NSLog(@"nilllll");
                        }
                        
                    }
                    
                    //[createCase release];
                }else
                {
                    //NSLog(@"Please insert plate name!!");
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Please Insert Program" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    
                    [alert show];
                }
            }else
            {
                //NSLog(@"Please insert plate name!!");
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Please Insert Switch Type" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        /*
        if ([txtPlateName.text length] != 0)
        {    
        }else
        {
            //NSLog(@"Please insert plate name!!");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                                            message:@"Please Insert Plate Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
            [alert release];
        }
        */
    }
    
   [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*===========================spring return=====================================*/

-(void)getspringreturn
{
    UIColor *color = [UIColor colorWithRed:230.0/255.0 green:221.0/255.0 blue:159.0/255.0 alpha:1.0];
    //int rightsidevalue=1380+104;
    
    int widthOffset = 0;
    int yOffset=1;
    int xOffset=0;
    for(int i=0;i<24;i++)
    {
        if(i!=0 && (i%1)==0)
        {
            yOffset+=27;
            xOffset=0;
        }

        UIButton *gbtnsaveEvent = [[UIButton alloc] initWithFrame:CGRectMake(widthOffset*xOffset, yOffset+ 310, 50, 27)];
        //gbtnsaveEvent.layer.borderColor=[UIColor blackColor].CGColor;
        //gbtnsaveEvent.layer.borderWidth = 1;
        gbtnsaveEvent.backgroundColor=[UIColor clearColor];
        gbtnsaveEvent.tag=i+1+100000000;
        [gbtnsaveEvent addTarget:self action:@selector(springReturnTapped:) forControlEvents:UIControlEventTouchDown];
        
        [self.mainscrollview addSubview:gbtnsaveEvent];
        //[gbtnsaveEvent release];
        
        /*===============right side======================*/
        /*
        UITextField *glblBodytextright = [[UITextField alloc] initWithFrame:CGRectMake(rightsidevalue+50+widthOffset*xOffset, yOffset+ 310, 155, 27)];
        glblBodytextright.textAlignment = UITextAlignmentCenter;
         glblBodytextright.backgroundColor = [UIColor yellowColor];
        glblBodytextright.layer.borderColor=[UIColor blackColor].CGColor;
        glblBodytextright.layer.borderWidth = 1;
        glblBodytextright.textColor = [UIColor blackColor];
        //glblBodytext.text=@"hello~";
        glblBodytextright.tag=i+ 300000000 +1;
        glblBodytextright.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytextright.autocorrectionType=UITextAutocorrectionTypeNo;
        glblBodytextright.delegate=self;
        [self.mainscrollview addSubview:glblBodytextright];
        [glblBodytextright release];
        */
        /*
        UILabel *glblBodytextExtraright = [[UILabel alloc] initWithFrame:CGRectMake(rightsidevalue+widthOffset*xOffset, yOffset+ 310, 50, 27)];
        glblBodytextExtraright.textAlignment = UITextAlignmentCenter;
        glblBodytextExtraright.layer.borderColor=[UIColor blackColor].CGColor;
        //glblBodytextExtra.backgroundColor = color;
        glblBodytextExtraright.layer.borderWidth = 1;
        glblBodytextExtraright.textColor = [UIColor blackColor];
        glblBodytextExtraright.tag=i+ 910000000 +1;
        glblBodytextExtraright.font = [UIFont fontWithName:@"Helvetica" size:12];
        [self.mainscrollview addSubview:glblBodytextExtraright];
        [glblBodytextExtraright release];
        */

        /*
        UIImageView *imgarrowTextvalueright = [[[UIImageView alloc]initWithFrame:CGRectMake(rightsidevalue+18+widthOffset*xOffset, yOffset+ 323, 30, 4)]autorelease ];
        imgarrowTextvalueright.tag=i+ 210000000 + 1;
        [self.mainscrollview addSubview:imgarrowTextvalueright];
        
        UIImageView *imgarrowStartright = [[[UIImageView alloc]initWithFrame:CGRectMake(rightsidevalue+18+widthOffset*xOffset, yOffset+ 323, 30, 4)]autorelease ];
        //gimgpicture1.image=[UIImage imageNamed:@"circle_1.png"];
        //imgarrowStart.backgroundColor=[UIColor blackColor];
        imgarrowStartright.tag=i+ 410000000 + 1;
        [self.mainscrollview addSubview:imgarrowStartright];
        
        
        UIImageView *imgarrowMidright = [[[UIImageView alloc]init ] autorelease];
        [imgarrowMidright setFrame:CGRectMake(rightsidevalue+18+widthOffset*xOffset, yOffset+ 323, 5, 30)];
        imgarrowMidright.tag=i+ 610000000 + 1;
        [self.mainscrollview addSubview:imgarrowMidright];
        */
        /*UIImageView *imgarrowMid2right = [[[UIImageView alloc]init ] autorelease];
        imgarrowMid2right.tag=i+ 710000000 + 1;
        [self.mainscrollview addSubview:imgarrowMid2right]; 
         */
        /*
        UIImageView *imgarrowEndupright = [[[UIImageView alloc]initWithFrame:CGRectMake(rightsidevalue+18+widthOffset*xOffset, yOffset+ 320, 30, 10)]autorelease ];
        imgarrowEndupright.tag=i+ 510000000+1;
        [self.mainscrollview addSubview:imgarrowEndupright];
        
        UIImageView *imgarrowEnddwnright = [[[UIImageView alloc]initWithFrame:CGRectMake(rightsidevalue+18+widthOffset*xOffset, yOffset+ 320, 30, 10)]autorelease ];
        imgarrowEnddwnright.tag=i+ 810000000+1;
        [self.mainscrollview addSubview:imgarrowEnddwnright];
        */
        /*===============left side======================*/
        
        UITextField *glblBodytext = [[UITextField alloc] initWithFrame:CGRectMake(50+widthOffset*xOffset, yOffset+ 310, 155, 27)];
        glblBodytext.textAlignment = UITextAlignmentCenter;
        glblBodytext.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        glblBodytext.backgroundColor = color;
//        glblBodytext.layer.borderColor=[UIColor blackColor].CGColor;
//        glblBodytext.layer.borderWidth = 1;
        glblBodytext.textColor = [UIColor blackColor];
        //glblBodytext.text=@"hello~";
        glblBodytext.tag=i+ 300000000 +1;
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytext.autocorrectionType=UITextAutocorrectionTypeNo;
        glblBodytext.delegate=self;
        [self.mainscrollview addSubview:glblBodytext];
        
        UILabel *glblBodytextExtra = [[UILabel alloc] initWithFrame:CGRectMake(1280+50+widthOffset*xOffset, yOffset+ 310, 155, 27)];
        glblBodytextExtra.textAlignment = UITextAlignmentCenter;
        //glblBodytextExtra.backgroundColor = color;
//        glblBodytextExtra.layer.borderColor=[UIColor blackColor].CGColor;
//        glblBodytextExtra.layer.borderWidth = 1;
        glblBodytextExtra.textColor = [UIColor blackColor];
        glblBodytextExtra.tag=i+ 900000000 +1;
        glblBodytextExtra.font = [UIFont fontWithName:@"Helvetica" size:12];
        [self.mainscrollview addSubview:glblBodytextExtra];
        
        
        UIImageView *imgarrowTextvalue= [[UIImageView alloc]initWithFrame:CGRectMake(0+18+widthOffset*xOffset, yOffset+ 323, 30, 4)];
        imgarrowTextvalue.tag=i+ 200000000 + 1;
        [self.mainscrollview addSubview:imgarrowTextvalue];
        
        UIImageView *imgarrowStart= [[UIImageView alloc]initWithFrame:CGRectMake(18+widthOffset*xOffset, yOffset+ 323, 30, 4)];
        //gimgpicture1.image=[UIImage imageNamed:@"circle_1.png"];
        //imgarrowStart.backgroundColor=[UIColor blackColor];
        imgarrowStart.tag=i+ 400000000 + 1;
        [self.mainscrollview addSubview:imgarrowStart];
        
        
        UIImageView *imgarrowMid= [[UIImageView alloc]init ];
        [imgarrowMid setFrame:CGRectMake(18+widthOffset*xOffset, yOffset+ 323, 5, 30)];
        imgarrowMid.tag=i+ 600000000 + 1;
        [self.mainscrollview addSubview:imgarrowMid];
        
        /*
        UIImageView *imgarrowMid2= [[[UIImageView alloc]init ] autorelease];
        imgarrowMid2.tag=i+ 700000000 + 1;
        [self.mainscrollview addSubview:imgarrowMid2];
        */
        
        UIImageView *imgarrowEndup= [[UIImageView alloc]initWithFrame:CGRectMake(18+widthOffset*xOffset, yOffset+ 320, 30, 10)];
        imgarrowEndup.tag=i+ 500000000+1;
        [self.mainscrollview addSubview:imgarrowEndup];
        
        UIImageView *imgarrowEnddwn= [[UIImageView alloc]initWithFrame:CGRectMake(18+widthOffset*xOffset, yOffset+ 320, 30, 10)];
        imgarrowEnddwn.tag=i+ 800000000+1;
        [self.mainscrollview addSubview:imgarrowEnddwn];

        widthOffset=50;
        xOffset++;
        
         
    }
    [self.tblSpringReturn reloadData];
    [self.imgSpringReturn setFrame:CGRectMake(0, 311, 205, 648)];
    [self.mainscrollview addSubview:self.imgSpringReturn ];
    [self.imgtblspringReturnValueExtra setFrame:CGRectMake(1330, 311, 155, 648)];
    [self.mainscrollview addSubview:self.imgtblspringReturnValueExtra ];
}

/*==================spring return textbox====================*/


- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.mainscrollview.contentSize=CGSizeMake(self.view.frame.size.width, 960+260);
    //NSLog(@"textField.tag~--->%d",textField.tag);
    
    if (textField.tag > 300000011) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 392-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000012) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 419-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000013) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 446-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000014) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 473-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000015) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 500-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000016) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 527-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000017) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 554-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000018) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 581-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000019) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 608-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000020) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 635-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000021) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 662-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000022) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 689-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000023) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 716-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }if (textField.tag > 300000024) {
        heightOfEditedView = textField.frame.size.height;
        heightOffset = textField.frame.origin.y+10;
        
        CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 743-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        self.view.frame = rectToShow;
        [UIView commitAnimations];
    }
    //NSLog(@"hello~~there~~");

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //NSLog(@"yoyo~~there~~lblSwitchingAngle.text>>>>%@",lblSwitchingAngle.text);
    //NSLog(@"textField.tag2~--->%d",textField.tag);
    
    if (textField.tag<=300000024) {
        UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:textField.tag+([lblSwitchingAngle.text intValue]/15)];
        
        if (textField.tag == textField.tag) {
            [textField resignFirstResponder];
            [glbltextvalue becomeFirstResponder];
        }
    }else
    {
         [textField resignFirstResponder];
    }

    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //NSLog(@"textField.tag3~--->%d",textField.tag);
    self.mainscrollview.contentSize=CGSizeMake(2304, 960);
    
    for (int i=0; i<=24; i++) {
        UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:300000000+i];
        UILabel *glbltextvalueExtra=(UILabel *)[self.mainscrollview viewWithTag:900000000+i];
        
        NSString *strSwitchingAngle=[[NSString alloc] initWithFormat:@"%d",(i)*15-15];
        if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0)
        {
            lblTotalSwitchingangle.text=strSwitchingAngle;
            glbltextvalueExtra.text=glbltextvalue.text;
            
            [self setrowcolor:glbltextvalue.tag];
            
        }else
        {
            glbltextvalueExtra.text=glbltextvalue.text;
            
            UIColor *color = [UIColor colorWithRed:230.0/255.0 green:221.0/255.0 blue:159.0/255.0 alpha:1.0];
            
            glbltextvalue.backgroundColor = color;
            glbltextvalueExtra.backgroundColor = [UIColor clearColor];;
            
            if (glbltextvalue.tag==300000000+1)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 1];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 1];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+2)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 2];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 2];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+24];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+3)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 3];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 3];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*2)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+4)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 4];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 4];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*3)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+5)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 5];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 5];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*4)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+6)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 6];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 6];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*5)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+7)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 7];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 7];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*6)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+8)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 8];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 8];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*7)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+9)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 9];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 9];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*8)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+10)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 10];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 10];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*9)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+11)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 11];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 11];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*10)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+12)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 12];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 12];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*11)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+13)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 13];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 13];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*12)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+14)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 14];
                lbltblrow.backgroundColor = [UIColor clearColor];
                //UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 14];
                //lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*13)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+15)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 15];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 15];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*14)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+16)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 16];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 16];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*15)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+17)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 17];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 17];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*16)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+18)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 18];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 18];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*17)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+19)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 19];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 19];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*18)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+20)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 20];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 20];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*19)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+21)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 21];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 21];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*20)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+22)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 22];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 22];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*21)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+23)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 23];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 23];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*22)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+24)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 24];
                lbltblrow.backgroundColor = [UIColor clearColor];
//                UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 24];
//                lbltblrowextra.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*23)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }

        }
    }

    [self reloadtblSpringReturn];
}

-(void)setrowcolor:(int)rowcolortag
{
    UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:rowcolortag];
//    UILabel *glbltextvalueExtra=(UILabel *)[self.mainscrollview viewWithTag:600000000+rowcolortag];
    UIColor *color = [UIColor colorWithRed:199/255.0 green:199/255.0 blue:199/255.0 alpha:1.0];
    glbltextvalue.backgroundColor = color;
//    glbltextvalueExtra.backgroundColor = color;
    
    if (glbltextvalue.tag==300000000+1)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 1];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 1];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<=24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+2)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 2];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 2];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<=24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ 24];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+3)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 3];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 3];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*2)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+4)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 4];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 4];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*3)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+5)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 5];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 5];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*4)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+6)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 6];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 6];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*5)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+7)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 7];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 7];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*6)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+8)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 8];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 8];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*7)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+9)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 9];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 9];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*8)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+10)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 10];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 10];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*9)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+11)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 11];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 11];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*10)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+12)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 12];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 12];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*11)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+13)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 13];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 13];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*12)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+14)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 14];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 14];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*13)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+15)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 15];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 15];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*14)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+16)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 16];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 16];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*15)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+17)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 17];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 17];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*16)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+18)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 18];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 18];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*17)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+19)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 19];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 19];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*18)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+20)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 20];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 20];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*19)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+21)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 21];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 21];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*20)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+22)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 22];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 22];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*21)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+23)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 23];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 23];
//        lbltblrowextra.backgroundColor = color;
//        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*22)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+24)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 24];
        lbltblrow.backgroundColor = color;
//        UILabel *lbltblrowextra=(UILabel *)[self.mainscrollview viewWithTag:7100+ 24];
//        lbltblrowextra.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*23)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }
    
}

-(void)setcancelrowcolor:(int)rowcolortag
{
    
}

/*==================spring return Tap====================*/
-(IBAction)springReturnTapped:(id)sender
{
    UIButton *button = (UIButton *)sender;
    //NSLog(@"springReturnTapped>>>>>%d", [button tag]);
    int btntag=[button tag];
    
    //KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KNESpringRPosition *gKNESpringRPosition=[[KNESpringRPosition alloc]init ];
    NSArray *positionsArr=self.springReturnPosition;
    //NSLog(@"positionsArr.count-------xxx------%d--->", positionsArr.count);
    for(int i = 0 ; i < self.mainscrollview.subviews.count; i++)
    {
        
        UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:btntag+ 200000000];
        NSString *gtextvalue=glbltextvalue.text;

        if (gtextvalue != nil && gtextvalue.length !=0)
        {

            if (springReturnStart==NO && springReturnEnd==NO)
            {
                //positionsArr=appDelegate.springReturnPosition;
                btnStart=btntag;
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 300000000];
                
                
                int deleteindex;
                NSString *Sndmid=[[NSString alloc]init];
                
                for (int idexss = 0; idexss<positionsArr.count; idexss++ )
                {
                    KNESpringRPosition* key=[positionsArr objectAtIndex:idexss];
                    
                    if ([key.SRStartPoint intValue]==btntag)
                    {

                        deleteindex=idexss;
                        clearStartTag=[key.SRStartPoint intValue];
                        clearEndTag=[key.SREndPoint intValue];
                        //NSLog(@"Yes");
                        Sndmid=@"Yes";
                        break;
                    }else if (([key.SRStartPoint intValue]>btntag && [key.SREndPoint intValue]<=btntag) || ([key.SRStartPoint intValue]<btntag && [key.SREndPoint intValue]>=btntag))
                    {
                        //NSLog(@"middle tag");
                        Sndmid=@"MiddleTag";
                        break;
                    }else
                    {
                        //Sndmid=@"No";
                        //NSLog(@"no");
                    }
                    
                }
                
                if ([Sndmid isEqualToString:@"Yes"])
                {

                    int indexspringreturn;
                    
                    if (clearStartTag< clearEndTag)
                    {
                        /*==============right row==================
                        UIImageView *imageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 310000000];
                        [imageViewstartright setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEndright=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 410000000];
                        [imageViewEndright setBackgroundColor:[UIColor clearColor]];
                        [imageViewEndright setImage:nil];
                        
                        for (indexspringreturn=clearStartTag; indexspringreturn<clearEndTag; indexspringreturn++) {
                            UIImageView *imageViewmidright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 510000000];
                            [imageViewmidright setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 110000000];
                            [TextimageViewstartright setBackgroundColor:[UIColor clearColor]];
                            
                        }
                        ==*/
                        /*==============left row====================*/
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 400000000];
                        [imageViewEnd setBackgroundColor:[UIColor clearColor]];
                        [imageViewEnd setImage:nil];
                        
                        for (indexspringreturn=clearStartTag; indexspringreturn<clearEndTag; indexspringreturn++) {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                            
                        }
                        
                        
                    }else
                    {
                        /*==============right row===================
                        UIImageView *imageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 310000000];
                        [imageViewstartright setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEndright=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 710000000];
                        [imageViewEndright setBackgroundColor:[UIColor clearColor]];
                        [imageViewEndright setImage:nil];
                        
                        for (indexspringreturn=clearEndTag; indexspringreturn<clearStartTag; indexspringreturn++) {
                            UIImageView *imageViewmidright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 510000000];
                            [imageViewmidright setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 110000000];
                            [TextimageViewstartright setBackgroundColor:[UIColor clearColor]];
                        }
                        =*/
                         /*==============left row====================*/
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 700000000];
                        [imageViewEnd setBackgroundColor:[UIColor clearColor]];
                        [imageViewEnd setImage:nil];
                        
                        for (indexspringreturn=clearEndTag; indexspringreturn<clearStartTag; indexspringreturn++) {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                        }
                        
                    }
                    [self deleteSpringReturnRowIndex:deleteindex];
                    
                }else if ([Sndmid isEqualToString:@"MiddleTag"])
                {
                    //NSLog(@"middle tag2");
                }else
                {
                    
                    [imageView setBackgroundColor:[UIColor blackColor]];
                    
                    springReturnStart=YES;
                    springReturnEnd=NO;
                }
                
            }else if (springReturnStart==YES && springReturnEnd==NO)
            {
                //positionsArr=appDelegate.springReturnPosition;
                btnEnd=btntag;
                
                NSString *Sndmid=[[NSString alloc]init];
                
                for (int idexss = 0; idexss<positionsArr.count; idexss++ )
                {
                    KNESpringRPosition* key=[positionsArr objectAtIndex:idexss];
                    
                     if (([key.SREndPoint intValue]>btnEnd && [key.SRStartPoint intValue]<=btnEnd) || ([key.SREndPoint intValue]<btnEnd && [key.SRStartPoint intValue]>=btnEnd))
                    {
                        NSLog(@"middle tag end");
                        Sndmid=@"MiddleTag";
                        break;
                    }else
                    {
                        //Sndmid=@"No";
                        NSLog(@"no");
                    }
                    
                }
                
                if (![Sndmid isEqualToString:@"MiddleTag"])
                {
                    int indexspringreturn;
                    
                    if (btnStart == btnEnd)
                    {
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor clearColor]];
                        
                        springReturnStart=NO;
                        springReturnEnd=NO;
                        linkcollection=NO;
                        NSLog(@"btnStart == btnEnd");
                        
                    }else if (btnStart< btnEnd)
                    {
                        NSLog(@"allitemrowArr1");
                        NSArray *allitemrowArr=[[NSArray alloc]init];
                        allitemrowArr=self.springReturnPosition;
                        
                        int i = 0;
                        while(i < allitemrowArr.count)
                        {
                            KNESpringRPosition* keys=[allitemrowArr objectAtIndex:i];
                            
                            
                            if (([keys.SRStartPoint intValue]>btnStart && [keys.SREndPoint intValue]<btnEnd) || ([keys.SRStartPoint intValue]<btnStart && [keys.SREndPoint intValue]>btnEnd)|| ([keys.SRStartPoint intValue]>btnStart && [keys.SRStartPoint intValue]<[keys.SREndPoint intValue] && [keys.SREndPoint intValue]==btnEnd))
                            {
                                // || ([keys.SREndPoint intValue]==btnEnd)
                                [self.springReturnPosition removeObjectAtIndex:i];
                                
                                UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SRStartPoint intValue]+ 300000000];
                                [imageViewstart setBackgroundColor:[UIColor clearColor]];
                                
                                UIImageView *imageViewEnddw=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SREndPoint intValue]+ 400000000];
                                [imageViewEnddw setBackgroundColor:[UIColor clearColor]];
                                [imageViewEnddw setImage:nil];
                                
                                UIImageView *imageViewEndup=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SREndPoint intValue]+ 700000000];
                                [imageViewEndup setBackgroundColor:[UIColor clearColor]];
                                [imageViewEndup setImage:nil];
                                
                                for (indexspringreturn=btnStart; indexspringreturn<btnEnd; indexspringreturn++)
                                {
                                    UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                                    [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                                }
                                
                            }else
                            {
                                i++;
                            }
                        }
                        
                        /*==============right row================
                         UIImageView *imageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 310000000];
                         [imageViewstartright setBackgroundColor:[UIColor blackColor]];
                         
                         //UIImage *imgbtnsaveEvent = [UIImage imageNamed:@"arrow.png"];
                         UIImageView *imageViewEndright=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 410000000];
                         //[imageViewEnd setBackgroundColor:[UIColor redColor]];
                         [imageViewEndright setImage:[UIImage imageNamed:@"arrow.png"]];
                         
                         for (indexspringreturn=btnStart; indexspringreturn<btnEnd; indexspringreturn++)
                         {
                         UIImageView *imageViewmidright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 510000000];
                         [imageViewmidright setBackgroundColor:[UIColor blackColor]];
                         
                         UITextField *glblBodytextvalueright=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 210000000];
                         NSString *textvalue=glblBodytextvalueright.text;
                         UIImageView *TextimageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 110000000];
                         
                         
                         if (textvalue != nil && textvalue.length !=0)
                         {
                         
                         [TextimageViewstartright setBackgroundColor:[UIColor blackColor]];
                         
                         }else
                         {
                         [TextimageViewstartright setBackgroundColor:[UIColor clearColor]];
                         }
                         }
                         ====*/
                        /*==============left row====================*/
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor blackColor]];
                        
                        //UIImage *imgbtnsaveEvent = [UIImage imageNamed:@"arrow.png"];
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 400000000];
                        //[imageViewEnd setBackgroundColor:[UIColor redColor]];
                        [imageViewEnd setImage:[UIImage imageNamed:@"arrow.png"]];
                        
                        for (indexspringreturn=btnStart; indexspringreturn<btnEnd; indexspringreturn++)
                        {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor blackColor]];
                            
                            UITextField *glblBodytextvalue=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 200000000];
                            NSString *textvalue=glblBodytextvalue.text;
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            
                            
                            if (textvalue != nil && textvalue.length !=0)
                            {
                                
                                [TextimageViewstart setBackgroundColor:[UIColor blackColor]];
                                
                            }else
                            {
                                [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                            }
                        }
                        
                        springReturnStart=NO;
                        springReturnEnd=NO;
                        linkcollection=YES;
                        
                    }else
                    {
                        NSLog(@"allitemrowArr2");
                        NSArray *allitemrowArr=[[NSArray alloc]init];
                        allitemrowArr=self.springReturnPosition;
                        
                        int i = 0;
                        while(i < allitemrowArr.count){
                            KNESpringRPosition* keys=[allitemrowArr objectAtIndex:i];
                            if (([keys.SRStartPoint intValue]>btnStart && [keys.SREndPoint intValue]<btnEnd) || ([keys.SRStartPoint intValue]<btnStart && [keys.SREndPoint intValue]>btnEnd)|| ([keys.SRStartPoint intValue]<btnStart && [keys.SRStartPoint intValue]>[keys.SREndPoint intValue] && [keys.SREndPoint intValue]==btnEnd)){
                                [self.springReturnPosition removeObjectAtIndex:i];
                                
                                UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SRStartPoint intValue]+ 300000000];
                                [imageViewstart setBackgroundColor:[UIColor clearColor]];
                                UIImageView *imageViewEnddw=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SREndPoint intValue]+ 400000000];
                                [imageViewEnddw setBackgroundColor:[UIColor clearColor]];
                                [imageViewEnddw setImage:nil];
                                
                                UIImageView *imageViewEndup=(UIImageView *)[self.mainscrollview viewWithTag:[keys.SREndPoint intValue]+ 700000000];
                                [imageViewEndup setBackgroundColor:[UIColor clearColor]];
                                [imageViewEndup setImage:nil];
                                for (indexspringreturn=btnStart; indexspringreturn<btnEnd; indexspringreturn++)
                                {
                                    UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                                    [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                                }
                                
                            }else{
                                i++;
                            }
                        }
                        
                        /*==============right row==================
                         UIImageView *imageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 310000000];
                         [imageViewstartright setBackgroundColor:[UIColor blackColor]];
                         
                         UIImageView *imageViewEndright=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 710000000];
                         //[imageViewEnd setBackgroundColor:[UIColor blueColor]];
                         [imageViewEndright setImage:[UIImage imageNamed:@"arrow.png"]];
                         
                         for (indexspringreturn=btnEnd; indexspringreturn<btnStart; indexspringreturn++) {
                         UIImageView *imageViewmidright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 510000000];
                         [imageViewmidright setBackgroundColor:[UIColor blackColor]];
                         
                         UITextField *glblBodytextvalueright=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 210000000];
                         NSString *textvalue=glblBodytextvalueright.text;
                         UIImageView *TextimageViewstartright=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 110000000];
                         
                         
                         if (textvalue != nil && textvalue.length !=0) {
                         
                         [TextimageViewstartright setBackgroundColor:[UIColor blackColor]];
                         
                         UIImageView *TextimageViewstart2right=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 110000000];
                         [TextimageViewstart2right setBackgroundColor:[UIColor clearColor]];
                         
                         }else
                         {
                         [TextimageViewstartright setBackgroundColor:[UIColor clearColor]];
                         }
                         
                         }
                         ==*/
                        /*==============left row====================*/
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor blackColor]];
                        
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 700000000];
                        //[imageViewEnd setBackgroundColor:[UIColor blueColor]];
                        [imageViewEnd setImage:[UIImage imageNamed:@"arrow.png"]];
                        
                        for (indexspringreturn=btnEnd; indexspringreturn<btnStart; indexspringreturn++) {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor blackColor]];
                            
                            UITextField *glblBodytextvalue=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 200000000];
                            NSString *textvalue=glblBodytextvalue.text;
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            
                            
                            if (textvalue != nil && textvalue.length !=0) {
                                
                                [TextimageViewstart setBackgroundColor:[UIColor blackColor]];
                                
                                UIImageView *TextimageViewstart2=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 100000000];
                                [TextimageViewstart2 setBackgroundColor:[UIColor clearColor]];
                                
                            }else
                            {
                                [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                            }
                            
                        }
                        
                        
                        springReturnStart=NO;
                        springReturnEnd=NO;
                        linkcollection=YES;
                    }
                }
                
                
                
                //NSLog(@"nthg~ %d--%d", btnStart, btnEnd);
                
            }
            
        }else
        {
            NSLog(@"text box nil");
            
            if (springReturnStart==NO && springReturnEnd==NO)
            {
                //positionsArr=appDelegate.springReturnPosition;
                btnStart=btntag;
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:btntag+ 300000000];
                
                
                int deleteindex;
                NSString *Sndmid=[[NSString alloc]init];
                
                for (int idexss = 0; idexss<positionsArr.count; idexss++ )
                {
                    KNESpringRPosition* key=[positionsArr objectAtIndex:idexss];
                    
                    if ([key.SRStartPoint intValue]==btntag)
                    {
                        
                        deleteindex=idexss;
                        clearStartTag=[key.SRStartPoint intValue];
                        clearEndTag=[key.SREndPoint intValue];
                        
                        Sndmid=@"Yes";
                        break;
                    }else if (([key.SRStartPoint intValue]>btntag && [key.SREndPoint intValue]<=btntag) || ([key.SRStartPoint intValue]<btntag && [key.SREndPoint intValue]>=btntag))
                    {
                        //NSLog(@"middle tag");
                        Sndmid=@"MiddleTag";
                        break;
                    }else
                    {
                        Sndmid=@"No";
                        //NSLog(@"no");
                    }
                    
                }
                
                if ([Sndmid isEqualToString:@"Yes"])
                {
                    
                    int indexspringreturn;
                    
                    if (clearStartTag< clearEndTag)
                    {
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 400000000];
                        [imageViewEnd setBackgroundColor:[UIColor clearColor]];
                        [imageViewEnd setImage:nil];
                        
                        for (indexspringreturn=clearStartTag; indexspringreturn<clearEndTag; indexspringreturn++) {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                            
                        }
                        
                        
                    }else
                    {
                        UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:clearStartTag+ 300000000];
                        [imageViewstart setBackgroundColor:[UIColor clearColor]];
                        
                        UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:clearEndTag+ 700000000];
                        [imageViewEnd setBackgroundColor:[UIColor clearColor]];
                        [imageViewEnd setImage:nil];
                        
                        for (indexspringreturn=clearEndTag; indexspringreturn<clearStartTag; indexspringreturn++) {
                            UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                            [imageViewmid setBackgroundColor:[UIColor clearColor]];
                            
                            UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                            [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                        }
                        
                    }
                    [self deleteSpringReturnRowIndex:deleteindex];
                    
                }else if ([Sndmid isEqualToString:@"MiddleTag"])
                {
                    NSLog(@"middle tag2");
                }else
                {
                    
                    [imageView setBackgroundColor:[UIColor clearColor]];
                    
                    springReturnStart=NO;
                    springReturnEnd=NO;
                }
                
            }
        }

        break;
    }

    if (linkcollection==YES) {
        
        NSString *strbtnStart=[[NSString alloc]initWithFormat:@"%d", btnStart];
        NSString *strbtnEnd=[[NSString alloc]initWithFormat:@"%d", btnEnd];
        
        gKNESpringRPosition.SRStartPoint=strbtnStart;
        gKNESpringRPosition.SREndPoint=strbtnEnd;
        
        [self gaddSpringReturnpoint:gKNESpringRPosition];
        linkcollection=NO;
    }
}

-(void)deleteSpringReturnRowIndex:(NSUInteger)index
{
    [springReturnPosition removeObjectAtIndex:index];
    
}

-(void)gaddSpringReturnpoint:(KNESpringRPosition *)gKNESpringRobjects
{
    KNESpringRPosition *ggKNESpringRPosition=[[KNESpringRPosition alloc]init];
    
    if(springReturnPosition==nil)
    {
        springReturnPosition= [[NSMutableArray alloc]init];
        
    }
    
    ggKNESpringRPosition.SRStartPoint=gKNESpringRobjects.SRStartPoint;
    ggKNESpringRPosition.SREndPoint=gKNESpringRobjects.SREndPoint;
    
    
    [springReturnPosition addObject:ggKNESpringRPosition];
    NSLog(@"springReturnPosition--->%@",springReturnPosition);
}

/**/
-(void)reloadtblSpringReturn
{
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.firstPnum=self.firstpNumbr;
    
    //[self.gKNESpringReturnViewController getfirstpNum];
    [self.tblSpringReturn reloadData];
    [self.tblSprinReturnExtra reloadData];
    
    //NSLog(@"reload table tapped!!>>>appDelegate.firstPnum>>>>%@", appDelegate.firstPnum);
    
    if ([appDelegate.firstPnum intValue]==0)
    {
        UITextField *glblBodytextvalue1=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt0.text=glblBodytextvalue1.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt330.text=glblBodytextvalue23.text;
        
        
        
        /**/
        
        
    }else if ([appDelegate.firstPnum intValue]==15)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==30)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==45)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==60)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==75)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:30000005];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==90)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==105)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==120)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==135)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==150)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==165)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==180)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==195)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==210)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==225)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==240)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==255)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==270)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==285)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:30000015];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==300)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==315)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==330)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000003];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000007];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000010];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000013];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000015];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000019];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000021];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000001];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==345)
    {
        UITextField *glblBodytextvalue24=(UITextField *)[self.mainscrollview viewWithTag:300000002];
        lbltxt0.text=glblBodytextvalue24.text;
        
        UITextField *glblBodytextvalue2=(UITextField *)[self.mainscrollview viewWithTag:300000004];
        lbltxt30.text=glblBodytextvalue2.text;
        
        UITextField *glblBodytextvalue3=(UITextField *)[self.mainscrollview viewWithTag:300000005];
        lbltxt45.text=glblBodytextvalue3.text;
        
        UITextField *glblBodytextvalue4=(UITextField *)[self.mainscrollview viewWithTag:300000006];
        lbltxt60.text=glblBodytextvalue4.text;
        
        UITextField *glblBodytextvalue7=(UITextField *)[self.mainscrollview viewWithTag:300000008];
        lbltxt90.text=glblBodytextvalue7.text;
        
        UITextField *glblBodytextvalue9=(UITextField *)[self.mainscrollview viewWithTag:300000009];
        lbltxt120.text=glblBodytextvalue9.text;
        
        UITextField *glblBodytextvalue10=(UITextField *)[self.mainscrollview viewWithTag:300000011];
        lbltxt135.text=glblBodytextvalue10.text;
        
        UITextField *glblBodytextvalue11=(UITextField *)[self.mainscrollview viewWithTag:300000012];
        lbltxt150.text=glblBodytextvalue11.text;
        
        UITextField *glblBodytextvalue13=(UITextField *)[self.mainscrollview viewWithTag:300000014];
        lbltxt180.text=glblBodytextvalue13.text;
        
        UITextField *glblBodytextvalue15=(UITextField *)[self.mainscrollview viewWithTag:300000016];
        lbltxt210.text=glblBodytextvalue15.text;
        
        UITextField *glblBodytextvalue16=(UITextField *)[self.mainscrollview viewWithTag:300000017];
        lbltxt225.text=glblBodytextvalue16.text;
        
        UITextField *glblBodytextvalue17=(UITextField *)[self.mainscrollview viewWithTag:300000018];
        lbltxt240.text=glblBodytextvalue17.text;
        
        UITextField *glblBodytextvalue19=(UITextField *)[self.mainscrollview viewWithTag:300000020];
        lbltxt270.text=glblBodytextvalue19.text;
        
        UITextField *glblBodytextvalue21=(UITextField *)[self.mainscrollview viewWithTag:300000022];
        lbltxt300.text=glblBodytextvalue21.text;
        
        UITextField *glblBodytextvalue22=(UITextField *)[self.mainscrollview viewWithTag:300000023];
        lbltxt315.text=glblBodytextvalue22.text;
        
        UITextField *glblBodytextvalue23=(UITextField *)[self.mainscrollview viewWithTag:300000024];
        lbltxt330.text=glblBodytextvalue23.text;
        
    }
}

-(void)StartBlack:(int)imgSblack
{
    UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:imgSblack+ 300000000];
    
    Startx=imageView.frame.origin.x;
    Starty=imageView.frame.origin.y;
    
    StartTag=imageView.tag;
    
    [imageView setBackgroundColor:[UIColor blackColor]];
}
 /*===========switch graft================*/

-(IBAction)btn0Tapped:(id)sender
{
    if (btn0T==YES) {
        self.btno.backgroundColor=[UIColor blackColor];
        self.btno.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btno setTitle:@"0" forState:UIControlStateNormal];
        [self.btno setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn0T=NO;
        
        self.firstpNumbr=self.lbl0.text;
        
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
        
    }else {
        btn0T=YES;
        self.btno.backgroundColor=[UIColor clearColor];
        self.btno.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btno setTitle:@"0" forState:UIControlStateNormal];
        [self.btno setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.firstpNumbr=nil;
        
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
    }
    
    
}

-(IBAction)btn30Tapped:(id)sender
{
    if (btn30T==YES) {
        self.btn30.backgroundColor=[UIColor blackColor];
        self.btn30.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn30 setTitle:@"30" forState:UIControlStateNormal];
        [self.btn30 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn30T=NO;
        
        self.firstpNumbr=self.lbl30.text;

        self.btno.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn30T=YES;
        self.btn30.backgroundColor=[UIColor clearColor];
        self.btn30.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn30 setTitle:@"30" forState:UIControlStateNormal];
        [self.btn30 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstpNumbr=nil;
        
        self.btno.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
    
}

-(IBAction)btn45Tapped:(id)sender
{
    if (btn45T==YES) {
        self.btn45.backgroundColor=[UIColor blackColor];
        self.btn45.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn45 setTitle:@"45" forState:UIControlStateNormal];
        [self.btn45 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn45T=NO;

        self.firstpNumbr=self.lbl45.text;
        
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn45T=YES;
        self.btn45.backgroundColor=[UIColor clearColor];
        self.btn45.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn45 setTitle:@"45" forState:UIControlStateNormal];
        [self.btn45 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstpNumbr=nil;
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn60Tapped:(id)sender
{
    if (btn60T==YES) {
        self.btn60.backgroundColor=[UIColor blackColor];
        self.btn60.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn60 setTitle:@"60" forState:UIControlStateNormal];
        [self.btn60 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn60T=NO;

        self.firstpNumbr=self.lbl60.text;
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn60T=YES;
        self.btn60.backgroundColor=[UIColor clearColor];
        self.btn60.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn60 setTitle:@"60" forState:UIControlStateNormal];
        [self.btn60 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstpNumbr =nil;
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
    }
    
}

-(IBAction)btn90Tapped:(id)sender
{
    if (btn90T==YES) {
        self.btn90.backgroundColor=[UIColor blackColor];
        self.btn90.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn90 setTitle:@"90" forState:UIControlStateNormal];
        [self.btn90 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn90T=NO;
        
        self.firstpNumbr=self.lbl90.text;
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn90T=YES;
        self.btn90.backgroundColor=[UIColor clearColor];
        self.btn90.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn90 setTitle:@"90" forState:UIControlStateNormal];
        [self.btn90 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstpNumbr=nil;
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn120Tapped:(id)sender
{
    if (btn120T==YES) {
        self.btn120.backgroundColor=[UIColor blackColor];
        btn120T=NO;
        
        self.btn120.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn120 setTitle:@"120" forState:UIControlStateNormal];
        [self.btn120 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

        self.firstpNumbr=self.lbl120.text;
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn120T=YES;
        self.btn120.backgroundColor=[UIColor clearColor];
        
        self.btn120.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn120 setTitle:@"120" forState:UIControlStateNormal];
        [self.btn120 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.firstpNumbr=nil;
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn135Tapped:(id)sender
{
    if (btn135T==YES) {
        self.btn135.backgroundColor=[UIColor blackColor];
        btn135T=NO;
        self.firstpNumbr=self.lbl135.text;

        self.btn135.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn135 setTitle:@"135" forState:UIControlStateNormal];
        [self.btn135 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn135T=YES;
        self.btn135.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn135.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn135 setTitle:@"135" forState:UIControlStateNormal];
        [self.btn135 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn150Tapped:(id)sender
{
    if (btn150T==YES) {
        self.btn150.backgroundColor=[UIColor blackColor];
        btn150T=NO;
        
        self.firstpNumbr=self.lbl150.text;
        
        self.btn150.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn150 setTitle:@"150" forState:UIControlStateNormal];
        [self.btn150 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn150T=YES;
        self.btn150.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn150.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn150 setTitle:@"150" forState:UIControlStateNormal];
        [self.btn150 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn180Tapped:(id)sender
{
    if (btn180T==YES) {
        self.btn180.backgroundColor=[UIColor blackColor];
        btn180T=NO;
        
        self.firstpNumbr=self.lbl180.text;
        
        self.btn180.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn180 setTitle:@"180" forState:UIControlStateNormal];
        [self.btn180 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn180T=YES;
        self.btn180.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn180.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn180 setTitle:@"180" forState:UIControlStateNormal];
        [self.btn180 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn210Tapped:(id)sender
{
    if (btn210T==YES) {
        self.btn210.backgroundColor=[UIColor blackColor];
        btn210T=NO;
        
        self.firstpNumbr=self.lbl210.text;
        
        self.btn210.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn210 setTitle:@"210" forState:UIControlStateNormal];
        [self.btn210 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn210T=YES;
        self.btn210.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn210.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn210 setTitle:@"210" forState:UIControlStateNormal];
        [self.btn210 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn225Tapped:(id)sender
{
    if (btn225T==YES) {
        self.btn225.backgroundColor=[UIColor blackColor];
        btn225T=NO;
        
        self.firstpNumbr=self.lbl225.text;
        
        self.btn225.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn225 setTitle:@"225" forState:UIControlStateNormal];
        [self.btn225 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn225T=YES;
        self.btn225.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn225.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn225 setTitle:@"225" forState:UIControlStateNormal];
        [self.btn225 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        gKNESwitchPoint.SwitchPoint=nil;
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn240Tapped:(id)sender
{
    if (btn240T==YES) {
        self.btn240.backgroundColor=[UIColor blackColor];
        btn240T=NO;
        
        self.firstpNumbr=self.lbl240.text;
        
        self.btn240.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn240 setTitle:@"240" forState:UIControlStateNormal];
        [self.btn240 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn240T=YES;
        self.btn240.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn240.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn240 setTitle:@"240" forState:UIControlStateNormal];
        [self.btn240 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn270Tapped:(id)sender
{
    if (btn270T==YES) {
        self.btn270.backgroundColor=[UIColor blackColor];
        btn270T=NO;
        
        self.firstpNumbr=self.lbl270.text;
        
        self.btn270.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn270 setTitle:@"270" forState:UIControlStateNormal];
        [self.btn270 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn270T=YES;
        self.btn270.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn270.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn270 setTitle:@"270" forState:UIControlStateNormal];
        [self.btn270 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn300Tapped:(id)sender
{
    if (btn300T==YES) {
        self.btn300.backgroundColor=[UIColor blackColor];
        btn300T=NO;
        
        self.firstpNumbr=self.lbl300.text;
        
        self.btn300.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn300 setTitle:@"300" forState:UIControlStateNormal];
        [self.btn300 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn315.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn300T=YES;
        self.btn300.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn300.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn300 setTitle:@"300" forState:UIControlStateNormal];
        [self.btn300 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn315.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn315Tapped:(id)sender
{
    if (btn315T==YES) {
        self.btn315.backgroundColor=[UIColor blackColor];
        btn315T=NO;
        
        self.firstpNumbr=self.lbl315.text;
        
        self.btn315.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn315 setTitle:@"315" forState:UIControlStateNormal];
        [self.btn315 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn330.hidden=YES;
        
    }else {
        btn315T=YES;
        self.btn315.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn315.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn315 setTitle:@"315" forState:UIControlStateNormal];
        [self.btn315 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn330.hidden=NO;
        
    }
    
}

-(IBAction)btn330Tapped:(id)sender
{
    if (btn330T==YES) {
        self.btn330.backgroundColor=[UIColor blackColor];
        btn330T=NO;
        
        self.firstpNumbr=self.lbl330.text;
        
        self.btn330.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn330 setTitle:@"330" forState:UIControlStateNormal];
        [self.btn330 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.btno.hidden=YES;
        self.btn30.hidden=YES;
        self.btn45.hidden=YES;
        self.btn60.hidden=YES;
        self.btn90.hidden=YES;
        self.btn120.hidden=YES;
        self.btn135.hidden=YES;
        self.btn150.hidden=YES;
        self.btn180.hidden=YES;
        self.btn210.hidden=YES;
        self.btn225.hidden=YES;
        self.btn240.hidden=YES;
        self.btn270.hidden=YES;
        self.btn300.hidden=YES;
        self.btn315.hidden=YES;
        
    }else {
        btn330T=YES;
        self.btn330.backgroundColor=[UIColor clearColor];
        
        self.firstpNumbr=nil;
        
        self.btn330.titleLabel.font = [UIFont systemFontOfSize:13];
        [self.btn330 setTitle:@"330" forState:UIControlStateNormal];
        [self.btn330 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        self.btno.hidden=NO;
        self.btn30.hidden=NO;
        self.btn45.hidden=NO;
        self.btn60.hidden=NO;
        self.btn90.hidden=NO;
        self.btn120.hidden=NO;
        self.btn135.hidden=NO;
        self.btn150.hidden=NO;
        self.btn180.hidden=NO;
        self.btn210.hidden=NO;
        self.btn225.hidden=NO;
        self.btn240.hidden=NO;
        self.btn270.hidden=NO;
        self.btn300.hidden=NO;
        self.btn315.hidden=NO;
        
    }
    
}

/*-============================thirdpart===============================-*/

-(void)getclosePickerTapped
{
    if (SwitchTypeSelected==YES) {
        SwitchTypeSelected=NO;
        NSLog(@"SwitchTypeSelected");
        
        self.txtSwitchType.text=Noperson;
    }else if (ProgramSelected==YES) {
        ProgramSelected=NO;
        NSLog(@"ProgramSelected");
        
        self.txtProgram.text=Noperson;
    }else if (LookSelected==YES) {
        LookSelected=NO;
        NSLog(@"LookSelected");
        
        self.txtlook.text=Noperson;
    }else if (MountingSelected==YES) {
        MountingSelected=NO;
        NSLog(@"MountingSelected");
        
        self.txtmounting.text=Noperson;
    }else if (EscuteonSelected==YES) {
        EscuteonSelected=NO;
        NSLog(@"EscuteonSelected");
        
        self.txtescutcheon.text=Noperson;
    }else if (HandleSeleted==YES) {
        HandleSeleted=NO;
        NSLog(@"HandleSeleted");
        
        self.txthandle.text=Noperson;
    }else if (LatchSelected==YES) {
        LatchSelected=NO;
        NSLog(@"LatchSelected");
        
        self.txtlatch_mech.text=Noperson;
    }else if (StopSelected==YES) {
        StopSelected=NO;
        NSLog(@"StopSelected");
        
        self.txtstop.text=Noperson;
    }
    
    [self.fauxView removeFromSuperview];
    for (UIView* child in self.fauxView.subviews) {
        [child removeFromSuperview];
    }
}

-(void)getcloseSwitchAnglePickerTapped
{
    if (SwitchAngle==YES) {
        SwitchAngle=NO;
        NSLog(@"SwitchAngle");
        
        self.firstpNumbr=Noperson;
        
        self.lbl0.backgroundColor=[UIColor clearColor];
        self.lbl0.textColor=[UIColor blackColor];
        
        self.lbl30.backgroundColor=[UIColor clearColor];
        self.lbl30.textColor=[UIColor blackColor];
        
        self.lbl45.backgroundColor=[UIColor clearColor];
        self.lbl45.textColor=[UIColor blackColor];
        
        self.lbl60.backgroundColor=[UIColor clearColor];
        self.lbl60.textColor=[UIColor blackColor];
        
        self.lbl90.backgroundColor=[UIColor clearColor];
        self.lbl90.textColor=[UIColor blackColor];
        
        self.lbl120.backgroundColor=[UIColor clearColor];
        self.lbl120.textColor=[UIColor blackColor];
        
        self.lbl135.backgroundColor=[UIColor clearColor];
        self.lbl135.textColor=[UIColor blackColor];
        
        self.lbl150.backgroundColor=[UIColor clearColor];
        self.lbl150.textColor=[UIColor blackColor];
        
        self.lbl180.backgroundColor=[UIColor clearColor];
        self.lbl180.textColor=[UIColor blackColor];
        
        self.lbl210.backgroundColor=[UIColor clearColor];
        self.lbl210.textColor=[UIColor blackColor];
        
        self.lbl225.backgroundColor=[UIColor clearColor];
        self.lbl225.textColor=[UIColor blackColor];
        
        self.lbl240.backgroundColor=[UIColor clearColor];
        self.lbl240.textColor=[UIColor blackColor];
        
        self.lbl270.backgroundColor=[UIColor clearColor];
        self.lbl270.textColor=[UIColor blackColor];
        
        self.lbl300.backgroundColor=[UIColor clearColor];
        self.lbl300.textColor=[UIColor blackColor];
        
        self.lbl315.backgroundColor=[UIColor clearColor];
        self.lbl315.textColor=[UIColor blackColor];
        
        self.lbl330.backgroundColor=[UIColor clearColor];
        self.lbl330.textColor=[UIColor blackColor];
        
        
        if ([self.firstpNumbr isEqualToString:@"0"]) {
            self.lbl0.backgroundColor=[UIColor blackColor];
            self.lbl0.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"30"])
        {
            self.lbl30.backgroundColor=[UIColor blackColor];
            self.lbl30.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"45"])
        {
            self.lbl45.backgroundColor=[UIColor blackColor];
            self.lbl45.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"60"])
        {
            self.lbl60.backgroundColor=[UIColor blackColor];
            self.lbl60.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"90"])
        {
            self.lbl90.backgroundColor=[UIColor blackColor];
            self.lbl90.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"120"])
        {
            self.lbl120.backgroundColor=[UIColor blackColor];
            self.lbl120.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"135"])
        {
            self.lbl135.backgroundColor=[UIColor blackColor];
            self.lbl135.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"150"])
        {
            self.lbl150.backgroundColor=[UIColor blackColor];
            self.lbl150.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"180"])
        {
            self.lbl180.backgroundColor=[UIColor blackColor];
            self.lbl180.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"210"])
        {
            self.lbl210.backgroundColor=[UIColor blackColor];
            self.lbl210.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"225"])
        {
            self.lbl225.backgroundColor=[UIColor blackColor];
            self.lbl225.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"240"])
        {
            self.lbl240.backgroundColor=[UIColor blackColor];
            self.lbl240.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"270"])
        {
            self.lbl270.backgroundColor=[UIColor blackColor];
            self.lbl270.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"300"])
        {
            self.lbl300.backgroundColor=[UIColor blackColor];
            self.lbl300.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"315"])
        {
            self.lbl315.backgroundColor=[UIColor blackColor];
            self.lbl315.textColor=[UIColor whiteColor];
        }else if ([self.firstpNumbr isEqualToString:@"330"])
        {
            self.lbl330.backgroundColor=[UIColor blackColor];
            self.lbl330.textColor=[UIColor whiteColor];
        }

    }
    
    
    [self.fauxView removeFromSuperview];
    for (UIView* child in self.fauxView.subviews) {
        [child removeFromSuperview];
    }
}

-(IBAction)SwitchAngleTapped:(id)sender
{
    SwitchAngle=YES;
    [self PickerTapped];
}

-(IBAction)StopTapped:(id)sender
{
    StopSelected=YES;
    [self PickerTapped];
}

-(IBAction)LatchTapped:(id)sender
{
    LatchSelected=YES;
    [self PickerTapped];
}

-(IBAction)HandleTapped:(id)sender
{
    HandleSeleted=YES;
    [self PickerTapped];
}

-(IBAction)EscutcheonTapped:(id)sender
{
    EscuteonSelected=YES;
    [self PickerTapped];
}

-(IBAction)MountingTapped:(id)sender
{
    MountingSelected=YES;
    [self PickerTapped];
}


-(IBAction)LookTapped:(id)sender
{
    LookSelected=YES;
    [self PickerTapped];
}


-(IBAction)SwitchTypeTapped:(id)sender
{
    
    SwitchTypeSelected=YES;
    [self PickerTapped];
}

-(IBAction)ProgramTapped:(id)sender
{
    ProgramSelected=YES;
    [self PickerTapped];
}

-(void)PickerTapped
{
    if (showblackselectpicker==NO)
    {
        self.fauxView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.fauxView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        //fauxView.alpha = 1.3;
        
        self.GraftNumPicker=[[UIPickerView alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(216/2), 320, 216)];
        self.GraftNumPicker.delegate=self;
        self.GraftNumPicker.dataSource=self;
        self.GraftNumPicker.showsSelectionIndicator=YES;
        
        UIButton *gbtnselectnum = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(320/2), (self.view.frame.size.height/2)-(50/2)+160, 320, 50)];
        //[gbtnsaveEvent setBackgroundImage:imgbtnsaveEvent forState:UIControlStateNormal];
        gbtnselectnum.layer.borderColor=[UIColor whiteColor].CGColor;
        gbtnselectnum.layer.borderWidth = 1;
        //gbtnselectnum.buttonType=UIButtonTypeRoundedRect;
        [gbtnselectnum setTitle:@"Select" forState:UIControlStateNormal];
        gbtnselectnum.titleLabel.Font = [UIFont fontWithName:@"Helvetica-Bold" size:17 ];
        gbtnselectnum.titleLabel.TextColor = [UIColor blackColor ];
        [gbtnselectnum.layer setMasksToBounds:YES];
        [gbtnselectnum.layer setCornerRadius:10.0f];
        [gbtnselectnum.layer setBackgroundColor:[UIColor whiteColor].CGColor];

        if (SwitchAngle==YES) {
            [gbtnselectnum addTarget:self action:@selector(getcloseSwitchAnglePickerTapped) forControlEvents:UIControlEventTouchDown];
        }else
        {
            [gbtnselectnum addTarget:self action:@selector(getclosePickerTapped) forControlEvents:UIControlEventTouchDown];
        }
        

        
        [self.fauxView addSubview:gbtnselectnum];
        [self.fauxView addSubview: self.GraftNumPicker];
        [self.view addSubview: self.fauxView];
        
    }
}

-(void)autosugestbtn
{
    UIImage *editButtonImage = [UIImage imageNamed:@"btn_dropdown.png"];
    /*=============button switch type=====================*/
    
    
    btnSwitchType = [[UIButton alloc] initWithFrame:CGRectMake( 341, 51, 24, 24)];
    //btnSwitchType.layer.borderColor=[UIColor blackColor].CGColor;
    //btnSwitchType.layer.borderWidth = 1;
    btnSwitchType.backgroundColor=[UIColor yellowColor];
    [btnSwitchType setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnSwitchType addTarget:self action:@selector(SwitchTypeTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnSwitchType];
    
    /*=============button Program=====================*/
    btnprogram = [[UIButton alloc] initWithFrame:CGRectMake( 492, 51, 24, 24)];
    //btnprogram.layer.borderColor=[UIColor blackColor].CGColor;
    //btnprogram.layer.borderWidth = 1;
    btnprogram.backgroundColor=[UIColor yellowColor];
    [btnprogram setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnprogram addTarget:self action:@selector(ProgramTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnprogram];
    
    
    int btnpbottom=1924;
    /*=============button Look=====================*/
    UIButton *btnlook = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 102, 26, 26)];
    //btnlook.layer.borderColor=[UIColor blackColor].CGColor;
    //btnlook.layer.borderWidth = 1;
    btnlook.backgroundColor=[UIColor yellowColor];
    [btnlook setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnlook addTarget:self action:@selector(LookTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnlook];
    
    /*=============button Mounting=====================*/
    UIButton *btnMounting = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 132, 26, 26)];
    //btnMounting.layer.borderColor=[UIColor blackColor].CGColor;
    //btnMounting.layer.borderWidth = 1;
    btnMounting.backgroundColor=[UIColor yellowColor];
    [btnMounting setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnMounting addTarget:self action:@selector(MountingTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnMounting];
    
    /*=============button Escutcheon=====================*/
    UIButton *btnEscutcheon = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 162, 26, 26)];
    //btnEscutcheon.layer.borderColor=[UIColor blackColor].CGColor;
    //btnEscutcheon.layer.borderWidth = 1;
    btnEscutcheon.backgroundColor=[UIColor yellowColor];
    [btnEscutcheon setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnEscutcheon addTarget:self action:@selector(EscutcheonTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnEscutcheon];
    
    /*=============button Handle=====================*/
    UIButton *btnHandle = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 192, 26, 26)];
    //btnHandle.layer.borderColor=[UIColor blackColor].CGColor;
    //btnHandle.layer.borderWidth = 1;
    //btnHandle.backgroundColor=[UIColor yellowColor];
    [btnHandle setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnHandle addTarget:self action:@selector(HandleTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnHandle];
    
    /*=============button Latch=====================*/
    UIButton *btnLatch = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 222, 26, 26)];
    //btnLatch.layer.borderColor=[UIColor blackColor].CGColor;
    //btnLatch.layer.borderWidth = 1;
    //btnLatch.backgroundColor=[UIColor yellowColor];
    [btnLatch setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnLatch addTarget:self action:@selector(LatchTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnLatch];
    
    /*=============button Stop=====================*/
    UIButton *btnStop = [[UIButton alloc] initWithFrame:CGRectMake( btnpbottom, 252, 26, 26)];
    //btnStop.layer.borderColor=[UIColor blackColor].CGColor;
    //btnStop.layer.borderWidth = 1;
    //btnStop.backgroundColor=[UIColor yellowColor];
    [btnStop setBackgroundImage:editButtonImage forState:UIControlStateNormal];
    [btnStop addTarget:self action:@selector(StopTapped:) forControlEvents:UIControlEventTouchDown];
    [self.mainscrollview addSubview:btnStop];
    
}

-(void)thirdpart
{

    
    /*======================top===================================*/
    [self.lblpage1 setFrame:CGRectMake(675, 49, 795, 28)];
    [self.lblpage2 setFrame:CGRectMake(self.view.frame.size.width+ 595, 49, 795, 28)];
    
    [self.mainscrollview addSubview:self.lblpage1];
    [self.mainscrollview addSubview:self.lblpage2];
    
    [self.lbl setFrame:CGRectMake(251, 77, 1032, 21)];
    self.lbl.layer.borderColor=[UIColor blackColor].CGColor;
    self.lbl.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.lbl ];
    
    [self.txtPlateName setFrame:CGRectMake(0, 77, 252, 21)];
    txtPlateName.autocorrectionType=UITextAutocorrectionTypeNo;
    [self.mainscrollview addSubview:self.txtPlateName ];
    
    [self.lblCA10 setFrame:CGRectMake(337, 49, 152, 28)];
    self.lblCA10.layer.borderColor=[UIColor blackColor].CGColor;
    self.lblCA10.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.lblCA10 ];
    
    [self.txtSwitchType setFrame:CGRectMake(368, 49, 117, 28)];
    txtSwitchType.autocorrectionType=UITextAutocorrectionTypeNo;
    //self.txtSwitchType.layer.borderColor=[UIColor blackColor].CGColor;
    //self.txtSwitchType.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.txtSwitchType ];
    
    
    
    [self.SGL719 setFrame:CGRectMake(488, 49, 795, 28)];
    self.SGL719.layer.borderColor=[UIColor blackColor].CGColor;
    self.SGL719.layer.borderWidth = 1;
    self.SGL719.backgroundColor=[UIColor clearColor];
    [self.mainscrollview addSubview:self.SGL719 ];
    
    [self.txtProgram setFrame:CGRectMake(520, 49, 150, 28)];
    txtProgram.autocorrectionType=UITextAutocorrectionTypeNo;
    //self.txtProgram.layer.borderColor=[UIColor blackColor].CGColor;
    //self.txtProgram.layer.borderWidth = 1;
    self.txtProgram.backgroundColor=[UIColor clearColor];
    [self.mainscrollview addSubview:self.txtProgram ];
    
    [self.Labellbl setFrame:CGRectMake(337, 20, 946, 30)];
    self.Labellbl.layer.borderColor=[UIColor blackColor].CGColor;
    self.Labellbl.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.Labellbl ];
    
    [self.txtcustomerArticleNum setFrame:CGRectMake(340, 20, 420, 30)];
    //self.txtcustomerArticleNum.layer.borderColor=[UIColor blackColor].CGColor;
    //self.txtcustomerArticleNum.layer.borderWidth = 1;
    txtcustomerArticleNum.autocorrectionType=UITextAutocorrectionTypeNo;
    [self.mainscrollview addSubview:self.txtcustomerArticleNum ];
    
    /*======================bottom===================================*/
    
    [self.tblinfocontent setFrame:CGRectMake(1540+15, 100, 730, 451)];
    //self.tblinfocontent.hidden=YES;
    
    
    [self.btnNewPlate setFrame:CGRectMake(1540+85, 580, 250, 60)];
    
    int lbllookswidth=150;
    int lbllookpwidth=1540 +15;
    
    int txtlookswidth=245;
    int txtlookpwidth=1695+15;
    
    
    [self.lbllook setFrame:CGRectMake(lbllookpwidth, 100, lbllookswidth, 30)];
    [self.txtlook setFrame:CGRectMake(txtlookpwidth, 100, txtlookswidth, 30)];
    txtlook.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lbllook.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lbllook.layer.borderWidth = 1;
//    self.txtlook.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtlook.layer.borderWidth = 1;
    //self.txtlook.enabled=NO;
    
    [self.lblMounting setFrame:CGRectMake(lbllookpwidth, 130, lbllookswidth, 30)];
    [self.txtmounting setFrame:CGRectMake(txtlookpwidth, 130, txtlookswidth, 30)];
    txtmounting.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblMounting.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblMounting.layer.borderWidth = 1;
//    self.txtmounting.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtmounting.layer.borderWidth = 1;
    
    [self.lblEscutcheonPlate setFrame:CGRectMake(lbllookpwidth, 160, lbllookswidth, 30)];
    [self.txtescutcheon setFrame:CGRectMake(txtlookpwidth, 160, txtlookswidth, 30)];
    txtescutcheon.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblEscutcheonPlate.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblEscutcheonPlate.layer.borderWidth = 1;
//    self.txtescutcheon.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtescutcheon.layer.borderWidth = 1;
    
    [self.lblHandle setFrame:CGRectMake(lbllookpwidth, 190, lbllookswidth, 30)];
    [self.txthandle setFrame:CGRectMake(txtlookpwidth, 190, txtlookswidth, 30)];
    txthandle.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblHandle.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblHandle.layer.borderWidth = 1;
//    self.txthandle.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txthandle.layer.borderWidth = 1;
    
    [self.lblLatchMech setFrame:CGRectMake(lbllookpwidth, 220, lbllookswidth, 30)];
    [self.txtlatch_mech setFrame:CGRectMake(txtlookpwidth, 220, txtlookswidth, 30)];
    txtlatch_mech.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblLatchMech.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblLatchMech.layer.borderWidth = 1;
//    self.txtlatch_mech.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtlatch_mech.layer.borderWidth = 1;
    
    [self.lblStop setFrame:CGRectMake(lbllookpwidth, 250, lbllookswidth, 30)];
    [self.txtstop setFrame:CGRectMake(txtlookpwidth, 250, txtlookswidth, 30)];
    txtstop.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblStop.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblStop.layer.borderWidth = 1;
//    self.txtstop.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtstop.layer.borderWidth = 1;
    
    [self.lblStopdegree setFrame:CGRectMake(lbllookpwidth, 280, lbllookswidth, 30)];
    [self.txtstop_degree setFrame:CGRectMake(txtlookpwidth, 280, txtlookswidth, 30)];
    txtstop_degree.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblStopdegree.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblStopdegree.layer.borderWidth = 1;
//    self.txtstop_degree.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtstop_degree.layer.borderWidth = 1;

    [self.lblNoofStages setFrame:CGRectMake(lbllookpwidth, 310, lbllookswidth, 30)];
    [self.txtno_of_stage setFrame:CGRectMake(txtlookpwidth, 310, txtlookswidth, 30)];
    txtno_of_stage.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblNoofStages.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblNoofStages.layer.borderWidth = 1;
//    self.txtno_of_stage.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtno_of_stage.layer.borderWidth = 1;
    
    [self.lblMasterdata setFrame:CGRectMake(lbllookpwidth, 340, lbllookswidth, 30)];
    [self.txtmaster_data setFrame:CGRectMake(txtlookpwidth, 340, txtlookswidth, 30)];
    txtmaster_data.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblMasterdata.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblMasterdata.layer.borderWidth = 1;
//    self.txtmaster_data.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtmaster_data.layer.borderWidth = 1;
    
    [self.lblReference setFrame:CGRectMake(lbllookpwidth, 370, lbllookswidth, 30)];
    [self.txtreference setFrame:CGRectMake(txtlookpwidth, 370, txtlookswidth, 30)];
    txtreference.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblReference.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblReference.layer.borderWidth = 1;
//    self.txtreference.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtreference.layer.borderWidth = 1;
    
    [self.lblDate setFrame:CGRectMake(lbllookpwidth, 400, lbllookswidth, 30)];
    [self.txtdate setFrame:CGRectMake(txtlookpwidth, 400, txtlookswidth, 30)];
    self.txtdate.enabled=NO;
//    self.lblDate.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblDate.layer.borderWidth = 1;
//    self.txtdate.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtdate.layer.borderWidth = 1;
    
    NSDate* currentDate = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSString *dateString = [dateFormat stringFromDate:currentDate];
    self.txtdate.text=dateString;
    
    
    [self.lblModifyDate setFrame:CGRectMake(lbllookpwidth, 430, lbllookswidth, 30)];
    [self.txtmodify_date setFrame:CGRectMake(txtlookpwidth, 430, txtlookswidth, 30)];
    self.txtmodify_date.enabled=NO;
//    self.lblModifyDate.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblModifyDate.layer.borderWidth = 1;
//    self.txtmodify_date.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtmodify_date.layer.borderWidth = 1;
    
    [self.lblCustNO setFrame:CGRectMake(lbllookpwidth, 460, lbllookswidth, 30)];
    [self.txtcust_no setFrame:CGRectMake(txtlookpwidth, 460, txtlookswidth, 30)];
    txtcust_no.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblCustNO.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblCustNO.layer.borderWidth = 1;
//    self.txtcust_no.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtcust_no.layer.borderWidth = 1;
    
    [self.lblCompany setFrame:CGRectMake(lbllookpwidth, 490, lbllookswidth, 30)];
    [self.txtcompany setFrame:CGRectMake(txtlookpwidth, 490, txtlookswidth, 30)];
    txtcompany.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.lblCompany.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblCompany.layer.borderWidth = 1;
//    self.txtcompany.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtcompany.layer.borderWidth = 1;
    
    [self.lblVersion setFrame:CGRectMake(lbllookpwidth, 520, lbllookswidth, 30)];
    [self.txtversion setFrame:CGRectMake(txtlookpwidth, 520, txtlookswidth, 30)];
    txtversion.autocorrectionType=UITextAutocorrectionTypeNo;
    txtversion.text=@"1";
//    self.lblVersion.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblVersion.layer.borderWidth = 1;
//    self.txtversion.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtversion.layer.borderWidth = 1;
    
    
    /*=================== PSC ==============================*/
    
    
    int txtPcspwidth=1945+15;
    int txtPcsswidth=55;
    
    [self.lblpsc setFrame:CGRectMake(1940+15, 100, 60, 30)];
//    self.lblpsc.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lblpsc.layer.borderWidth = 1;
    
    [self.txtPcs1 setFrame:CGRectMake(txtPcspwidth, 129, txtPcsswidth, 30)];
//    self.txtPcs1.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs1.layer.borderWidth = 1;
    
    [self.txtPcs2 setFrame:CGRectMake(txtPcspwidth, 159, txtPcsswidth, 30)];
//    self.txtPcs2.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs2.layer.borderWidth = 1;
    
    [self.txtPcs3 setFrame:CGRectMake(txtPcspwidth, 189, txtPcsswidth, 30)];
//    self.txtPcs3.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs3.layer.borderWidth = 1;
    
    [self.txtPcs4 setFrame:CGRectMake(txtPcspwidth, 219, txtPcsswidth, 30)];
//    self.txtPcs4.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs4.layer.borderWidth = 1;
    
    [self.txtPcs5 setFrame:CGRectMake(txtPcspwidth, 249, txtPcsswidth, 30)];
//    self.txtPcs5.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs5.layer.borderWidth = 1;
    
    [self.txtPcs6 setFrame:CGRectMake(txtPcspwidth, 279, txtPcsswidth, 30)];
//    self.txtPcs6.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs6.layer.borderWidth = 1;
    
    [self.txtPcs7 setFrame:CGRectMake(txtPcspwidth, 309, txtPcsswidth, 30)];
//    self.txtPcs7.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs7.layer.borderWidth = 1;
    
    [self.txtPcs8 setFrame:CGRectMake(txtPcspwidth, 339, txtPcsswidth, 30)];
//    self.txtPcs8.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs8.layer.borderWidth = 1;
    
    [self.txtPcs9 setFrame:CGRectMake(txtPcspwidth, 369, txtPcsswidth, 30)];
//    self.txtPcs9.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtPcs9.layer.borderWidth = 1;
    
    
    int txtOptExtrapwidth=2005+15;
    int txtOptExtraswidth=265;
    
    [self.lbloptextra setFrame:CGRectMake(2000+15, 100, 270, 30)];
//    self.lbloptextra.layer.borderColor=[UIColor blackColor].CGColor;
//    self.lbloptextra.layer.borderWidth = 1;
    
    [self.txtOptExtra1 setFrame:CGRectMake(txtOptExtrapwidth, 129, txtOptExtraswidth, 30)];
    txtOptExtra1.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra1.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra1.layer.borderWidth = 1;
    
    [self.txtOptExtra2 setFrame:CGRectMake(txtOptExtrapwidth, 159, txtOptExtraswidth, 30)];
    txtOptExtra2.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra2.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra2.layer.borderWidth = 1;
    
    [self.txtOptExtra3 setFrame:CGRectMake(txtOptExtrapwidth, 189, txtOptExtraswidth, 30)];
    txtOptExtra3.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra3.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra3.layer.borderWidth = 1;
    
    [self.txtOptExtra4 setFrame:CGRectMake(txtOptExtrapwidth, 219, txtOptExtraswidth, 30)];
    txtOptExtra4.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra4.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra4.layer.borderWidth = 1;
    
    [self.txtOptExtra5 setFrame:CGRectMake(txtOptExtrapwidth, 249, txtOptExtraswidth, 30)];
    txtOptExtra5.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra5.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra5.layer.borderWidth = 1;
    
    [self.txtOptExtra6 setFrame:CGRectMake(txtOptExtrapwidth, 279, txtOptExtraswidth, 30)];
    txtOptExtra6.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra6.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra6.layer.borderWidth = 1;
    
    [self.txtOptExtra7 setFrame:CGRectMake(txtOptExtrapwidth, 309, txtOptExtraswidth, 30)];
    txtOptExtra7.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra7.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra7.layer.borderWidth = 1;
    
    [self.txtOptExtra8 setFrame:CGRectMake(txtOptExtrapwidth, 339, txtOptExtraswidth, 30)];
    txtOptExtra8.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra8.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra8.layer.borderWidth = 1;
    
    [self.txtOptExtra9 setFrame:CGRectMake(txtOptExtrapwidth, 369, txtOptExtraswidth, 30)];
    txtOptExtra9.autocorrectionType=UITextAutocorrectionTypeNo;
//    self.txtOptExtra9.layer.borderColor=[UIColor blackColor].CGColor;
//    self.txtOptExtra9.layer.borderWidth = 1;
    
    
    [self.txtinfocontent setFrame:CGRectMake(1945+15, 425, 320, 99)];
    txtinfocontent.autocorrectionType=UITextAutocorrectionTypeNo;
    self.txtinfocontent.layer.borderColor=[UIColor clearColor].CGColor;
    self.txtinfocontent.layer.borderWidth = 1;
    
    
    
    [self.mainscrollview addSubview:self.lbllook];
    [self.mainscrollview addSubview:self.lblMounting];
    [self.mainscrollview addSubview:self.lblEscutcheonPlate];
    [self.mainscrollview addSubview:self.lblHandle];
    [self.mainscrollview addSubview:self.lblLatchMech];
    
    [self.mainscrollview addSubview:self.lblStop];
    [self.mainscrollview addSubview:self.lblStopdegree];
    [self.mainscrollview addSubview:self.lblNoofStages];
    [self.mainscrollview addSubview:self.lblMasterdata];
    [self.mainscrollview addSubview:self.lblReference];
    
    [self.mainscrollview addSubview:self.lblDate];
    [self.mainscrollview addSubview:self.lblModifyDate];
    [self.mainscrollview addSubview:self.lblCustNO];
    [self.mainscrollview addSubview:self.lblCompany];
    [self.mainscrollview addSubview:self.lblVersion];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    // limit the number of lines in textview
    NSString* newText = [txtinfocontent.text stringByReplacingCharactersInRange:range withString:text];
    
    // pretend there's more vertical space to get that extra line to check on
    CGSize tallerSize = CGSizeMake(txtinfocontent.frame.size.width-15, txtinfocontent.frame.size.height*2);
    
    CGSize newSize = [newText sizeWithFont:txtinfocontent.font constrainedToSize:tallerSize lineBreakMode:UILineBreakModeWordWrap];
    
    if (newSize.height > txtinfocontent.frame.size.height)
    {
        NSLog(@"two lines are full");
        
        [txtinfocontent resignFirstResponder];
        return NO;
    }
    
    
    // dismiss keyboard and send comment
//    if([text isEqualToString:@"\n"]) {
//        [txtinfocontent resignFirstResponder];
//        
//        return NO;
//    }
    
    return YES;
}


/*============copy new plate========================*/
-(IBAction)passPlateInfoToNewPlate:(id)sender
{
    //here pass the value to the plate
    PlateInfo *pi = [[PlateInfo alloc] init];

    pi.desc1 = lbltxt330.text;
    pi.desc2 = lbltxt315.text;
    pi.desc3 = lbltxt300.text;
    pi.desc4 = lbltxt270.text;
    pi.desc5 = lbltxt240.text;
    pi.desc6 = lbltxt225.text;
    pi.desc7 = lbltxt210.text;
    
    pi.desc8 = lbltxt30.text;
    pi.desc9 = lbltxt45.text;
    pi.desc10 = lbltxt60.text;
    pi.desc11 = lbltxt90.text;
    pi.desc12 = lbltxt120.text;
    pi.desc13 = lbltxt135.text;
    pi.desc14 = lbltxt150.text;
    pi.title1 = txtPlateName.text;
    pi.title2 = lbltxt0.text;
    pi.title3 = lbltxt180.text;
    pi.reference = txtreference.text;
    pi.company = txtcompany.text;
    pi.customerNo = txtcust_no.text;
    
    EscutheonPlatePortrait *gKNEEscutheonPlateViewController = [[EscutheonPlatePortrait alloc] initWithNibName:@"EscutheonPlatePortrait" bundle:nil];
    gKNEEscutheonPlateViewController.option = @"passPlate";
    gKNEEscutheonPlateViewController.plateInfo = pi;
    
    [self.navigationController pushViewController:gKNEEscutheonPlateViewController animated:YES];
    
}




/**/



-(void)getfirstpNum
{
    //NSLog(@"getfirstpNum--->>>>");
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    //appDelegate.firstPnum=@"330";
    
    int fP=[appDelegate.firstPnum intValue];
    int firstposition = fP-15;
    //int rowvalue=0;
    
    NSMutableArray *graftNumbermArr = [[NSMutableArray alloc]init];
    
    for(int i=0;i<24;i++)
    {
        
        
        
        if (firstposition >=360) {
            firstposition=15;
        }else {
            firstposition+=15;
        }
        
        
        //NSLog(@"firstposition--->>>>%d",firstposition);
        
        
        
        NSString *invalue=[[NSString alloc]initWithFormat:@"%d",firstposition];
        
        if ([invalue isEqualToString:@"360"]) {
            invalue=@"0";
        }
        [graftNumbermArr addObject:invalue];
        
    }
    
    self.numarr=graftNumbermArr;
    
    //[self.tblSpringReturn reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return numarr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //KNESpringReturnCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[KNESpringReturnCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    //BpEvent* gBpEventcell=[self.numarr objectAtIndex:indexPath.row];
    //NSString *springnum=[[NSString alloc]initWithFormat:@"%@",[self.numarr objectAtIndex:indexPath.row]];
    
    //cell.delegate=self;
    //cell.delegate=self.gKNESwitchShowViewController;
    
    
    cell.lblSpringNum.text=[self.numarr objectAtIndex:indexPath.row];
    //txtindexpath=indexPath.row;
    
    //cell.txtuserremark.delegate = self;
 
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    cell= (KNESpringReturnCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    NSLog(@"indexPath>>>%d",indexPath.row);
    
    
    NSString* key=[numarr objectAtIndex:indexPath.row];
    
    NSLog(@"key>>>%@",key);
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.firstPnum=key;
    
    
    //[self.tblSpringReturn reloadData];
    //[self performSelector:@selector(reloadtbldata) withObject:nil afterDelay:1];
    [self reloadtbldata];
}

-(void)reloadtbldata
{
    NSLog(@"reloadtbldata");
    [self getfirstpNum];
    [self.tblSpringReturn reloadData];
    [self reloadtblSpringReturn];
}


@end














