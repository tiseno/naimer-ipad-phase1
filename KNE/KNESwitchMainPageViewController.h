//
//  KNESwitchMainPageViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import <UIKit/UIKit.h>
#import "KNEAppDelegate.h"
#import "KNEtblallCaseCell.h"
#import "KNEStartBallViewController.h"
#import "DBCase.h"
#import "KNECase.h"
#import "KNESwitchShowViewController.h"
#import "KNEEditSwitchCaseViewController.h"
#import "KNESwitchingFirstValue.h"
#import "DBSwitchingfirstValue.h"

@interface KNESwitchMainPageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
    KNEtblallCaseCell *cell;
}
@property (nonatomic,strong) KNEtblallCaseCell *gKNEtblallCaseCell;
@property (nonatomic,strong) NSArray *CaseArr;
@property (strong, nonatomic) IBOutlet UITableView *tblCase;

-(IBAction)newCaseTapped:(id)sender;

@end
