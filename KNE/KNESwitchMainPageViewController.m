//
//  KNESwitchMainPageViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import "KNESwitchMainPageViewController.h"

@implementation KNESwitchMainPageViewController

@synthesize gKNEtblallCaseCell, CaseArr;
@synthesize tblCase;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DBCase *createCase=[[DBCase alloc]init ];
    //NSArray *caseIDArr=[[NSArray alloc] init];
    //caseIDArr=[createCase selectItem];
     
    self.CaseArr=[createCase selectItem];
    
    //NSLog(@"caseIDArr-->%@",caseIDArr);
    
    [tblCase reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setTblCase:nil];
    
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{

    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.CaseArr count ];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //KNESpringReturnCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [[KNEtblallCaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[KNEtblallCaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    KNECase* classKNECase=[self.CaseArr objectAtIndex:indexPath.row];
    cell.Case=classKNECase;
    
    cell.lblCaseID.text=classKNECase.CaseID;
    cell.lblCaseName.text=classKNECase.CaseName;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    //BpCouponCell *selectedcell= (BpCouponCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    cell= (KNEtblallCaseCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    /*KNESwitchShowViewController *gKNESwitchShowViewController=[[KNESwitchShowViewController alloc] initWithNibName:@"KNESwitchShowViewController"  bundle:nil];
    gKNESwitchShowViewController.KNECaseclass=cell.Case;
    [self.navigationController pushViewController:gKNESwitchShowViewController animated:YES];
    [gKNESwitchShowViewController release];*/
    
    /*===========switching first value===============*/
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    DBSwitchingfirstValue *gDBSwitchingfirstValue=[[DBSwitchingfirstValue alloc] init];
    NSArray *gDBSwitchingfirstValueAr =[gDBSwitchingfirstValue selectSwitchingFirstValueItem:cell.Case];
    
    for (KNESwitchingFirstValue *gitem in gDBSwitchingfirstValueAr) {
        appDelegate.firstPnum=gitem.SwitchingFirstValue;
    }
    appDelegate.gKNECase=cell.Case;
    
    KNEEditSwitchCaseViewController *gKNEEditSwitchCaseViewController=[[KNEEditSwitchCaseViewController alloc] initWithNibName:@"KNEEditSwitchCaseViewController"  bundle:nil];
    gKNEEditSwitchCaseViewController.KNECaseclass=cell.Case;
    [self.navigationController pushViewController:gKNEEditSwitchCaseViewController animated:YES];
    
    //NSLog(@"indexPath>>>%@",indexPath);
}

-(IBAction)newCaseTapped:(id)sender
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KNEStartBallViewController *gtBpMainViewController=[[KNEStartBallViewController alloc] initWithNibName:@"KNEStartBallViewController"  bundle:nil];
    appDelegate.firstPnum=@"0";
    [self.navigationController pushViewController:gtBpMainViewController animated:YES];
    
    
}


@end
