//
//  KNESwitchPoint.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/21/12.
//
//

#import <Foundation/Foundation.h>
#import "KNECase.h"

@interface KNESwitchPoint : KNECase{
    
}

@property (nonatomic, strong) NSString *SwitchPoint;

@end
