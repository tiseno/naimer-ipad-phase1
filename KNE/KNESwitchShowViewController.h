//
//  KNESwitchShowViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/19/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KNESpringGraftViewController.h"
#import "KNESpringGraftCell.h"
#import "ButtonWithGraftindex.h"
#import "KNESpringRPosition.h"
#import "KNEAppDelegate.h"
#import "KNETblGraftPositionTag.h"
#import "DBTableGraft.h"
#import "KNESwitchShowViewController.h"
#import "KNECase.h"
#import "DBSpringReturn.h"
#import "DBSpringReturnContain.h"
#import "KNESpringReturnContain.h"
#import "KNEShowSRViewController.h"
#import "JumperView.h"
#import "KNEJShow.h"
#import <MessageUI/MessageUI.h>
#import "DBCase.h"
#import "MBProgressHUD.h"

@interface KNESwitchShowViewController : UIViewController<MFMailComposeViewControllerDelegate, UIPrintInteractionControllerDelegate, UIScrollViewDelegate>{
    
    /*=====================number of stage================================*/
    BOOL stage1;
    BOOL stage2;
    BOOL stage3;
    BOOL stage4;
    BOOL stage5;
    BOOL stage6;
    BOOL stage7;
    BOOL stage8;
    BOOL stage9;
    BOOL stage10;
    BOOL stage11;
    BOOL stage12;
    
    int stageno;
    
    UIPrintInteractionController *pic;
    NSData *imageData;
    
    BOOL printclicked;
}
@property (strong, nonatomic) IBOutlet UILabel *lblpage1;

@property (strong, nonatomic) IBOutlet UILabel *lblpage2;
@property (strong, nonatomic) UIButton *optionButton;
@property (strong, nonatomic) UIButton *closeButton;
@property (strong, nonatomic) UIPopoverController *popoverController;
@property (strong, nonatomic) IBOutlet UIImageView *imgLogo;
@property (strong, nonatomic) UIButton *btnprintSecondPage;
@property (strong, nonatomic) UIButton *btnprint;
@property (strong, nonatomic) UIBarButtonItem *btnprintfull1;
@property (strong, nonatomic) UIBarButtonItem *btnPrint1;
@property (strong, nonatomic) UIBarButtonItem *btnPrintSecondPage1;
@property (nonatomic, strong) KNEJShow *jumperview;
@property (strong, nonatomic) KNEShowSRViewController *gKNEShowSRViewController;
@property (strong, nonatomic) IBOutlet UIScrollView *mainscrollview;
@property (strong, nonatomic) IBOutlet UITableView *tblSprinReturn;
@property (strong, nonatomic) IBOutlet UITableView *tblSprinReturnExtra;
@property (strong, nonatomic) KNECase *KNECaseclass;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *imageViewover;
@property (strong, nonatomic) UILabel *lblgraftNum;
@property (strong, nonatomic) UILabel *lblgraftvalueposition;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
@property (nonatomic, strong) NSString *strlbllook;
@property (nonatomic, strong) NSString *strlblMounting;
@property (nonatomic, strong) NSString *strlblEscutcheonPlate;
@property (nonatomic, strong) NSString *strlblHandle;
@property (nonatomic, strong) NSString *strlblLatchMech;
@property (nonatomic, strong) NSString *strlblStop;
@property (nonatomic, strong) NSString *strlblStopdegree;
@property (nonatomic, strong) NSString *strlblNoofStages;
@property (nonatomic, strong) NSString *strlblMasterdata;
@property (nonatomic, strong) NSString *strlblReference;
@property (nonatomic, strong) NSString *strlblDate;
@property (nonatomic, strong) NSString *strlblModifyDate;
@property (nonatomic, strong) NSString *strlblCustNO;
@property (nonatomic, strong) NSString *strlblCompany;
@property (nonatomic, strong) NSString *strlblVersion;

@property (strong, nonatomic) IBOutlet UILabel *lbllook;
@property (strong, nonatomic) IBOutlet UILabel *lblMounting;
@property (strong, nonatomic) IBOutlet UILabel *lblEscutcheonPlate;
@property (strong, nonatomic) IBOutlet UILabel *lblHandle;
@property (strong, nonatomic) IBOutlet UILabel *lblLatchMech;
@property (strong, nonatomic) IBOutlet UILabel *lblStop;
@property (strong, nonatomic) IBOutlet UILabel *lblStopdegree;
@property (strong, nonatomic) IBOutlet UILabel *lblNoofStages;
@property (strong, nonatomic) IBOutlet UILabel *lblMasterdata;
@property (strong, nonatomic) IBOutlet UILabel *lblReference;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblModifyDate;
@property (strong, nonatomic) IBOutlet UILabel *lblCustNO;
@property (strong, nonatomic) IBOutlet UILabel *lblCompany;
@property (strong, nonatomic) IBOutlet UILabel *lblVersion;

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

@property (strong, nonatomic) IBOutlet UILabel *lbltxtlook;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtMounting;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtEscutcheonPlate;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtHandle;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtLatchMech;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtStop;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtStopdegree;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtNoofStages;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtMasterdata;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtReference;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtDate;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtModifyDate;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtCustNO;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtCompany;
@property (strong, nonatomic) IBOutlet UILabel *lbltxtVersion;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional;

@property (strong, nonatomic) IBOutlet UILabel *lblpcs1;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs2;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs3;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs4;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs5;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs6;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs7;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs8;
@property (strong, nonatomic) IBOutlet UILabel *lblpcs9;

@property (strong, nonatomic) IBOutlet UILabel *lbloptional1;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional2;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional3;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional4;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional5;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional6;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional7;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional8;
@property (strong, nonatomic) IBOutlet UILabel *lbloptional9;

@property (strong, nonatomic) IBOutlet UILabel *lbloptionalComment;

@property (strong, nonatomic) IBOutlet UIImageView *imgtblspringReturn;
@property (strong, nonatomic) IBOutlet UIImageView *imgtblbottom;
@property (strong, nonatomic) IBOutlet UIImageView *imgtblcontact;
@property (strong, nonatomic) IBOutlet UIImageView *imgSwitch;

@property (strong, nonatomic) IBOutlet UILabel *lblSwitchingAngle;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalSwitchingangle;


@property (strong, nonatomic) IBOutlet UILabel *lbltxt0;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt30;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt45;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt60;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt90;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt120;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt135;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt150;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt180;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt210;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt225;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt240;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt270;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt300;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt315;
@property (strong, nonatomic) IBOutlet UILabel *lbltxt330;

@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UILabel *lblCA10;
@property (strong, nonatomic) IBOutlet UILabel *SGL719;
@property (strong, nonatomic) IBOutlet UILabel *Labellbl;

@property (strong, nonatomic) IBOutlet UILabel *lbltext0;
@property (strong, nonatomic) IBOutlet UILabel *lbltext30;
@property (strong, nonatomic) IBOutlet UILabel *lbltext45;
@property (strong, nonatomic) IBOutlet UILabel *lbltext60;
@property (strong, nonatomic) IBOutlet UILabel *lbltext90;
@property (strong, nonatomic) IBOutlet UILabel *lbltext120;
@property (strong, nonatomic) IBOutlet UILabel *lbltext135;
@property (strong, nonatomic) IBOutlet UILabel *lbltext150;
@property (strong, nonatomic) IBOutlet UILabel *lbltext180;
@property (strong, nonatomic) IBOutlet UILabel *lbltext210;
@property (strong, nonatomic) IBOutlet UILabel *lbltext225;
@property (strong, nonatomic) IBOutlet UILabel *lbltext240;
@property (strong, nonatomic) IBOutlet UILabel *lbltext270;
@property (strong, nonatomic) IBOutlet UILabel *lbltext300;
@property (strong, nonatomic) IBOutlet UILabel *lbltext315;
@property (strong, nonatomic) IBOutlet UILabel *lbltext330;

@property (strong, nonatomic) IBOutlet UILabel *lblcustomerArticleNum;
@property (strong, nonatomic) IBOutlet UILabel *lblPlateName;
@property (strong, nonatomic) IBOutlet UILabel *lblSwitchType;
@property (strong, nonatomic) IBOutlet UILabel *lblProgram;

@property (strong, nonatomic) IBOutlet UIImageView *imgtblswitch;
@property (strong, nonatomic) IBOutlet UIImageView *tblimgback;
@property (strong, nonatomic) UIView *jumperView;
@end
