//
//  KNESwitchShowViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/19/12.
//
//

#import "KNESwitchShowViewController.h"


@implementation KNESwitchShowViewController
@synthesize tblSprinReturn, tblSprinReturnExtra;
@synthesize mainscrollview,popoverController, closeButton, btnprintSecondPage, btnPrintSecondPage1, optionButton;
@synthesize KNECaseclass, jumperView;
@synthesize gKNEShowSRViewController;
@synthesize jumperview, btnprint,btnPrint1, btnprintfull1;
@synthesize lblCompany, lblCustNO, lblDate,lblEscutcheonPlate, lblHandle, lblLatchMech, lbllook, lblMasterdata, lblModifyDate, lblMounting , lblNoofStages, lblReference, lblStop, lblStopdegree, lblVersion;
@synthesize lbltxtCompany, lbltxtCustNO, lbltxtDate,lbltxtEscutcheonPlate, lbltxtHandle, lbltxtLatchMech, lbltxtlook, lbltxtMasterdata, lbltxtModifyDate, lbltxtMounting , lbltxtNoofStages, lbltxtReference, lbltxtStop, lbltxtStopdegree, lbltxtVersion;

@synthesize lblpcs, lblpcs1, lblpcs2, lblpcs3, lblpcs4, lblpcs5, lblpcs6, lblpcs7, lblpcs8, lblpcs9;
@synthesize lbloptional, lbloptional1,lbloptional2, lbloptional3, lbloptional4, lbloptional5, lbloptional6, lbloptional7, lbloptional8, lbloptional9;
@synthesize imgtblbottom, imgtblcontact, imgtblspringReturn, imgSwitch;

@synthesize lbltxt0, lbltxt120, lbltxt135, lbltxt150, lbltxt180, lbltxt210, lbltxt225, lbltxt240, lbltxt270, lbltxt30, lbltxt300, lbltxt315, lbltxt330, lbltxt45, lbltxt60, lbltxt90;
@synthesize lbltext0, lbltext120, lbltext135, lbltext150, lbltext180, lbltext210, lbltext225, lbltext240, lbltext270, lbltext30, lbltext300, lbltext315, lbltext330, lbltext45, lbltext60, lbltext90;
@synthesize strlbllook, strlblCompany, strlblCustNO, strlblDate, strlblEscutcheonPlate, strlblHandle, strlblLatchMech, strlblMasterdata, strlblModifyDate, strlblMounting, strlblNoofStages, strlblReference, strlblStop, strlblStopdegree, strlblVersion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        /*===================== label title =================================*/
        strlbllook=@" Look";
        strlblMounting=@" Mounting";
        strlblEscutcheonPlate=@" Escutcheon Plate";
        strlblHandle=@" Handle";
        strlblLatchMech=@" Latch. Mech.";
        strlblStop=@" Stop";
        strlblStopdegree=@" Stop Degree";
        strlblNoofStages=@" No. of Stages";
        strlblMasterdata=@" Master Data";
        strlblReference=@" Reference";
        strlblDate=@" Date";
        strlblModifyDate=@" Modify Date";
        strlblCustNO=@" Cus. No.";
        strlblCompany=@" Company";
        strlblVersion=@" Version";
        
        stage1=NO;
        stage2=NO;
        stage3=NO;
        stage4=NO;
        stage5=NO;
        stage6=NO;
        stage7=NO;
        stage8=NO;
        stage9=NO;
        stage10=NO;
        stage11=NO;
        stage12=NO;
        printclicked=NO;
    }
    return self;
}

- (void)viewDidUnload
{
    [self setMainscrollview:nil];
    [self setTblSprinReturn:nil];
    [self setLblpcs:nil];
    [self setLbloptional:nil];
    [self setLbloptionalComment:nil];
    [self setImgtblspringReturn:nil];
    [self setImgtblbottom:nil];
    [self setImgtblcontact:nil];
    [self setImgSwitch:nil];
    [self setLbl:nil];
    [self setLblCA10:nil];
    [self setSGL719:nil];
    [self setLabellbl:nil];
    [self setLbltext0:nil];
    [self setLbltext30:nil];
    [self setLbltext45:nil];
    [self setLbltext60:nil];
    [self setLbltext90:nil];
    [self setLbltext120:nil];
    [self setLbltext135:nil];
    [self setLbltext150:nil];
    [self setLbltext180:nil];
    [self setLbltext210:nil];
    [self setLbltext225:nil];
    [self setLbltext240:nil];
    [self setLbltext270:nil];
    [self setLbltext300:nil];
    [self setLbltext315:nil];
    [self setLbltext330:nil];
    [self setTblSprinReturnExtra:nil];
    [self setLblPlateName:nil];
    [self setLblSwitchType:nil];
    [self setLblcustomerArticleNum:nil];
    [self setLblProgram:nil];
    [self setImgtblswitch:nil];
    [self setTblimgback:nil];
    [self setLblSwitchingAngle:nil];
    [self setLblTotalSwitchingangle:nil];

    [self setImgLogo:nil];
    [self setLblpage1:nil];
    [self setLblpage2:nil];
    [super viewDidUnload];
}


    
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lbllook.text=strlbllook;
    self.lblMounting.text=strlblMounting;
    self.lblEscutcheonPlate.text=strlblEscutcheonPlate;
    self.lblHandle.text=strlblHandle;
    self.lblLatchMech.text=strlblLatchMech;
    self.lblStop.text=strlblStop;
    self.lblStopdegree.text=strlblStopdegree;
    self.lblNoofStages.text=strlblNoofStages;
    self.lblMasterdata.text=strlblMasterdata;
    self.lblReference.text=strlblReference;
    self.lblDate.text=strlblDate;
    self.lblModifyDate.text=strlblModifyDate;
    self.lblCustNO.text=strlblCustNO;
    self.lblCompany.text=strlblCompany;
    self.lblVersion.text=strlblVersion;

    
    [self loadingViewscreen];
    KNESwitchShowViewController *gKNESwitchShowViewController=[[KNESwitchShowViewController alloc] initWithNibName:@"KNESwitchShowViewController" bundle:nil];
    
    UIToolbar *tool = [UIToolbar new];
    tool.frame = CGRectMake(0, 0, 160, 43); 
     
    UIImage *image = [[UIImage imageNamed:@"black_bar.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UIToolbar appearance] setBackgroundImage:image forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    //tool.frame = self.navigationController.navigationBar.frame;
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:2];
    
    /*
    btnprint  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnprint setImage:[UIImage imageNamed:@"btn_print_half.png"] forState:UIControlStateNormal];
    btnprint.frame = CGRectMake(0, 0, 62, 33);
    [btnprint addTarget:self action:@selector(printdochalf:) forControlEvents:UIControlEventTouchUpInside];
    btnPrint1 = [[UIBarButtonItem alloc] initWithCustomView: btnprint] ;
    [items addObject:btnPrint1];
    
    UIButton *btnprintfull  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnprintfull setImage:[UIImage imageNamed:@"btn_print_full.png"] forState:UIControlStateNormal];
    btnprintfull.frame = CGRectMake(0, 0, 62, 33);
    [btnprintfull addTarget:self action:@selector(printdocfull:) forControlEvents:UIControlEventTouchUpInside];
    btnprintfull1 = [[UIBarButtonItem alloc] initWithCustomView: btnprintfull] ;
    [items addObject:btnprintfull1];
    
    UIButton *rightButton1  =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton1 setImage:[UIImage imageNamed:@"btn_email_half.png"] forState:UIControlStateNormal];
    rightButton1.frame = CGRectMake(0, 0, 62, 33);
    [rightButton1 addTarget:self action:@selector(CaptureResultTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithCustomView: rightButton1] ;
    [items addObject:btn1];
    
    
    UIButton *rightButton  =[UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_email_full.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(CaptureResultTappedss2) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn2 = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    [items addObject:btn2];
    
    */
    
    optionButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [optionButton setImage:[UIImage imageNamed:@"btn_option.png"] forState:UIControlStateNormal];
    optionButton.frame = CGRectMake(0, 0, 62, 33);
    [optionButton addTarget:self action:@selector(popoverselected) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnoption = [[UIBarButtonItem alloc] initWithCustomView: optionButton];
    [items addObject:btnoption];
    
    closeButton =[UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setImage:[UIImage imageNamed:@"btn_close.png"] forState:UIControlStateNormal];
    closeButton.frame = CGRectMake(0, 0, 62, 33);
    [closeButton addTarget:self action:@selector(CloseEmailTapped) forControlEvents:UIControlEventTouchUpInside];
    
    //[closeButton addTarget:self action:@selector(CloseEmailTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btnclose = [[UIBarButtonItem alloc] initWithCustomView: closeButton];
    [items addObject:btnclose];
  
    /*================================================*/

    [tool setItems:items];
    tool.barStyle =UIBarStyleDefault;
    tool.backgroundColor = [UIColor clearColor];
    
    self.navigationItem.rightBarButtonItem =  [[UIBarButtonItem alloc] initWithCustomView:tool];
    
    gKNESwitchShowViewController.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView: tool];// [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    //[gKNESwitchShowViewController release];
    
    
    

    [self initwithtblSwitchrowcolor];
    [self initwithGraftrowcolor];
    
    stageno=0;

    if(gKNEShowSRViewController == nil)
    {
        KNEShowSRViewController *ggKNEShowSRViewController = [[KNEShowSRViewController alloc] init];
        self.gKNEShowSRViewController = ggKNEShowSRViewController;
        //self.gKNESpringReturnViewController.gKNESwitchShowViewController=self;
    }
    
    
    [tblSprinReturn setDataSource:self.gKNEShowSRViewController];
    [tblSprinReturnExtra setDataSource:self.gKNEShowSRViewController];
    //[tblSprinReturn setDelegate:self.gKNEShowSRViewController];
    
    
    self.gKNEShowSRViewController.view = self.gKNEShowSRViewController.tableView;
    //self.gBpEventTableViewController.view = self.gBpEventTableViewController.view;
    //tblSpringReturn.frame=CGRectMake(45, 310, 205, 648);
    tblSprinReturn.frame=CGRectMake(302, 253, 70, 550);
    tblSprinReturn.rowHeight=18;
    [tblSprinReturn reloadData];
    self.tblSprinReturn.separatorColor = [UIColor clearColor];
    tblSprinReturn.scrollEnabled=NO;
    tblSprinReturn.backgroundColor=[UIColor clearColor];
    [self.mainscrollview addSubview:tblSprinReturn];

    
    tblSprinReturnExtra.frame=CGRectMake(1187, 253, 50, 550);
    tblSprinReturnExtra.rowHeight=18;
    [tblSprinReturnExtra reloadData];
    tblSprinReturnExtra.scrollEnabled=NO;
    self.tblSprinReturnExtra.separatorColor = [UIColor clearColor];
    //[self.mainscrollview addSubview:tblSprinReturnExtra];
    /*=====================
    
    [self.lbl setFrame:CGRectMake(50, 53, 1138, 23)];
    self.lbl.layer.borderColor=[UIColor blackColor].CGColor;
    self.lbl.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.lbl ];
    
    [self.lblCA10 setFrame:CGRectMake(415, 31, 75, 23)];
    self.lblCA10.layer.borderColor=[UIColor blackColor].CGColor;
    self.lblCA10.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.lblCA10 ];
    
    [self.SGL719 setFrame:CGRectMake(490, 31, 698, 23)];
    self.SGL719.layer.borderColor=[UIColor blackColor].CGColor;
    self.SGL719.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.SGL719 ];
    
    [self.Labellbl setFrame:CGRectMake(415, 2, 773, 30)];
    self.Labellbl.layer.borderColor=[UIColor blackColor].CGColor;
    self.Labellbl.layer.borderWidth = 1;
    [self.mainscrollview addSubview:self.Labellbl ];
    ======*/
    
    /*===========================*/
    jumperView=[[UIView alloc] initWithFrame:CGRectMake(348, 75, 840, 180)];
    
    jumperview = [[KNEJShow alloc]initWithFrame:CGRectMake(0, 0, 840, 180)];
    jumperView.backgroundColor=[UIColor clearColor];
    jumperview.backgroundColor=[UIColor clearColor];
    [jumperView addSubview:jumperview];
    [self.mainscrollview addSubview:jumperView];
    
    [jumperview retrievejumper];
    
    
    [self.imgLogo setFrame:CGRectMake(self.view.frame.size.width+ 15, 3, 213, 49)];
    [self.lblpage1 setFrame:CGRectMake(670, 31, 98, 21)];
    [self.lblpage2 setFrame:CGRectMake(self.view.frame.size.width+590, 31, 98, 21)];
    
    
    self.mainscrollview.contentSize=CGSizeMake(1536, 960);
    
    [self performSelector:@selector(initwithGraft) withObject:nil afterDelay:0];
    
    [self initwithballleftwing];
    [self initwithballrightwing];
    [self initwithBallleftimage];
    [self initwithBallrightimage];
    
    //[self initwithGraft];
    [self getspringreturn];

    [self loadThirdPart];
    
}

-(void)popoverselected
{
    /*=====================pop over===========================*/
    UIViewController* popoverContent = [[UIViewController alloc] init];
    UIView *popoverView = [[UIView alloc] init];
    popoverView.backgroundColor = [UIColor blackColor];
    
    //UIToolbar *toolbar=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,400 ,44)];
    //toolbar.barStyle =UIBarStyleBlackTranslucent;
    NSMutableArray *ButtonArray=[[NSMutableArray alloc ]init];
    //Save=[[UIBarButtonItem alloc ]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(save_pressed)];
    UIBarButtonItem *space=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //UIBarButtonItem *cancel=[[UIBarButtonItem alloc ]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel_pressed)];
    
    //[ButtonArray addObject:cancel];
    
    [ButtonArray addObject:space];
    
    //[ButtonArray addObject:Save];
    
    //[toolbar setItems:ButtonArray];
    
    //[popoverView addSubview:toolbar];
    
    /*===========print================*/
    
    UILabel *lblPrint=[[UILabel alloc]initWithFrame:CGRectMake(15, 5, 80, 30)];
    lblPrint.text=@"Print";
    lblPrint.backgroundColor=[UIColor clearColor];
    lblPrint.textColor=[UIColor whiteColor];
    [popoverView addSubview:lblPrint];
    
    btnprint  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnprint setImage:[UIImage imageNamed:@"btn_print_pg1.png"] forState:UIControlStateNormal];
    btnprint.frame = CGRectMake(15, 45, 200, 45);
    [btnprint addTarget:self action:@selector(printdochalf:) forControlEvents:UIControlEventTouchUpInside];
    btnPrint1 = [[UIBarButtonItem alloc] initWithCustomView: btnprint] ;
    
    [popoverView addSubview:self.btnprint];
    
    btnprintSecondPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnprintSecondPage setImage:[UIImage imageNamed:@"btn_print_pg2.png"] forState:UIControlStateNormal];
    btnprintSecondPage.frame = CGRectMake(15, 100, 200, 45);
    [btnprintSecondPage addTarget:self action:@selector(printdocSecondPage:) forControlEvents:UIControlEventTouchUpInside];
    btnPrintSecondPage1 = [[UIBarButtonItem alloc] initWithCustomView: btnprintSecondPage] ;
    
    [popoverView addSubview:self.btnprintSecondPage];
    
     UIButton *btnprintfull  =[UIButton buttonWithType:UIButtonTypeCustom];
     [btnprintfull setImage:[UIImage imageNamed:@"btn_print_full.png"] forState:UIControlStateNormal];
     btnprintfull.frame = CGRectMake(15, 155, 200, 45);
     [btnprintfull addTarget:self action:@selector(printdocfull:) forControlEvents:UIControlEventTouchUpInside];
     btnprintfull1 = [[UIBarButtonItem alloc] initWithCustomView: btnprintfull] ;

    [popoverView addSubview:btnprintfull];
    
    /*============email===============*/
     
     UILabel *lblEmail=[[UILabel alloc]initWithFrame:CGRectMake(15, 210, 80, 30)];
     lblEmail.text=@"Email";
     lblEmail.backgroundColor=[UIColor clearColor];
     lblEmail.textColor=[UIColor whiteColor];
     [popoverView addSubview:lblEmail];
    
     UIButton *rightButton1  =[UIButton buttonWithType:UIButtonTypeCustom];
     [rightButton1 setImage:[UIImage imageNamed:@"btn_email_pg1.png"] forState:UIControlStateNormal];
     rightButton1.frame = CGRectMake(15, 250, 200, 45);
     [rightButton1 addTarget:self action:@selector(CaptureResultTapped) forControlEvents:UIControlEventTouchUpInside];
     //UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithCustomView: rightButton1] ;
     [popoverView addSubview:rightButton1];
    
    //CapturesecondPageResultTapped
    UIButton *emailSecondPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [emailSecondPage setImage:[UIImage imageNamed:@"btn_email_pg2.png"] forState:UIControlStateNormal];
    emailSecondPage.frame = CGRectMake(15, 305, 200, 45);
    [emailSecondPage addTarget:self action:@selector(CapturesecondPageResultTapped) forControlEvents:UIControlEventTouchUpInside];
    //UIBarButtonItem *btn1 = [[UIBarButtonItem alloc] initWithCustomView: rightButton1] ;
    [popoverView addSubview:emailSecondPage];
    
    
     UIButton *rightButton  =[UIButton buttonWithType:UIButtonTypeCustom];
     [rightButton setImage:[UIImage imageNamed:@"btn_email_full.png"] forState:UIControlStateNormal];
     rightButton.frame = CGRectMake(15, 360, 200, 45);
     [rightButton addTarget:self action:@selector(CaptureResultTappedss2) forControlEvents:UIControlEventTouchUpInside];
     //UIBarButtonItem *btn2 = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
     [popoverView addSubview:rightButton];
    
    /*===========save================*/
    
    UILabel *lblSave=[[UILabel alloc]initWithFrame:CGRectMake(15, 415, 150, 30)];
    lblSave.text=@"Save as image";
    lblSave.backgroundColor=[UIColor clearColor];
    lblSave.textColor=[UIColor whiteColor];
    [popoverView addSubview:lblSave];
    
    UIButton *btnsavefirstPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsavefirstPage setImage:[UIImage imageNamed:@"btn_save_pg1.png"] forState:UIControlStateNormal];
    btnsavefirstPage.frame = CGRectMake(15, 455, 200, 45);
    [btnsavefirstPage addTarget:self action:@selector(saveimagefirstPage) forControlEvents:UIControlEventTouchUpInside];
    [popoverView addSubview:btnsavefirstPage];
    
    
    UIButton *btnsavesecondPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsavesecondPage setImage:[UIImage imageNamed:@"btn_save_pg2.png"] forState:UIControlStateNormal];
    btnsavesecondPage.frame = CGRectMake(15, 510, 200, 45);
    [btnsavesecondPage addTarget:self action:@selector(saveimagesecondPage) forControlEvents:UIControlEventTouchUpInside];
    [popoverView addSubview:btnsavesecondPage];
    
    
    UIButton *btnsaveFullPage  =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnsaveFullPage setImage:[UIImage imageNamed:@"btn_save_full.png"] forState:UIControlStateNormal];
    btnsaveFullPage.frame = CGRectMake(15, 565, 200, 45);
    [btnsaveFullPage addTarget:self action:@selector(saveimagefullPage) forControlEvents:UIControlEventTouchUpInside];
    
    [popoverView addSubview:btnsaveFullPage];
    
     /*===========================*/
    
    popoverContent.view = popoverView;
    popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    [popoverController setPopoverContentSize:CGSizeMake(230, 640) animated:NO];
    [popoverController presentPopoverFromRect:CGRectMake(635, 0, 10, 1) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionUp animated:YES];

}

-(void)savedImageAlert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Image has been saved in your photo gallery" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];
}

-(void)saveimagefirstPage
{
    [self loadingViewscreen];
    UIImage* image = nil;
    //self.view.frame.size  mainscrollview.contentSize
    UIGraphicsBeginImageContext(self.view.frame.size );
    {
        
        /**/
        CGPoint savedContentOffset =CGPointMake(0, mainscrollview.contentSize.height);// mainscrollview.contentOffset;
        CGRect savedFrame = self.view.frame;
        
        
        mainscrollview.contentOffset =CGPointMake(0, mainscrollview.contentSize.height);//CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;
        
    }
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    [self savedImageAlert];
}

-(void)saveimagesecondPage
{
    [self loadingViewscreen];
    UIImage* image = nil;
    UIGraphicsBeginImageContext(self.view.frame.size );
    {
        
        [mainscrollview setFrame:CGRectMake(-768, 0, 2*768, mainscrollview.contentSize.height)];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        image = UIGraphicsGetImageFromCurrentImageContext();

    }
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [mainscrollview setFrame:CGRectMake(0, 0, 768, mainscrollview.contentSize.height)];
    
    [self savedImageAlert];
}

-(void)saveimagefullPage
{
    [self loadingViewscreen];
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(mainscrollview.contentSize );
    {
        CGPoint savedContentOffset = mainscrollview.contentOffset;
        CGRect savedFrame = mainscrollview.frame;
        
        mainscrollview.contentOffset = CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 2*768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    [self savedImageAlert];
}

-(void)loadingViewscreen
{

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    NSLog(@"warning !!!");
}


/*================print image=============================*/

-(IBAction)printdochalf:(id)sender
{
    
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(self.view.frame.size );
    {
        CGPoint savedContentOffset = mainscrollview.contentOffset;
        CGRect savedFrame = mainscrollview.frame;
        
        mainscrollview.contentOffset = CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil)
    {
        imageData = UIImagePNGRepresentation(image);
        [self printimage:imageData];
        
    }
    
}

-(IBAction)printdocSecondPage:(id)sender
{
    [self loadingViewscreen];
    
    UIImage* image = nil;
    UIGraphicsBeginImageContext(self.view.frame.size );
    {
        
        [mainscrollview setFrame:CGRectMake(-768, 0, 2*768, mainscrollview.contentSize.height)];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        
    }
    UIGraphicsEndImageContext();
    
    if (image != nil)
    {
        imageData = UIImagePNGRepresentation(image);
        [self printimageSecondPage:imageData];
         [mainscrollview setFrame:CGRectMake(0, 0, 768, mainscrollview.contentSize.height)];
    }
    
}

-(IBAction)printdocfull:(id)sender
{
    
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(mainscrollview.contentSize );
    {
        CGPoint savedContentOffset = mainscrollview.contentOffset;
        CGRect savedFrame = mainscrollview.frame;
        
        mainscrollview.contentOffset = CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 2*768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil)
    {
        imageData = UIImagePNGRepresentation(image);
        [self printimagefull:imageData];
        
    }
    
}

-(void)printimage:(NSData*)image
{
    pic = [UIPrintInteractionController sharedPrintController];
    
    
    if(pic && [UIPrintInteractionController canPrintData: image] ) {
        
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        //printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = imageData;
        
        
    }
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        //self.content = nil;
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    [pic presentFromBarButtonItem:self.btnPrint1 animated:YES completionHandler:completionHandler];
}

-(void)printimageSecondPage:(NSData*)image
{
    pic = [UIPrintInteractionController sharedPrintController];
    
    
    if(pic && [UIPrintInteractionController canPrintData: image] ) {
        
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        //printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = imageData;
        
        
    }
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        //self.content = nil;
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    [pic presentFromBarButtonItem:self.btnPrintSecondPage1 animated:YES completionHandler:completionHandler];
}

-(void)printimagefull:(NSData*)image
{
    pic = [UIPrintInteractionController sharedPrintController];
    
    
    if(pic && [UIPrintInteractionController canPrintData: image] ) {
        
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        //printInfo.jobName = [path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = imageData;
        
        
    }
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        //self.content = nil;
        if (!completed && error) {
            NSLog(@"FAILED! due to error in domain %@ with error code %u", error.domain, error.code);
        }
    };
    
    [pic presentFromBarButtonItem:self.btnprintfull1 animated:YES completionHandler:completionHandler];
}

- (BOOL)presentFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated completionHandler:(UIPrintInteractionCompletionHandler)completion {
    return YES;
}

- (BOOL)presentFromBarButtonItem:(UIBarButtonItem *)item animated:(BOOL)animated completionHandler:(UIPrintInteractionCompletionHandler)completion {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];

}

/*====================capture email==============================*/
-(void)CloseEmailTapped
{
    KNEAppDelegate *appDelegateSave = [UIApplication sharedApplication].delegate;
    [appDelegateSave clearfirstPnum];
    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex:0] animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)CaptureResultTapped
{
    [self loadingViewscreen];
    [self performSelector:@selector(loadCaptureResultTapped) withObject:nil afterDelay:1];
    
}

-(void)CapturesecondPageResultTapped
{
    [self loadingViewscreen];
    [self performSelector:@selector(capturesecondPage) withObject:nil afterDelay:1];
    
}
-(void)capturesecondPage
{
    UIImage* image = nil;
    UIGraphicsBeginImageContext(self.view.frame.size );
    {
        
        [mainscrollview setFrame:CGRectMake(-768, 0, 2*768, mainscrollview.contentSize.height)];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
        UIGraphicsBeginImageContext(self.view.frame.size);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        
    }
    UIGraphicsEndImageContext();
    
    
    if (image != nil) {
        
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
        NSData *imageData = UIImagePNGRepresentation(image);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:path atomically:YES];
        
        
        if ([MFMailComposeViewController canSendMail])
        {
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            NSString *mailsubject=[[NSString alloc]initWithFormat:@""];
            [mailer setSubject:mailsubject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            //NSData *myData = UIImagePNGRepresentation(image);
            //[mailer addAttachmentData:myData mimeType:@"image/png" fileName:@"coolImage.png"];
            [mailer addAttachmentData:imageData mimeType:@"image/png"   fileName:@"SwitchImage.png"];
            
            NSString *emailBody = [[NSString alloc]initWithFormat:@""];
            [mailer setMessageBody:emailBody isHTML:NO];
            
            // only for iPad
            // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
        
        
        [mainscrollview setFrame:CGRectMake(0, 0, 768, mainscrollview.contentSize.height)];
    }
    //[self CaptureResultTappedss2];
    //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)loadCaptureResultTapped
{
     UIImage* image = nil;
    //self.view.frame.size  mainscrollview.contentSize
    UIGraphicsBeginImageContext(self.view.frame.size );
    {

        /**/
        CGPoint savedContentOffset =CGPointMake(0, mainscrollview.contentSize.height);// mainscrollview.contentOffset;
        CGRect savedFrame = self.view.frame;
        
        
        mainscrollview.contentOffset =CGPointMake(0, mainscrollview.contentSize.height);//CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;

    }
    UIGraphicsEndImageContext();
    
    
    if (image != nil) {
        
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
        NSData *imageData = UIImagePNGRepresentation(image);
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:path atomically:YES];
        
        
        if ([MFMailComposeViewController canSendMail])
        {
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            NSString *mailsubject=[[NSString alloc]initWithFormat:@""];
            [mailer setSubject:mailsubject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            //NSData *myData = UIImagePNGRepresentation(image);
            //[mailer addAttachmentData:myData mimeType:@"image/png" fileName:@"coolImage.png"];
            [mailer addAttachmentData:imageData mimeType:@"image/png"   fileName:@"SwitchImage.png"];
            
            NSString *emailBody = [[NSString alloc]initWithFormat:@""];
            [mailer setMessageBody:emailBody isHTML:NO];
            
            // only for iPad
            // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
        
        
        
    }
    //[self CaptureResultTappedss2];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

     /**/
}

-(void)CaptureResultTappedss2
{
    [self loadingViewscreen];
    [self performSelector:@selector(loadCaptureResultTappedss2) withObject:nil afterDelay:1];
    
}

-(void)loadCaptureResultTappedss2
{
    
    UIImage* image = nil;
    
    UIGraphicsBeginImageContext(mainscrollview.contentSize );
    {
        CGPoint savedContentOffset = mainscrollview.contentOffset;
        CGRect savedFrame = mainscrollview.frame;
        
        mainscrollview.contentOffset = CGPointZero;
        mainscrollview.frame = CGRectMake(0, 0, 2*768, mainscrollview.contentSize.height);
        
        [mainscrollview.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        mainscrollview.contentOffset = savedContentOffset;
        mainscrollview.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    
    if (image != nil) {
        
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        NSData *imageData = UIImagePNGRepresentation(image);

        
        
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            NSString *mailsubject=[[NSString alloc]initWithFormat:@""];
            [mailer setSubject:mailsubject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            //NSData *myData = UIImagePNGRepresentation(image);
            //[mailer addAttachmentData:myData mimeType:@"image/png" fileName:@"coolImage.png"];
            [mailer addAttachmentData:imageData mimeType:@"image/png"   fileName:@"SwitchImage.png"];
            
            NSString *emailBody = [[NSString alloc]initWithFormat:@""];
            [mailer setMessageBody:emailBody isHTML:NO];
            
            // only for iPad
            // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
            
            [self presentModalViewController:mailer animated:YES];

        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
        }
        
    }

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
}

#pragma mark - MFMailComposeController delegate


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

-(void)CaptureResultTapped2
{
    if(UIGraphicsBeginImageContextWithOptions != NULL)
    {
        UIGraphicsBeginImageContextWithOptions(mainscrollview.frame.size, NO, 0.0);
        NSLog(@"1");
    } else {
        UIGraphicsBeginImageContext(mainscrollview.frame.size);
        NSLog(@"2");
    }
    
    [mainscrollview.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    [self secondPageCaptureResult];
}

-(void)secondPageCaptureResult
{
    NSLog(@"2nd");
    //self.mainscrollview.frame=CGRectMake(-768, 0, 768, 1076);
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    //UIGraphicsBeginImageContextWithOptions(mainscrollview.frame.size, NO, 0.0);
    //UIGraphicsBeginImageContextWithOptions(<#CGSize size#>, <#BOOL opaque#>, <#CGFloat scale#>)
    
    CGContextRef resizedContext = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(resizedContext, -768, 0);
    //CGContextTranslateCTM(<#CGContextRef c#>, <#CGFloat tx#>, <#CGFloat ty#>)
    [mainscrollview.layer renderInContext:resizedContext];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    //UIImageWriteToSavedPhotosAlbum(<#UIImage *image#>, <#id completionTarget#>, <#SEL completionSelector#>, <#void *contextInfo#>)
}


/*=================================================contact table============================================================*/

-(void)initwithGraft
{
    int widthOffset = 0;
    int yOffset=24;
    int xOffset=0;
    for(int i=0;i<576;i++)
    {
        if(i!=0 && (i%24)==0)
        {
            yOffset+=18;
            xOffset=0;
        }
        
        /*===============column 1=====
        
        UIButton *gbtnsaveEvent = [[UIButton alloc] initWithFrame:CGRectMake(348+widthOffset*xOffset, yOffset+ 160, 35, 21)];
        gbtnsaveEvent.layer.borderColor=[UIColor blackColor].CGColor;
        gbtnsaveEvent.layer.borderWidth = 1;
        gbtnsaveEvent.tag=i+1;
        
        [gbtnsaveEvent addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
        [gbtnsaveEvent addTarget:self action:@selector(touchDownRepeat:) forControlEvents:UIControlEventTouchDownRepeat];
        
        
        [self.mainscrollview addSubview:gbtnsaveEvent];
        [gbtnsaveEvent release];
         
         =================*/
        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(348+widthOffset*xOffset, yOffset+ 229, 35, 18)];
        //glblgraftNum1.layer.borderColor=[UIColor blackColor].CGColor;
        //glblgraftNum1.layer.borderWidth = 1;
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 3000+1;
        [self.mainscrollview addSubview:glblgraftNum1];
        
        UILabel *glblvalueposition = [[UILabel alloc] initWithFrame:CGRectMake(251 +widthOffset*xOffset, yOffset+ 287, 45, 27)];
        glblvalueposition.textAlignment = UITextAlignmentCenter;
        glblvalueposition.textColor = [UIColor clearColor];
        glblvalueposition.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblvalueposition.backgroundColor = [UIColor clearColor];
        glblvalueposition.tag=i + 9000+1;
        [self.mainscrollview addSubview:glblvalueposition];
        
        
        
        UIImageView *gimgpicture0= [[UIImageView alloc]initWithFrame:CGRectMake(357+widthOffset*xOffset, yOffset+ 229, 18, 18)];
        gimgpicture0.tag=i+ 2000+1;
        [self.mainscrollview addSubview:gimgpicture0];
        
        UIImageView *gimgpictureoverdwn= [[UIImageView alloc]initWithFrame:CGRectMake(357+widthOffset*xOffset, yOffset+ 229, 18, 6)];
        gimgpictureoverdwn.tag=i+ 50000+1;
        gimgpictureoverdwn.hidden=YES;
        gimgpictureoverdwn.image=[UIImage imageNamed:@"n_lite.png"];
        [self.mainscrollview addSubview:gimgpictureoverdwn];
        
        UIImageView *gimgpictureoverup= [[UIImageView alloc]initWithFrame:CGRectMake(357+widthOffset*xOffset, yOffset+ 242, 18, 6)];
        gimgpictureoverup.tag=i+ 40000+1;
        gimgpictureoverup.hidden=YES;
        gimgpictureoverup.image=[UIImage imageNamed:@"u_lite.png"];
        [self.mainscrollview addSubview:gimgpictureoverup];
        
        
        
        widthOffset=35;
        xOffset++;

    }
    
    self.imgtblcontact.frame=CGRectMake(347, 253, 841, 433);
    [self.mainscrollview addSubview:self.imgtblcontact];
    self.imgtblcontact.backgroundColor=[UIColor clearColor];
    [self loadcontact];
    
    
    
}

-(void)initwithGraftrowcolor
{
    int widthOffset = 0;
    int yOffset=24;
    int xOffset=0;
    
    for(int i = 0; i < 576; i++)
    {
        if(i != 0 && (i%24) == 0)
        {
            yOffset += 18;
            xOffset = 0;
        }
        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(348+widthOffset*xOffset, yOffset+ 229, 35, 18)];
        //glblgraftNum1.layer.borderColor=[UIColor blackColor].CGColor;
        //glblgraftNum1.layer.borderWidth = 1;
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 8000+1;
        
        
        [self.mainscrollview addSubview:glblgraftNum1];
        
        widthOffset=35;
        xOffset++;
    }
    
}

-(void)initwithtblSwitchrowcolor
{
    int widthOffset = 0;
    int yOffset=1;
    int xOffset=0;
    
    for(int i = 0; i < 24; i++)
    {
        if(i != 0 && (i%1) == 0)
        {
            yOffset += 18;
            xOffset = 0;
        }
        
        UILabel *glblgraftNum1 = [[UILabel alloc] initWithFrame:CGRectMake(304 +widthOffset*xOffset, yOffset+ 251, 45, 18)];
        glblgraftNum1.textAlignment = UITextAlignmentCenter;
        glblgraftNum1.textColor = [UIColor blackColor];
        glblgraftNum1.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblgraftNum1.backgroundColor = [UIColor clearColor];
        glblgraftNum1.tag=i + 7000+1;
        
        [self.mainscrollview addSubview:glblgraftNum1];
        
        widthOffset=35;
        xOffset++;
    }
}

-(void)loadcontact
{
    DBTableGraft *gDBTableGraft=[[DBTableGraft alloc]init ];
   
    NSArray *contactArr =[gDBTableGraft selectItem:KNECaseclass];
    //NSLog(@"contactArr------>%@",contactArr);
    
    for (KNETblGraftPositionTag *teim in contactArr ) {
        //NSLog(@"imgGbtnTag--->%@",teim.imgGbtnTag);
        //NSLog(@"imgGbtnValueTag>>>>>%@",teim.imgGbtnValueTag);
        //NSLog(@"CaseID~%@",teim.CaseID);
        //NSLog(@"CaseID~---------------------------------------");
        
        /*
        for(int i = 0 ; i < self.mainscrollview.subviews.count; i++)
        {
            
        }*/
            if (![teim.imgGbtnValueTag isEqualToString:@"0"] && [teim.Vposition isEqualToString:@"1"])
            {
                
                
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+2000];
                [imageView setBackgroundColor:[UIColor clearColor]];
                
                UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 40000];
                imageViewover.hidden=NO;
                
                UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 3000];
                lblgraftNum.text=teim.imgGbtnValueTag;
                
                UILabel *lblgraftvalueposition=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue] + 9000];
                lblgraftvalueposition.text=teim.Vposition;
                
                
                
            }else if (![teim.imgGbtnValueTag isEqualToString:@"0"] && [teim.Vposition isEqualToString:@"2"])
            {
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 2000];
                [imageView setBackgroundColor:[UIColor clearColor]];
                
                UIImageView *imageViewover=(UIImageView *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 50000];
                imageViewover.hidden=NO;
                
                UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 3000];
                lblgraftNum.text=teim.imgGbtnValueTag;
                
                UILabel *lblgraftvalueposition=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue] + 9000];
                lblgraftvalueposition.text=teim.Vposition;
                
            }else
            {
                UIImageView *imageView=(UIImageView *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 2000];
                [imageView setBackgroundColor:[UIColor blackColor]];
                
                UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue]+ 3000];
                lblgraftNum.text=teim.imgGbtnValueTag;
                
                UILabel *lblgraftvalueposition=(UILabel *)[self.mainscrollview viewWithTag:[teim.imgGbtnTag intValue] + 9000];
                lblgraftvalueposition.text=teim.Vposition;
            }
            
        
            [self contactjumperTapped:[teim.imgGbtnTag intValue]];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

/*=================================================Spring Return============================================================*/

-(void)getspringreturn
{
    
    int widthOffset = 0;
    int yOffset=0;
    int xOffset=50;
    for(int i=0;i<24;i++)
    {
        if(i!=0 && (i%1)==0)
        {
            yOffset+=18;
            xOffset=0;
        }
        
        /*===============row 1======================*/
        
        
        UIButton *gbtnsaveEvent = [[UIButton alloc] initWithFrame:CGRectMake(30+20+widthOffset*xOffset, yOffset+ 253, 35, 18)];
        //gbtnsaveEvent.layer.borderColor=[UIColor blackColor].CGColor;
        //gbtnsaveEvent.layer.borderWidth = 1;
        gbtnsaveEvent.tag=i+1+100000000;
        [self.mainscrollview addSubview:gbtnsaveEvent];
        
        /*
        UITextField *glblBodytext = [[UITextField alloc] initWithFrame:CGRectMake(30+54+widthOffset*xOffset, yOffset+ 253, 220, 18)];//294
        glblBodytext.textAlignment = UITextAlignmentCenter;
        glblBodytext.layer.borderColor=[UIColor blackColor].CGColor;
        glblBodytext.layer.borderWidth = 1;
        glblBodytext.textColor = [UIColor blackColor];
        //glblBodytext.text=@"hello~";
        glblBodytext.tag=i+ 300000000 +1;
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytext.backgroundColor = [UIColor clearColor];
        [self.mainscrollview addSubview:glblBodytext];
        [glblBodytext release];
        */
        
        UILabel *glblBodytext = [[UILabel alloc] initWithFrame:CGRectMake(30+54+widthOffset*xOffset, yOffset+ 253, 220, 18)];//294
        glblBodytext.textAlignment = UITextAlignmentCenter;
        glblBodytext.textColor = [UIColor blackColor];
        glblBodytext.tag=i+ 300000000 +1;
        glblBodytext.font = [UIFont fontWithName:@"Helvetica" size:12];
        UIColor *color = [UIColor colorWithRed:230.0/255.0 green:221.0/255.0 blue:159.0/255.0 alpha:1.0];
        glblBodytext.backgroundColor = color;
        //glblBodytext.backgroundColor =[UIColor colorWithRed:230.0 green:221.0 blue:159.0 alpha:0.9];
        [self.mainscrollview addSubview:glblBodytext];
        
        UILabel *glblBodytextextra = [[UILabel alloc] initWithFrame:CGRectMake(1179+54+widthOffset*xOffset, yOffset+ 253, 220, 18)];//294
        glblBodytextextra.textAlignment = UITextAlignmentCenter;
        glblBodytextextra.layer.borderColor=[UIColor blackColor].CGColor;
        glblBodytextextra.layer.borderWidth = 1;
        glblBodytextextra.textColor = [UIColor blackColor];
        glblBodytextextra.tag=i+ 900000000 +1;
        glblBodytextextra.font = [UIFont fontWithName:@"Helvetica" size:12];
        glblBodytextextra.backgroundColor = [UIColor clearColor];
        [self.mainscrollview addSubview:glblBodytextextra];
        
        UIImageView *imgarrowTextvalue= [[UIImageView alloc]initWithFrame:CGRectMake(30+14+18+widthOffset*xOffset, yOffset+ 260, 23, 3)];
        imgarrowTextvalue.tag=i+ 200000000 + 1;
        [self.mainscrollview addSubview:imgarrowTextvalue];
        
        UIImageView *imgarrowStart= [[UIImageView alloc]initWithFrame:CGRectMake(30+14+18+widthOffset*xOffset, yOffset+ 260, 23, 3)];
        imgarrowStart.tag=i+ 400000000 + 1;
        [self.mainscrollview addSubview:imgarrowStart];
        
        UIImageView *imgarrowMid= [[UIImageView alloc]init ];
        [imgarrowMid setFrame:CGRectMake(30+14+18+widthOffset*xOffset, yOffset+ 260, 4, 21)];
        imgarrowMid.tag=i+ 600000000 + 1;
        [self.mainscrollview addSubview:imgarrowMid];

        UIImageView *imgarrowEndup= [[UIImageView alloc]initWithFrame:CGRectMake(30+14+18+widthOffset*xOffset, yOffset+ 258, 23, 7)];
        imgarrowEndup.tag=i+ 500000000+1;
        [self.mainscrollview addSubview:imgarrowEndup];
        
        UIImageView *imgarrowEnddwn= [[UIImageView alloc]initWithFrame:CGRectMake(30+14+18+widthOffset*xOffset, yOffset+ 258, 23, 7)];
        imgarrowEnddwn.tag=i+ 800000000+1;
        [self.mainscrollview addSubview:imgarrowEnddwn];
        
        widthOffset=35;
        xOffset++;
        
        
    }
    
    self.imgtblspringReturn.frame=CGRectMake(50, 253, 254, 433);
    [self.mainscrollview addSubview:self.imgtblspringReturn];
    
    [self loadSpringReturnContain];
    
    
} 

-(void)loadSpringReturnContain 
{
    DBSpringReturnContain *gDBSpringReturnContain=[[DBSpringReturnContain alloc] init];
    
    NSArray *SRContainArr = [gDBSpringReturnContain selectSRContainItem:KNECaseclass];
    
    for (KNESpringReturnContain *SRContainitem in SRContainArr)
    {
        UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:[SRContainitem.ContainTag intValue]];
        glbltextvalue.text=SRContainitem.ContainValue;
        
        UITextField *glbltextvalueextra=(UITextField *)[self.mainscrollview viewWithTag:[SRContainitem.ContainTag intValue]+600000000];
        glbltextvalueextra.text=SRContainitem.ContainValue;
        
        if (glbltextvalue.text !=nil && glbltextvalue.text.length !=0)
        {
            //lblTotalSwitchingangle.text=strSwitchingAngle;
            
            [self setrowcolor:glbltextvalue.tag];
            
        }else
        {
            //NSLog(@"textboX~~nil!~~~~");
            /**/
            UIColor *color = [UIColor colorWithRed:230.0/255.0 green:221.0/255.0 blue:159.0/255.0 alpha:1.0];
            
            glbltextvalue.backgroundColor = color;
            if (glbltextvalue.tag==300000000+1)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 1];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+2)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 2];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+24];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+3)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 3];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*2)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+4)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 4];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*3)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+5)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 5];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*4)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+6)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 6];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*5)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+7)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 7];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*6)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+8)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 8];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*7)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+9)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 9];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*8)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+10)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 10];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*9)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+11)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 11];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*10)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+12)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 12];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*11)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+13)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 13];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*12)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+14)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 14];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*13)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+15)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 15];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*14)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+16)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 16];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*15)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+17)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 17];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*16)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+18)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 18];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*17)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+19)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 19];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*18)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+20)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 20];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*19)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+21)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 21];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*20)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+22)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 22];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*21)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+23)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 23];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*22)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }else if (glbltextvalue.tag==300000000+24)
            {
                //glbltextvalue.backgroundColor = color;
                UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 24];
                lbltblrow.backgroundColor = [UIColor clearColor];
                
                for (int i=0; i<=24; i++) {
                    
                    UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i +1+ (24*23)];
                    lblgraftNumdwn.backgroundColor = [UIColor clearColor];
                    
                }
            }
            
        }

    }
    [self loadSpringReturn];
}

-(void)loadSpringReturn
{
    DBSpringReturn *gDBSpringReturn=[[DBSpringReturn alloc] init];
    NSArray *SRArr=[gDBSpringReturn selectItem:KNECaseclass];
    
    for (KNESpringRPosition *itemSpringreturn in SRArr)
    {
        int btnStart=[itemSpringreturn.SRStartPoint intValue];
        int btnEnd=[itemSpringreturn.SREndPoint intValue];
        int indexspringreturn;
        
        if (btnStart< btnEnd)
        {
            UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 300000000];
            [imageViewstart setBackgroundColor:[UIColor blackColor]];

            UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:btnEnd+ 400000000];
            [imageViewEnd setImage:[UIImage imageNamed:@"arrow.png"]];
            
            for (indexspringreturn=btnStart; indexspringreturn<btnEnd; indexspringreturn++) {
                UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                [imageViewmid setBackgroundColor:[UIColor blackColor]];
                
                UITextField *glblBodytextvalue=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 200000000];
                NSString *textvalue=glblBodytextvalue.text;
                UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                
                 
                if (textvalue != nil && textvalue.length !=0) {
                    
                    [TextimageViewstart setBackgroundColor:[UIColor blackColor]];
                    
                }else
                {
                    [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                }
            }
            
            
        }else
        {
            UIImageView *imageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:btnStart+ 300000000];
            [imageViewstart setBackgroundColor:[UIColor blackColor]];
            
            UIImageView *imageViewEnd=(UIImageView *)[self.mainscrollview viewWithTag:btnEnd+ 700000000];
            [imageViewEnd setImage:[UIImage imageNamed:@"arrow.png"]];
            
            for (indexspringreturn=btnEnd; indexspringreturn<btnStart; indexspringreturn++) {
                UIImageView *imageViewmid=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 500000000];
                [imageViewmid setBackgroundColor:[UIColor blackColor]];
                
                UITextField *glblBodytextvalue=(UITextField *)[self.mainscrollview viewWithTag:indexspringreturn+ 200000000];
                NSString *textvalue=glblBodytextvalue.text;
                UIImageView *TextimageViewstart=(UIImageView *)[self.mainscrollview viewWithTag:indexspringreturn+ 100000000];
                
                
                if (textvalue != nil && textvalue.length !=0) {
                    
                    [TextimageViewstart setBackgroundColor:[UIColor blackColor]];
                    
                }else
                {
                    [TextimageViewstart setBackgroundColor:[UIColor clearColor]];
                }
            }
            
        }
        
        
    }
    
    [self reloadtblSpringReturn];
}

-(void)setrowcolor:(int)rowcolortag
{
    UITextField *glbltextvalue=(UITextField *)[self.mainscrollview viewWithTag:rowcolortag];
    UIColor *color = [UIColor colorWithRed:199/255.0 green:199/255.0 blue:199/255.0 alpha:1.0];
    glbltextvalue.backgroundColor = color;
    
    if (glbltextvalue.tag==300000000+1)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 1];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<=24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+2)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 2];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ 24];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+3)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 3];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*2)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+4)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 4];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*3)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+5)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 5];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*4)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+6)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 6];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*5)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+7)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 7];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*6)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+8)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 8];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*7)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+9)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 9];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*8)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+10)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 10];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*9)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+11)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 11];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*10)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+12)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 12];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*11)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+13)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 13];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*12)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+14)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 14];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*13)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+15)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 15];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*14)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+16)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 16];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*15)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+17)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 17];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*16)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+18)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 18];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*17)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+19)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 19];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*18)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+20)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 20];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*19)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+21)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 21];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*20)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+22)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 22];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*21)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+23)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 23];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*22)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }else if (glbltextvalue.tag==300000000+24)
    {
        UILabel *lbltblrow=(UILabel *)[self.mainscrollview viewWithTag:7000+ 24];
        lbltblrow.backgroundColor = color;
        
        for (int i=0; i<24; i++) {
            
            UILabel *lblgraftNumdwn=(UILabel *)[self.mainscrollview viewWithTag:8000+ i+1+ (24*23)];
            lblgraftNumdwn.backgroundColor = color;
            
        }
    }
    
}

-(void)reloadtblSpringReturn
{
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
 
    [self.gKNEShowSRViewController getfirstpNum];
    [self.tblSprinReturn reloadData];

    if ([appDelegate.firstPnum intValue]==0)
    {
        UILabel *glblBodytextvalue1=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext0.text=glblBodytextvalue1.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext330.text=glblBodytextvalue23.text;
        
        
        
        /**/
        
        
    }else if ([appDelegate.firstPnum intValue]==15)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==30)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==45)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==60)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==75)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:30000005];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==90)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==105)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==120)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==135)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==150)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==165)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==180)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==195)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==210)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==225)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==240)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==255)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==270)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==285)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:30000015];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==300)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==315)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==330)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000003];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000007];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000010];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000013];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000015];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000019];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000021];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000001];
        lbltext330.text=glblBodytextvalue23.text;
        
    }else if ([appDelegate.firstPnum intValue]==345)
    {
        UILabel *glblBodytextvalue24=(UILabel *)[self.mainscrollview viewWithTag:300000002];
        lbltext0.text=glblBodytextvalue24.text;
        
        UILabel *glblBodytextvalue2=(UILabel *)[self.mainscrollview viewWithTag:300000004];
        lbltext30.text=glblBodytextvalue2.text;
        
        UILabel *glblBodytextvalue3=(UILabel *)[self.mainscrollview viewWithTag:300000005];
        lbltext45.text=glblBodytextvalue3.text;
        
        UILabel *glblBodytextvalue4=(UILabel *)[self.mainscrollview viewWithTag:300000006];
        lbltext60.text=glblBodytextvalue4.text;
        
        UILabel *glblBodytextvalue7=(UILabel *)[self.mainscrollview viewWithTag:300000008];
        lbltext90.text=glblBodytextvalue7.text;
        
        UILabel *glblBodytextvalue9=(UILabel *)[self.mainscrollview viewWithTag:300000009];
        lbltext120.text=glblBodytextvalue9.text;
        
        UILabel *glblBodytextvalue10=(UILabel *)[self.mainscrollview viewWithTag:300000011];
        lbltext135.text=glblBodytextvalue10.text;
        
        UILabel *glblBodytextvalue11=(UILabel *)[self.mainscrollview viewWithTag:300000012];
        lbltext150.text=glblBodytextvalue11.text;
        
        UILabel *glblBodytextvalue13=(UILabel *)[self.mainscrollview viewWithTag:300000014];
        lbltext180.text=glblBodytextvalue13.text;
        
        UILabel *glblBodytextvalue15=(UILabel *)[self.mainscrollview viewWithTag:300000016];
        lbltext210.text=glblBodytextvalue15.text;
        
        UILabel *glblBodytextvalue16=(UILabel *)[self.mainscrollview viewWithTag:300000017];
        lbltext225.text=glblBodytextvalue16.text;
        
        UILabel *glblBodytextvalue17=(UILabel *)[self.mainscrollview viewWithTag:300000018];
        lbltext240.text=glblBodytextvalue17.text;
        
        UILabel *glblBodytextvalue19=(UILabel *)[self.mainscrollview viewWithTag:300000020];
        lbltext270.text=glblBodytextvalue19.text;
        
        UILabel *glblBodytextvalue21=(UILabel *)[self.mainscrollview viewWithTag:300000022];
        lbltext300.text=glblBodytextvalue21.text;
        
        UILabel *glblBodytextvalue22=(UILabel *)[self.mainscrollview viewWithTag:300000023];
        lbltext315.text=glblBodytextvalue22.text;
        
        UILabel *glblBodytextvalue23=(UILabel *)[self.mainscrollview viewWithTag:300000024];
        lbltext330.text=glblBodytextvalue23.text;
        
    }
}
/*=======================================jumper image==============================================*/
-(void)initwithBallleftimage
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(356 +widthOffset*xOffset, yOffset+ 128, 55, 55)];
        gimgpictureball1.tag=i+ 10000+1;
        gimgpictureball1.image=[UIImage imageNamed:@"circle_2.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=70;
        xOffset++;
        
        
    }
}

-(void)initwithballleftwing
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(351+widthOffset*xOffset, yOffset+ 125, 9, 60)];
        gimgpictureball1.tag=i+ 20000+1;
        gimgpictureball1.image=[UIImage imageNamed:@"left.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=70;
        xOffset++;
        
        
    }
}

-(void)initwithBallrightimage
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        /**/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(356 +widthOffset*xOffset, yOffset+ 128, 55, 55)];
        gimgpictureball1.tag=i+ 35000+1;
        //gimgpictureball1.backgroundColor=[UIColor blueColor];
        gimgpictureball1.image=[UIImage imageNamed:@"circle_2.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=70;
        xOffset++;
        
        
    }
}

-(void)initwithballrightwing
{
    int widthOffset = 0;
    int yOffset=12;
    int xOffset=0;
    for(int i=0;i<12;i++)
    {
        if(i!=0 && (i%12)==0)
        {
            yOffset+=30;
            xOffset=0;
        }
        
        /*===============column 1======================*/
        UIImageView *gimgpictureball1= [[UIImageView alloc]initWithFrame:CGRectMake(407 +widthOffset*xOffset, yOffset+ 125, 9, 60)];
        gimgpictureball1.tag=i+ 30000+1;
        gimgpictureball1.image=[UIImage imageNamed:@"right.png"];
        [self.mainscrollview addSubview:gimgpictureball1];
        
        gimgpictureball1.hidden=YES;
        widthOffset=70;
        xOffset++;
        
        
    }
}

-(void)loadThirdPart
{
    [self.imgtblswitch setFrame:CGRectMake(50, 53, 299, 202)];
    //[self.mainscrollview addSubview:self.imgtblswitch ];
    
    [self.tblimgback setFrame:CGRectMake(1188, 53, 265, 202)];
    
    DBCase *gdbcase=[[DBCase alloc] init];
    
    
    NSArray *caseinfoArr =[gdbcase selectCaseItem:KNECaseclass];
    //NSLog(@"contactArr------>%@",contactArr);
    
    for (KNECase *item in caseinfoArr )
    {
        
        /*===================new extra top============================*/
        
        int framwidth=685;
        
        
        UILabel *exlblArticleNum=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 415, 2, 353, 30)];
        exlblArticleNum.layer.borderColor=[UIColor blackColor].CGColor;
        exlblArticleNum.layer.borderWidth = 1;
        [self.mainscrollview addSubview:exlblArticleNum];
        
        UILabel *exlblswitchtype=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 415, 31, 75, 23)];    exlblswitchtype.layer.borderColor=[UIColor blackColor].CGColor;
        exlblswitchtype.layer.borderWidth = 1;
        [self.mainscrollview addSubview:exlblswitchtype];
        
        UILabel *exlblprogram=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 490, 31, 278, 23)];
        exlblprogram.layer.borderColor=[UIColor blackColor].CGColor;
        exlblprogram.layer.borderWidth = 1;
        [self.mainscrollview addSubview:exlblprogram];
        [self.mainscrollview addSubview:self.lblpage2];
        
        /*==========*/
        UILabel *extxtArticleNum=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 420, 2, 343, 30)];
//        extxtArticleNum.layer.borderColor=[UIColor redColor].CGColor;
//        extxtArticleNum.layer.borderWidth = 1;
        extxtArticleNum.font = [UIFont fontWithName:@"Arial" size:11];
        extxtArticleNum.backgroundColor=[UIColor clearColor];
        [self.mainscrollview addSubview:extxtArticleNum];
        extxtArticleNum.text=item.CustomerArticleNumber;
        
        
        UILabel *extxtswitchtype=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 420, 31, 65, 23)];
//        extxtswitchtype.layer.borderColor=[UIColor redColor].CGColor;
//        extxtswitchtype.layer.borderWidth = 1;
        extxtswitchtype.font = [UIFont fontWithName:@"Arial" size:11];
        extxtswitchtype.backgroundColor=[UIColor clearColor];
        [self.mainscrollview addSubview:extxtswitchtype];
        extxtswitchtype.text=item.SwitchType;
        
        UILabel *extxtprogram=[[UILabel alloc]initWithFrame:CGRectMake(framwidth + 500, 31, 263, 23)];
//        extxtprogram.layer.borderColor=[UIColor redColor].CGColor;
//        extxtprogram.layer.borderWidth = 1;
        extxtprogram.font = [UIFont systemFontOfSize:11];
        extxtprogram.backgroundColor=[UIColor clearColor];
        [self.mainscrollview addSubview:extxtprogram];
        extxtprogram.text=item.Program;
        
        /*===========================*/
        
         [self.lbl setFrame:CGRectMake(50, 53, 1138, 23)];
         self.lbl.layer.borderColor=[UIColor blackColor].CGColor;
         self.lbl.layer.borderWidth = 1;
         [self.mainscrollview addSubview:self.lbl ];
        
        [self.lblPlateName setFrame:CGRectMake(60, 53, 288, 23)];
        //self.lblPlateName.layer.borderColor=[UIColor blackColor].CGColor;
        //self.lblPlateName.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lblPlateName ];
        self.lblPlateName.text=item.PlateName;
        
         [self.lblCA10 setFrame:CGRectMake(415, 31, 75, 23)];
         self.lblCA10.layer.borderColor=[UIColor blackColor].CGColor;
         self.lblCA10.layer.borderWidth = 1;
         [self.mainscrollview addSubview:self.lblCA10 ];
        
        [self.lblSwitchType setFrame:CGRectMake(420, 31, 65, 23)];
        //self.lblSwitchType.layer.borderColor=[UIColor blackColor].CGColor;
        //self.lblSwitchType.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lblSwitchType ];
        self.lblSwitchType.text=item.SwitchType;
        
        [self.SGL719 setFrame:CGRectMake(490, 31, 278, 23)];
        self.SGL719.layer.borderColor=[UIColor blackColor].CGColor;
        self.SGL719.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.SGL719 ];
        
        [self.lblProgram setFrame:CGRectMake(500, 31, 263, 23)];
        //self.lblProgram.layer.borderColor=[UIColor blackColor].CGColor;
        //self.lblProgram.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lblProgram ];
        self.lblProgram.text=item.Program;
        
         [self.Labellbl setFrame:CGRectMake(415, 2, 353, 30)];
         self.Labellbl.layer.borderColor=[UIColor blackColor].CGColor;
         self.Labellbl.layer.borderWidth = 1;
         [self.mainscrollview addSubview:self.Labellbl ];
        
        [self.lblcustomerArticleNum setFrame:CGRectMake(420, 2, 343, 30)];
        //self.lblcustomerArticleNum.layer.borderColor=[UIColor blackColor].CGColor;
        //self.lblcustomerArticleNum.layer.borderWidth = 1;
        [self.mainscrollview addSubview:self.lblcustomerArticleNum ];
        self.lblcustomerArticleNum.text=item.CustomerArticleNumber;
        
        self.lblTotalSwitchingangle.text=item.TotalSwitchingAngle;
        self.lblSwitchingAngle.text=item.SwitchingAngle;
        
        self.imgtblbottom.frame=CGRectMake(50, 685, 509, 256);
        [self.mainscrollview addSubview:self.imgtblbottom];
        
        NSString *strlook=[[NSString alloc] initWithFormat:@" %@",item.FLook];
        [self.lbllook setFrame:CGRectMake(50 , 685, 110, 17)];
        [self.lbltxtlook setFrame:CGRectMake(50+ 110, 685, 144, 17)];
        self.lbltxtlook.text=strlook;
        
        NSString *strmounting=[[NSString alloc] initWithFormat:@" %@",item.FMounting];
        [self.lblMounting setFrame:CGRectMake(50 , 702, 110, 17)];
        [self.lbltxtMounting setFrame:CGRectMake(50+ 110, 702, 144, 17)];
        self.lbltxtMounting.text=strmounting;
        
        NSString *strEscu=[[NSString alloc] initWithFormat:@" %@",item.FEscutcheon_plate];
        [self.lblEscutcheonPlate setFrame:CGRectMake(50, 719, 110, 17)];
        [self.lbltxtEscutcheonPlate setFrame:CGRectMake(50 + 110, 719, 144, 17)];
        self.lbltxtEscutcheonPlate.text=strEscu;
        
        NSString *strhandle=[[NSString alloc] initWithFormat:@" %@",item.FHandle];
        [self.lblHandle setFrame:CGRectMake(50 , 736, 110, 17)];
        [self.lbltxtHandle setFrame:CGRectMake(50+ 110, 736, 144, 17)];
        self.lbltxtHandle.text=strhandle;
        
        NSString *strmatch=[[NSString alloc] initWithFormat:@" %@",item.FLatch_mech];
        [self.lblLatchMech setFrame:CGRectMake(50, 753, 110, 17)];
        [self.lbltxtLatchMech setFrame:CGRectMake(50+110, 753, 144, 17)];
        self.lbltxtLatchMech.text=strmatch;
        
        NSString *strstop=[[NSString alloc] initWithFormat:@" %@",item.FStop];
        [self.lblStop setFrame:CGRectMake(50, 770, 110, 17)];
        [self.lbltxtStop setFrame:CGRectMake(50+110, 770, 144, 17)];
        self.lbltxtStop.text=strstop;
        
        NSString *strstopDegree=[[NSString alloc] initWithFormat:@" %@",item.FStop_Degree];
        [self.lblStopdegree setFrame:CGRectMake(50, 787, 110, 17)];
        [self.lbltxtStopdegree setFrame:CGRectMake(50 +110, 787, 144, 17)];
        self.lbltxtStopdegree.text=strstopDegree;
        
        NSString *strnofstage=[[NSString alloc] initWithFormat:@" %@",item.FNofStage];
        [self.lblNoofStages setFrame:CGRectMake(50, 804, 110, 17)];
        [self.lbltxtNoofStages setFrame:CGRectMake(50+115, 804, 144, 17)];
        self.lbltxtNoofStages.text=strnofstage;
        
        NSString *strmasterdata=[[NSString alloc] initWithFormat:@" %@",item.FMasterfData];
        [self.lblMasterdata setFrame:CGRectMake(50, 821, 110, 17)];
        [self.lbltxtMasterdata setFrame:CGRectMake(50 +110, 821, 144, 17)];
        self.lbltxtMasterdata.text=strmasterdata;
        
        NSString *strreference=[[NSString alloc] initWithFormat:@" %@",item.FReference];
        [self.lblReference setFrame:CGRectMake(50, 838, 110, 17)];
        [self.lbltxtReference setFrame:CGRectMake(50 +110, 838, 144, 17)];
        self.lbltxtReference.text=strreference;
        
        NSString *strdate=[[NSString alloc] initWithFormat:@" %@",item.FDate];
        [self.lblDate setFrame:CGRectMake(50, 855, 110, 17)];
        [self.lbltxtDate setFrame:CGRectMake(50+110, 855, 144, 17)];
        self.lbltxtDate.text=strdate;
        
        NSString *strmodifydate=[[NSString alloc] initWithFormat:@" %@",item.FModify_Date];
        [self.lblModifyDate setFrame:CGRectMake(50, 872, 110, 17)];
        [self.lbltxtModifyDate setFrame:CGRectMake(50+110, 872, 144, 17)];
        self.lbltxtModifyDate.text=strmodifydate;
        
        NSString *strcusno=[[NSString alloc] initWithFormat:@" %@",item.FCustNo];
        [self.lblCustNO setFrame:CGRectMake(50, 889, 110, 17)];
        [self.lbltxtCustNO setFrame:CGRectMake(50+110, 889, 144, 17)];
        self.lbltxtCustNO.text=strcusno;
        
        NSString *strcompany=[[NSString alloc] initWithFormat:@" %@",item.FCompany];
        [self.lblCompany setFrame:CGRectMake(50, 906, 110, 17)];
        [self.lbltxtCompany setFrame:CGRectMake(50+110, 906, 144, 17)];
        self.lbltxtCompany.text=strcompany;
        
        NSString *strversion=[[NSString alloc] initWithFormat:@" %@",item.FVersion];
        [self.lblVersion setFrame:CGRectMake(50, 923, 110, 17)];
        [self.lbltxtVersion setFrame:CGRectMake(50+110, 923, 144, 17)];
        self.lbltxtVersion.text=strversion;
        
        /**/
        [self.lblpcs setFrame:CGRectMake(304, 685, 45, 17)];
        [self.lbloptional setFrame:CGRectMake(348, 685, 210, 17)];
        
        [self.lblpcs1 setFrame:CGRectMake(304, 702, 45, 17)];
        self.lblpcs1.text=item.pcs1;
        [self.lbloptional1 setFrame:CGRectMake(348, 702, 210, 17)];
        self.lbloptional1.text=item.optionalextra1;
        
        [self.lblpcs2 setFrame:CGRectMake(304, 719, 45, 17)];
        self.lblpcs2.text=item.pcs2;
        [self.lbloptional2 setFrame:CGRectMake(348, 719, 210, 17)];
        self.lbloptional2.text=item.optionalextra2;
        
        [self.lblpcs3 setFrame:CGRectMake(304, 736, 45, 17)];
        self.lblpcs3.text=item.pcs3;
        [self.lbloptional3 setFrame:CGRectMake(348, 736, 210, 17)];
        self.lbloptional3.text=item.optionalextra3;
        
        [self.lblpcs4 setFrame:CGRectMake(304, 753, 45, 17)];
        self.lblpcs4.text=item.pcs4;
        [self.lbloptional4 setFrame:CGRectMake(348, 753, 210, 17)];
        self.lbloptional4.text=item.optionalextra4;
        
        [self.lblpcs5 setFrame:CGRectMake(304, 770, 45, 17)];
        self.lblpcs5.text=item.pcs5;
        [self.lbloptional5 setFrame:CGRectMake(348, 770, 210, 17)];
        self.lbloptional5.text=item.optionalextra5;
        
        [self.lblpcs6 setFrame:CGRectMake(304, 787, 45, 17)];
        self.lblpcs6.text=item.pcs6;
        [self.lbloptional6 setFrame:CGRectMake(348, 787, 210, 17)];
        self.lbloptional6.text=item.optionalextra6;
        
        [self.lblpcs7 setFrame:CGRectMake(304, 804, 45, 17)];
        self.lblpcs7.text=item.pcs7;
        [self.lbloptional7 setFrame:CGRectMake(348, 804, 210, 17)];
        self.lbloptional7.text=item.optionalextra7;        
        
        [self.lblpcs8 setFrame:CGRectMake(304, 821, 45, 17)];
        self.lblpcs8.text=item.pcs8;
        [self.lbloptional8 setFrame:CGRectMake(348, 821, 210, 17)];
        self.lbloptional8.text=item.optionalextra8;
        
        [self.lblpcs9 setFrame:CGRectMake(304, 838, 45, 17)];
        self.lblpcs9.text=item.pcs9;
        [self.lbloptional9 setFrame:CGRectMake(348, 838, 210, 17)];
        self.lbloptional9.text=item.optionalextra9;

        [self.lbloptionalComment setFrame:CGRectMake(309, 855, 250, 85)];
        self.lbloptionalComment.text=item.extra_comment;
        self.lbloptionalComment.numberOfLines=20;
        self.lbloptionalComment.textAlignment=UITextAlignmentLeft;
        
        //NSLog(@"item.SwitchPoint>>>>>>>%@",item.SwitchPoint);
        
        if ([item.SwitchPoint isEqualToString:@"0"])
        {
            self.lbltxt0.backgroundColor=[UIColor blackColor];
            [self.lbltxt0 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"30"])
        {
            self.lbltxt30.backgroundColor=[UIColor blackColor];
            [self.lbltxt30 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"45"])
        {
            self.lbltxt45.backgroundColor=[UIColor blackColor];
            [self.lbltxt45 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"60"])
        {
            self.lbltxt60.backgroundColor=[UIColor blackColor];
            [self.lbltxt60 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"90"])
        {
            self.lbltxt90.backgroundColor=[UIColor blackColor];
            [self.lbltxt90 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"120"])
        {
            self.lbltxt120.backgroundColor=[UIColor blackColor];
            [self.lbltxt120 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"135"])
        {
            self.lbltxt135.backgroundColor=[UIColor blackColor];
            [self.lbltxt135 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"150"])
        {
            self.lbltxt150.backgroundColor=[UIColor blackColor];
            [self.lbltxt150 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"180"])
        {
            self.lbltxt180.backgroundColor=[UIColor blackColor];
            [self.lbltxt180 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"210"])
        {
            self.lbltxt210.backgroundColor=[UIColor blackColor];
            [self.lbltxt210 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"225"])
        {
            self.lbltxt225.backgroundColor=[UIColor blackColor];
            [self.lbltxt225 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"240"])
        {
            self.lbltxt240.backgroundColor=[UIColor blackColor];
            [self.lbltxt240 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"270"])
        {
            self.lbltxt270.backgroundColor=[UIColor blackColor];
            [self.lbltxt270 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"300"])
        {
            self.lbltxt300.backgroundColor=[UIColor blackColor];
            [self.lbltxt300 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"315"])
        {
            self.lbltxt315.backgroundColor=[UIColor blackColor];
            [self.lbltxt315 setTextColor:[UIColor whiteColor]];
            
        }else if ([item.SwitchPoint isEqualToString:@"330"])
        {
            self.lbltxt330.backgroundColor=[UIColor blackColor];
            [self.lbltxt330 setTextColor:[UIColor whiteColor]];
            
        }
            
    }
    
    
    
    
}

/*======================================NumberOfStage=====================================================*/
-(void)NumberOfStageTapped:(int)textField
{
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553)  ||  (lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2) )
    {
        
        if (stage1==NO)
        {
            NSLog(@"stage 1");
            stageno+=1;
            
            stage1=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3)   ||   (lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        
        if (stage2==NO)
        {
            NSLog(@"stage 2");
            stageno+=1;
            
            stage2=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5)     ||     (lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        if (stage3==NO)
        {
            NSLog(@"stage 3");
            stageno+=1;
            
            stage3=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7)    ||   (lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        if (stage4==NO)
        {
            NSLog(@"stage 4");
            stageno+=1;
            
            stage4=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9)    ||    (lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        if (stage5==NO)
        {
            NSLog(@"stage 5");
            stageno+=1;
            
            stage5=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11)  ||    (lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        if (stage6==NO)
        {
            NSLog(@"stage 6");
            stageno+=1;
            
            stage6=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13)    ||    (lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        if (stage7==NO)
        {
            NSLog(@"stage 7");
            stageno+=1;
            
            stage7=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15)    ||    (lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        if (stage8==NO)
        {
            NSLog(@"stage 8");
            stageno+=1;
            
            stage8=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17)    ||    (lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        if (stage9==NO)
        {
            NSLog(@"stage 9");
            stageno+=1;
            
            stage9=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19)    ||    (lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        if (stage10==NO)
        {
            NSLog(@"stage 10");
            stageno+=1;
            
            stage10=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21)    ||    (lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        if (stage11==NO)
        {
            NSLog(@"stage 11");
            stageno+=1;
            
            stage11=YES;
        }
        
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23)    ||    (lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        if (stage12==NO)
        {
            NSLog(@"stage 12");
            stageno+=1;
            
            stage12=YES;
        }
    }
    
    
    [self ShowNumberOfStageTapped];
    
}

-(void)ShowNumberOfStageTapped
{
    NSString *strnoofStage=[[NSString alloc] initWithFormat:@"%d",stageno];
    lbltxtNoofStages.text=strnoofStage; 
}

-(void)cancelNumberOfStageTapped:(int)textField
{
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 3000 + 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553)  ||  (lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2) )
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3025];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3049];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3073];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3097];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3121];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3145];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3169];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3193];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3217];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3241];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3265];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3289];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3313];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3337];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3361];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3385];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3409];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3433];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3457];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3481];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3505];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3529];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3553];
        
        
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 +2];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 +2];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 +2];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 +2];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 +2];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 +2];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 +2];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 +2];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 +2];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 +2];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 +2];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 +2];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 +2];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 +2];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 +2];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 +2];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 +2];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 +2];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 +2];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 +2];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 +2];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 +2];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 +2];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 +2];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage1==YES)
                {
                    NSLog(@"stage 1");
                    stageno-=1;
                    
                    stage1=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3)   ||   (lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 3];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 3];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 3];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 3];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 3];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 3];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 3];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 3];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 3];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 3];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 3];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 3];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 3];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 3];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 3];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 3];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 3];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 3];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 3];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 3];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 3];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 3];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 3];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 3];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 4];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 4];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 4];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 4];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 4];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 4];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 4];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 4];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 4];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 4];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 4];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 4];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 4];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 4];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 4];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 4];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 4];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 4];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 4];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 4];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 4];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 4];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 4];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 4];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage2==YES)
                {
                    NSLog(@"stage 2");
                    stageno-=1;
                    
                    stage2=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5)     ||     (lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 5];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 5];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 5];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 5];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 5];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 5];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 5];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 5];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 5];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 5];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 5];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 5];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 5];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 5];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 5];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 5];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 5];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 5];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 5];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 5];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 5];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 5];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 5];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 5];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 6];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 6];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 6];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 6];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 6];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 6];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 6];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 6];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 6];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 6];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 6];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 6];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 6];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 6];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 6];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 6];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 6];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 6];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 6];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 6];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 6];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 6];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 6];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 6];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage3==YES)
                {
                    NSLog(@"stage 3");
                    stageno-=1;
                    
                    stage3=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7)    ||   (lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 7];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 7];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 7];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 7];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 7];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 7];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 7];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 7];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 7];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 7];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 7];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 7];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 7];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 7];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 7];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 7];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 7];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 7];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 7];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 7];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 7];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 7];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 7];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 7];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 8];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 8];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 8];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 8];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 8];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 8];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 8];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 8];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 8];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 8];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 8];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 8];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 8];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 8];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 8];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 8];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 8];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 8];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 8];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 8];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 8];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 8];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 8];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 8];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage4==YES)
                {
                    NSLog(@"stage 4");
                    stageno-=1;
                    
                    stage4=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9)    ||    (lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 9];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 9];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 9];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 9];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 9];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 9];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 9];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 9];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 9];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 9];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 9];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 9];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 9];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 9];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 9];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 9];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 9];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 9];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 9];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 9];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 9];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 9];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 9];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 9];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 10];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 10];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 10];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 10];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 10];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 10];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 10];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 10];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 10];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 10];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 10];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 10];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 10];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 10];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 10];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 10];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 10];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 10];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 10];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 10];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 10];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 10];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 10];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 10];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage5==YES)
                {
                    NSLog(@"stage 5");
                    stageno-=1;
                    
                    stage5=NO;
                }
            }
            
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11)  ||    (lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 11];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 11];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 11];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 11];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 11];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 11];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 11];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 11];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 11];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 11];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 11];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 11];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 11];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 11];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 11];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 11];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 11];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 11];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 11];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 11];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 11];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 11];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 11];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 11];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 12];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 12];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 12];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 12];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 12];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 12];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 12];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 12];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 12];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 12];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 12];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 12];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 12];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 12];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 12];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 12];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 12];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 12];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 12];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 12];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 12];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 12];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 12];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 12];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage6==YES)
                {
                    NSLog(@"stage 6");
                    stageno-=1;
                    
                    stage6=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13)    ||    (lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 13];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 13];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 13];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 13];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 13];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 13];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 13];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 13];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 13];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 13];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 13];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 13];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 13];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 13];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 13];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 13];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 13];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 13];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 13];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 13];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 13];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 13];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 13];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 13];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3001 + 14];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 14];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 14];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 14];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 14];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 14];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 14];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 14];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 14];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 14];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 14];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 14];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 14];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 14];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 14];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 14];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 14];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 14];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 14];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 14];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 14];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 14];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 14];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 14];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage7==YES)
                {
                    NSLog(@"stage 7");
                    stageno-=1;
                    
                    stage7=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15)    ||    (lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 15];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 15];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 15];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 15];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 15];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 15];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 15];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 15];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 15];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 15];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 15];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 15];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 15];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 15];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 15];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 15];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 15];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 15];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 15];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 15];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 15];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 15];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 15];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 15];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 16];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 16];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 16];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 16];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 16];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 16];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 16];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 16];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 16];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 16];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 16];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 16];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 16];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 16];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 16];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 16];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 16];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 16];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 16];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 16];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 16];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 16];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 16];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 16];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage8==YES)
                {
                    NSLog(@"stage 8");
                    stageno-=1;
                    
                    stage8=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17)    ||    (lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 17];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 17];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 17];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 17];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 17];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 17];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 17];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 17];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 17];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 17];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 17];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 17];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 17];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 17];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 17];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 17];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 17];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 17];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 17];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 17];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 17];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 17];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 17];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 17];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 18];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 18];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 18];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 18];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 18];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 18];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 18];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 18];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 18];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 18];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 18];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 18];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 18];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 18];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 18];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 18];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 18];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 18];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 18];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 18];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 18];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 18];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 18];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 18];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage9==YES)
                {
                    NSLog(@"stage 9");
                    stageno-=1;
                    
                    stage9=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19)    ||    (lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 19];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 19];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 19];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 19];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 19];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 19];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 19];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 19];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 19];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 19];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 19];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 19];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 19];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 19];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 19];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 19];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 19];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 19];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 19];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 19];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 19];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 19];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 19];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 19];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 20];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 20];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 20];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 20];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 20];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 20];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 20];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 20];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 20];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 20];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 20];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 20];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 20];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 20];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 20];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 20];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 20];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 20];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 20];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 20];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 20];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 20];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 20];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 20];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage10==YES)
                {
                    NSLog(@"stage 10");
                    stageno-=1;
                    
                    stage10=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21)    ||    (lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 21];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 21];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 21];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 21];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 21];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 21];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 21];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 21];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 21];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 21];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 21];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 21];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 21];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 21];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 21];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 21];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 21];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 21];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 21];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 21];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 21];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 21];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 21];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 21];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 22];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 22];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 22];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 22];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 22];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 22];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 22];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 22];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 22];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 22];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 22];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 22];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 22];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 22];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 22];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 22];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 22];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 22];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 22];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 22];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 22];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 22];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 22];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 22];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage11==YES)
                {
                    NSLog(@"stage 11");
                    stageno-=1;
                    
                    stage11=NO;
                }
            }
        }
        
        
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23)    ||    (lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        
        UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 23];
        UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 23];
        UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 23];
        UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 23];
        UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 23];
        UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 23];
        UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 23];
        UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 23];
        UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 23];
        UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 23];
        UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 23];
        UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 23];
        UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 23];
        UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 23];
        UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 23];
        UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 23];
        UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 23];
        UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 23];
        UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 23];
        UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 23];
        UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 23];
        UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 23];
        UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 23];
        UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 23];
        
        if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
        {
            
            UILabel *lblgraftNumcheck1=(UILabel *)[self.mainscrollview viewWithTag:3000 + 24];
            UILabel *lblgraftNumcheck2=(UILabel *)[self.mainscrollview viewWithTag:3024 + 24];
            UILabel *lblgraftNumcheck3=(UILabel *)[self.mainscrollview viewWithTag:3048 + 24];
            UILabel *lblgraftNumcheck4=(UILabel *)[self.mainscrollview viewWithTag:3072 + 24];
            UILabel *lblgraftNumcheck5=(UILabel *)[self.mainscrollview viewWithTag:3096 + 24];
            UILabel *lblgraftNumcheck6=(UILabel *)[self.mainscrollview viewWithTag:3120 + 24];
            UILabel *lblgraftNumcheck7=(UILabel *)[self.mainscrollview viewWithTag:3144 + 24];
            UILabel *lblgraftNumcheck8=(UILabel *)[self.mainscrollview viewWithTag:3168 + 24];
            UILabel *lblgraftNumcheck9=(UILabel *)[self.mainscrollview viewWithTag:3192 + 24];
            UILabel *lblgraftNumcheck10=(UILabel *)[self.mainscrollview viewWithTag:3216 + 24];
            UILabel *lblgraftNumcheck11=(UILabel *)[self.mainscrollview viewWithTag:3240 + 24];
            UILabel *lblgraftNumcheck12=(UILabel *)[self.mainscrollview viewWithTag:3264 + 24];
            UILabel *lblgraftNumcheck13=(UILabel *)[self.mainscrollview viewWithTag:3288 + 24];
            UILabel *lblgraftNumcheck14=(UILabel *)[self.mainscrollview viewWithTag:3312 + 24];
            UILabel *lblgraftNumcheck15=(UILabel *)[self.mainscrollview viewWithTag:3336 + 24];
            UILabel *lblgraftNumcheck16=(UILabel *)[self.mainscrollview viewWithTag:3360 + 24];
            UILabel *lblgraftNumcheck17=(UILabel *)[self.mainscrollview viewWithTag:3384 + 24];
            UILabel *lblgraftNumcheck18=(UILabel *)[self.mainscrollview viewWithTag:3408 + 24];
            UILabel *lblgraftNumcheck19=(UILabel *)[self.mainscrollview viewWithTag:3432 + 24];
            UILabel *lblgraftNumcheck20=(UILabel *)[self.mainscrollview viewWithTag:3456 + 24];
            UILabel *lblgraftNumcheck21=(UILabel *)[self.mainscrollview viewWithTag:3480 + 24];
            UILabel *lblgraftNumcheck22=(UILabel *)[self.mainscrollview viewWithTag:3504 + 24];
            UILabel *lblgraftNumcheck23=(UILabel *)[self.mainscrollview viewWithTag:3528 + 24];
            UILabel *lblgraftNumcheck24=(UILabel *)[self.mainscrollview viewWithTag:3552 + 24];
            
            if (lblgraftNumcheck1.text==nil && lblgraftNumcheck2.text==nil && lblgraftNumcheck3.text==nil && lblgraftNumcheck4.text==nil && lblgraftNumcheck5.text==nil && lblgraftNumcheck6.text==nil && lblgraftNumcheck7.text==nil && lblgraftNumcheck8.text==nil && lblgraftNumcheck9.text==nil && lblgraftNumcheck10.text==nil && lblgraftNumcheck11.text==nil && lblgraftNumcheck12.text==nil && lblgraftNumcheck13.text==nil &&  lblgraftNumcheck14.text==nil && lblgraftNumcheck15.text==nil && lblgraftNumcheck16.text==nil && lblgraftNumcheck17.text==nil && lblgraftNumcheck18.text==nil && lblgraftNumcheck19.text==nil && lblgraftNumcheck20.text==nil && lblgraftNumcheck21.text==nil && lblgraftNumcheck22.text==nil && lblgraftNumcheck23.text==nil && lblgraftNumcheck24.text==nil )
            {
                if (stage12==YES)
                {
                    NSLog(@"stage 12");
                    stageno-=1;
                    
                    stage12=NO;
                }
            }
        }
        
    }
    
    
    [self ShowNumberOfStageTapped];
    
}

/*======================================contact jumper image=====================================================*/

-(void)contactjumperTapped:(int)textField
{
    [self NumberOfStageTapped:textField];
    UILabel *lblgraftNum=(UILabel *)[self.mainscrollview viewWithTag:textField+ 3000];
    
    if((lblgraftNum.tag == 3000 + 1)||(lblgraftNum.tag ==3000 + 25)||(lblgraftNum.tag == 3000 + 49)||(lblgraftNum.tag ==3000 + 73)||(lblgraftNum.tag == 97)||(lblgraftNum.tag ==3000 + 121)||(lblgraftNum.tag ==3000 + 145) ||(lblgraftNum.tag ==3000 + 169)||(lblgraftNum.tag ==3000 + 193) || (lblgraftNum.tag ==3000 + 217) || (lblgraftNum.tag ==3000 + 241) || (lblgraftNum.tag ==3000 + 265) || (lblgraftNum.tag ==3000 + 289) || (lblgraftNum.tag ==3000 + 313) || (lblgraftNum.tag ==3000 + 337) || (lblgraftNum.tag ==3000 + 361) || (lblgraftNum.tag ==3000 + 385) || (lblgraftNum.tag ==3000 + 409) || (lblgraftNum.tag ==3000 + 433) || (lblgraftNum.tag ==3000 + 457) || (lblgraftNum.tag ==3000 + 481) || (lblgraftNum.tag ==3000 + 505) || (lblgraftNum.tag ==3000 + 529) || (lblgraftNum.tag ==3000 + 553) )
    {
        
        //NSLog(@"here");
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10001];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20001];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
        
    }else if((lblgraftNum.tag == 3000 + 2)||(lblgraftNum.tag ==3000 + 24 +2)||(lblgraftNum.tag == 3000 + 48 +2)||(lblgraftNum.tag ==3000 + 72 +2)||(lblgraftNum.tag == 3000 + 96 +2 )||(lblgraftNum.tag ==3000 + 120 +2) ||(lblgraftNum.tag ==3000 + 144 +2) ||(lblgraftNum.tag ==3000 + 168 +2) ||(lblgraftNum.tag ==3000 + 192 +2) ||(lblgraftNum.tag ==3000 + 216 +2) ||(lblgraftNum.tag ==3000 + 240 +2) ||(lblgraftNum.tag ==3000 + 264 +2) ||(lblgraftNum.tag ==3000 + 288 +2) ||(lblgraftNum.tag ==3000 + 312 +2) ||(lblgraftNum.tag ==3000 + 336 +2) ||(lblgraftNum.tag ==3000 + 360 +2) ||(lblgraftNum.tag ==3000 + 384 +2) ||(lblgraftNum.tag ==3000 + 408 +2) ||(lblgraftNum.tag ==3000 + 432 +2) ||(lblgraftNum.tag ==3000 + 456 +2) ||(lblgraftNum.tag ==3000 + 480 +2) ||(lblgraftNum.tag ==3000 + 504 +2) ||(lblgraftNum.tag ==3000 + 528 +2) ||(lblgraftNum.tag ==3000 + 552 +2))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35001];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30001];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +3)||(lblgraftNum.tag ==3000 + 24 +3)||(lblgraftNum.tag == 3000 + 48 +3)||(lblgraftNum.tag ==3000 + 72 +3)||(lblgraftNum.tag == 3000 + 96 +3 )||(lblgraftNum.tag ==3000 + 120 +3) ||(lblgraftNum.tag ==3000 + 144 +3) ||(lblgraftNum.tag ==3000 + 168 +3) ||(lblgraftNum.tag ==3000 + 192 +3) ||(lblgraftNum.tag ==3000 + 216 +3) ||(lblgraftNum.tag ==3000 + 240 +3) ||(lblgraftNum.tag ==3000 + 264 +3) ||(lblgraftNum.tag ==3000 + 288 +3) ||(lblgraftNum.tag ==3000 + 312 +3) ||(lblgraftNum.tag ==3000 + 336 +3) ||(lblgraftNum.tag ==3000 + 360 +3) ||(lblgraftNum.tag ==3000 + 384 +3) ||(lblgraftNum.tag ==3000 + 408 +3) ||(lblgraftNum.tag ==3000 + 432 +3) ||(lblgraftNum.tag ==3000 + 456 +3) ||(lblgraftNum.tag ==3000 + 480 +3) ||(lblgraftNum.tag ==3000 + 504 +3) ||(lblgraftNum.tag ==3000 + 528 +3) ||(lblgraftNum.tag ==3000 + 552 +3))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10002];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20002];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +4)||(lblgraftNum.tag ==3000 + 24 +4)||(lblgraftNum.tag == 3000 + 48 +4)||(lblgraftNum.tag ==3000 + 72 +4)||(lblgraftNum.tag == 3000 + 96 +4 )||(lblgraftNum.tag ==3000 + 120 +4) ||(lblgraftNum.tag ==3000 + 144 +4) ||(lblgraftNum.tag ==3000 + 168 +4) ||(lblgraftNum.tag ==3000 + 192 +4) ||(lblgraftNum.tag ==3000 + 216 +4) ||(lblgraftNum.tag ==3000 + 240 +4) ||(lblgraftNum.tag ==3000 + 264 +4) ||(lblgraftNum.tag ==3000 + 288 +4) ||(lblgraftNum.tag ==3000 + 312 +4) ||(lblgraftNum.tag ==3000 + 336 +4) ||(lblgraftNum.tag ==3000 + 360 +4) ||(lblgraftNum.tag ==3000 + 384 +4) ||(lblgraftNum.tag ==3000 + 408 +4) ||(lblgraftNum.tag ==3000 + 432 +4) ||(lblgraftNum.tag ==3000 + 456 +4) ||(lblgraftNum.tag ==3000 + 480 +4) ||(lblgraftNum.tag ==3000 + 504 +4) ||(lblgraftNum.tag ==3000 + 528 +4) ||(lblgraftNum.tag ==3000 + 552 +4))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35002];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30002];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +5)||(lblgraftNum.tag ==3000 + 24 +5)||(lblgraftNum.tag == 3000 + 48 +5)||(lblgraftNum.tag ==3000 + 72 +5)||(lblgraftNum.tag == 3000 + 96 +5 )||(lblgraftNum.tag ==3000 + 120 +5) ||(lblgraftNum.tag ==3000 + 144 +5) ||(lblgraftNum.tag ==3000 + 168 +5) ||(lblgraftNum.tag ==3000 + 192 +5) ||(lblgraftNum.tag ==3000 + 216 +5) ||(lblgraftNum.tag ==3000 + 240 +5) ||(lblgraftNum.tag ==3000 + 264 +5) ||(lblgraftNum.tag ==3000 + 288 +5) ||(lblgraftNum.tag ==3000 + 312 +5) ||(lblgraftNum.tag ==3000 + 336 +5) ||(lblgraftNum.tag ==3000 + 360 +5) ||(lblgraftNum.tag ==3000 + 384 +5) ||(lblgraftNum.tag ==3000 + 408 +5) ||(lblgraftNum.tag ==3000 + 432 +5) ||(lblgraftNum.tag ==3000 + 456 +5) ||(lblgraftNum.tag ==3000 + 480 +5) ||(lblgraftNum.tag ==3000 + 504 +5) ||(lblgraftNum.tag ==3000 + 528 +5) ||(lblgraftNum.tag ==3000 + 552 +5))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10003];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20003];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +6)||(lblgraftNum.tag ==3000 + 24 +6)||(lblgraftNum.tag == 3000 + 48 +6)||(lblgraftNum.tag ==3000 + 72 +6)||(lblgraftNum.tag == 3000 + 96 +6 )||(lblgraftNum.tag ==3000 + 120 +6) ||(lblgraftNum.tag ==3000 + 144 +6) ||(lblgraftNum.tag ==3000 + 168 +6) ||(lblgraftNum.tag ==3000 + 192 +6) ||(lblgraftNum.tag ==3000 + 216 +6) ||(lblgraftNum.tag ==3000 + 240 +6) ||(lblgraftNum.tag ==3000 + 264 +6) ||(lblgraftNum.tag ==3000 + 288 +6) ||(lblgraftNum.tag ==3000 + 312 +6) ||(lblgraftNum.tag ==3000 + 336 +6) ||(lblgraftNum.tag ==3000 + 360 +6) ||(lblgraftNum.tag ==3000 + 384 +6) ||(lblgraftNum.tag ==3000 + 408 +6) ||(lblgraftNum.tag ==3000 + 432 +6) ||(lblgraftNum.tag ==3000 + 456 +6) ||(lblgraftNum.tag ==3000 + 480 +6) ||(lblgraftNum.tag ==3000 + 504 +6) ||(lblgraftNum.tag ==3000 + 528 +6) ||(lblgraftNum.tag ==3000 + 552 +6))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35003];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30003];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +7)||(lblgraftNum.tag ==3000 + 24 +7)||(lblgraftNum.tag == 3000 + 48 +7)||(lblgraftNum.tag ==3000 + 72 +7)||(lblgraftNum.tag == 3000 + 96 +7 )||(lblgraftNum.tag ==3000 + 120 +7) ||(lblgraftNum.tag ==3000 + 144 +7) ||(lblgraftNum.tag ==3000 + 168 +7) ||(lblgraftNum.tag ==3000 + 192 +7) ||(lblgraftNum.tag ==3000 + 216 +7) ||(lblgraftNum.tag ==3000 + 240 +7) ||(lblgraftNum.tag ==3000 + 264 +7) ||(lblgraftNum.tag ==3000 + 288 +7) ||(lblgraftNum.tag ==3000 + 312 +7) ||(lblgraftNum.tag ==3000 + 336 +7) ||(lblgraftNum.tag ==3000 + 360 +7) ||(lblgraftNum.tag ==3000 + 384 +7) ||(lblgraftNum.tag ==3000 + 408 +7) ||(lblgraftNum.tag ==3000 + 432 +7) ||(lblgraftNum.tag ==3000 + 456 +7) ||(lblgraftNum.tag ==3000 + 480 +7) ||(lblgraftNum.tag ==3000 + 504 +7) ||(lblgraftNum.tag ==3000 + 528 +7) ||(lblgraftNum.tag ==3000 + 552 +7))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10004];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20004];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +8)||(lblgraftNum.tag ==3000 + 24 +8)||(lblgraftNum.tag == 3000 + 48 +8)||(lblgraftNum.tag ==3000 + 72 +8)||(lblgraftNum.tag == 3000 + 96 +8 )||(lblgraftNum.tag ==3000 + 120 +8) ||(lblgraftNum.tag ==3000 + 144 +8) ||(lblgraftNum.tag ==3000 + 168 +8) ||(lblgraftNum.tag ==3000 + 192 +8) ||(lblgraftNum.tag ==3000 + 216 +8) ||(lblgraftNum.tag ==3000 + 240 +8) ||(lblgraftNum.tag ==3000 + 264 +8) ||(lblgraftNum.tag ==3000 + 288 +8) ||(lblgraftNum.tag ==3000 + 312 +8) ||(lblgraftNum.tag ==3000 + 336 +8) ||(lblgraftNum.tag ==3000 + 360 +8) ||(lblgraftNum.tag ==3000 + 384 +8) ||(lblgraftNum.tag ==3000 + 408 +8) ||(lblgraftNum.tag ==3000 + 432 +8) ||(lblgraftNum.tag ==3000 + 456 +8) ||(lblgraftNum.tag ==3000 + 480 +8) ||(lblgraftNum.tag ==3000 + 504 +8) ||(lblgraftNum.tag ==3000 + 528 +8) ||(lblgraftNum.tag ==3000 + 552 +8))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35004];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30004];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 +9)||(lblgraftNum.tag ==3000 + 24 +9)||(lblgraftNum.tag == 3000 + 48 +9)||(lblgraftNum.tag ==3000 + 72 +9)||(lblgraftNum.tag == 3000 + 96 +9 )||(lblgraftNum.tag ==3000 + 120 +9) ||(lblgraftNum.tag ==3000 + 144 +9) ||(lblgraftNum.tag ==3000 + 168 +9) ||(lblgraftNum.tag ==3000 + 192 +9) ||(lblgraftNum.tag ==3000 + 216 +9) ||(lblgraftNum.tag ==3000 + 240 +9) ||(lblgraftNum.tag ==3000 + 264 +9) ||(lblgraftNum.tag ==3000 + 288 +9) ||(lblgraftNum.tag ==3000 + 312 +9) ||(lblgraftNum.tag ==3000 + 336 +9) ||(lblgraftNum.tag ==3000 + 360 +9) ||(lblgraftNum.tag ==3000 + 384 +9) ||(lblgraftNum.tag ==3000 + 408 +9) ||(lblgraftNum.tag ==3000 + 432 +9) ||(lblgraftNum.tag ==3000 + 456 +9) ||(lblgraftNum.tag ==3000 + 480 +9) ||(lblgraftNum.tag ==3000 + 504 +9) ||(lblgraftNum.tag ==3000 + 528 +9) ||(lblgraftNum.tag ==3000 + 552 +9))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10005];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20005];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 10)||(lblgraftNum.tag ==3000 + 24 + 10)||(lblgraftNum.tag == 3000 + 48 + 10)||(lblgraftNum.tag ==3000 + 72 + 10)||(lblgraftNum.tag == 3000 + 96 + 10 )||(lblgraftNum.tag ==3000 + 120 + 10) ||(lblgraftNum.tag ==3000 + 144 + 10) ||(lblgraftNum.tag ==3000 + 168 + 10) ||(lblgraftNum.tag ==3000 + 192 + 10) ||(lblgraftNum.tag ==3000 + 216 + 10) ||(lblgraftNum.tag ==3000 + 240 + 10) ||(lblgraftNum.tag ==3000 + 264 + 10) ||(lblgraftNum.tag ==3000 + 288 + 10) ||(lblgraftNum.tag ==3000 + 312 + 10) ||(lblgraftNum.tag ==3000 + 336 + 10) ||(lblgraftNum.tag ==3000 + 360 + 10) ||(lblgraftNum.tag ==3000 + 384 + 10) ||(lblgraftNum.tag ==3000 + 408 + 10) ||(lblgraftNum.tag ==3000 + 432 + 10) ||(lblgraftNum.tag ==3000 + 456 + 10) ||(lblgraftNum.tag ==3000 + 480 + 10) ||(lblgraftNum.tag ==3000 + 504 + 10) ||(lblgraftNum.tag ==3000 + 528 + 10) ||(lblgraftNum.tag ==3000 + 552 + 10))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35005];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30005];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 11)||(lblgraftNum.tag ==3000 + 24 + 11)||(lblgraftNum.tag == 3000 + 48 + 11)||(lblgraftNum.tag ==3000 + 72 + 11)||(lblgraftNum.tag == 3000 + 96 + 11 )||(lblgraftNum.tag ==3000 + 120 + 11) ||(lblgraftNum.tag ==3000 + 144 + 11) ||(lblgraftNum.tag ==3000 + 168 + 11) ||(lblgraftNum.tag ==3000 + 192 + 11) ||(lblgraftNum.tag ==3000 + 216 + 11) ||(lblgraftNum.tag ==3000 + 240 + 11) ||(lblgraftNum.tag ==3000 + 264 + 11) ||(lblgraftNum.tag ==3000 + 288 + 11) ||(lblgraftNum.tag ==3000 + 312 + 11) ||(lblgraftNum.tag ==3000 + 336 + 11) ||(lblgraftNum.tag ==3000 + 360 + 11) ||(lblgraftNum.tag ==3000 + 384 + 11) ||(lblgraftNum.tag ==3000 + 408 + 11) ||(lblgraftNum.tag ==3000 + 432 + 11) ||(lblgraftNum.tag ==3000 + 456 + 11) ||(lblgraftNum.tag ==3000 + 480 + 11) ||(lblgraftNum.tag ==3000 + 504 + 11) ||(lblgraftNum.tag ==3000 + 528 + 11) ||(lblgraftNum.tag ==3000 + 552 + 11))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10006];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20006];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 12)||(lblgraftNum.tag ==3000 + 24 + 12)||(lblgraftNum.tag == 3000 + 48 + 12)||(lblgraftNum.tag ==3000 + 72 + 12)||(lblgraftNum.tag == 3000 + 96 + 12 )||(lblgraftNum.tag ==3000 + 120 + 12) ||(lblgraftNum.tag ==3000 + 144 + 12) ||(lblgraftNum.tag ==3000 + 168 + 12) ||(lblgraftNum.tag ==3000 + 192 + 12) ||(lblgraftNum.tag ==3000 + 216 + 12) ||(lblgraftNum.tag ==3000 + 240 + 12) ||(lblgraftNum.tag ==3000 + 264 + 12) ||(lblgraftNum.tag ==3000 + 288 + 12) ||(lblgraftNum.tag ==3000 + 312 + 12) ||(lblgraftNum.tag ==3000 + 336 + 12) ||(lblgraftNum.tag ==3000 + 360 + 12) ||(lblgraftNum.tag ==3000 + 384 + 12) ||(lblgraftNum.tag ==3000 + 408 + 12) ||(lblgraftNum.tag ==3000 + 432 + 12) ||(lblgraftNum.tag ==3000 + 456 + 12) ||(lblgraftNum.tag ==3000 + 480 + 12) ||(lblgraftNum.tag ==3000 + 504 + 12) ||(lblgraftNum.tag ==3000 + 528 + 12) ||(lblgraftNum.tag ==3000 + 552 + 12))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35006];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30006];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 13)||(lblgraftNum.tag ==3000 + 24 + 13)||(lblgraftNum.tag == 3000 + 48 + 13)||(lblgraftNum.tag ==3000 + 72 + 13)||(lblgraftNum.tag == 3000 + 96 + 13 )||(lblgraftNum.tag ==3000 + 120 + 13) ||(lblgraftNum.tag ==3000 + 144 + 13) ||(lblgraftNum.tag ==3000 + 168 + 13) ||(lblgraftNum.tag ==3000 + 192 + 13) ||(lblgraftNum.tag ==3000 + 216 + 13) ||(lblgraftNum.tag ==3000 + 240 + 13) ||(lblgraftNum.tag ==3000 + 264 + 13) ||(lblgraftNum.tag ==3000 + 288 + 13) ||(lblgraftNum.tag ==3000 + 312 + 13) ||(lblgraftNum.tag ==3000 + 336 + 13) ||(lblgraftNum.tag ==3000 + 360 + 13) ||(lblgraftNum.tag ==3000 + 384 + 13) ||(lblgraftNum.tag ==3000 + 408 + 13) ||(lblgraftNum.tag ==3000 + 432 + 13) ||(lblgraftNum.tag ==3000 + 456 + 13) ||(lblgraftNum.tag ==3000 + 480 + 13) ||(lblgraftNum.tag ==3000 + 504 + 13) ||(lblgraftNum.tag ==3000 + 528 + 13) ||(lblgraftNum.tag ==3000 + 552 + 13))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10007];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20007];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 14)||(lblgraftNum.tag ==3000 + 24 + 14)||(lblgraftNum.tag == 3000 + 48 + 14)||(lblgraftNum.tag ==3000 + 72 + 14)||(lblgraftNum.tag == 3000 + 96 + 14 )||(lblgraftNum.tag ==3000 + 120 + 14) ||(lblgraftNum.tag ==3000 + 144 + 14) ||(lblgraftNum.tag ==3000 + 168 + 14) ||(lblgraftNum.tag ==3000 + 192 + 14) ||(lblgraftNum.tag ==3000 + 216 + 14) ||(lblgraftNum.tag ==3000 + 240 + 14) ||(lblgraftNum.tag ==3000 + 264 + 14) ||(lblgraftNum.tag ==3000 + 288 + 14) ||(lblgraftNum.tag ==3000 + 312 + 14) ||(lblgraftNum.tag ==3000 + 336 + 14) ||(lblgraftNum.tag ==3000 + 360 + 14) ||(lblgraftNum.tag ==3000 + 384 + 14) ||(lblgraftNum.tag ==3000 + 408 + 14) ||(lblgraftNum.tag ==3000 + 432 + 14) ||(lblgraftNum.tag ==3000 + 456 + 14) ||(lblgraftNum.tag ==3000 + 480 + 14) ||(lblgraftNum.tag ==3000 + 504 + 14) ||(lblgraftNum.tag ==3000 + 528 + 14) ||(lblgraftNum.tag ==3000 + 552 + 14))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35007];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30007];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 15)||(lblgraftNum.tag ==3000 + 24 + 15)||(lblgraftNum.tag == 3000 + 48 + 15)||(lblgraftNum.tag ==3000 + 72 + 15)||(lblgraftNum.tag == 3000 + 96 + 15 )||(lblgraftNum.tag ==3000 + 120 + 15) ||(lblgraftNum.tag ==3000 + 144 + 15) ||(lblgraftNum.tag ==3000 + 168 + 15) ||(lblgraftNum.tag ==3000 + 192 + 15) ||(lblgraftNum.tag ==3000 + 216 + 15) ||(lblgraftNum.tag ==3000 + 240 + 15) ||(lblgraftNum.tag ==3000 + 264 + 15) ||(lblgraftNum.tag ==3000 + 288 + 15) ||(lblgraftNum.tag ==3000 + 312 + 15) ||(lblgraftNum.tag ==3000 + 336 + 15) ||(lblgraftNum.tag ==3000 + 360 + 15) ||(lblgraftNum.tag ==3000 + 384 + 15) ||(lblgraftNum.tag ==3000 + 408 + 15) ||(lblgraftNum.tag ==3000 + 432 + 15) ||(lblgraftNum.tag ==3000 + 456 + 15) ||(lblgraftNum.tag ==3000 + 480 + 15) ||(lblgraftNum.tag ==3000 + 504 + 15) ||(lblgraftNum.tag ==3000 + 528 + 15) ||(lblgraftNum.tag ==3000 + 552 + 15))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10008];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20008];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 16)||(lblgraftNum.tag ==3000 + 24 + 16)||(lblgraftNum.tag == 3000 + 48 + 16)||(lblgraftNum.tag ==3000 + 72 + 16)||(lblgraftNum.tag == 3000 + 96 + 16 )||(lblgraftNum.tag ==3000 + 120 + 16) ||(lblgraftNum.tag ==3000 + 144 + 16) ||(lblgraftNum.tag ==3000 + 168 + 16) ||(lblgraftNum.tag ==3000 + 192 + 16) ||(lblgraftNum.tag ==3000 + 216 + 16) ||(lblgraftNum.tag ==3000 + 240 + 16) ||(lblgraftNum.tag ==3000 + 264 + 16) ||(lblgraftNum.tag ==3000 + 288 + 16) ||(lblgraftNum.tag ==3000 + 312 + 16) ||(lblgraftNum.tag ==3000 + 336 + 16) ||(lblgraftNum.tag ==3000 + 360 + 16) ||(lblgraftNum.tag ==3000 + 384 + 16) ||(lblgraftNum.tag ==3000 + 408 + 16) ||(lblgraftNum.tag ==3000 + 432 + 16) ||(lblgraftNum.tag ==3000 + 456 + 16) ||(lblgraftNum.tag ==3000 + 480 + 16) ||(lblgraftNum.tag ==3000 + 504 + 16) ||(lblgraftNum.tag ==3000 + 528 + 16) ||(lblgraftNum.tag ==3000 + 552 + 16))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35008];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30008];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 17)||(lblgraftNum.tag ==3000 + 24 + 17)||(lblgraftNum.tag == 3000 + 48 + 17)||(lblgraftNum.tag ==3000 + 72 + 17)||(lblgraftNum.tag == 3000 + 96 + 17 )||(lblgraftNum.tag ==3000 + 120 + 17) ||(lblgraftNum.tag ==3000 + 144 + 17) ||(lblgraftNum.tag ==3000 + 168 + 17) ||(lblgraftNum.tag ==3000 + 192 + 17) ||(lblgraftNum.tag ==3000 + 216 + 17) ||(lblgraftNum.tag ==3000 + 240 + 17) ||(lblgraftNum.tag ==3000 + 264 + 17) ||(lblgraftNum.tag ==3000 + 288 + 17) ||(lblgraftNum.tag ==3000 + 312 + 17) ||(lblgraftNum.tag ==3000 + 336 + 17) ||(lblgraftNum.tag ==3000 + 360 + 17) ||(lblgraftNum.tag ==3000 + 384 + 17) ||(lblgraftNum.tag ==3000 + 408 + 17) ||(lblgraftNum.tag ==3000 + 432 + 17) ||(lblgraftNum.tag ==3000 + 456 + 17) ||(lblgraftNum.tag ==3000 + 480 + 17) ||(lblgraftNum.tag ==3000 + 504 + 17) ||(lblgraftNum.tag ==3000 + 528 + 17) ||(lblgraftNum.tag ==3000 + 552 + 17))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10009];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20009];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 18)||(lblgraftNum.tag ==3000 + 24 + 18)||(lblgraftNum.tag == 3000 + 48 + 18)||(lblgraftNum.tag ==3000 + 72 + 18)||(lblgraftNum.tag == 3000 + 96 + 18 )||(lblgraftNum.tag ==3000 + 120 + 18) ||(lblgraftNum.tag ==3000 + 144 + 18) ||(lblgraftNum.tag ==3000 + 168 + 18) ||(lblgraftNum.tag ==3000 + 192 + 18) ||(lblgraftNum.tag ==3000 + 216 + 18) ||(lblgraftNum.tag ==3000 + 240 + 18) ||(lblgraftNum.tag ==3000 + 264 + 18) ||(lblgraftNum.tag ==3000 + 288 + 18) ||(lblgraftNum.tag ==3000 + 312 + 18) ||(lblgraftNum.tag ==3000 + 336 + 18) ||(lblgraftNum.tag ==3000 + 360 + 18) ||(lblgraftNum.tag ==3000 + 384 + 18) ||(lblgraftNum.tag ==3000 + 408 + 18) ||(lblgraftNum.tag ==3000 + 432 + 18) ||(lblgraftNum.tag ==3000 + 456 + 18) ||(lblgraftNum.tag ==3000 + 480 + 18) ||(lblgraftNum.tag ==3000 + 504 + 18) ||(lblgraftNum.tag ==3000 + 528 + 18) ||(lblgraftNum.tag ==3000 + 552 + 18))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35009];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30009];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 19)||(lblgraftNum.tag ==3000 + 24 + 19)||(lblgraftNum.tag == 3000 + 48 + 19)||(lblgraftNum.tag ==3000 + 72 + 19)||(lblgraftNum.tag == 3000 + 96 + 19 )||(lblgraftNum.tag ==3000 + 120 + 19) ||(lblgraftNum.tag ==3000 + 144 + 19) ||(lblgraftNum.tag ==3000 + 168 + 19) ||(lblgraftNum.tag ==3000 + 192 + 19) ||(lblgraftNum.tag ==3000 + 216 + 19) ||(lblgraftNum.tag ==3000 + 240 + 19) ||(lblgraftNum.tag ==3000 + 264 + 19) ||(lblgraftNum.tag ==3000 + 288 + 19) ||(lblgraftNum.tag ==3000 + 312 + 19) ||(lblgraftNum.tag ==3000 + 336 + 19) ||(lblgraftNum.tag ==3000 + 360 + 19) ||(lblgraftNum.tag ==3000 + 384 + 19) ||(lblgraftNum.tag ==3000 + 408 + 19) ||(lblgraftNum.tag ==3000 + 432 + 19) ||(lblgraftNum.tag ==3000 + 456 + 19) ||(lblgraftNum.tag ==3000 + 480 + 19) ||(lblgraftNum.tag ==3000 + 504 + 19) ||(lblgraftNum.tag ==3000 + 528 + 19) ||(lblgraftNum.tag ==3000 + 552 + 19))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10010];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20010];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 20)||(lblgraftNum.tag ==3000 + 24 + 20)||(lblgraftNum.tag == 3000 + 48 + 20)||(lblgraftNum.tag ==3000 + 72 + 20)||(lblgraftNum.tag == 3000 + 96 + 20 )||(lblgraftNum.tag ==3000 + 120 + 20) ||(lblgraftNum.tag ==3000 + 144 + 20) ||(lblgraftNum.tag ==3000 + 168 + 20) ||(lblgraftNum.tag ==3000 + 192 + 20) ||(lblgraftNum.tag ==3000 + 216 + 20) ||(lblgraftNum.tag ==3000 + 240 + 20) ||(lblgraftNum.tag ==3000 + 264 + 20) ||(lblgraftNum.tag ==3000 + 288 + 20) ||(lblgraftNum.tag ==3000 + 312 + 20) ||(lblgraftNum.tag ==3000 + 336 + 20) ||(lblgraftNum.tag ==3000 + 360 + 20) ||(lblgraftNum.tag ==3000 + 384 + 20) ||(lblgraftNum.tag ==3000 + 408 + 20) ||(lblgraftNum.tag ==3000 + 432 + 20) ||(lblgraftNum.tag ==3000 + 456 + 20) ||(lblgraftNum.tag ==3000 + 480 + 20) ||(lblgraftNum.tag ==3000 + 504 + 20) ||(lblgraftNum.tag ==3000 + 528 + 20) ||(lblgraftNum.tag ==3000 + 552 + 20))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35010];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30010];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 21)||(lblgraftNum.tag ==3000 + 24 + 21)||(lblgraftNum.tag == 3000 + 48 + 21)||(lblgraftNum.tag ==3000 + 72 + 21)||(lblgraftNum.tag == 3000 + 96 + 21 )||(lblgraftNum.tag ==3000 + 120 + 21) ||(lblgraftNum.tag ==3000 + 144 + 21) ||(lblgraftNum.tag ==3000 + 168 + 21) ||(lblgraftNum.tag ==3000 + 192 + 21) ||(lblgraftNum.tag ==3000 + 216 + 21) ||(lblgraftNum.tag ==3000 + 240 + 21) ||(lblgraftNum.tag ==3000 + 264 + 21) ||(lblgraftNum.tag ==3000 + 288 + 21) ||(lblgraftNum.tag ==3000 + 312 + 21) ||(lblgraftNum.tag ==3000 + 336 + 21) ||(lblgraftNum.tag ==3000 + 360 + 21) ||(lblgraftNum.tag ==3000 + 384 + 21) ||(lblgraftNum.tag ==3000 + 408 + 21) ||(lblgraftNum.tag ==3000 + 432 + 21) ||(lblgraftNum.tag ==3000 + 456 + 21) ||(lblgraftNum.tag ==3000 + 480 + 21) ||(lblgraftNum.tag ==3000 + 504 + 21) ||(lblgraftNum.tag ==3000 + 528 + 21) ||(lblgraftNum.tag ==3000 + 552 + 21))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10011];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20011];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 22)||(lblgraftNum.tag ==3000 + 24 + 22)||(lblgraftNum.tag == 3000 + 48 + 22)||(lblgraftNum.tag ==3000 + 72 + 22)||(lblgraftNum.tag == 3000 + 96 + 22 )||(lblgraftNum.tag ==3000 + 120 + 22) ||(lblgraftNum.tag ==3000 + 144 + 22) ||(lblgraftNum.tag ==3000 + 168 + 22) ||(lblgraftNum.tag ==3000 + 192 + 22) ||(lblgraftNum.tag ==3000 + 216 + 22) ||(lblgraftNum.tag ==3000 + 240 + 22) ||(lblgraftNum.tag ==3000 + 264 + 22) ||(lblgraftNum.tag ==3000 + 288 + 22) ||(lblgraftNum.tag ==3000 + 312 + 22) ||(lblgraftNum.tag ==3000 + 336 + 22) ||(lblgraftNum.tag ==3000 + 360 + 22) ||(lblgraftNum.tag ==3000 + 384 + 22) ||(lblgraftNum.tag ==3000 + 408 + 22) ||(lblgraftNum.tag ==3000 + 432 + 22) ||(lblgraftNum.tag ==3000 + 456 + 22) ||(lblgraftNum.tag ==3000 + 480 + 22) ||(lblgraftNum.tag ==3000 + 504 + 22) ||(lblgraftNum.tag ==3000 + 528 + 22) ||(lblgraftNum.tag ==3000 + 552 + 22))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35011];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30011];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 23)||(lblgraftNum.tag ==3000 + 24 + 23)||(lblgraftNum.tag == 3000 + 48 + 23)||(lblgraftNum.tag ==3000 + 72 + 23)||(lblgraftNum.tag == 3000 + 96 + 23 )||(lblgraftNum.tag ==3000 + 120 + 23) ||(lblgraftNum.tag ==3000 + 144 + 23) ||(lblgraftNum.tag ==3000 + 168 + 23) ||(lblgraftNum.tag ==3000 + 192 + 23) ||(lblgraftNum.tag ==3000 + 216 + 23) ||(lblgraftNum.tag ==3000 + 240 + 23) ||(lblgraftNum.tag ==3000 + 264 + 23) ||(lblgraftNum.tag ==3000 + 288 + 23) ||(lblgraftNum.tag ==3000 + 312 + 23) ||(lblgraftNum.tag ==3000 + 336 + 23) ||(lblgraftNum.tag ==3000 + 360 + 23) ||(lblgraftNum.tag ==3000 + 384 + 23) ||(lblgraftNum.tag ==3000 + 408 + 23) ||(lblgraftNum.tag ==3000 + 432 + 23) ||(lblgraftNum.tag ==3000 + 456 + 23) ||(lblgraftNum.tag ==3000 + 480 + 23) ||(lblgraftNum.tag ==3000 + 504 + 23) ||(lblgraftNum.tag ==3000 + 528 + 23) ||(lblgraftNum.tag ==3000 + 552 + 23))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:10012];
        UIImageView *imgleftwingView=(UIImageView *)[self.mainscrollview viewWithTag:20012];
        
        imgleftwingView.hidden=NO;
        imgballView.hidden=NO;
    }else if((lblgraftNum.tag == 3000 + 24)||(lblgraftNum.tag ==3000 + 24 + 24)||(lblgraftNum.tag == 3000 + 48 + 24)||(lblgraftNum.tag ==3000 + 72 + 24)||(lblgraftNum.tag == 3000 + 96 + 24 )||(lblgraftNum.tag ==3000 + 120 + 24) ||(lblgraftNum.tag ==3000 + 144 + 24) ||(lblgraftNum.tag ==3000 + 168 + 24) ||(lblgraftNum.tag ==3000 + 192 + 24) ||(lblgraftNum.tag ==3000 + 216 + 24) ||(lblgraftNum.tag ==3000 + 240 + 24) ||(lblgraftNum.tag ==3000 + 264 + 24) ||(lblgraftNum.tag ==3000 + 288 + 24) ||(lblgraftNum.tag ==3000 + 312 + 24) ||(lblgraftNum.tag ==3000 + 336 + 24) ||(lblgraftNum.tag ==3000 + 360 + 24) ||(lblgraftNum.tag ==3000 + 384 + 24) ||(lblgraftNum.tag ==3000 + 408 + 24) ||(lblgraftNum.tag ==3000 + 432 + 24) ||(lblgraftNum.tag ==3000 + 456 + 24) ||(lblgraftNum.tag ==3000 + 480 + 24) ||(lblgraftNum.tag ==3000 + 504 + 24) ||(lblgraftNum.tag ==3000 + 528 + 24) ||(lblgraftNum.tag ==3000 + 552 + 24))
    {
        
        UIImageView *imgballView=(UIImageView *)[self.mainscrollview viewWithTag:35012];
        UIImageView *imgrighttwingView=(UIImageView *)[self.mainscrollview viewWithTag:30012];
        imgrighttwingView.hidden=NO;
        imgballView.hidden=NO;
    }
}
@end
