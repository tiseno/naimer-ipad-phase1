//
//  KNESwitchingFirstValue.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/6/12.
//
//

#import <Foundation/Foundation.h>
#import "KNECase.h"

@interface KNESwitchingFirstValue : KNECase{
    
}
@property (nonatomic, strong) NSString *SwitchingFirstValueID;
@property (nonatomic, strong) NSString *SwitchingFirstValue;


@end
