//
//  KNETblGraftPositionTag.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/28/12.
//
//

#import <Foundation/Foundation.h>
#import "KNECase.h"

@interface KNETblGraftPositionTag : KNECase{
    
}
//@property (nonatomic,retain) NSString *CaseID;
@property (nonatomic,strong) NSString *contactID;
@property (nonatomic, strong) NSString *imgGbtnTag;
@property (nonatomic, strong) NSString *imgGbtnValueTag;
@property (nonatomic, strong) NSString *tblgraftvalue;
@property (nonatomic, strong) NSString *Vposition;
 

@end
