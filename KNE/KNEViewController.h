//
//  KNEViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KNEFirstScreenViewController.h"
#import "KNEStartBallViewController.h"
#import "KNESwitchMainPageViewController.h"
#import "KNEMenuPageViewController.h"

@interface KNEViewController : UIViewController{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imgSplashScreen;
//@property (retain, nonatomic) KNEMenuPageViewController *gKNEMenuPageViewController;
@property (strong, nonatomic) KNEMenuPageViewController *gKNEMenuPageViewController;
@end
