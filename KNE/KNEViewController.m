//
//  KNEViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KNEViewController.h"

@implementation KNEViewController
@synthesize imgSplashScreen;
@synthesize gKNEMenuPageViewController;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self performSelector:@selector(KNSplash) withObject:nil afterDelay:1];
    //[self performSelector:@selector(KNEMainPage) withObject:nil afterDelay:1];
    //[self SwitchPage];
}

- (void)viewDidUnload
{
    [self setImgSplashScreen:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)SwitchPage
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    

    /*======switch========*/
    /*
    KNEMenuPageViewController *gKNEMenuPageViewController=[[KNEMenuPageViewController alloc] initWithNibName:@"KNEMenuPageViewController"  bundle:nil];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gKNEMenuPageViewController];
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    [self.view addSubview:tnavController.view];
     */
}

-(void)KNSplash
{
    self.imgSplashScreen.hidden=YES;
    [self KNEMainPage];
}

-(void)KNEMainPage
{ 
    
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    
    //NSString *titleAlignment=[@"Kraus & Naimer" stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(110, 3, 200, 24)];
    label.text=@"Kraus & Naimer";
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 3;
    label.lineBreakMode=UILineBreakModeWordWrap;
    label.font = [UIFont fontWithName:@"Arial-BoldMT" size:21];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    //self.navigationItem.titleView = label;
    
    
    gKNEMenuPageViewController=[[KNEMenuPageViewController alloc] initWithNibName:@"KNEMenuPageViewController" bundle:nil];
//    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gKNEMenuPageViewController];
//    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    gKNEMenuPageViewController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
//    gKNEMenuPageViewController.navigationItem.titleView = label;
//
//    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
//    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:gKNEMenuPageViewController.view];

}

@end
