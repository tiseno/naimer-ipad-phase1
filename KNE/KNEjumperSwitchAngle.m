//
//  KNEjumperSwitchAngle.m
//  KNE
//
//  Created by Tiseno Mac 2 on 1/7/13.
//
//

#import "KNEjumperSwitchAngle.h"

@implementation KNEjumperSwitchAngle
@synthesize Anglenum;
@synthesize mutblAngleSwitch;
@synthesize switch1, switch10, switch11, switch12, switch13, switch14, switch15, switch16, switch17, switch18, switch19, switch2, switch20, switch21, switch22, switch23, switch24, switch3, switch4, switch5, switch6, switch7, switch8, switch9;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //Anglenum=@"1";
        
        //mutblAngleSwitch=[[NSMutableArray alloc]init];
        
        if(mutblAngleSwitch==nil)
        {
            mutblAngleSwitch= [[NSMutableArray alloc]init];
            
        }
        
    }
    return self;
}


-(void)getSwitchAngle1:(NSString *)getid
{
    if ([getid isEqualToString:@"1"])
    {
        [self addSwitchAngle:getid];
        
    }else if ([getid isEqualToString:@"25"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"49"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"73"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"97"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"121"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"145"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"169"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"193"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"217"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"241"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"265"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"289"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"313"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"337"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"361"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"385"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"409"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"433"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"457"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"481"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"505"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"529"])
    {
        [self addSwitchAngle:getid];
    }else if ([getid isEqualToString:@"553"])
    {
        [self addSwitchAngle:getid];
    }
    
    //[self setNeedsDisplay];
}

-(void)angle2:(NSString *)getid
{
    switch2=getid;
}

-(void)addSwitchAngle:(NSString *)gSwitchAngle
{
    //jumperSwitchAngle *gjumperSwitchAngle =[[[jumperSwitchAngle alloc] init] autorelease];
    
    if(mutblAngleSwitch==nil)
    {
        mutblAngleSwitch= [[NSMutableArray alloc]init];
        
    }
    
    //gjumperSwitchAngle.switch1=gSwitchAngle.switch1;
    //gjumperSwitchAngle.switch2=gSwitchAngle.switch2;
    
    
    [mutblAngleSwitch addObject:gSwitchAngle];
    
    [self setNeedsDisplay];
    
}

-(void)cancleSwitchAngle:(NSString *)gSwitchAngle
{
    for(int i =0; i < mutblAngleSwitch.count; i++)
    {
        NSString* key=[mutblAngleSwitch objectAtIndex:i];
        
        if ([key isEqualToString:gSwitchAngle]) {
            [self deleteSwitchAngleRowIndex:i];
        }else
        {
            NSLog(@"nothing~");
        }
    }
}

-(void)deleteSwitchAngleRowIndex:(NSUInteger)index
{
    [mutblAngleSwitch removeObjectAtIndex:index];
    
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
    [super drawRect:(CGRect)rect];
    //NSLog(@"x: %f, y: %f, width: %f, height: %f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    /*
    CGContextBeginPath(ctx);
    CGContextMoveToPoint   (ctx, CGRectGetMinX(rect), CGRectGetMinY(rect));  // top left
    CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMidY(rect));  // mid right
    CGContextAddLineToPoint(ctx, CGRectGetMinX(rect), CGRectGetMaxY(rect));  // bottom left
    CGContextClosePath(ctx);
     */
    CGContextBeginPath(ctx);
    
    for(int i =0; i < mutblAngleSwitch.count; i++)
    {
        NSString* key=[mutblAngleSwitch objectAtIndex:i];
        
        if ([key isEqualToString:@"1"]) {
            /*=========angle 1===========*/
            
            CGContextMoveToPoint   (ctx, 12, 102);  // top left
            CGContextAddLineToPoint(ctx, 43, 106.5);  // mid right
            CGContextAddLineToPoint(ctx, 12, 111);
        }else if ([key isEqualToString:@"25"])
        {
            /*=========angle 2===========*/
            
            CGContextMoveToPoint   (ctx, 15, 94);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 12, 102.0);
            
        }else if ([key isEqualToString:@"49"])
        {
            /*=========angle 3===========*/
            
            CGContextMoveToPoint   (ctx, 18, 88);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 15, 94);
            
        }else if ([key isEqualToString:@"73"])
        {
            /*=========angle 4===========*/
            
            CGContextMoveToPoint   (ctx, 24, 82);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 18, 88);
            
        }else if ([key isEqualToString:@"97"])
        {
            /*=========angle 5===========*/
            
            CGContextMoveToPoint   (ctx, 30, 78);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 24, 82);
            
        }else if ([key isEqualToString:@"121"])
        {
            /*=========angle 6===========*/
            
            CGContextMoveToPoint   (ctx, 39, 76);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 30, 78);
            
        }else if ([key isEqualToString:@"145"])
        {
            /*=========angle 7===========*/
            
            CGContextMoveToPoint   (ctx, 47, 76);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 39, 76);
            
        }else if ([key isEqualToString:@"169"])
        {
            /*=========angle 8===========*/
            
            CGContextMoveToPoint   (ctx, 55, 77);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 47, 76);
            
        }else if ([key isEqualToString:@"193"])
        {
            /*=========angle 9===========*/
            
            CGContextMoveToPoint   (ctx, 62, 81);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 55, 77);
            
        }else if ([key isEqualToString:@"217"])
        {
            /*=========angle 10===========*/
            
            CGContextMoveToPoint   (ctx, 68, 88);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 62, 81);
            
        }else if ([key isEqualToString:@"241"])
        {
            /*=========angle 11===========*/
            
            CGContextMoveToPoint   (ctx, 73, 95);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 68, 88);
            
        }else if ([key isEqualToString:@"265"])
        {
            /*=========angle 12===========*/
            
            CGContextMoveToPoint   (ctx, 74, 102);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 73, 95);
            
        }else if ([key isEqualToString:@"289"])
        {
            /*=========angle 13===========*/
            
            CGContextMoveToPoint   (ctx, 74, 111);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 74, 102);
            
        }else if ([key isEqualToString:@"313"])
        {
            /*=========angle 14===========*/
            
            CGContextMoveToPoint   (ctx, 73, 118);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 74, 110);
            
        }else if ([key isEqualToString:@"337"])
        {
            /*=========angle 15===========*/
            
            CGContextMoveToPoint   (ctx, 69, 125);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 73, 118);
            
        }else if ([key isEqualToString:@"361"])
        {
            /*=========angle 16===========*/
            
            CGContextMoveToPoint   (ctx, 62, 132);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 69, 125);
            
        }else if ([key isEqualToString:@"385"])
        {
            /*=========angle 17===========*/
            
            CGContextMoveToPoint   (ctx, 56, 135);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 62, 132);
            
        }else if ([key isEqualToString:@"409"])
        {
            /*=========angle 18===========*/
            
            CGContextMoveToPoint   (ctx, 49, 138);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 56, 135);
            
        }else if ([key isEqualToString:@"433"])
        {
            /*=========angle 19===========*/
            
            CGContextMoveToPoint   (ctx, 38, 138);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 49, 138);
            
        }else if ([key isEqualToString:@"457"])
        {
            /*=========angle 20===========*/
            
            CGContextMoveToPoint   (ctx, 30, 136);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 38, 138);
            
        }else if ([key isEqualToString:@"481"])
        {
            /*=========angle 21===========*/
            
            CGContextMoveToPoint   (ctx, 24, 132);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 30, 136);
            
        }else if ([key isEqualToString:@"505"])
        {
            /*=========angle 22===========*/
            
            CGContextMoveToPoint   (ctx, 17, 125);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 24, 132);
            
        }else if ([key isEqualToString:@"529"])
        {
            /*=========angle 23===========*/
            
            CGContextMoveToPoint   (ctx, 14, 119);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 17, 125);
            
        }else if ([key isEqualToString:@"553"])
        {
            /*=========angle 24===========*/
            
            CGContextMoveToPoint   (ctx, 12, 111);
            CGContextAddLineToPoint(ctx, 43, 106.5);
            CGContextAddLineToPoint(ctx, 14, 119);
            
        }
        
        
        
        
    }
    /**/


    

    CGContextClosePath(ctx);
    
    CGContextSetRGBFillColor(ctx, 255, 0, 255, 1);
    CGContextFillPath(ctx);
}

/*
- (void)drawRect:(CGRect)rect {

    
    // Get the graphics context and clear it
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //CGContextClearRect(ctx, rect);
    
    // Draw a purple triangle with using lines
    CGContextSetRGBStrokeColor(ctx, 255, 0, 255, 1);
    
    CGPoint points[6] = { CGPointMake(100, 100), CGPointMake(150, 150), CGPointMake(150, 150), CGPointMake(50, 150), CGPointMake(50, 150), CGPointMake(100, 100) };
    CGContextStrokeLineSegments(ctx, points, 6);

}
*/
/*
- (void)drawRect:(CGRect)rect
{
    [super drawRect:(CGRect)rect];
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGFloat black[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    CGContextSetStrokeColor(c, black);
    CGContextBeginPath(c);
    if(normalXY.count > 0 && normalXY.count % 4 == 0)
    {
        for(int i =0; i < normalXY.count; i+=4)
        {
            CGContextMoveToPoint(c, [[normalXY objectAtIndex:i] CGPointValue].x, [[normalXY objectAtIndex:i] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+1] CGPointValue].x, [[normalXY objectAtIndex:i+1] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+2] CGPointValue].x, [[normalXY objectAtIndex:i+2] CGPointValue].y);
            CGContextAddLineToPoint(c, [[normalXY objectAtIndex:i+3] CGPointValue].x, [[normalXY objectAtIndex:i+3] CGPointValue].y);
        }
    }
    
    if(invertXY.count > 0 && invertXY.count % 6 == 0)
    {
        for(int i =0; i < invertXY.count; i+=6)
        {
            CGContextMoveToPoint(c, [[invertXY objectAtIndex:i] CGPointValue].x, [[invertXY objectAtIndex:i] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+1] CGPointValue].x, [[invertXY objectAtIndex:i+1] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+2] CGPointValue].x, [[invertXY objectAtIndex:i+2] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+3] CGPointValue].x, [[invertXY objectAtIndex:i+3] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+4] CGPointValue].x, [[invertXY objectAtIndex:i+4] CGPointValue].y);
            CGContextAddLineToPoint(c, [[invertXY objectAtIndex:i+5] CGPointValue].x, [[invertXY objectAtIndex:i+5] CGPointValue].y);
        }
    }
    CGContextStrokePath(c);
}
*/

@end
