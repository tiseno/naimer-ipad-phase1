//
//  KNEtblallCaseCell.h
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KNECase.h"
#import "SwitchMaindelegate.h"

@interface KNEtblallCaseCell : UITableViewCell{
    
}
@property (nonatomic, strong) id<SwitchMaindelegate> delegate;
@property (nonatomic, strong) KNECase *Case;
@property (nonatomic, strong) UILabel *lblCaseID;
@property (nonatomic, strong) UILabel *lblCaseName;

@property (nonatomic, strong) UIButton *btnSwitchEdit;
@property (nonatomic, strong) UIButton *btnSwitchPrint;
@property (nonatomic, strong) UIButton *btnSwitchDelete;

@property (nonatomic, strong) UILabel *lblCustNo;
@property (nonatomic, strong) UILabel *lblcompany;
@property (nonatomic, strong) UILabel *lblCreateDate;
@property (nonatomic, strong) UILabel *lblModifyDate;
@property (nonatomic, strong) UILabel *lblEmpty;

//Switchtype, Drawing No, No of Stages, Number of Position, Switching Angle, Customer Name, Created Date, Modified Date, Reference
@property (nonatomic, strong) UILabel *lblSwitchtype;
@property (nonatomic, strong) UILabel *lblNoOFStages;
@property (nonatomic, strong) UILabel *lblNoOFPosition;
@property (nonatomic, strong) UILabel *lblDrawingNo;
@property (nonatomic, strong) UILabel *lblNoSwitchingAngle;
@property (nonatomic, strong) UILabel *lblReference;

@property (nonatomic, strong) UILabel *lbltitleSwitchtype;
@property (nonatomic, strong) UILabel *lbltitleNoOFStages;
@property (nonatomic, strong) UILabel *lbltitleNoOFPosition;
@property (nonatomic, strong) UILabel *lbltitleDrawingNo;
@property (nonatomic, strong) UILabel *lbltitleNoSwitchingAngle;
@property (nonatomic, strong) UILabel *lbltitleReference;
@property (nonatomic, strong) UILabel *lbltitleCustNo;
@property (nonatomic, strong) UILabel *lbltitlecompany;
@property (nonatomic, strong) UILabel *lbltitleCreateDate;
@property (nonatomic, strong) UILabel *lbltitleModifyDate;
@end
