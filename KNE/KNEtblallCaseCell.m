//
//  KNEtblallCaseCell.m
//  KNE
//
//  Created by Tiseno Mac 2 on 10/22/12.
//
//

#import "KNEtblallCaseCell.h"

@implementation KNEtblallCaseCell
@synthesize lblCaseID, Case, lblCaseName;
@synthesize btnSwitchEdit, btnSwitchPrint, btnSwitchDelete;
@synthesize delegate;
@synthesize lblcompany, lblCreateDate, lblCustNo, lblModifyDate, lblEmpty;
@synthesize lblDrawingNo, lblNoOFPosition, lblNoOFStages, lblNoSwitchingAngle, lblReference, lblSwitchtype;
@synthesize lbltitlecompany, lbltitleCreateDate, lbltitleCustNo, lbltitleDrawingNo, lbltitleModifyDate, lbltitleNoOFPosition, lbltitleNoOFStages, lbltitleNoSwitchingAngle, lbltitleReference, lbltitleSwitchtype;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        /*==============Button===================*/
        UIImage *editButtonImage = [UIImage imageNamed:@"btn_edit.png"];
        UIButton *teditButton = [[UIButton alloc] initWithFrame:CGRectMake(655, 200, 50, 30)];
        [teditButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
        [teditButton addTarget:self action:@selector(edit_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        self.btnSwitchEdit = teditButton;
        [self addSubview:btnSwitchEdit];
        
        
        UIImage *emailButtonImage = [UIImage imageNamed:@"btn_export.png"];
        UIButton *temailbtn = [[UIButton alloc] initWithFrame:CGRectMake(713, 200, 50, 30)];
        [temailbtn setBackgroundImage:emailButtonImage forState:UIControlStateNormal];
        [temailbtn addTarget:self action:@selector(email_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        self.btnSwitchPrint = temailbtn;
        [self addSubview:btnSwitchPrint];
        
        UIImage *deleteButtonImage = [UIImage imageNamed:@"btn_email.png"];
        UIButton *tbtnSwitchDelete = [[UIButton alloc] initWithFrame:CGRectMake(700, 200, 50, 30)];
        [tbtnSwitchDelete setBackgroundImage:deleteButtonImage forState:UIControlStateNormal];
        [tbtnSwitchDelete addTarget:self action:@selector(delete_ButtonTapped:)
            forControlEvents:UIControlEventTouchDown];
        self.btnSwitchDelete = tbtnSwitchDelete;
        [self addSubview:btnSwitchDelete];
        

        /*
        UILabel *glblCaseID = [[UILabel alloc] initWithFrame:CGRectMake(150, 12, 170, 21)];
        glblCaseID.textAlignment = UITextAlignmentLeft;
        glblCaseID.textColor = [UIColor blackColor];
        glblCaseID.backgroundColor = [UIColor clearColor];
        self.lblCaseID = glblCaseID;
        [glblCaseID release];
        [self addSubview:lblCaseID];
        
         
        UILabel *glblCaseName = [[UILabel alloc] initWithFrame:CGRectMake(145, 12, 100, 21)];
        glblCaseName.textAlignment = UITextAlignmentLeft;
        glblCaseName.textColor = [UIColor blackColor];
        glblCaseName.backgroundColor = [UIColor clearColor];
        glblCaseName.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblCaseName = glblCaseName;
        [glblCaseName release];
        [self addSubview:lblCaseName];
        
        
        UILabel *glblcusno = [[UILabel alloc] initWithFrame:CGRectMake(278, 12, 100, 21)];
        glblcusno.textAlignment = UITextAlignmentLeft;
        glblcusno.textColor = [UIColor blackColor];
        glblcusno.backgroundColor = [UIColor clearColor];
         glblcusno.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblCustNo = glblcusno;
        [glblcusno release];
        [self addSubview:lblCustNo];
        */
        
        /*========row 1=========
        
        UILabel *glblSwitchtype = [[UILabel alloc] initWithFrame:CGRectMake(190-170, 12, 110, 21)];
        glblSwitchtype.textAlignment = UITextAlignmentLeft;
        glblSwitchtype.textColor = [UIColor blackColor];
        //glblSwitchtype.layer.borderColor=[UIColor blackColor].CGColor;
        //glblSwitchtype.layer.borderWidth = 1;
        glblSwitchtype.backgroundColor = [UIColor clearColor];
        glblSwitchtype.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblSwitchtype = glblSwitchtype;
        [glblSwitchtype release];
        [self addSubview:lblSwitchtype];
        
        UILabel *glblDrawingNo = [[UILabel alloc] initWithFrame:CGRectMake(320-170, 12, 110, 21)];
        glblDrawingNo.textAlignment = UITextAlignmentLeft;
        glblDrawingNo.textColor = [UIColor blackColor];
        //glblDrawingNo.layer.borderColor=[UIColor blackColor].CGColor;
        //glblDrawingNo.layer.borderWidth = 1;
        glblDrawingNo.backgroundColor = [UIColor clearColor];
        glblDrawingNo.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblDrawingNo = glblDrawingNo;
        [glblDrawingNo release];
        [self addSubview:lblDrawingNo];
        
        UILabel *glblNoOFStages = [[UILabel alloc] initWithFrame:CGRectMake(458-170, 12, 110, 21)];
        glblNoOFStages.textAlignment = UITextAlignmentLeft;
        glblNoOFStages.textColor = [UIColor blackColor];
        //glblNoOFStages.layer.borderColor=[UIColor blackColor].CGColor;
        //glblNoOFStages.layer.borderWidth = 1;
        glblNoOFStages.backgroundColor = [UIColor clearColor];
        glblNoOFStages.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoOFStages = glblNoOFStages;
        [glblNoOFStages release];
        [self addSubview:lblNoOFStages];
        
        UILabel *glblNoOFPosition = [[UILabel alloc] initWithFrame:CGRectMake(586-170, 12, 110, 21)];
        glblNoOFPosition.textAlignment = UITextAlignmentLeft;
        glblNoOFPosition.textColor = [UIColor blackColor];
        //glblNoOFPosition.layer.borderColor=[UIColor blackColor].CGColor;
        //glblNoOFPosition.layer.borderWidth = 1;
        glblNoOFPosition.backgroundColor = [UIColor clearColor];
        glblNoOFPosition.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoOFPosition = glblNoOFPosition;
        [glblNoOFPosition release];
        [self addSubview:lblNoOFPosition];
        ==*/
        /*========row 2==========
        
        
        UILabel *glblNoSwitchingAngle = [[UILabel alloc] initWithFrame:CGRectMake(190-170, 40, 110, 21)];
        glblNoSwitchingAngle.textAlignment = UITextAlignmentLeft;
        glblNoSwitchingAngle.textColor = [UIColor blackColor];
        glblNoSwitchingAngle.backgroundColor = [UIColor clearColor];
        glblNoSwitchingAngle.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoSwitchingAngle = glblNoSwitchingAngle;
        [glblNoSwitchingAngle release];
        [self addSubview:lblNoSwitchingAngle];
        
        
        UILabel *glblcompany = [[UILabel alloc] initWithFrame:CGRectMake(320-170, 40, 110, 21)];
        glblcompany.textAlignment = UITextAlignmentLeft;
        glblcompany.textColor = [UIColor blackColor];
        glblcompany.backgroundColor = [UIColor clearColor];
         glblcompany.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblcompany = glblcompany;
        [glblcompany release];
        [self addSubview:lblcompany];
        
        UILabel *glblcrdate = [[UILabel alloc] initWithFrame:CGRectMake(458-170, 40, 110, 21)];
        glblcrdate.textAlignment = UITextAlignmentLeft;
        glblcrdate.textColor = [UIColor blackColor];
        glblcrdate.backgroundColor = [UIColor clearColor];
         glblcrdate.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblCreateDate = glblcrdate;
        [glblcrdate release];
        [self addSubview:lblCreateDate];
        
        UILabel *glblmdate = [[UILabel alloc] initWithFrame:CGRectMake(586-170, 40, 110, 21)];
        glblmdate.textAlignment = UITextAlignmentLeft;
        glblmdate.textColor = [UIColor blackColor];
        glblmdate.backgroundColor = [UIColor clearColor];
        glblmdate.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblModifyDate = glblmdate;
        [glblmdate release];
        [self addSubview:lblModifyDate];
        
        UILabel *glblempty = [[UILabel alloc] initWithFrame:CGRectMake(586, 40, 110, 21)];
        glblempty.textAlignment = UITextAlignmentLeft;
        glblempty.textColor = [UIColor blackColor];
        glblempty.backgroundColor = [UIColor clearColor];
        glblempty.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblEmpty = glblempty;
        [glblempty release];
        [self addSubview:lblEmpty];
        =*/
        
        /*========row 3=========
        //UILabel *glblNoOFPosition = [[UILabel alloc] initWithFrame:CGRectMake(586, 12, 110, 21)];
        //UILabel *glblReference = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, 100, 21)];
        UILabel *glblReference = [[UILabel alloc] initWithFrame:CGRectMake(586, 12, 110, 21)];
        glblReference.textAlignment = UITextAlignmentLeft;
        glblReference.textColor = [UIColor blackColor];
        glblReference.backgroundColor = [UIColor clearColor];
        glblReference.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblReference = glblReference;
        [glblReference release];
        [self addSubview:lblReference];
        ==*/
        
        /*=============col 1 title=================*/
        
        UILabel *glbltitleSwitchtype = [[UILabel alloc] initWithFrame:CGRectMake(15, 30, 150, 21)];
        glbltitleSwitchtype.textAlignment = UITextAlignmentLeft;
        glbltitleSwitchtype.textColor = [UIColor blackColor];
//        glbltitleSwitchtype.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleSwitchtype.layer.borderWidth = 1;
        glbltitleSwitchtype.backgroundColor = [UIColor clearColor];
        glbltitleSwitchtype.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleSwitchtype.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleSwitchtype = glbltitleSwitchtype;
        [self addSubview:lbltitleSwitchtype];
        
        UILabel *glbltitleDrawingNo = [[UILabel alloc] initWithFrame:CGRectMake(15, 70, 150, 21)];
        glbltitleDrawingNo.textAlignment = UITextAlignmentLeft;
        glbltitleDrawingNo.textColor = [UIColor blackColor];
//        glbltitleDrawingNo.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleDrawingNo.layer.borderWidth = 1;
        glbltitleDrawingNo.backgroundColor = [UIColor clearColor];
        glbltitleDrawingNo.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleDrawingNo.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleDrawingNo = glbltitleDrawingNo;
        [self addSubview:lbltitleDrawingNo];
        
        UILabel *glbltitleNoOFStages = [[UILabel alloc] initWithFrame:CGRectMake(15, 110, 150, 21)];
        glbltitleNoOFStages.textAlignment = UITextAlignmentLeft;
        glbltitleNoOFStages.textColor = [UIColor blackColor];
//        glbltitleNoOFStages.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleNoOFStages.layer.borderWidth = 1;
        glbltitleNoOFStages.backgroundColor = [UIColor clearColor];
        glbltitleNoOFStages.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleNoOFStages.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleNoOFStages = glbltitleNoOFStages;
        [self addSubview:lbltitleNoOFStages];
        
        UILabel *glbltitleNoSwitchingAngle = [[UILabel alloc] initWithFrame:CGRectMake(15, 150, 150, 21)];
        glbltitleNoSwitchingAngle.textAlignment = UITextAlignmentLeft;
        glbltitleNoSwitchingAngle.textColor = [UIColor blackColor];
//        glbltitleNoSwitchingAngle.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleNoSwitchingAngle.layer.borderWidth = 1;
        glbltitleNoSwitchingAngle.backgroundColor = [UIColor clearColor];
        glbltitleNoSwitchingAngle.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleNoSwitchingAngle.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleNoSwitchingAngle = glbltitleNoSwitchingAngle;
        [self addSubview:lbltitleNoSwitchingAngle];
        
        UILabel *glbltitlecompany = [[UILabel alloc] initWithFrame:CGRectMake(15, 190, 150, 21)];
        glbltitlecompany.textAlignment = UITextAlignmentLeft;
        glbltitlecompany.textColor = [UIColor blackColor];
//        glbltitlecompany.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitlecompany.layer.borderWidth = 1;
        glbltitlecompany.backgroundColor = [UIColor clearColor];
        glbltitlecompany.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitlecompany.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitlecompany = glbltitlecompany;
        [self addSubview:lbltitlecompany];
        
        /*=============col 1=================*/
        
        UILabel *glblSwitchtype = [[UILabel alloc] initWithFrame:CGRectMake(165, 30, 150, 21)];
        glblSwitchtype.textAlignment = UITextAlignmentCenter;
        glblSwitchtype.textColor = [UIColor blackColor];
//        glblSwitchtype.layer.borderColor=[UIColor blackColor].CGColor;
//        glblSwitchtype.layer.borderWidth = 1;
        glblSwitchtype.backgroundColor = [UIColor clearColor];
        glblSwitchtype.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblSwitchtype = glblSwitchtype;
        [self addSubview:lblSwitchtype];
        
        UILabel *glblDrawingNo = [[UILabel alloc] initWithFrame:CGRectMake(165, 70, 150, 21)];
        glblDrawingNo.textAlignment = UITextAlignmentCenter;
        glblDrawingNo.textColor = [UIColor blackColor];
//        glblDrawingNo.layer.borderColor=[UIColor blackColor].CGColor;
//        glblDrawingNo.layer.borderWidth = 1;
        glblDrawingNo.backgroundColor = [UIColor clearColor];
        glblDrawingNo.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblDrawingNo = glblDrawingNo;
        [self addSubview:lblDrawingNo];
        
        UILabel *glblNoOFStages = [[UILabel alloc] initWithFrame:CGRectMake(165, 110, 150, 21)];
        glblNoOFStages.textAlignment = UITextAlignmentCenter;
        glblNoOFStages.textColor = [UIColor blackColor];
//        glblNoOFStages.layer.borderColor=[UIColor blackColor].CGColor;
//        glblNoOFStages.layer.borderWidth = 1;
        glblNoOFStages.backgroundColor = [UIColor clearColor];
        glblNoOFStages.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoOFStages = glblNoOFStages;
        [self addSubview:lblNoOFStages];
        
        UILabel *glblNoSwitchingAngle = [[UILabel alloc] initWithFrame:CGRectMake(165, 150, 150, 21)];
        glblNoSwitchingAngle.textAlignment = UITextAlignmentCenter;
        glblNoSwitchingAngle.textColor = [UIColor blackColor];
        glblNoSwitchingAngle.backgroundColor = [UIColor clearColor];
        glblNoSwitchingAngle.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoSwitchingAngle = glblNoSwitchingAngle;
        [self addSubview:lblNoSwitchingAngle];

        UILabel *glblcompany = [[UILabel alloc] initWithFrame:CGRectMake(165, 190, 150, 21)];
        glblcompany.textAlignment = UITextAlignmentCenter;
        glblcompany.textColor = [UIColor blackColor];
        glblcompany.backgroundColor = [UIColor clearColor];
        glblcompany.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblcompany = glblcompany;
        [self addSubview:lblcompany];
        
        /*=============col 2 title=================*/
        
        UILabel *glbltitleNoOFPosition = [[UILabel alloc] initWithFrame:CGRectMake(330, 30, 150, 21)];
        glbltitleNoOFPosition.textAlignment = UITextAlignmentLeft;
        glbltitleNoOFPosition.textColor = [UIColor blackColor];
//        glbltitleNoOFPosition.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleNoOFPosition.layer.borderWidth = 1;
        glbltitleNoOFPosition.backgroundColor = [UIColor clearColor];
        glbltitleNoOFPosition.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleNoOFPosition.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleNoOFPosition = glbltitleNoOFPosition;
        [self addSubview:lbltitleNoOFPosition];
        
        UILabel *glbltitleReference = [[UILabel alloc] initWithFrame:CGRectMake(330, 70, 150, 21)];
        glbltitleReference.textAlignment = UITextAlignmentLeft;
        glbltitleReference.textColor = [UIColor blackColor];
//        glbltitleReference.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleReference.layer.borderWidth = 1;
        glbltitleReference.backgroundColor = [UIColor clearColor];
        glbltitleReference.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleReference.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleReference = glbltitleReference;
        [self addSubview:lbltitleReference];
        
        UILabel *glbltitleCreateDate = [[UILabel alloc] initWithFrame:CGRectMake(330, 110, 150, 21)];
        glbltitleCreateDate.textAlignment = UITextAlignmentLeft;
        glbltitleCreateDate.textColor = [UIColor blackColor];
//        glbltitleCreateDate.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleCreateDate.layer.borderWidth = 1;
        glbltitleCreateDate.backgroundColor = [UIColor clearColor];
        glbltitleCreateDate.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleCreateDate.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleCreateDate = glbltitleCreateDate;
        [self addSubview:lbltitleCreateDate];
        
        UILabel *glbltitleModifyDate = [[UILabel alloc] initWithFrame:CGRectMake(330, 150, 150, 21)];
        glbltitleModifyDate.textAlignment = UITextAlignmentLeft;
        glbltitleModifyDate.textColor = [UIColor blackColor];
//        glbltitleModifyDate.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltitleModifyDate.layer.borderWidth = 1;
        glbltitleModifyDate.backgroundColor = [UIColor clearColor];
        glbltitleModifyDate.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltitleModifyDate.font = [UIFont boldSystemFontOfSize:14];
        self.lbltitleModifyDate = glbltitleModifyDate;
        [self addSubview:lbltitleModifyDate];
        
         /*=============col 2=================*/
        UILabel *glblNoOFPosition = [[UILabel alloc] initWithFrame:CGRectMake(480, 30, 150, 21)];
        glblNoOFPosition.textAlignment = UITextAlignmentCenter;
        glblNoOFPosition.textColor = [UIColor blackColor];
//        glblNoOFPosition.layer.borderColor=[UIColor blackColor].CGColor;
//        glblNoOFPosition.layer.borderWidth = 1;
        glblNoOFPosition.backgroundColor = [UIColor clearColor];
        glblNoOFPosition.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblNoOFPosition = glblNoOFPosition;
        [self addSubview:lblNoOFPosition];
        
        UILabel *glblReference = [[UILabel alloc] initWithFrame:CGRectMake(480, 70, 150, 21)];
        glblReference.textAlignment = UITextAlignmentCenter;
        glblReference.textColor = [UIColor blackColor];
        glblReference.backgroundColor = [UIColor clearColor];
        glblReference.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblReference = glblReference;
        [self addSubview:lblReference];
        
        UILabel *glblcrdate = [[UILabel alloc] initWithFrame:CGRectMake(480, 110, 150, 21)];
        glblcrdate.textAlignment = UITextAlignmentCenter;
        glblcrdate.textColor = [UIColor blackColor];
        glblcrdate.backgroundColor = [UIColor clearColor];
        glblcrdate.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblCreateDate = glblcrdate;

        [self addSubview:lblCreateDate];
        
        UILabel *glblmdate = [[UILabel alloc] initWithFrame:CGRectMake(480, 150, 150, 21)];
        glblmdate.textAlignment = UITextAlignmentCenter;
        glblmdate.textColor = [UIColor blackColor];
        glblmdate.backgroundColor = [UIColor clearColor];
//        glblmdate.layer.borderColor=[UIColor blackColor].CGColor;
//        glblmdate.layer.borderWidth = 1;
        glblmdate.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.lblModifyDate = glblmdate;

        [self addSubview:lblModifyDate];
        
    }
    return self;
}  

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)edit_ButtonTapped:(id)sender
{
    //NSLog(@"EditCase!!");

    [self.delegate EditCaseTapped:Case];
}

-(IBAction)email_ButtonTapped:(id)sender
{
    //NSLog(@"email!!");
    
    [self.delegate emailCaseTapped:Case];
}

-(IBAction)delete_ButtonTapped:(id)sender
{
    //NSLog(@"delete!!");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Switch" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 1)
    {
        [self.delegate DeleteCaseTapped:Case];
    }
    
}



@end
