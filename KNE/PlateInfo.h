//
//  PlateInfo.h
//  KNE
//
//  Created by Jermin Bazazian on 1/21/13.
//
//

#import <Foundation/Foundation.h>

@interface PlateInfo : NSObject
{
    
}

@property (nonatomic, strong) NSString *desc1, *desc2, *desc3, *desc4, *desc5, *desc6, *desc7, *desc8, *desc9, *desc10, *desc11, *desc12, *desc13, *desc14, *title1, *title2, *title3, *reference, *company, *customerNo, *desc15, *desc16;

@end
