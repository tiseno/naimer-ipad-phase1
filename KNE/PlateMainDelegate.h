//
//  PlateMainDelegate.h
//  KNE
//
//  Created by Jermin Bazazian on 12/26/12.
//
//

#ifndef KNE_PlateMainDelegate_h
#define KNE_PlateMainDelegate_h



#endif

@protocol PlateMainDelegate <NSObject>

-(void)EditPlateTapped:(NSString*) PlateID;
-(void)emailPlateTapped:(NSString*) PlateID;

@end
