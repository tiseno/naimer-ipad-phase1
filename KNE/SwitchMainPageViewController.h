//
//  SwitchMainPageViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/24/12.
//
//

#import <UIKit/UIKit.h>
#import "tblSwitchMainPageViewController.h"
#import "DBCase.h"
#import "KNEEditSwitchCaseViewController.h"
#import "KNECase.h"
#import "KNEMenuPageViewController.h"
#import "MBProgressHUD.h"

@interface SwitchMainPageViewController : UIViewController<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    //NSArray *originalListOfItems;
    //NSMutableArray *copyListOfItems;
}
@property (strong, nonatomic) NSMutableArray *thecopyListOfItemss;
@property (strong, nonatomic) NSArray *originalListOfItems;
@property (strong, nonatomic) IBOutlet UITableView *tblSwitchMainPage;
@property (nonatomic, strong) tblSwitchMainPageViewController *gtblSwitchMainPageViewController;
-(IBAction)newCaseTapped:(id)sender;
-(IBAction)RefreshTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) SwitchMainPageViewController *gBpNoPersonViewController;
@end
