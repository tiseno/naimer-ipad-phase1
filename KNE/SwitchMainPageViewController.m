//
//  SwitchMainPageViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/24/12.
//
//

#import "SwitchMainPageViewController.h"


@implementation SwitchMainPageViewController
@synthesize tblSwitchMainPage, gtblSwitchMainPageViewController;
@synthesize searchbar;
@synthesize thecopyListOfItemss, originalListOfItems, gBpNoPersonViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        [self.tblSwitchMainPage reloadData];
    }
    
    return self;
}


-(void) DoneTapped:(id)gesture
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    [self.view removeFromSuperview];
    [self.navigationController dismissModalViewControllerAnimated:YES];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    gBpNoPersonViewController=[[SwitchMainPageViewController alloc] initWithNibName:@"SwitchMainPageViewController" bundle:nil];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    gBpNoPersonViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];

    
    
    self.thecopyListOfItemss = [[NSMutableArray alloc] init];
    self.originalListOfItems = [[NSArray alloc] init];
    
    //self.searchbar.layer.borderColor =color.CGColor;
    //self.searchbar.layer.borderWidth = 1;
    self.searchbar.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if(gtblSwitchMainPageViewController == nil)
    {
        tblSwitchMainPageViewController *ggtblSwitchMainPageViewController = [[tblSwitchMainPageViewController alloc] init];
        self.gtblSwitchMainPageViewController = ggtblSwitchMainPageViewController;
        self.gtblSwitchMainPageViewController.gSwitchMainPageViewController=self;
        self.originalListOfItems=self.gtblSwitchMainPageViewController.CaseArray;

    }
     
    [tblSwitchMainPage setDataSource:self.gtblSwitchMainPageViewController];
    [tblSwitchMainPage setDelegate:self.gtblSwitchMainPageViewController];
    self.tblSwitchMainPage.rowHeight=240;
    self.tblSwitchMainPage.layer.borderWidth = 0.8;
    UIColor *color = [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
    self.tblSwitchMainPage.layer.borderColor = color.CGColor;
    
    self.gtblSwitchMainPageViewController.view = self.gtblSwitchMainPageViewController.tableView;
    //self.gBpEventTableViewController.view = self.gBpEventTableViewController.view;
    
    //[self.tblSwitchMainPage reloadData];
    NSLog(@"hi");
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[self tblSwitchMainPage] reloadData];
    
    [gtblSwitchMainPageViewController initwithCase];

    
    [self.tblSwitchMainPage reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidUnload
{
    [self setTblSwitchMainPage:nil];
    
    [self setSearchbar:nil];
    
    [super viewDidUnload];
}



-(void)loadingViewscreen
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)DeleteCaseTapped:(KNECase*)gcase
{
    //NSLog(@"deletecase~%@",gcase.CaseID);
    [self loadingViewscreen];

    DBCase *gdbcase = [[DBCase alloc] init];
    [gdbcase deletselectedCase:gcase]; 
    
    
    
    [self performSelector:@selector(getdeletecase:) withObject:gcase afterDelay:1];
}

-(void)getdeletecase:(KNECase*)gcase
{
    [self.gtblSwitchMainPageViewController initwithCase];
    
    [self.tblSwitchMainPage reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)EditCaseTapped:(KNECase*)gcase
{
    //NSLog(@"editcase~");
    /*
    NSLog(@"gcase.pcs8>>>>%@",gcase.pcs1);
    NSLog(@"gcase.pcs8>>>>%@",gcase.pcs2);
    NSLog(@"gcase.pcs8>>>>%@",gcase.pcs3);
    NSLog(@"gcase.pcs8>>>>%@",gcase.pcs4);
    */
    
    [self loadingViewscreen];
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    DBSwitchingfirstValue *gDBSwitchingfirstValue=[[DBSwitchingfirstValue alloc] init];
    NSArray *gDBSwitchingfirstValueAr =[gDBSwitchingfirstValue selectSwitchingFirstValueItem:gcase];
    
    for (KNESwitchingFirstValue *gitem in gDBSwitchingfirstValueAr) {
        appDelegate.firstPnum=gitem.SwitchingFirstValue;
    }
    appDelegate.gKNECase=gcase;
    
    [self performSelector:@selector(geteditTapped:) withObject:gcase afterDelay:1];
    
}

-(void)geteditTapped:(KNECase*)gcase
{
    DBCase *gdbcase = [[DBCase alloc] init];
    
    NSArray *caseinfoArr = [gdbcase selectCaseItem:gcase];
    
    KNEEditSwitchCaseViewController *gKNEEditSwitchCaseViewController=[[KNEEditSwitchCaseViewController alloc] initWithNibName:@"KNEEditSwitchCaseViewController" bundle:nil];
    //gKNEEditSwitchCaseViewController.KNECaseclass=gcase;
    
    for (KNECase *item in caseinfoArr)
    {
        gKNEEditSwitchCaseViewController.KNECaseclass=item;
    }
    
    /*
    CATransition* transition = [CATransition animation];
    transition.duration = 0.2;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromTop;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];*/
    
    //[self.navigationController pushViewController:gridController animated:NO];

    [self.navigationController pushViewController:gKNEEditSwitchCaseViewController animated:YES];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

}

-(void)emailCaseTapped:(KNECase*)gcase
{
    [self loadingViewscreen];
    [self performSelector:@selector(getemailTapped:) withObject:gcase afterDelay:1];
}

-(void)getemailTapped:(KNECase*)gcase
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    DBSwitchingfirstValue *gDBSwitchingfirstValue=[[DBSwitchingfirstValue alloc] init];
    
    NSArray *gDBSwitchingfirstValueAr =[gDBSwitchingfirstValue selectSwitchingFirstValueItem:gcase];
    
    for (KNESwitchingFirstValue *gitem in gDBSwitchingfirstValueAr)
    {
        appDelegate.firstPnum=gitem.SwitchingFirstValue;
    }
    
    appDelegate.gKNECase=gcase;
    
    KNESwitchShowViewController *gKNESwitchShowViewController = [[KNESwitchShowViewController alloc] initWithNibName:@"KNESwitchShowViewController" bundle:nil];
    
    gKNESwitchShowViewController.KNECaseclass = gcase;
    
    [self.navigationController pushViewController:gKNESwitchShowViewController animated:YES];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}

-(IBAction)newCaseTapped:(id)sender
{
//    for(UIView* uiview in self.view.subviews)
//    {
//        [uiview removeFromSuperview];
//    }
    //[self.view removeFromSuperview];
    //[self performSelector:@selector(KNSplash) withObject:nil afterDelay:1];
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    KNEStartBallViewController *gtBpMainViewController = [[KNEStartBallViewController alloc] initWithNibName:@"KNEStartBallViewController"  bundle:nil];
    
    appDelegate.firstPnum = @"0";
    
       [self.navigationController pushViewController:gtBpMainViewController animated:YES];
    //
    //    [gtBpMainViewController release];
    //[self.view addSubview:gtBpMainViewController.view];
    
}
-(void)KNSplash
{
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    KNEStartBallViewController *gtBpMainViewController = [[KNEStartBallViewController alloc] initWithNibName:@"KNEStartBallViewController"  bundle:nil];
    
    appDelegate.firstPnum = @"0";
    
    //    [self.navigationController pushViewController:gtBpMainViewController animated:YES];
    //
    //    [gtBpMainViewController release];
    //[self.view addSubview:gtBpMainViewController.view];
    [self presentModalViewController:gtBpMainViewController animated:YES];
}

-(IBAction)RefreshTapped:(id)sender
{
    [self.tblSwitchMainPage reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchbar resignFirstResponder];
    
    [self searchTableView];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [thecopyListOfItemss removeAllObjects];
    
    if([searchText length] > 0)
    {
        [self searchTableView];
    }
    else
    {
        //NSLog(@"here here");
        
        gtblSwitchMainPageViewController.CaseArray = originalListOfItems;
    }
    
    [self.tblSwitchMainPage reloadData];
}

- (void) searchTableView
{
    NSString *searchText = searchbar.text;
    NSMutableArray *searchArray = [[NSMutableArray alloc] init];
    
    for(KNECase *ep in originalListOfItems)
    {
        //NSLog(@"id ... %d", ep.PlateID);
        //NSLog(@"name ... %@", ep.name);
        
        //NSLog(@"... %@", ep.CaseName);
        
        [searchArray addObject:ep];
    }
    
    for(KNECase *cs in searchArray)
    {
        bool addToArray = false;
        
        NSString *sTemp1 = cs.CaseName;
                
        NSRange titleResultRange1 = [sTemp1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
        if(titleResultRange1.length > 0)
        {
            //[copyListOfItems addObject:ep];
            addToArray = true;
        }
        
        if(addToArray == false)
        {
            NSString *sTemp2 = cs.FCustNo;
            
            NSRange titleResultRange2 = [sTemp2 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange2.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp3 = cs.FCompany;
            
            NSRange titleResultRange3 = [sTemp3 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange3.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp4 = cs.SwitchType;
            
            NSRange titleResultRange4 = [sTemp4 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange4.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp5 = cs.Program;
            
            NSRange titleResultRange5 = [sTemp5 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange5.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp6 = cs.SwitchingAngle;
            
            NSRange titleResultRange6 = [sTemp6 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange6.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp7 = cs.numberOfPosition;
            
            NSRange titleResultRange7 = [sTemp7 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange7.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp8 = cs.FNofStage;
            
            NSRange titleResultRange8 = [sTemp8 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange8.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray)
        {
            [thecopyListOfItemss addObject:cs];
        }
    }
    
    gtblSwitchMainPageViewController.CaseArray = thecopyListOfItemss;
    
    
    searchArray = nil;
}

@end
