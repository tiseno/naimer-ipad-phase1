//
//  SwitchMaindelegate.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/24/12.
//
//

#ifndef KNE_SwitchMaindelegate_h
#define KNE_SwitchMaindelegate_h



#endif

@protocol SwitchMaindelegate <NSObject>

-(void)EditCaseTapped:(KNECase*)gcase;
-(void)emailCaseTapped:(KNECase*)gcase;
-(void)DeleteCaseTapped:(KNECase*)gcase;

@end
