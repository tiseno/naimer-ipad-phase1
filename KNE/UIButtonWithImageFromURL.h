//
//  UIButtonWithImageFromURL.h
//  KNE
//
//  Created by Tiseno Mac 2 on 9/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface UIButtonWithImageFromURL : UIButton<AsyncImageViewDelegate>{
    
}

@end
