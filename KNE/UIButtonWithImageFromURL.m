//
//  UIButtonWithImageFromURL.m
//  KNE
//
//  Created by Tiseno Mac 2 on 9/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIButtonWithImageFromURL.h"

@implementation UIButtonWithImageFromURL

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)handleRecieveImage:(UIImage*)image sender:(AsyncImageView*)asyncImageView
{
    [self setImage:image forState:UIControlStateNormal];
}
@end
