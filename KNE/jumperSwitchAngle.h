//
//  jumperSwitchAngle.h
//  KNE
//
//  Created by Tiseno Mac 2 on 1/9/13.
//
//

#import <Foundation/Foundation.h>

@interface jumperSwitchAngle : NSObject{
    
}

@property (nonatomic, strong) NSString *switch1;
@property (nonatomic, strong) NSString *switch2;
@property (nonatomic, strong) NSString *switch3;
@property (nonatomic, strong) NSString *switch4;
@property (nonatomic, strong) NSString *switch5;
@property (nonatomic, strong) NSString *switch6;
@property (nonatomic, strong) NSString *switch7;
@property (nonatomic, strong) NSString *switch8;
@property (nonatomic, strong) NSString *switch9;
@property (nonatomic, strong) NSString *switch10;

@property (nonatomic, strong) NSString *switch11;
@property (nonatomic, strong) NSString *switch12;
@property (nonatomic, strong) NSString *switch13;
@property (nonatomic, strong) NSString *switch14;
@property (nonatomic, strong) NSString *switch15;
@property (nonatomic, strong) NSString *switch16;
@property (nonatomic, strong) NSString *switch17;
@property (nonatomic, strong) NSString *switch18;
@property (nonatomic, strong) NSString *switch19;
@property (nonatomic, strong) NSString *switch20;

@property (nonatomic, strong) NSString *switch21;
@property (nonatomic, strong) NSString *switch22;
@property (nonatomic, strong) NSString *switch23;
@property (nonatomic, strong) NSString *switch24;

@end
