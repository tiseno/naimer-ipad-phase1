//
//  EscutheonPlate.h
//  KNE
//
//  Created by Jermin Bazazian on 12/18/12.
//
//

#import <Foundation/Foundation.h>

@interface EscutheonPlate : NSObject
{

}

@property (nonatomic) int PlateID;
@property (nonatomic, strong) NSString *desc1, *desc2, *desc3, *desc4, *desc5, *desc6, *desc7, *desc8, *desc9, *desc10, *desc11, *desc12, *desc13, *desc14, *title1, *title2, *title3, *reference, *company, *modified, *created, *customerNo, *header, *name, *headerF, *NoOfPosition, *desc15, *desc16;

@end 
