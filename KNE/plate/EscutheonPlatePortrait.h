//
//  EscutheonPlatePortrait.h
//  KNE
//
//  Created by Jermin Bazazian on 12/28/12.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface EscutheonPlatePortrait : UIViewController<UITextFieldDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate>
{
    int heightOfEditedView;
    int heightOffset;
}

@property(strong, nonatomic) NSString *option;
@property int PlateID;
@property (strong, nonatomic) IBOutlet UITextView *txtViewDesc1, *txtViewDesc2, *txtViewDesc3, *txtViewDesc4, *txtViewDesc5, *txtViewDesc6, *txtViewDesc7, *txtViewDesc8, *txtViewDesc9, *txtViewDesc10, *txtViewDesc11, *txtViewDesc12, *txtViewDesc13, *txtViewDesc14;
@property (strong, nonatomic) IBOutlet UITextField *txtReference, *txtCreated, *txtCompany, *txtModified, *txtCustomerNo;
@property (strong, nonatomic) IBOutlet UITextView *txtViewTitle1, *txtViewTitle2, *txtViewTitle3;
@property (strong, nonatomic) IBOutlet UITextField *txtHeader1, *txtHeader2, *txtHeader3, *txtHeader4, *txtHeader5, *txtHeader6;

@end
