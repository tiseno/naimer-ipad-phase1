//
//  EscutheonPlateWindow.h
//  KNE
//
//  Created by Jermin Bazazian on 12/15/12.
//
//

#import <UIKit/UIKit.h>
#import "tblPlateMainPageViewController.h"
#import "MBProgressHUD.h"

@interface EscutheonPlateWindow : UIViewController<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
{
    
    
}
@property (nonatomic, strong) NSMutableArray *originalListOfItems;
@property (nonatomic, strong) NSMutableArray *thiscopyListOfItems;
@property (strong, nonatomic) IBOutlet UITableView *tblPlateMainPage;
@property (nonatomic, strong) tblPlateMainPageViewController *gtblPlateMainPageViewController;

//-(IBAction)RefreshTapped:(id)sender;
-(IBAction)newPlate:(id)sender;

@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;

@end
