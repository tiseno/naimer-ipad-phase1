//
//  EscutheonPlateWindow.m
//  KNE
//
//  Created by Jermin Bazazian on 12/15/12.
//
//

#import "EscutheonPlateWindow.h"
#import "NewEscutheonPlate.h"
#import "KNEFirstScreenViewController.h"
#import "DatabaseAction.h"
#import "EscutheonPlate.h"
#import "EscutheonPlatePortrait.h"

@implementation EscutheonPlateWindow

@synthesize tblPlateMainPage, gtblPlateMainPageViewController, searchbar;
@synthesize thiscopyListOfItems, originalListOfItems;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        [self.tblPlateMainPage reloadData];
    }
    
    return self;
}

-(void) DoneTapped:(id)gesture
{
    
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    [self.view removeFromSuperview];
    [self.navigationController dismissModalViewControllerAnimated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[self tblSwitchMainPage] reloadData];
    
    [self.gtblPlateMainPageViewController initwithCase];
    
    
    [self.tblPlateMainPage reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    EscutheonPlateWindow *gEscutheonPlateWindow=[[EscutheonPlateWindow alloc] initWithNibName:@"EscutheonPlateWindow" bundle:nil];
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    gEscutheonPlateWindow.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    //Initialize the copy array.
    self.thiscopyListOfItems = [[NSMutableArray alloc] init];
    self.originalListOfItems = [[NSMutableArray alloc] init];
    
    //self.searchbar.layer.borderColor = [[UIColor whiteColor] CGColor];
    //self.searchbar.layer.borderWidth = 1;
    
    self.searchbar.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if(gtblPlateMainPageViewController == nil)
    {
        tblPlateMainPageViewController *ggtblPlateMainPageViewController = [[tblPlateMainPageViewController alloc] init];
        
        self.gtblPlateMainPageViewController = ggtblPlateMainPageViewController;
        self.gtblPlateMainPageViewController.gPlateMainPageViewController = self;
        self.originalListOfItems = self.gtblPlateMainPageViewController.PlateArray;
        
    }
    
      [tblPlateMainPage setDataSource:self.gtblPlateMainPageViewController];
//    [tblPlateMainPage setDelegate:self.gtblPlateMainPageViewController];
    tblPlateMainPage.layer.borderWidth = 0.8;
    tblPlateMainPage.rowHeight=135;
    UIColor *color = [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0];
    tblPlateMainPage.layer.borderColor = color.CGColor;
    
    self.gtblPlateMainPageViewController.view = self.gtblPlateMainPageViewController.tableView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setTblPlateMainPage:nil];
    
    [super viewDidUnload];
}

/* change orientation start */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (BOOL)shouldAutorotate
{
    return YES;
    
    //return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    //NSLog(@"ragnarok 1 ... ");
    
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

/* change orientation end*/

-(IBAction)newPlate:(id)sender
{
    EscutheonPlatePortrait *gtBpMainViewController = [[EscutheonPlatePortrait alloc] initWithNibName:@"EscutheonPlatePortrait" bundle:nil];
    
    [self.navigationController pushViewController:gtBpMainViewController animated:YES];
    
}

-(void)EditPlateTapped:(NSString*) PlateID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self performSelector:@selector(editPlateTappedDelay:) withObject:PlateID afterDelay:1];

}

-(void)emailPlateTapped:(NSString*) PlateID
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self performSelector:@selector(emailPlateTappedDelay:) withObject:PlateID afterDelay:1];
    
}

-(void) editPlateTappedDelay:(NSString*) PlateID
{
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    int x = [PlateID intValue];
    NSArray *epArr = [da retrievePlate:x];
    
    EscutheonPlate *ep = [epArr objectAtIndex:0];
    
    EscutheonPlatePortrait *gKNEEditSwitchCaseViewController = [[EscutheonPlatePortrait alloc] initWithNibName:@"EscutheonPlatePortrait" bundle:nil];
    gKNEEditSwitchCaseViewController.option = @"edit";
    gKNEEditSwitchCaseViewController.PlateID = ep.PlateID;
    
    [self.navigationController pushViewController:gKNEEditSwitchCaseViewController animated:YES];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void) emailPlateTappedDelay:(NSString*) PlateID
{
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    int x = [PlateID intValue];
    NSArray *epArr = [da retrievePlate:x];
    
    EscutheonPlate *ep = [epArr objectAtIndex:0];
    
    EscutheonPlatePortrait *gKNEEditSwitchCaseViewController = [[EscutheonPlatePortrait alloc] initWithNibName:@"EscutheonPlatePortrait" bundle:nil];
    gKNEEditSwitchCaseViewController.option = @"email";
    gKNEEditSwitchCaseViewController.PlateID = ep.PlateID;
    
    [self.navigationController pushViewController:gKNEEditSwitchCaseViewController animated:YES];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


/* SearchBar Delegate Methods start */

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchbar resignFirstResponder];
    
    [self searchTableView];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [thiscopyListOfItems removeAllObjects];
    
    if([searchText length] > 0)
    {
        [self searchTableView];
    }
    else
    {
        //NSLog(@"here here");
        
        gtblPlateMainPageViewController.PlateArray = originalListOfItems;
    }
    
    [self.tblPlateMainPage reloadData];
}

- (void) searchTableView
{
    NSString *searchText = searchbar.text;
    NSMutableArray *searchArray = [[NSMutableArray alloc] init];
    
    for(EscutheonPlate *ep in originalListOfItems)
    {
        //NSLog(@"id ... %d", ep.PlateID);
        //NSLog(@"name ... %@", ep.name);
        
        [searchArray addObject:ep];
    }
    
    for(EscutheonPlate *ep in searchArray)
    {
        bool addToArray = false;
        
        NSString *sTemp1 = ep.name;
        
        NSRange titleResultRange1 = [sTemp1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
        if(titleResultRange1.length > 0)
        {
            //[copyListOfItems addObject:ep];
            addToArray = true;
        }
        
        if(addToArray == false)
        {
            NSString *sTemp2 = ep.customerNo;
        
            NSRange titleResultRange2 = [sTemp2 rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
            if(titleResultRange2.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp3 = ep.company;
            
            NSRange titleResultRange3 = [sTemp3 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange3.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp4 = ep.headerF;
            
            NSRange titleResultRange4 = [sTemp4 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange4.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray == false)
        {
            NSString *sTemp5 = ep.NoOfPosition;
            
            NSRange titleResultRange5 = [sTemp5 rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(titleResultRange5.length > 0)
            {
                //[copyListOfItems addObject:ep];
                addToArray = true;
            }
        }
        
        if(addToArray)
        {
            [thiscopyListOfItems addObject:ep];
        }
    }
    
    gtblPlateMainPageViewController.PlateArray = thiscopyListOfItems;
    
    
    searchArray = nil;
}

/* SearchBar Delegate Method ends */

@end
