//
//  NewEscutheonPlate.m
//  KNE
//
//  Created by Jermin Bazazian on 12/15/12.
//
//

#import "NewEscutheonPlate.h"
#import "DatabaseAction.h"
#import <QuartzCore/QuartzCore.h>

@interface NewEscutheonPlate ()

@end

@implementation NewEscutheonPlate

@synthesize option, PlateID;
@synthesize txtViewDesc1, txtViewDesc2, txtViewDesc3, txtViewDesc4, txtViewDesc5, txtViewDesc6, txtViewDesc7, txtViewDesc8, txtViewDesc9, txtViewDesc10, txtViewDesc11, txtViewDesc12, txtViewDesc13, txtViewDesc14;
@synthesize txtReference, txtCompany, txtCreated, txtCustomerNo, txtModified;
@synthesize txtViewTitle1, txtViewTitle2, txtViewTitle3;
@synthesize txtHeader1, txtHeader2, txtHeader3, txtHeader4, txtHeader5, txtHeader6;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    /*self.view.transform = CGAffineTransformIdentity;
    self.view.transform = CGAffineTransformMakeRotation((M_PI * (90) / 180.0));
    self.view.bounds = CGRectMake(5.0, -10.0, 1024, 748);*/
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    NewEscutheonPlate *gKNEEditSwitchCaseViewController = [[NewEscutheonPlate alloc] initWithNibName:@"NewEscutheonPlate" bundle:nil];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_done.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: rightButton];
    
    gKNEEditSwitchCaseViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    
    
    //add a new back button here
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 62, 33);
    [leftButton addTarget:self action:@selector(BackTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    gKNEEditSwitchCaseViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    
    
    
    if([option isEqualToString:@"edit"])
    {
        [self initializePlate];
    }
    else if([option isEqualToString:@"email"])
    {
        [self initializePlate];
        
        [self disableInput];
    }
    else
    {
    
    }
}

/* change orientation start */

- (BOOL)shouldAutorotate
{
    return YES;
    
    //return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight | UIInterfaceOrientationMaskLandscapeLeft;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight | UIInterfaceOrientationLandscapeLeft;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

/* change orientation end */

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

-(void) initializePlate
{
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    NSArray *plateDetails = [da retrievePlate: PlateID];
    
    EscutheonPlate *ep = [plateDetails objectAtIndex:0];
    
    txtViewDesc1.text = ep.desc1;
    txtViewDesc2.text = ep.desc2;
    txtViewDesc3.text = ep.desc3;
    txtViewDesc4.text = ep.desc4;
    txtViewDesc5.text = ep.desc5;
    txtViewDesc6.text = ep.desc6;
    txtViewDesc7.text = ep.desc7;
    txtViewDesc8.text = ep.desc8;
    txtViewDesc9.text = ep.desc9;
    txtViewDesc10.text = ep.desc10;
    txtViewDesc11.text = ep.desc11;
    txtViewDesc12.text = ep.desc12;
    txtViewDesc13.text = ep.desc13;
    txtViewDesc14.text = ep.desc14;
    
    txtReference.text = ep.reference;
    txtCreated.text = ep.created;
    txtCustomerNo.text = ep.customerNo;
    txtCompany.text = ep.company;
    txtModified.text = ep.modified;
    
    txtViewTitle1.text = ep.title1;
    txtViewTitle2.text = ep.title2;
    txtViewTitle3.text = ep.title3;
    
    NSString *testString = ep.header;
    
    NSArray *array = [testString componentsSeparatedByString:@","];
    
    if(array.count > 0)
    {
        txtHeader1.text = [array objectAtIndex:0];
        txtHeader2.text = [array objectAtIndex:1];
        txtHeader3.text = [array objectAtIndex:2];
        txtHeader4.text = [array objectAtIndex:3];
        txtHeader5.text = [array objectAtIndex:4];
        txtHeader6.text = [array objectAtIndex:5];
    }
    
}

-(void) disableInput
{
    [txtViewDesc1 setEditable:NO];
    [txtViewDesc2 setEditable:NO];
    [txtViewDesc3 setEditable:NO];
    [txtViewDesc4 setEditable:NO];
    [txtViewDesc5 setEditable:NO];
    [txtViewDesc6 setEditable:NO];
    [txtViewDesc7 setEditable:NO];
    [txtViewDesc8 setEditable:NO];
    [txtViewDesc9 setEditable:NO];
    [txtViewDesc10 setEditable:NO];
    [txtViewDesc11 setEditable:NO];
    [txtViewDesc12 setEditable:NO];
    [txtViewDesc13 setEditable:NO];
    [txtViewDesc14 setEditable:NO];
    
    [txtReference setEnabled:NO];
    [txtCreated setEnabled:NO];
    [txtCustomerNo setEnabled:NO];
    [txtCompany setEnabled:NO];
    [txtModified setEnabled:NO];
    
    [txtViewTitle1 setEditable:NO];
    [txtViewTitle2 setEditable:NO];
    [txtViewTitle3 setEditable:NO];
    
    [txtHeader1 setEnabled:NO];
    [txtHeader2 setEnabled:NO];
    [txtHeader3 setEnabled:NO];
    [txtHeader4 setEnabled:NO];
    [txtHeader5 setEnabled:NO];
    [txtHeader6 setEnabled:NO];
}

-(void) BackTapped:(id)gesture
{
    NSLog(@"back button tapped");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) DoneTapped:(id)gesture
{
    if([option isEqualToString:@"edit"])
    {
        [self insertIntoDB];
    }
    else if([option isEqualToString:@"email"])
    {
        [self sendPrintScreen];
    }
    else
    {
        //validation here... need to ensure only when user enter all the information will the data can be saved
        
        if(txtViewDesc1.text.length == 0 || txtViewDesc2.text.length == 0 || txtViewDesc3.text.length == 0 || txtViewDesc4.text.length == 0 ||txtViewDesc5.text.length == 0 || txtViewDesc6.text.length == 0 || txtViewDesc7.text.length == 0 || txtViewDesc8.text.length == 0 || txtViewDesc9.text.length == 0 || txtViewDesc10.text.length == 0 || txtViewDesc11.text.length == 0 || txtViewDesc12.text.length == 0 || txtViewDesc13.text.length == 0 || txtViewDesc14.text.length == 0 || txtViewTitle1.text.length == 0 || txtViewTitle2.text.length == 0 || txtViewTitle3.text.length == 0 || txtReference.text.length == 0 || txtCustomerNo.text.length == 0 || txtCreated.text.length == 0 || txtCompany.text.length == 0 || txtModified.text.length == 0)
        {
            NSLog(@"nani");
        }
        else
        {
            [self insertIntoDB];
            NSLog(@"okay.jpg");
        }
    }
}

-(void)sendPrintScreen
{
    UIGraphicsBeginImageContext(self.view.frame.size);
	
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
	
    UIGraphicsEndImageContext();
    
    if (viewImage != nil)
    {
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
        NSData *imageData = UIImagePNGRepresentation(viewImage);
        
        //you can also use UIImageJPEGRepresentation(img,1); for jpegs
        [imageData writeToFile:path atomically:YES];
        
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            
            mailer.mailComposeDelegate = self;
            
            NSString *mailsubject=[[NSString alloc]initWithFormat:@"hello~"];
            [mailer setSubject:mailsubject];
            
            NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
            [mailer setToRecipients:toRecipients];
            
            [mailer addAttachmentData:imageData mimeType:@"image/png"   fileName:@"mobiletutsImage"];
            
            NSString *emailBody = [[NSString alloc]initWithFormat:@"image---> "];
            
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [self presentModalViewController:mailer animated:YES];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                  message:@"Your device doesn't support the composer sheet"
                        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [alert show];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissModalViewControllerAnimated:YES];
}

-(void) insertIntoDB
{
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    EscutheonPlate *ep = [[EscutheonPlate alloc] init];
    ep.desc1 = txtViewDesc1.text;
    ep.desc2 = txtViewDesc2.text;
    ep.desc3 = txtViewDesc3.text;
    ep.desc4 = txtViewDesc4.text;
    ep.desc5 = txtViewDesc5.text;
    ep.desc6 = txtViewDesc6.text;
    ep.desc7 = txtViewDesc7.text;
    ep.desc8 = txtViewDesc8.text;
    ep.desc9 = txtViewDesc9.text;
    ep.desc10 = txtViewDesc10.text;
    ep.desc11 = txtViewDesc11.text;
    ep.desc12 = txtViewDesc12.text;
    ep.desc13 = txtViewDesc13.text;
    ep.desc14 = txtViewDesc14.text;
    
    ep.title1 = txtViewTitle1.text;
    ep.title2 = txtViewTitle2.text;
    ep.title3 = txtViewTitle3.text;
    
    ep.reference = txtReference.text;
    ep.customerNo = txtCustomerNo.text;
    ep.created = txtCreated.text;
    ep.company = txtCompany.text;
    ep.modified = txtModified.text;
    
    ep.header = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@", txtHeader1.text, txtHeader2.text, txtHeader3.text, txtHeader4.text, txtHeader5.text, txtHeader6.text];
    
    ep.name = [NSString stringWithFormat:@"S%@  F99%@/%@/%@  -%@", txtHeader1.text, txtHeader2.text, txtHeader3.text, txtHeader4.text, txtHeader5.text];
    
    if([option isEqualToString:@"edit"])
    {
        ep.PlateID = PlateID;
        
        [da updatePlateInformation:ep];
    }
    else
    {
        [da insertPlate:ep];
    }
    
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtHeader1 || textField == txtHeader2 || textField == txtHeader3 || textField == txtHeader5 || textField == txtHeader6)
    {
        return;
    }
    
    heightOfEditedView = textField.frame.size.height;
    heightOffset = textField.frame.origin.y+10;
        
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}

-(void) textViewDidBeginEditing:(UITextView *) textView
{    
    heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}

// set the maximum number of character can be typed
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtHeader1)
    {
        /*  limit to only numeric characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return (newLength > 2) ? NO : YES;
    }
    
    if(textField == txtHeader2)
    {
        /*  limit to only 1 or 0 characters  */
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"01"];
        
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return (newLength > 1) ? NO : YES;
    }
    
    if(textField == txtHeader3)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return (newLength > 3) ? NO : YES;
    }
    
//    if(textField == txtHeader4)
//    {
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        
//        return (newLength > 1) ? NO : YES;
//    }
    
    if(textField == txtHeader5)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return (newLength > 5) ? NO : YES;
    }
    
    return YES;
}
@end
