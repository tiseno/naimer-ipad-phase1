//
//  tblPlateCell.h
//  KNE
//
//  Created by Jermin Bazazian on 12/26/12.
//
//

#import <Foundation/Foundation.h>
#import "PlateMainDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface tblPlateCell : UITableViewCell
{
    
}

@property (nonatomic, strong) id<PlateMainDelegate> delegate;
//@property (nonatomic, retain) KNECase *Case;

@property (nonatomic, strong) UILabel *lblPlateID;
@property (nonatomic, strong) UILabel *lblPlateName;

@property (nonatomic, strong) UIButton *btnPlateEdit;
@property (nonatomic, strong) UIButton *btnPlatePrint;
@property (nonatomic, strong) UILabel *lblCreateDate;
@property (nonatomic, strong) UILabel *lblCustomerNo;
@property (nonatomic, strong) UILabel *lblCompany;


@property (nonatomic, strong) UILabel *lbltPlateName;

@property (nonatomic, strong) UILabel *lbltCreateDate;
@property (nonatomic, strong) UILabel *lbltCustomerNo;
@property (nonatomic, strong) UILabel *lbltCompany;

@property (nonatomic, strong) UILabel *lblEngravingNum;
@property (nonatomic, strong) UILabel *lblNumofStage;
@property (nonatomic, strong) UILabel *lbltEngravingNum;
@property (nonatomic, strong) UILabel *lbltNumofStage;
@end
