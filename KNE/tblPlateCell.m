//
//  tblPlateCell.m
//  KNE
//
//  Created by Jermin Bazazian on 12/26/12.
//
//

#import "tblPlateCell.h"

@implementation tblPlateCell

@synthesize lblPlateID, lblPlateName;
@synthesize btnPlateEdit, btnPlatePrint;
@synthesize delegate, lblCreateDate, lblCustomerNo, lblCompany;
@synthesize lbltCompany, lbltCreateDate, lbltCustomerNo, lbltPlateName;
@synthesize lblEngravingNum, lblNumofStage, lbltEngravingNum, lbltNumofStage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        // Initialization code
        UILabel *glblCaseID = [[UILabel alloc] initWithFrame:CGRectMake(2, 7, 10, 21)];
        glblCaseID.font = [UIFont fontWithName:@"Helvetica" size:14];
        glblCaseID.textAlignment = UITextAlignmentLeft;
        glblCaseID.textColor = [UIColor blackColor];
        glblCaseID.backgroundColor = [UIColor clearColor];
        
        glblCaseID.hidden = YES;
        
        self.lblPlateID = glblCaseID;
        [self addSubview:lblPlateID];
        
        UILabel *glbltCaseName = [[UILabel alloc] initWithFrame:CGRectMake(20, 12, 150, 21)];
        glbltCaseName.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltCaseName.font = [UIFont boldSystemFontOfSize:14];
        glbltCaseName.textAlignment = UITextAlignmentLeft;
        glbltCaseName.textColor = [UIColor blackColor];
        glbltCaseName.backgroundColor = [UIColor clearColor];
        glbltCaseName.text=@"Escutheon Plate :";
//        glbltCaseName.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltCaseName.layer.borderWidth = 1;
        self.lbltPlateName = glbltCaseName;
        [self addSubview:lbltPlateName];
        
        
        UILabel *glblCaseName = [[UILabel alloc] initWithFrame:CGRectMake(160, 12, 170, 21)];
        glblCaseName.font = [UIFont fontWithName:@"Helvetica" size:14];
        glblCaseName.textAlignment = UITextAlignmentCenter;
        glblCaseName.textColor = [UIColor blackColor];
        glblCaseName.backgroundColor = [UIColor clearColor];
//        glblCaseName.layer.borderColor=[UIColor blackColor].CGColor;
//        glblCaseName.layer.borderWidth = 1;
        self.lblPlateName = glblCaseName;
        [self addSubview:lblPlateName];
       
        UILabel *glbltCustomerNo = [[UILabel alloc] initWithFrame:CGRectMake(20, 42, 150, 21)];
        glbltCustomerNo.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltCustomerNo.font = [UIFont boldSystemFontOfSize:14];
        glbltCustomerNo.textAlignment = UITextAlignmentLeft;
        glbltCustomerNo.textColor = [UIColor blackColor];
        glbltCustomerNo.text=@"Cust. No. :";
//        glbltCustomerNo.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltCustomerNo.layer.borderWidth = 1;
        glbltCustomerNo.backgroundColor = [UIColor clearColor];
        self.lbltCustomerNo = glbltCustomerNo;
        [self addSubview:lbltCustomerNo];
        
        UILabel *glblCustomerNo = [[UILabel alloc] initWithFrame:CGRectMake(160, 42, 170, 21)];
        glblCustomerNo.font = [UIFont fontWithName:@"Helvetica" size:14];
        glblCustomerNo.textAlignment = UITextAlignmentCenter;
        glblCustomerNo.textColor = [UIColor blackColor];
//        glblCustomerNo.layer.borderColor=[UIColor blackColor].CGColor;
//        glblCustomerNo.layer.borderWidth = 1;
        glblCustomerNo.backgroundColor = [UIColor clearColor];
        self.lblCustomerNo = glblCustomerNo;
        [self addSubview:lblCustomerNo];
        
        UILabel *glbltEngravingNum = [[UILabel alloc] initWithFrame:CGRectMake(20, 72, 150, 21)];
        glbltEngravingNum.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltEngravingNum.font = [UIFont boldSystemFontOfSize:14];
        glbltEngravingNum.textAlignment = UITextAlignmentLeft;
        glbltEngravingNum.textColor = [UIColor blackColor];
        glbltEngravingNum.text=@"Engraving No. :";
        //        glbltCustomerNo.layer.borderColor=[UIColor blackColor].CGColor;
        //        glbltCustomerNo.layer.borderWidth = 1;
        glbltEngravingNum.backgroundColor = [UIColor clearColor];
        self.lbltEngravingNum = glbltEngravingNum;
        [self addSubview:lbltEngravingNum];
        
        UILabel *glblEngravingNum = [[UILabel alloc] initWithFrame:CGRectMake(160, 72, 170, 21)];
        glblEngravingNum.font = [UIFont fontWithName:@"Helvetica" size:14];
        glblEngravingNum.textAlignment = UITextAlignmentCenter;
        glblEngravingNum.textColor = [UIColor blackColor];
        //        glblCustomerNo.layer.borderColor=[UIColor blackColor].CGColor;
        //        glblCustomerNo.layer.borderWidth = 1;
        glblEngravingNum.backgroundColor = [UIColor clearColor];
        self.lblEngravingNum = glblEngravingNum;
        [self addSubview:lblEngravingNum];
        
        /*==========================2nd row==================================*/
        
        UILabel *glbltCompany = [[UILabel alloc] initWithFrame:CGRectMake(350, 12, 150, 21)];
        glbltCompany.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltCompany.font = [UIFont boldSystemFontOfSize:14];
        glbltCompany.textAlignment = UITextAlignmentLeft;
        glbltCompany.textColor = [UIColor blackColor];
        glbltCompany.backgroundColor = [UIColor clearColor];
        glbltCompany.text=@"Company :";
//        glbltCompany.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltCompany.layer.borderWidth = 1;
        self.lbltCompany = glbltCompany;
        [self addSubview:lbltCompany];
        
        UILabel *glblCompany = [[UILabel alloc] initWithFrame:CGRectMake(500, 12, 150, 21)];
        glblCompany.font = [UIFont fontWithName:@"Helvetica" size:14];
        glblCompany.textAlignment = UITextAlignmentCenter;
        glblCompany.textColor = [UIColor blackColor];
        glblCompany.backgroundColor = [UIColor clearColor];
//        glblCompany.layer.borderColor=[UIColor blackColor].CGColor;
//        glblCompany.layer.borderWidth = 1;
        self.lblCompany = glblCompany;
        [self addSubview:lblCompany];
        
        
        UILabel *glbltcrdate = [[UILabel alloc] initWithFrame:CGRectMake(350, 42, 150, 21)];
        glbltcrdate.textAlignment = UITextAlignmentLeft;
        glbltcrdate.textColor = [UIColor blackColor];
        glbltcrdate.backgroundColor = [UIColor clearColor];
        glbltcrdate.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltcrdate.font = [UIFont boldSystemFontOfSize:14];
        glbltcrdate.text=@"Create Date :";
//        glbltcrdate.layer.borderColor=[UIColor blackColor].CGColor;
//        glbltcrdate.layer.borderWidth = 1;
        self.lbltCreateDate = glbltcrdate;
        [self addSubview:lbltCreateDate];
        
        UILabel *glblcrdate = [[UILabel alloc] initWithFrame:CGRectMake(500, 42, 150, 21)];
        glblcrdate.textAlignment = UITextAlignmentCenter;
        glblcrdate.textColor = [UIColor blackColor];
        glblcrdate.backgroundColor = [UIColor clearColor];
        glblcrdate.font = [UIFont fontWithName:@"Helvetica" size:14];
//        glblcrdate.layer.borderColor=[UIColor blackColor].CGColor;
//        glblcrdate.layer.borderWidth = 1;
        self.lblCreateDate = glblcrdate;
        [self addSubview:lblCreateDate];
        
        UILabel *glbltNumofStage = [[UILabel alloc] initWithFrame:CGRectMake(350, 72, 150, 21)];
        glbltNumofStage.textAlignment = UITextAlignmentLeft;
        glbltNumofStage.textColor = [UIColor blackColor];
        glbltNumofStage.backgroundColor = [UIColor clearColor];
        glbltNumofStage.font = [UIFont fontWithName:@"Helvetica" size:14];
        glbltNumofStage.font = [UIFont boldSystemFontOfSize:14];
        glbltNumofStage.text=@"No. of Position :";
        //        glbltcrdate.layer.borderColor=[UIColor blackColor].CGColor;
        //        glbltcrdate.layer.borderWidth = 1;
        self.lbltNumofStage = glbltNumofStage;
        [self addSubview:lbltNumofStage];
        
        UILabel *glblNumofStage = [[UILabel alloc] initWithFrame:CGRectMake(500, 72, 150, 21)];
        glblNumofStage.textAlignment = UITextAlignmentCenter;
        glblNumofStage.textColor = [UIColor blackColor];
        glblNumofStage.backgroundColor = [UIColor clearColor];
        glblNumofStage.font = [UIFont fontWithName:@"Helvetica" size:14];
        //        glblcrdate.layer.borderColor=[UIColor blackColor].CGColor;
        //        glblcrdate.layer.borderWidth = 1;
        self.lblNumofStage = glblNumofStage;
        [self addSubview:lblNumofStage];
        /*====================button=============================*/
        
        UIImage *editButtonImage = [UIImage imageNamed:@"btn_edit.png"];
        UIButton *teditButton = [[UIButton alloc] initWithFrame:CGRectMake(650, 100, 50, 30)];
        [teditButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
        [teditButton addTarget:self action:@selector(edit_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        self.btnPlateEdit = teditButton;
        [self addSubview:btnPlateEdit];
        
        UIImage *emailButtonImage = [UIImage imageNamed:@"btn_export.png"];
        UIButton *temailbtn = [[UIButton alloc] initWithFrame:CGRectMake(710, 100, 50, 30)];
        [temailbtn setBackgroundImage:emailButtonImage forState:UIControlStateNormal];
        [temailbtn addTarget:self action:@selector(email_ButtonTapped:)
            forControlEvents:UIControlEventTouchDown];
        self.btnPlatePrint = temailbtn;
        [self addSubview:btnPlatePrint];        
    }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(IBAction)edit_ButtonTapped:(id)sender
{
    //NSLog(@"EditCase!!");
    
    //[self.delegate EditPlateTapped];
    [self.delegate EditPlateTapped:self.lblPlateID.text];
}

-(IBAction)email_ButtonTapped:(id)sender
{
    //NSLog(@"email!!");
    
    [self.delegate emailPlateTapped:self.lblPlateID.text];
}



@end
