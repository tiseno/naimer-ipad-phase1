//
//  tblPlateMainPageViewController.h
//  KNE
//
//  Created by Jermin Bazazian on 12/26/12.
//
//

#import <Foundation/Foundation.h>

@class EscutheonPlateWindow;

@interface tblPlateMainPageViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource>
{
    NSIndexPath *gindexPath;
}

@property (nonatomic, strong) NSMutableArray *PlateArray;

@property (nonatomic, strong) EscutheonPlateWindow *gPlateMainPageViewController;
-(void)initwithCase;

@end