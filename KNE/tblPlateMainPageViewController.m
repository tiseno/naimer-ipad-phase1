//
//  tblPlateMainPageViewController.m
//  KNE
//
//  Created by Jermin Bazazian on 12/26/12.
//
//

#import "tblPlateMainPageViewController.h"
#import "tblPlateCell.h"
#import "EscutheonPlate.h"
#import "DatabaseAction.h"

@interface tblPlateMainPageViewController ()

@end

@implementation tblPlateMainPageViewController

@synthesize PlateArray;
@synthesize gPlateMainPageViewController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self)
    {
        [self initwithCase];
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"count ... %d", self.PlateArray.count);
    
    return [self.PlateArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"reloading... ");
    
    static NSString *CellIdentifier = @"Cell";
    
    EscutheonPlate *ep = [self.PlateArray objectAtIndex:indexPath.row];
    
    tblPlateCell* cell = [[tblPlateCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate = self.gPlateMainPageViewController;
    
    cell.lblPlateID.text = [NSString stringWithFormat:@"%d", ep.PlateID];
    
    //filter n replace to appropriate character
    
    NSString *temp = [ep.name stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    cell.lblPlateName.text = [temp stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    temp = [ep.customerNo stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    cell.lblCustomerNo.text = [temp stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    temp = [ep.company stringByReplacingOccurrencesOfString:@"tiseno_Squote" withString:@"'"];
    cell.lblCompany.text = [temp stringByReplacingOccurrencesOfString:@"tiseno_AndSign" withString:@"&"];
    
    cell.lblCreateDate.text = ep.created;
    
    cell.lblEngravingNum.text=ep.headerF;
    cell.lblNumofStage.text=ep.NoOfPosition;
    
    //cell.lblPlateName.text = ep.name;
    //cell.lblCustomerNo.text = ep.customerNo;
    //cell.lblCompany.text = ep.company;
    //cell.lblModifyDate.text = ep.modified;
    
    if (indexPath.row % 2==0)
    {
        cell.contentView.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1];
        
    }
    
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)initwithCase
{
    // select all the plate stored within the sqlite
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    self.PlateArray = [da retrieveAllPlate];

}


/* swipe to delete start */

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*EscutheonPlate *ep = [self.PlateArray objectAtIndex:indexPath.row];
    
    DatabaseAction *da = [[DatabaseAction alloc] init];
    
    [da deletePlate:ep.PlateID];
    
    NSUInteger row = [indexPath row];
    
    [self.PlateArray removeObjectAtIndex:row];*/
    
    //[self.tableView reloadData];
    
    gindexPath=indexPath;
//
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Switch" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    
    //[self initwithCase];
    //[self.tableView beginUpdates];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        EscutheonPlate *ep = [self.PlateArray objectAtIndex:gindexPath.row];
        
        DatabaseAction *da = [[DatabaseAction alloc] init];
        
        [da deletePlate:ep.PlateID];
        
        NSUInteger row = [gindexPath row];
        
        [self.PlateArray removeObjectAtIndex:row];
    }
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];
}

/* swipe to delete end */

@end
