//
//  tblSwitchMainPageViewController.h
//  KNE
//
//  Created by Tiseno Mac 2 on 12/24/12.
//
//

#import <UIKit/UIKit.h>
#import "KNEtblallCaseCell.h"
#import "KNECase.h"
#import "DBCase.h"
#import "KNEAppDelegate.h"
#import "DBSwitchingfirstValue.h"
#import "KNEEditSwitchCaseViewController.h"
#import "MBProgressHUD.h"

@class SwitchMainPageViewController;
@interface tblSwitchMainPageViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>
{
    KNEtblallCaseCell *cell;
    NSIndexPath *gindexPath;
    
}
@property (nonatomic, strong) NSMutableArray *casePlateArray;
@property (nonatomic, strong) NSArray *CaseArray;
@property (nonatomic, strong) SwitchMainPageViewController *gSwitchMainPageViewController;
-(void)initwithCase;

@end
