//
//  tblSwitchMainPageViewController.m
//  KNE
//
//  Created by Tiseno Mac 2 on 12/24/12.
//
//

#import "tblSwitchMainPageViewController.h"

@interface tblSwitchMainPageViewController ()

@end

@implementation tblSwitchMainPageViewController
@synthesize CaseArray;
@synthesize gSwitchMainPageViewController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        [self initwithCase];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [self.CaseArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    KNECase* classKNECase=[self.CaseArray objectAtIndex:indexPath.row];


    cell = [[KNEtblallCaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate =self.gSwitchMainPageViewController;
    cell.Case=classKNECase;
    
    cell.lbltitleSwitchtype.text=@"Switch Type :";
    cell.lbltitleDrawingNo.text=@"Drawing No :"; 
    cell.lbltitleNoOFStages.text=@"No of Stages :";
    cell.lbltitleNoSwitchingAngle.text=@"Switching Angle :";
    cell.lbltitlecompany.text=@"Customer Name :";
    
    cell.lbltitleNoOFPosition.text=@"No of Position :";
    cell.lbltitleReference.text=@"Reference :";
    cell.lbltitleCreateDate.text=@"Create Date :";
    cell.lbltitleModifyDate.text=@"Modify Date :";
    
    //cell.lblCaseID.text=classKNECase.CaseID;
    //cell.lblCaseName.text=classKNECase.CaseName;
    //cell.lblCustNo.text=classKNECase.FCustNo;
    cell.lblSwitchtype.text=classKNECase.SwitchType;
    cell.lblDrawingNo.text=classKNECase.Program;
    cell.lblNoOFStages.text=classKNECase.FNofStage;
    cell.lblNoOFPosition.text=classKNECase.numberOfPosition;
    cell.lblNoSwitchingAngle.text=classKNECase.SwitchingAngle;
    cell.lblcompany.text=classKNECase.FCompany;
    cell.lblCreateDate.text=classKNECase.FDate;
    cell.lblModifyDate.text=classKNECase.FModify_Date;
    cell.lblReference.text=classKNECase.FReference;
    cell.btnSwitchDelete.hidden=YES;
    //cell.lblEmpty.text=@" ";

    
    if (indexPath.row % 2==0)
    {
        cell.contentView.backgroundColor = [UIColor clearColor];
        
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1];
        
    }
    
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


#pragma mark - Table view delegate
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self loadingViewscreen];
    cell= (KNEtblallCaseCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    KNEAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    DBSwitchingfirstValue *gDBSwitchingfirstValue=[[[DBSwitchingfirstValue alloc] init]autorelease];
    NSArray *gDBSwitchingfirstValueAr=[gDBSwitchingfirstValue selectSwitchingFirstValueItem:cell.Case];
    
    for (KNESwitchingFirstValue *gitem in gDBSwitchingfirstValueAr) {
        appDelegate.firstPnum=gitem.SwitchingFirstValue;
    }
    appDelegate.gKNECase=cell.Case;
    
    [self performSelector:@selector(geteditTapped:) withObject:cell.Case afterDelay:1];
}

-(void)geteditTapped:(KNECase*)gcase
{
    NSLog(@"hello there~");
    DBCase *gdbcase = [[[DBCase alloc] init] autorelease];
    
    NSArray *caseinfoArr  = [gdbcase selectCaseItem:gcase];
    
    KNEEditSwitchCaseViewController *gKNEEditSwitchCaseViewController=[[KNEEditSwitchCaseViewController alloc] initWithNibName:@"KNEEditSwitchCaseViewController" bundle:nil];
    //gKNEEditSwitchCaseViewController.KNECaseclass=gcase;
    
    for (KNECase *item in caseinfoArr)
    {
        gKNEEditSwitchCaseViewController.KNECaseclass=item;
    }
    
    [self.navigationController pushViewController:gKNEEditSwitchCaseViewController animated:YES];
    
    [gKNEEditSwitchCaseViewController release];
    
    [self.loadingView removeView];
}
*/
-(void)loadingViewscreen
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)initwithCase
{
    DBCase *createCase=[[DBCase alloc]init ];
    self.CaseArray=[createCase selectItem];
    
}

/* swipe to delete start */

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    KNECase *ep = [self.CaseArray objectAtIndex:indexPath.row];
    
    DBCase *da = [[DBCase alloc] init];
    
    [da deletselectedCase:ep];
    
    
    //NSUInteger row = [indexPath row];
    
    //[self.PlateArray removeObjectAtIndex:row];
    
    [self initwithCase];
//    
//    gindexPath=indexPath;
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Switch" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
//    [alert show];
    
    //[self initwithCase];
    //[self.tableView beginUpdates];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 1)
    {
        //[self.delegate DeleteCaseTapped:Case];

    
        
    [self.tableView reloadData];
        
    //[self.CaseArray removeObjectAtIndex:gindexPath.row];
    //[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:gindexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    //[self.tableView endUpdates];
        
        NSLog(@"reload data zzzz");
        
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    if(editing){
        // Entered Edit mode
        
        // Show the new tableView and reload it.
         NSLog(@"editing");
    }
    
    else {
        
         NSLog(@"no editing");
        // End of edit mode
        
        // Bring back the tableview and again reload it.
    }
    
    
    [super setEditing:editing animated:animated];
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@" tableView reload data");
    [tableView reloadData];

}

/* swipe to delete end */

@end
